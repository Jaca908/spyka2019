# [CSS] Infinite autoplay carousel
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/studiojvla/pen/qVbQqW](https://codepen.io/studiojvla/pen/qVbQqW).

 Useful for startup landing pages where you need to display brand partners and other cool logos or whatever. 

Enjoy. :)