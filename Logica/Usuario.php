<?php
session_start();
	
require ("../Conexion/Conexion.php");
date_default_timezone_set("America/Costa_Rica");

//Comprbar si el usuario presiono el boton de Login

if(isset($_POST["btnLogin"]))
{
	LogIn();
}
else if(isset($_POST["btnEnviarUsuario"]))
{
	GuardarOModificarUsuario();
}
else if(isset($_POST["MostrarDatos"]))
{
	ConsultarUsuario();
}
else if(isset($_POST["ModificarPerfil"]))
{
	ModificarPerfil();
}
else if(isset($_POST['ObtenerConsecutivos']))
{
	ObtenerConsecutivos($_POST['IDUsuario']);
}
else if(isset($_POST['ModificarConsecutivos']))
{
	ModificarConsecutivos($_POST['IDUsuario']);
}
else if(isset($_FILES["file"]["type"]))
{
	CargarLogo();
}
else if(isset($_POST['btnAgregarAE']))
{
	AgregarActividadEconomica();
}

//Funcion para inicio de sesion

function LogIn(){
	$Cedula=$_POST['txtCedula'];
	$Clave=$_POST['txtClave'];
	
	//Encriptar Clave
	
	//$ClaveEncip=password_hash($Clave, PASSWORD_ARGON2I);
	
	//Instancia de Conexion
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) {
		die("Connection failed: " . $Conexion->connect_error);
	} 

	/*$sql = "SELECT IDUsuario,Clave,Cedula,Perfil,Activo,NombreRepresentante,Zona,Email,Telefono1,Direccion,EnLinea
						FROM Usuario
						WHERE Cedula = '" .$Cedula."'";*/
						
	$sql = "SELECT IDUsuario,Clave,Cedula,Perfil,Activo,NombreRepresentante,Zona,Email,Telefono1,Direccion
						FROM usuario
						WHERE Cedula = '" .$Cedula."'";
						
	$result = $Conexion->query($sql);

	if ($result->num_rows > 0) {

		//Convertir datos de la tabla en vector
		
		$row = $result->fetch_assoc();
		$ClaveEnciptada=$row["Clave"];
		$EstadoUsuario=$row["Activo"];
		//$EnLinea=$row["EnLinea"];

		if(password_verify($Clave,$ClaveEnciptada)){

			//Verificar que el usuario este activo
			
			//usuario activo entra
			if($EstadoUsuario==1)
			{
				//if($EnLinea==0)
				//{
						//actualizar fecha de ultimo ingreso y estado en linea
					//$sql= "UPDATE Usuario SET UltimoIngreso=now(), EnLinea=1 WHERE IDUsuario=". $row["IDUsuario"].";";
					$sql= "UPDATE usuario SET UltimoIngreso=now() WHERE IDUsuario=". $row["IDUsuario"].";";
					$result = $Conexion->query($sql);

					if($row["Perfil"]=='A'){
						
						$_SESSION['IDUsuario']=$row["IDUsuario"];
						$_SESSION['NombreRepresentante']=$row["NombreRepresentante"];
						$_SESSION['Cedula']=$row["Cedula"];
						$_SESSION['Perfil']=$row["Perfil"];
						$_SESSION['NombreComercial']=$row['NombreComercial'];
						$_SESSION['Zona']=$row['Zona'];
						$_SESSION['Email']=$row['Email'];
						$_SESSION['Telefono']=$row['Telefono1'];
						$_SESSION['Direccion']=$row['Direccion'];
						$_SESSION['UsuarioLogIn']=1;
						
						header("location:../Pantalla.php");
					}
					else if($row["Perfil"]=='C'){
						
						$_SESSION['IDUsuario']=$row["IDUsuario"];
						$_SESSION['NombreRepresentante']=$row["NombreRepresentante"];
						$_SESSION['Cedula']=$row["Cedula"];
						$_SESSION['Perfil']=$row["Perfil"];
						$_SESSION['NombreComercial']=$row['NombreComercial'];
						$_SESSION['Zona']=$row['Zona'];
						$_SESSION['Email']=$row['Email'];
						$_SESSION['Telefono']=$row['Telefono1'];
						$_SESSION['Direccion']=$row['Direccion'];
						$_SESSION['UsuarioLogIn']=1;
						
						header("location:../Pantalla.php");
					}
					else{
						
						$_SESSION['IDUsuario']=$row["IDUsuario"];
						$_SESSION['NombreRepresentante']=$row["NombreRepresentante"];
						$_SESSION['Cedula']=$row["Cedula"];
						$_SESSION['Perfil']=$row["Perfil"];
						$_SESSION['Zona']=$row['Zona'];
						$_SESSION['Email']=$row['Email'];
						$_SESSION['Telefono']=$row['Telefono1'];
						$_SESSION['Direccion']=$row['Direccion'];
						$_SESSION['UsuarioLogIn']=1;
						
						header("location:../Pantalla.php");
					}
				//}
				/*else
				{
					$_SESSION['msj']='El usuario ingresado ya tiene<br>una sesión activa en otro<br>dispositivo o navegador. Cierre<br>todas las sesiones y e intenténtelo<br>nuevamente';                                                                          					header("location:../login.php");
				}*/
			}else
			{
				$_SESSION['msj']='El usuario ingresado se encuentra inactivo';
				header("location:../login.php"); 
			}
		}
		else{
			$_SESSION['msj']='La clave es incorrecta';
			header("location:../login.php"); 
		}
	} else {
			$_SESSION['msj']='El usuario que ingresó no existe';
			header("location:../login.php");
	}
		$Conexion->close();
			 
}


function GuardarOModificarUsuario()
{
	$IDUsuario=$_POST["txtIDUsuario"];
	$Cedula=$_POST["txtCedula"];
	$ID_MH=$_POST["txtIDMH"];
	$ID_API=$_POST["txtIDAPI"];
	
	$Respuesta='';
	$GuarMod='';

	#ver si existe el campo ID

	if ($IDUsuario=="") 
	{
		# guardar...

		#sino existe: verificar la cedula del campo txtCedula en la BD
		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		} 

		$sql = "SELECT IDUsuario,Cedula FROM usuario WHERE Cedula = '" .$Cedula."'";
							
		$result = $Conexion->query($sql);

		if ($result->num_rows > 0) 
		{
		$Respuesta = "El usuario que intenta guardar ya existe en la Base de Datos";
		$GuarMod='Error'; 
		}
		else # si la cedula y el id tampoco existe: verificar ID_MH
		{			
			$sql="SELECT IDUsuario,Cedula FROM usuario WHERE ID_MH = '" .$ID_MH."'";

			$result = $Conexion->query($sql);
			
			if ($result->num_rows > 0) 
			{
			$Respuesta = "Ya existe un usuario con el ID de Ministerio de Hacienda ingresado"; 
			$GuarMod='Error';
			}
			else # si la cedula y el id y el id_MH tampoco existe: verificar el IDAPI
			{
				$sql="SELECT IDUsuario,Cedula FROM usuario WHERE ID_API = '" .$ID_API."'";

				$result = $Conexion->query($sql);
				
				if ($result->num_rows > 0) 
				{
				$Respuesta = "Ya existe un usuario con el ID API ingresado"; 
				$GuarMod='Error';
				}
				else # si la cedula y el id y el id_MH y el IDAPI tampoco existe guardar:
				{

					$Provincia=$_POST["cbmProvincia"];
					$Canton=$_POST["cbmCanton"];
					$Distrito=$_POST["cbmDistrito"];
					$Barrio=$_POST["cbmBarrio"];
					
					$TipoCedula=$_POST["cbmTipoCedula"];
					$NombreRepresentante=$_POST["txtNombreRepresentante"];
					$NombreComercial=$_POST["txtNombreComercial"];
					$CodigoPostal=$Provincia.$Canton.$Distrito.$Barrio;
					$Email=$_POST["txtEmail"];
					$Direccion=$_POST["txtDireccion"];
					$Telefono1=$_POST["txtTelefono1"];
					$Telefono2=$_POST["txtTelefono2"];
					$Zona=$Provincia.$Canton.$Distrito.$Barrio;
					$Cuota=$_POST["txtCuota"];
					$Moneda=$_POST["cbmMoneda"];
					$Expediente=$_POST["txtExpediente"];
					$FechaIngreso='now()';
					$Clave=password_hash($_POST["txtClave"], PASSWORD_BCRYPT);
					$Perfil=$_POST["cbmPerfil"];
					$Observacion=$_POST["txtObservaciones"];
					$Activo=$_POST["cbmEstado"];
					$UltimoIngreso='now()';
					$TipoUsuario=$_POST["cbmTipoUsuario"];


					//sanitize el sql
					$sql = "INSERT INTO usuario
							(ID_MH,ID_API,TipoCedula,Cedula,NombreRepresentante,NombreComercial,CodigoPostal,Email,Direccion,Telefono1,Telefono2,
							 Zona,Cuota,Moneda,Expediente,FechaIngreso,Clave,Perfil,Observacion,Activo,UltimoIngreso,TipoUsuario)
							values
							('$ID_MH','$ID_API','$TipoCedula','$Cedula','$NombreRepresentante','$NombreComercial','$CodigoPostal','$Email','$Direccion','$Telefono1','$Telefono2','$Zona',$Cuota,'$Moneda','$Expediente',$FechaIngreso,'$Clave','$Perfil','$Observacion',$Activo,$UltimoIngreso,'$TipoUsuario');";
									
					if($Conexion->query($sql) === TRUE) 
					{ 
						/*Traer el Id del Usuario con la cedula que se ingreso*/
						
						$result=$Conexion->query("SELECT IDUsuario FROM usuario where Cedula='".$Cedula."';");
						$IDUsuarioIngresado=0;

						if($result->num_rows > 0)
						{
							$row = $result->fetch_assoc();
							$IDUsuarioIngresado=$row["IDUsuario"];
						}

					     $sql="insert consecutivo(FK_Usuario,Factura,NotaDebito,NotaCredito,Tiquete,Compra,FacturaSimplificada,Terminal,Sucursal)
							   values($IDUsuarioIngresado,0000000001,0000000001,0000000001,0000000001,0000000001,0000000001,001,00001);";

							if($Conexion->query($sql) === TRUE) 
							{
								/*Insert de cliente generico*/
								$sql = "insert into cliente
										(Cedula,FK_Usuario,Nombre,Direccion,Telefono,FechaIngreso,FechaUltimaFacturacion,
										TipoCedula,Email1,Email2,Zona,Exonerado,Activo,Expediente)
							values('000000000',$IDUsuarioIngresado,'CONTADO','','',now(),null,
								'','','','',0,1,'');";

								if($Conexion->query($sql) === TRUE) 
								{
									/*Insert de Grupo para comentarios*/
									$sql = "INSERT INTO grupo(IDGrupo,FK_Usuario,Grupo) 
											Values('COM',$IDUsuarioIngresado,'Comentario');";

									if($Conexion->query($sql) === TRUE) 
									{
										//insert producto
										$sql = "INSERT INTO producto
(IDProducto,FK_Usuario,NombreProducto,PrecioVenta,Utilidad,PrecioCosto,Impuesto,SaldoAnterior,SaldoActual,Descuento,UltimaVenta,Activo,UnidadMedida,FK_Grupo)
values('000000000000000000',$IDUsuarioIngresado,'',0,0,0,0,0,0,0,now(),1,'Unidad','COM');";

										if($Conexion->query($sql) === TRUE) 
										{
											//$_SESSION['IDUsuarioPLogo']=$IDUsuarioIngresado;
											//$_SESSION['IDUsuarioPAE']=$IDUsuarioIngresado;
							  				$Respuesta = "Usuario guardado exitosamente";
							  				$GuarMod='Guardo';
							  			}
							  			else
							  			{
							  				$sql = "DELETE FROM consecutivo WHERE FK_Usuario=".$IDUsuarioIngresado.";";
				  				
						  					$result=$Conexion->query($sql);
							  				
							  				$sql = "DELETE FROM cliente WHERE FK_Usuario=".$IDUsuarioIngresado.";";
							  				
							  				$result=$Conexion->query($sql);
							  				
							  				$sql = "DELETE FROM grupo WHERE FK_Usuario=".$IDUsuarioIngresado.";";
							  				
							  				$result=$Conexion->query($sql);
							  				
							  				$sql = "DELETE FROM usuario WHERE IDUsuario=".$IDUsuarioIngresado.";";
							  				
							  				$result=$Conexion->query($sql);
							  				
							  				$Respuesta = "Error al guardar el usuario";
							  				$GuarMod='Error';
										}
						  			}
						  			else
						  			{
						  				$sql = "DELETE FROM consecutivo WHERE FK_Usuario=".$IDUsuarioIngresado.";";
				  				
					  					$result=$Conexion->query($sql);
						  				
						  				$sql = "DELETE FROM cliente WHERE FK_Usuario=".$IDUsuarioIngresado.";";
						  				
						  				$result=$Conexion->query($sql);
						  				
						  				$sql = "DELETE FROM usuario WHERE IDUsuario=".$IDUsuarioIngresado.";";
						  				
						  				$result=$Conexion->query($sql);
						  				
						  				$Respuesta = "Error al guardar el usuario";
						  				$GuarMod='Error';
									}
					  			}
					  			else
					  			{
					  				
					  				$sql = "DELETE FROM consecutivo WHERE FK_Usuario=".$IDUsuarioIngresado.";";
				  				
				  					$result=$Conexion->query($sql);
					  				
					  				$sql = "DELETE FROM usuario WHERE IDUsuario=".$IDUsuarioIngresado.";";
					  				
					  				$result=$Conexion->query($sql);
					  				
					  				$Respuesta = "Error al guardar el usuario";
					  				$GuarMod='Error';
					  			} 
				  			}
				  			else
				  			{				  				
				  				$sql = "DELETE FROM usuario WHERE IDUsuario=".$IDUsuarioIngresado.";";
				  				
				  				$result=$Conexion->query($sql);
				  				
				  				$Respuesta = "Error al guardar el usuario";
				  				$GuarMod='Error';
				  			} 			  
					} 
					else 
					{
						 $Respuesta = "Error al guardar el usuario";
						 $GuarMod='Error';
					}
				}
			}
		}
	}
	else if($IDUsuario!="")
	{
		# modificar...

		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		} 
			
			$sql="SELECT IDUsuario,Cedula FROM usuario WHERE ID_MH = '$ID_MH' AND IDUSuario != $IDUsuario";

			$result = $Conexion->query($sql);
			
			if ($result->num_rows > 0) 
			{
			$Respuesta = "Ya existe otro usuario con el ID de Ministerio de Hacienda ingresado";
			$GuarMod='Error'; 
			}
			else # si la cedula y el id y el id_MH tampoco existe: verificar el IDAPI
			{
				$sql="SELECT IDUsuario,Cedula FROM usuario WHERE ID_API = '$ID_API' AND IDUSuario != $IDUsuario";

				$result = $Conexion->query($sql);
				
				if ($result->num_rows > 0) 
				{
				$Respuesta = "Ya existe otro usuario con el ID API ingresado";
				$GuarMod='Error'; 
				}
				else # si la cedula y el id y el id_MH y el IDAPI tampoco existe guardar:
				{ 

					$Provincia=$_POST["cbmProvincia"];
					$Canton=$_POST["cbmCanton"];
					$Distrito=$_POST["cbmDistrito"];
					$Barrio=$_POST["cbmBarrio"];
					
					$TipoCedula=$_POST["cbmTipoCedula"];
					$NombreRepresentante=$_POST["txtNombreRepresentante"];
					$NombreComercial=$_POST["txtNombreComercial"];
					$CodigoPostal=$Provincia.$Canton.$Distrito.$Barrio;
					$Email=$_POST["txtEmail"];
					$Direccion=$_POST["txtDireccion"];
					$Telefono1=$_POST["txtTelefono1"];
					$Telefono2=$_POST["txtTelefono2"];
					$Zona=$Provincia.$Canton.$Distrito.$Barrio;
					$Cuota=$_POST["txtCuota"];
					$Moneda=$_POST["cbmMoneda"];
					$Expediente=$_POST["txtExpediente"];
					$FechaIngreso=DateTime::createFromFormat('d-m-Y', $_POST["txtFechaIngreso"])->format('Y-m-d');
					$Clave=($_POST["txtClave"]!="")?password_hash($_POST["txtClave"], PASSWORD_BCRYPT):"null";
					$Perfil=$_POST["cbmPerfil"];
					$Observacion=$_POST["txtObservaciones"];
					$Activo=$_POST["cbmEstado"];
					$UltimoIngreso=DateTime::createFromFormat('d-m-Y H:i:s', $_POST["txtFechaUltimoIngreso"])->format('Y-m-d H:i:s');
					$TipoUsuario=$_POST["cbmTipoUsuario"];

					//sanitize el sql
					if($Clave!='null')
					{
						$sql = "UPDATE usuario SET ID_MH='$ID_MH',ID_API='$ID_API',TipoCedula='$TipoCedula',Cedula='$Cedula',NombreRepresentante='$NombreRepresentante',NombreComercial='$NombreComercial',CodigoPostal='$CodigoPostal',Email='$Email',Direccion='$Direccion',Telefono1='$Telefono1',Telefono2='$Telefono2',Zona='$Zona',Cuota=$Cuota,Moneda='$Moneda',Expediente='$Expediente',FechaIngreso='$FechaIngreso',Clave='$Clave',Perfil='$Perfil',Observacion='$Observacion',Activo=$Activo,UltimoIngreso='$UltimoIngreso',TipoUsuario='$TipoUsuario' WHERE IDUsuario=$IDUsuario;";
					}
					else
					{
						$sql = "UPDATE usuario SET ID_MH='$ID_MH',ID_API='$ID_API',TipoCedula='$TipoCedula',Cedula='$Cedula',NombreRepresentante='$NombreRepresentante',NombreComercial='$NombreComercial',CodigoPostal='$CodigoPostal',Email='$Email',Direccion='$Direccion',Telefono1='$Telefono1',Telefono2='$Telefono2',Zona='$Zona',Cuota=$Cuota,Moneda='$Moneda',Expediente='$Expediente',FechaIngreso='$FechaIngreso',Perfil='$Perfil',Observacion='$Observacion',Activo=$Activo,UltimoIngreso='$UltimoIngreso',TipoUsuario='$TipoUsuario' WHERE IDUsuario=$IDUsuario;";
					}
									
					if($Conexion->query($sql) === TRUE) 
					{
						if($_SESSION['IDUsuario']==$IDUsuario)
						{
							$_SESSION['NombreRepresentante']=$NombreRepresentante;
						}
						
						$Respuesta = "Usuario modificado exitosamente";
						$GuarMod = 'Modifico';
					}
					else
					{
						$Respuesta = "Error al modificar el usuario";
						$GuarMod = 'Error';
					}
				}
			}
	}
	
	$users_arr[] = array( 
                         "Respuesta"=>$Respuesta,"GuarMod"=>$GuarMod,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
	
	  	/*si existe: modifica revisa que los cmb no esten seleccionado el campo Provincia canton o distrito, sin cambiar la cedula

	 		
	 		si la cedula existe: error ya hay un usuario en la base de datos con el numero de cedula ingresar
	 		*/
}

function ConsultarUsuario()
{
	$IDUsuario=$_POST['IDUsuario'];

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
		die("Connection failed: " . $Conexion->connect_error);
	} 

	$sql = "SELECT IDUsuario,ID_MH,ID_API,TipoCedula,Cedula,NombreRepresentante,NombreComercial,CodigoPostal,Email,Direccion,Telefono1,Telefono2,Zona,Cuota,Moneda,Expediente,DATE_FORMAT(FechaIngreso,'%d-%m-%Y') AS FechaIngreso,Clave,Perfil,Observacion,Activo,DATE_FORMAT(`UltimoIngreso`,'%d-%m-%Y  %h:%i:%s') as UltimoIngreso,MsjCompraPHacienda,TipoUsuario,Logo FROM usuario WHERE IDUsuario=$IDUsuario;";
						
	$result = $Conexion->query($sql);
	

	if ($result->num_rows > 0) 
	{
		$row = $result->fetch_assoc();

		$IDUsuario= $row["IDUsuario"];
		$ID_MH= $row["ID_MH"];
		$ID_API= $row["ID_API"];
		$TipoCedula= $row["TipoCedula"];
		$Cedula= $row["Cedula"];
		$NombreRepresentante= $row["NombreRepresentante"];
		$NombreComercial= $row["NombreComercial"];
		$CodigoPostal= $row["CodigoPostal"];
		$Email= $row["Email"];
		$Direccion= $row["Direccion"];
		$Telefono1= $row["Telefono1"];
		$Telefono2= $row["Telefono2"];
		$Zona= $row["Zona"];
		$Cuota= $row["Cuota"];
		$Moneda= $row["Moneda"];
		$Expediente= $row["Expediente"];
		$FechaIngreso= $row["FechaIngreso"];
		$Clave= $row["Clave"];
		$Perfil= $row["Perfil"];
		$Observacion= $row["Observacion"];
		$Activo= $row["Activo"];
		$UltimoIngreso= $row["UltimoIngreso"];
		$MsjCompraPHacienda= $row["MsjCompraPHacienda"];
		$TipoUsuario= $row["TipoUsuario"];
		$Logo=strstr($row["Logo"], 'Logos').'?rnd='.date('H:i:s');
	}
	
	/*Actividades Economicas*/
	
	$sql="SELECT AE.Codigo,AE.Nombre,AEXU.Predominante 
			FROM actividadeconomica_x_usuario AEXU
			INNER JOIN actividadeconomica AE 
			ON AEXU.FK_ActividadEconomica=AE.Codigo
			WHERE AEXU.FK_Usuario=$IDUsuario";

	  $result = $Conexion->query($sql);
	  
	  $AE = array();
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $Codigo= $ri['Codigo'];
	         $Nombre= $ri['Nombre'];
	         $Predominante= $ri['Predominante'];
	         
	         $AE[] = array(
	                         "Codigo" => $Codigo,
	                         "Nombre" => $Nombre, 
	                         "Predominante" => $Predominante,
	                        );
	  }

	$users_arr[] = array( 
						"IDUsuario"=>$IDUsuario,
						"ID_MH"=>$ID_MH,
						"ID_API"=>$ID_API,
						"TipoCedula"=>$TipoCedula,
						"Cedula"=>$Cedula,
						"NombreRepresentante"=>$NombreRepresentante,
						"NombreComercial"=>$NombreComercial,
						"CodigoPostal"=>$CodigoPostal,
						"Email"=>$Email,
						"Direccion"=>$Direccion,
						"Telefono1"=>$Telefono1,
						"Telefono2"=>$Telefono2,
						"Zona"=>$Zona,
						"Cuota"=>$Cuota,
						"Moneda"=>$Moneda,
						"Expediente"=>$Expediente,
						"FechaIngreso"=>$FechaIngreso,
						"Clave"=>$Clave,
						"Perfil"=>$Perfil,
						"Observacion"=>$Observacion,
						"Activo"=>$Activo,
						"UltimoIngreso"=>$UltimoIngreso,
						"MsjCompraPHacienda"=>$MsjCompraPHacienda,
						"TipoUsuario"=>$TipoUsuario,
						"Logo"=>$Logo,
						
						/*AE*/
                            "AE"=>$AE,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}

function ModificarPerfil()
{
	$IDUsuario=$_POST["txtIDUsuario"];
	$Cedula=$_POST["txtCedula"];
	$ID_MH=$_POST["txtIDMH"];
	$ID_API=$_POST["txtIDAPI"];
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		} 
			
			$sql="SELECT IDUsuario,Cedula FROM usuario WHERE ID_MH = '$ID_MH' AND IDUSuario != $IDUsuario";

			$result = $Conexion->query($sql);
			
			if ($result->num_rows > 0) 
			{
			echo "Ya existe otro usuario con el ID de Ministerio de Hacienda ingresado"; 
			}
			else # si la cedula y el id y el id_MH tampoco existe: verificar el IDAPI
			{
				$sql="SELECT IDUsuario,Cedula FROM usuario WHERE ID_API = '$ID_API' AND IDUsuario != $IDUsuario";

				$result = $Conexion->query($sql);
				
				if ($result->num_rows > 0) 
				{
				echo "Ya existe otro usuario con el ID API ingresado"; 
				}
				else # si la cedula y el id y el id_MH y el IDAPI tampoco existe guardar:
				{ 

					$Provincia=$_POST["cbmProvincia"];
					$Canton=$_POST["cbmCanton"];
					$Distrito=$_POST["cbmDistrito"];
					$Barrio=$_POST["cbmBarrio"];
					
					$TipoCedula=$_POST["cbmTipoCedula"];
					$NombreRepresentante=$_POST["txtNombreRepresentante"];
					$NombreComercial=$_POST["txtNombreComercial"];
					$CodigoPostal=$Provincia.$Canton.$Distrito.$Barrio;
					$Email=$_POST["txtEmail"];
					$Direccion=$_POST["txtDireccion"];
					$Telefono1=$_POST["txtTelefono1"];
					$Telefono2=$_POST["txtTelefono2"];
					$Zona=$Provincia.$Canton.$Distrito.$Barrio;
					$Cuota=$_POST["txtCuota"];
					$Moneda=$_POST["Moneda"];

					$FechaIngreso=DateTime::createFromFormat('d-m-Y', $_POST["txtFechaIngreso"])->format('Y-m-d');
					$Clave=($_POST["txtClave"]!="")?password_hash($_POST["txtClave"], PASSWORD_BCRYPT):"null";
					$Observacion=$_POST["txtObservaciones"];
					
					$MsjCompraPHacienda=$_POST['txtMsjCompraPHacienda'];

					//sanitize el sql
					if($Clave!='null')
					{
						$sql = "UPDATE usuario SET ID_MH='$ID_MH',ID_API='$ID_API',TipoCedula='$TipoCedula',Cedula='$Cedula',NombreRepresentante='$NombreRepresentante',NombreComercial='$NombreComercial',CodigoPostal='$CodigoPostal',Email='$Email',Direccion='$Direccion',Telefono1='$Telefono1',Telefono2='$Telefono2',Zona='$Zona',Cuota=$Cuota,Moneda='$Moneda',FechaIngreso='$FechaIngreso',Clave='$Clave',Observacion='$Observacion',MsjCompraPHacienda='$MsjCompraPHacienda' WHERE IDUsuario=$IDUsuario;";
					}
					else
					{
						$sql = "UPDATE usuario SET ID_MH='$ID_MH',ID_API='$ID_API',TipoCedula='$TipoCedula',Cedula='$Cedula',NombreRepresentante='$NombreRepresentante',NombreComercial='$NombreComercial',CodigoPostal='$CodigoPostal',Email='$Email',Direccion='$Direccion',Telefono1='$Telefono1',Telefono2='$Telefono2',Zona='$Zona',Cuota=$Cuota,Moneda='$Moneda',FechaIngreso='$FechaIngreso',Observacion='$Observacion',MsjCompraPHacienda='$MsjCompraPHacienda' WHERE IDUsuario=$IDUsuario;";
					}
									
					if($Conexion->query($sql) === TRUE) 
					{
						if($_SESSION['IDUsuario']==$IDUsuario)
						{
							$_SESSION['NombreRepresentante']=$NombreRepresentante;
						}
						
						echo "Usuario modificado exitosamente";
					}
					else
					{
						echo "Error al modificar el perfil";
					}
				}
			}
	
}

function ObtenerConsecutivos($IDUsuario)
{
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
		die("Connection failed: " . $Conexion->connect_error);
	} 

	$sql = "select Factura, Tiquete, NotaCredito, NotaDebito, Compra, FacturaSimplificada, Terminal, Sucursal from consecutivo where FK_Usuario=$IDUsuario;";
						
	$result = $Conexion->query($sql);

	if ($result->num_rows > 0) 
	{
		$row = $result->fetch_assoc();
	
		$ConsFactura=$row["Factura"];
		$ConsTiquete=$row["Tiquete"];
		$ConsNotaCredito=$row["NotaCredito"];
		$ConsNotaDebito=$row["NotaDebito"];
		$ConsFacturaCompra=$row["Compra"];
		$ConsFacturaSimplificada=$row["FacturaSimplificada"];
		$Terminal=$row["Terminal"];
		$Sucursal=$row["Sucursal"];
	}
	
		$users_arr[] = array( 
						"ConsFactura"=>$ConsFactura,
						"ConsTiquete"=>$ConsTiquete,
						"ConsNotaCredito"=>$ConsNotaCredito,
						"ConsNotaDebito"=>$ConsNotaDebito,
						"ConsFacturaCompra"=>$ConsFacturaCompra,
				        "ConsFacturaSimplificada"=>$ConsFacturaSimplificada,
				        "Terminal"=>$Terminal,
				        "Sucursal"=>$Sucursal,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}

function ModificarConsecutivos()
{
	 $IDUsuario=$_POST['IDUsuario'];
     $ConsFactura=$_POST['ConsFactura'];
	 $ConsTiquete=$_POST['ConsTiquete'];
	 $ConsNotaCredito=$_POST['ConsNotaCredito'];
	 $ConsNotaDebito=$_POST['ConsNotaDebito'];
	 $ConsFacturaCompra=$_POST['ConsFacturaCompra'];
	 $ConsFacturaSimplificada=$_POST['ConsFacturaSimplificada'];
	 $Terminal=$_POST['Terminal'];
	 $Sucursal=$_POST['Sucursal'];
	
	 $Respuesta='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
		die("Connection failed: " . $Conexion->connect_error);
	} 	

		$sql = "UPDATE consecutivo SET Factura=$ConsFactura, Tiquete=$ConsTiquete, NotaCredito=$ConsNotaCredito, NotaDebito=$ConsNotaDebito, Compra=$ConsFacturaCompra, FacturaSimplificada=$ConsFacturaSimplificada, Terminal=$Terminal,Sucursal=$Sucursal WHERE FK_Usuario=$IDUsuario";
	
					
	if($Conexion->query($sql) === TRUE) 
	{
		$Respuesta= "Consecutivos modificados exitosamente";
	}
	else
	{
		$Respuesta= "Error al modificar los consecutivos";
	}
	
		$users_arr[] = array( 
						"Respuesta"=>$Respuesta,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
		
}

function AgregarActividadEconomica()
{
	$ActividadEconomica=$_POST['Actividades'];
	$IDUsuario=$_POST['FK_Usuario'];
	$TotalFilasGuardadas=0;
	$ListaAENuevas= array();
	$ListaAEMod= array();

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
		die("Connection failed: " . $Conexion->connect_error);
	}

    /*Recorrer el vector con vectores (Matriz) y guardar en la BD*/
    foreach($ActividadEconomica as $i => $item) 
    {
	   $Codigo= $ActividadEconomica[$i]['Codigo'];
       $Nombre= $ActividadEconomica[$i]['Nombre'];
       $Predominante=($ActividadEconomica[$i]['Predominante']=='Predominante')?1:0;
       
	   //Consultar antes de guardar
	   $sql = "SELECT FK_ActividadEconomica,FK_Usuario,Predominante from actividadeconomica_x_usuario WHERE FK_ActividadEconomica='$Codigo' AND FK_Usuario=$IDUsuario;";
						
		$result = $Conexion->query($sql);

		if ($result->num_rows == 0)//sino existe guarde, si ya existe modifique el predomimante 
		{
			$ListaAENuevas[]=$Codigo;
			
			$sql="INSERT INTO actividadeconomica_x_usuario(FK_ActividadEconomica,FK_Usuario,Predominante)
			values('$Codigo',$IDUsuario,$Predominante);";

		      if($Conexion->query($sql) === TRUE) 
		      { 
		          $TotalFilasGuardadas++;
		      }
		}
		else//modificar
		{
			if($Predominante==1)
			{
				$ListaAEMod[]=$Codigo;
				
				$sql="UPDATE actividadeconomica_x_usuario SET Predominante=1 
					  WHERE FK_ActividadEconomica=$Codigo AND FK_Usuario=$IDUsuario;";

			      if($Conexion->query($sql) === TRUE) 
			      { 
			          $TotalFilasGuardadas++;
			      }	
			}
			else
			{
				$ListaAEMod[]=$Codigo;
				
				$sql="UPDATE actividadeconomica_x_usuario SET Predominante=0 
					  WHERE FK_ActividadEconomica=$Codigo AND FK_Usuario=$IDUsuario;";

			      if($Conexion->query($sql) === TRUE) 
			      { 
			          $TotalFilasGuardadas++;
			      }
			}
			
		}
      
	}
			
	if((count($ListaAENuevas)+count($ListaAEMod))==$TotalFilasGuardadas)
	{
		$Respuesta = "Actividades económicas guardadas o modificadas exitosamente";
		$GuarMod='Guardo';
	}
	else
	{
		foreach($ListaAENuevas as $CodigoAE) 
    	{
    		$CodigoAENueva= $CodigoAE;
    		
    		$sql="DELETE FROM actividadeconomica_x_usuario WHERE FK_ActividadEconomica = '$CodigoAENueva' AND FK_Usuario = $IDUsuario;";

		      if($Conexion->query($sql) === TRUE) 
		      { 

		      }	
		}
		
		$Respuesta = "Error al guardar o modificar las actividades económicas";
		$GuarMod='Error';
		
	}
	
	$users_arr[] = array( 
                         "Respuesta"=>$Respuesta,"GuarMod"=>$GuarMod,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
			
}

function CargarLogo()
{
	$validextensions = array("jpeg", "jpg", "png");
	$temporary = explode(".", $_FILES["file"]["name"]);
	$file_extension = end($temporary);
	
	if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
	) && ($_FILES["file"]["size"] < 100000)//Approx. 100kb files can be uploaded.
	&& in_array($file_extension, $validextensions)) {
		if ($_FILES["file"]["error"] > 0)
		{
			echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
		}
		else
		{
				$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

				if ($Conexion->connect_error) 
				{
					die("Connection failed: " . $Conexion->connect_error);
				}
				//todo cambiar nombre antes de guardarlo
				$IDUsuario=$_POST['IDUsuario'];
				
				$Cedula='';
				
				//traer Cedula con el ID3_BEST
				$sql="SELECT Cedula FROM usuario WHERE IDUSuario = $IDUsuario";
				
				$result = $Conexion->query($sql);

				if ($result->num_rows > 0) 
				{
					$row = $result->fetch_assoc();

					$Cedula= $row["Cedula"];
					
					$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				
					$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
					$targetPath = __DIR__."/../Logos/".$Cedula.'.'.$ext; // Target path where file is to be stored
					
					$targetPath=str_replace("\\","/",$targetPath);
									
					$sql = "UPDATE usuario SET Logo='$targetPath' WHERE IDUsuario=$IDUsuario;";
		
					if($Conexion->query($sql) === TRUE) 
					{
						move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
						unset($_SESSION['IDUsuarioPLogo']);
					    echo "<span id='success'>Logo cargado exitosamente...!!</span><br/>";
					}
					else if (empty($IDUsuario))
					{
						echo "<span id='success'>Error al cargar el logo: No hay un usuario disponible para asignarle el logo. Cree un usuario nuevo o seleccione uno de la lista para asignarle el logo</span><br/>";
					}
					else
					{
						echo "<span id='success'>Error al cargar el logo</span><br/>";
					}
				}
				else if (empty($IDUsuario))
				{
					echo "<span id='success'>Error al cargar el logo: No hay un usuario disponible para asignarle el logo. Cree un usuario nuevo o seleccione uno de la lista para asignarle el logo</span><br/>";
				}
				else
				{
					echo "<span id='success'>Error al cargar el logo</span><br/>";
				}			
		}
	}
	else
	{
		echo "<span id='invalid'>Tamaño o tipo de imagen inválida. Solo imágenes de tipo jpeg, jpg o png y no mayores a 100Kb apróx. son permitidas.<span>";
	}
}

?>

