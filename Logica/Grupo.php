<?php
 
 session_start();
 
require ("../Conexion/Conexion.php");


//Comprbar si el usuario presiono el boton de EnviarGrupo

if(isset($_POST["btnEnviarGrupo"]))/*Para guardar o modificar*/
{
	GuardarOModificarGrupo();
}
else if(isset($_POST['MostrarDatos']))/*Para consultar y llenar los campos*/
{
	ConsultarGrupo();
}



function GuardarOModificarGrupo()
{
	$GuardarModificar=$_POST["GuardarModificar"];
	
	$Respuesta='';
	$GuarMod='';

	#ver si es guardar o Modificar
	
	if($GuardarModificar=="Guardar")
	{
		//Guardar

		$IDGrupo=$_POST["txtIDGrupo"];
		$Grupo=$_POST["txtGrupo"];
		$FK_Usuario=$_SESSION['IDUsuario'];
		
		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		} 

		$sql = "SELECT IDGrupo,Grupo FROM grupo WHERE IDGrupo = '" .$IDGrupo."' and FK_Usuario=".$FK_Usuario."";
							
		$result = $Conexion->query($sql);

		if ($result->num_rows > 0) 
		{
		$Respuesta = "Ya existe un grupo de productos con ese ID Grupo";
		$GuarMod='Error'; 
		}
		else # si no existe verificar que no haya otro grupo con el mismo nombre
		{		
			$sql = "SELECT IDGrupo,Grupo FROM grupo WHERE Grupo like '%" .$Grupo."%' and FK_Usuario=".$FK_Usuario."";
							
			$result = $Conexion->query($sql);

			if ($result->num_rows > 0) 
			{
			$Respuesta = "Ya existe un grupo de productos con ese nombre"; 
			}
			else
			{
				//sanitize el sql
				$sql = "INSERT INTO grupo(IDGrupo,FK_Usuario,Grupo)values('$IDGrupo',$FK_Usuario,'$Grupo');";
								
				if($Conexion->query($sql) === TRUE) 
				{   
				  $Respuesta = "Grupo guardado exitosamente";
				  $GuarMod='Guardo'; 			  
				} 
				else 
				{
				  $Respuesta = "Error al guardar el grupo";
				  $GuarMod='Error';
				}
			}		
		}		
	}			
	else if($GuardarModificar=="Modificar")
	{
		# modificar...
		$IDGrupo=$_POST["txtIDGrupo"];
		$Grupo=$_POST["txtGrupo"];
		$FK_Usuario=$_SESSION['IDUsuario'];
		
		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
		 	die("Connection failed: " . $Conexion->connect_error);
		} 
		
		/*
		$sql = "SELECT IDGrupo,Grupo FROM Grupo WHERE Grupo like '%" .$Grupo."%' and FK_Usuario=".$FK_Usuario."";
						
		$result = $Conexion->query($sql);

		if ($result->num_rows > 0) 
		{
		$Respuesta = "Ya existe un grupo de productos con ese nombre";
		$GuarMod='Error'; 
		}
		else
		{*/
			//sanitize el sql
			$sql = "UPDATE grupo SET Grupo='$Grupo' where IDGrupo='$IDGrupo' and FK_Usuario=$FK_Usuario;";
							
			if($Conexion->query($sql) === TRUE) 
			{   
			  $Respuesta = "Grupo modificado exitosamente";
			  $GuarMod = 'Modifico'; 			  
			} 
			else 
			{
				 $Respuesta = "Error al modificar el grupo";
				 $GuarMod='Error';
			}
		//}		
	}
	
	$users_arr[] = array( 
                            "Respuesta"=>$Respuesta,
                            "GuarMod"=>$GuarMod,
                       );

      // encoding array to json format
      echo json_encode($users_arr);
      exit;
}

function ConsultarGrupo()
{
	$IDGrupo=$_POST['CodigoGrupo'];
	$FK_Usuario=$_SESSION['IDUsuario'];

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
		die("Connection failed: " . $Conexion->connect_error);
	} 

	$sql = "SELECT IDGrupo,Grupo FROM grupo WHERE IDGrupo = '" .$IDGrupo."' and FK_Usuario=".$FK_Usuario."";
						
	$result = $Conexion->query($sql);

	if ($result->num_rows > 0) 
	{
		$row = $result->fetch_assoc();

		$CodigoGrupo= $row["IDGrupo"];
		$Grupo=$row["Grupo"];
	}

	$users_arr[] = array( 
                         "IDGrupo"=>$CodigoGrupo,"Grupo"=>$Grupo,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}


?>
