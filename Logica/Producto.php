<?php
	
session_start();
require ("../Conexion/Conexion.php");


//Comprbar si el usuario presiono el boton de Login

if(isset($_POST["btnEnviarProducto"]))
{
	GuardarOModificarProducto();
}
else if(isset($_POST['MostrarDatos']))/*Para consultar y llenar los campos*/
{
	ConsultarProducto();
}


//Funcion para Guardar o Modificar

function GuardarOModificarProducto()
{
	$GuardarModificar=$_POST["GuardarModificar"];
	
	$Respuesta='';
	$GuarMod='';

	#ver si es guardar o Modificar
	
	if($GuardarModificar=="Guardar")
	{
		$IDProducto=$_POST["txtIDProducto"];
		$FK_Usuario=$_SESSION['IDUsuario'];
		$Producto=$_POST["txtProducto"];
		$PrecioCosto=$_POST["txtPrecioCosto"];
		$PrecioVenta=$_POST["txtPrecioVenta"];
		$Utilidad=$_POST["txtUtilidad"];
		$Impuesto=$_POST["txtImpuesto"];
		$SaldoActual=$_POST["txtSaldoActual"];
		$SaldoAnterior=$_POST["txtSaldoAnterior"];
		$Descuento=$_POST["txtDescuento"];
		$UltimaVenta='now()';
		$FK_Grupo=$_POST["cbmGrupo"];
		$Activo=$_POST["cbmEstado"];
		$UnidadMedida=$_POST["cbmUnidadMedida"];
		
		
		# guardar...

		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		} 

		$sql = "SELECT IDProducto FROM producto WHERE IDProducto = '" .$IDProducto."' AND FK_Usuario='".$FK_Usuario."'";
							
		$result = $Conexion->query($sql);

		if ($result->num_rows > 0) 
		{
		$Respuesta = "El producto que intenta guardar ya existe en la Base de Datos";
		$GuarMod='Error';  
		}
		else 
		{	
			$sql = "SELECT IDProducto FROM producto WHERE NombreProducto = '" .$Producto."' AND FK_Usuario='".$FK_Usuario."'";
							
			$result = $Conexion->query($sql);

			if ($result->num_rows > 0) 
			{
			$Respuesta = "Ya existe un producto con ese nombre";
			$GuarMod='Error'; 
			}
			else
			{
				
			//sanitize el sql
			    $sql = "INSERT INTO producto
						(IDProducto,FK_Usuario,NombreProducto,PrecioVenta,Utilidad,PrecioCosto,Impuesto,SaldoAnterior,SaldoActual,Descuento,UltimaVenta,Activo,
						UnidadMedida,FK_Grupo)
						values
	('$IDProducto',$FK_Usuario,'$Producto',$PrecioVenta,$Utilidad,$PrecioCosto,$Impuesto,$SaldoAnterior,$SaldoActual,$Descuento,$UltimaVenta,$Activo,
	'$UnidadMedida','$FK_Grupo');";
								
				if($Conexion->query($sql) === TRUE) 
				{  
				  $Respuesta = "Producto guardado exitosamente";
				  $GuarMod='Guardo';  			  
				} 
				else 
				{
		    		 $Respuesta = "Error al guardar el producto";
		    		 $GuarMod='Error'; 
				}
			}
		}
	}
	else if($GuardarModificar=="Modificar")
	{
		# modificar...

		$IDProducto=$_POST["txtIDProducto"];
		$FK_Usuario=$_SESSION['IDUsuario'];
		$Producto=$_POST["txtProducto"];
		$PrecioCosto=$_POST["txtPrecioCosto"];
		$PrecioVenta=$_POST["txtPrecioVenta"];
		$Utilidad=$_POST["txtUtilidad"];
		$Impuesto=$_POST["txtImpuesto"];
		$SaldoActual=$_POST["txtSaldoActual"];
		$SaldoAnterior=$_POST["txtSaldoAnterior"];
		$Descuento=$_POST["txtDescuento"]; 
		$UltimaVenta=DateTime::createFromFormat('d-m-Y', $_POST["txtUltimaVenta"])->format('Y-m-d');/*Dar Formato*/
		$FK_Grupo=$_POST["cbmGrupo"];
		$Activo=$_POST["cbmEstado"];
		$UnidadMedida=$_POST["cbmUnidadMedida"];
		
		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		 if ($Conexion->connect_error) 
		 {
		 	die("Connection failed: " . $Conexion->connect_error);
		 } 

		// $sql = "SELECT IDProducto FROM Producto WHERE NombreProducto = '" .$Producto."' AND FK_Usuario='".$FK_Usuario."'";
							
		// $result = $Conexion->query($sql);

		// if ($result->num_rows > 0) 
		// {
		// echo "Ya existe un producto con ese nombre"; 
		// }
		// else 
		// {			
			//sanitize el sql
		    $sql = "UPDATE producto SET NombreProducto='$Producto',PrecioVenta=$PrecioVenta,Utilidad=$Utilidad,PrecioCosto=$PrecioCosto,Impuesto=$Impuesto,SaldoAnterior=$SaldoAnterior,SaldoActual=$SaldoActual,Descuento=$Descuento,UltimaVenta='$UltimaVenta',Activo=$Activo,UnidadMedida='$UnidadMedida',FK_Grupo='$FK_Grupo' WHERE FK_Usuario=$FK_Usuario AND IDProducto='$IDProducto';";
							
			if($Conexion->query($sql) === TRUE) 
			{  
			  $Respuesta = "Producto modificado exitosamente"; 
			  $GuarMod='Modifico';			  
			} 
			else 
			{
	    		 $Respuesta = "Error al modificar el producto";
	    		 $GuarMod='Error';
			}

		//}
	}
	
	$users_arr[] = array( 
                            "Respuesta"=>$Respuesta,
                            "GuarMod"=>$GuarMod,
                       );

      // encoding array to json format
      echo json_encode($users_arr);
      exit;
	  	/*si existe: modifica revisa que los cmb no esten seleccionado el campo Provincia canton o distrito, sin cambiar la cedula

	 		
	 		si la cedula existe: error ya hay un usuario en la base de datos con el numero de cedula ingresar
	 		*/
}

function ConsultarProducto()
{
	$IDProducto=$_POST['CodigoProducto'];
	$FK_Usuario=$_SESSION['IDUsuario'];

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
		die("Connection failed: " . $Conexion->connect_error);
	} 

	$sql = "SELECT IDProducto, NombreProducto,PrecioVenta,Utilidad,PrecioCosto,Impuesto,SaldoAnterior,SaldoActual,Descuento, 
			DATE_FORMAT(`UltimaVenta`,'%d-%m-%Y') AS UltimaVenta, Activo,UnidadMedida,FK_Grupo FROM producto WHERE FK_Usuario=$FK_Usuario AND IDProducto='$IDProducto';";
						
	$result = $Conexion->query($sql);

	if ($result->num_rows > 0) 
	{
		$row = $result->fetch_assoc();

		$CodigoProducto= $row["IDProducto"];
		$NombreProducto=$row["NombreProducto"];
		$PrecioVenta=$row["PrecioVenta"];
		$Utilidad=$row["Utilidad"];
		$PrecioCosto=$row["PrecioCosto"];
		$Impuesto=$row["Impuesto"];
		$SaldoAnterior=$row["SaldoAnterior"];
		$SaldoActual=$row["SaldoActual"];
		$Descuento=$row["Descuento"];
		$UltimaVenta=$row["UltimaVenta"];
		$Activo=$row["Activo"];
		$UnidadMedida=$row["UnidadMedida"];
		$FK_Grupo=$row["FK_Grupo"];
	}

	$users_arr[] = array( 
                        "CodigoProducto"=>$CodigoProducto,
						"NombreProducto"=>$NombreProducto,
						"PrecioVenta"=>$PrecioVenta,
						"Utilidad"=>$Utilidad,
						"PrecioCosto"=>$PrecioCosto,
						"Impuesto"=>$Impuesto,
						"SaldoAnterior"=>$SaldoAnterior,
						"SaldoActual"=>$SaldoActual,
						"Descuento"=>$Descuento,
						"UltimaVenta"=>$UltimaVenta,
						"Activo"=>$Activo,
						"UnidadMedida"=>$UnidadMedida,
						"FK_Grupo"=>$FK_Grupo,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}


// $frac  = $PVSinIV - (int) $PVSinIV;  // .7
		
// 		$SinDecimales=explode("0.", $frac);
		
// 		$frac = $SinDecimales[1];
		
// 		if(substr($frac,0,1)=='0' OR substr($frac,0,1)=='1' OR substr($frac,0,1)=='2')
// 		{
// 			/*Redondea a 0 hacia abajo*/
// 		}
// 		else if(substr($frac,0,1)=='3' 
// 				OR substr($frac,0,1)=='4' 
// 				OR substr($frac,0,1)=='5' 
// 				OR substr($frac,0,1)=='6' 
// 				OR substr($frac,0,1)=='7')
// 		{
// 			/*Redondea a 5*/
// 			$PVSinIV=round($PVSinIV/5) * 5;
// 		}
// 		else if(substr($frac,0,1)=='8' OR substr($frac,0,1)=='9')
// 		{
// 			/*Redondea a 0 hacia abajo*/
// 		}



?>
