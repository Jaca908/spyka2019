<?php
session_start();

require('mc_table.php');
require('NumeroALetras.php');
require('../../Conexion/Conexion.php');

//Consultar datos antes de generar PDF

if(isset($_POST['btnPDF']))
{
	$_SESSION["IDRecibo"]=$_POST['IDRecibo'];
	$_SESSION["TipoDocumentoCXC"]=$_POST['TipoDocumento'];
	$_SESSION["CedulaClienteCXC"]=$_POST['CedulaCliente'];
	$_SESSION["NombreClienteCXC"]=$_POST['NombreCliente'];
	$_SESSION["FechaCXC"]=$_POST['Fecha'];
	$_SESSION["SaldoAnteriorCXC"]=$_POST['SaldoAnterior'];
	$_SESSION["MontoReciboCXC"]=$_POST['MontoRecibo'];
	$_SESSION["SaldoActualCXC"]=$_POST['SaldoActual'];
	$_SESSION["FacturasCXC"]=$_POST['Facturas'];
}
else
{
	if(!isset($_SESSION["IDRecibo"],$_SESSION["TipoDocumentoCXC"],$_SESSION["CedulaClienteCXC"],
			  $_SESSION["NombreClienteCXC"],$_SESSION["FechaCXC"],$_SESSION["SaldoAnteriorCXC"],
			  $_SESSION["MontoReciboCXC"],$_SESSION["SaldoActualCXC"],$_SESSION["FacturasCXC"]))
	{
		echo "<script>window.close();</script>";
	}

	$IDRecibo=$_SESSION["IDRecibo"];
	$TipoDocumento=$_SESSION["TipoDocumentoCXC"];
	$CedulaCliente=$_SESSION["CedulaClienteCXC"];
	$NombreCliente=$_SESSION["NombreClienteCXC"];
	$Fecha=DateTime::createFromFormat('d-m-Y H:i:s', $_SESSION["FechaCXC"])->format('d-m-Y H:i:s');
	$SaldoAnterior=QuitarFormatoNumero($_SESSION["SaldoAnteriorCXC"]);
	$MontoRecibo=QuitarFormatoNumero($_SESSION["MontoReciboCXC"]);
	$SaldoActual=QuitarFormatoNumero($_SESSION["SaldoActualCXC"]);
	$FacturasCredito=$_SESSION["FacturasCXC"];
	
	$ListaFacturas='';
	
	foreach($FacturasCredito as $i => $item) 
	{
		if($FacturasCredito[$i]['Abono']!='0.00')
		{
			$ListaFacturas.= $FacturasCredito[$i]['NoFacturaElect'].' ';
		}
	}
	
	$IDUsuario=$_SESSION['IDUsuario'];
	
	unset($_SESSION["IDRecibo"],$_SESSION["TipoDocumentoCXC"],$_SESSION["CedulaClienteCXC"],
		  $_SESSION["NombreClienteCXC"],$_SESSION["FechaCXC"],$_SESSION["SaldoAnteriorCXC"],
		  $_SESSION["MontoReciboCXC"],$_SESSION["SaldoActualCXC"],$_SESSION["FacturasCXC"]);

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
	    die("Connection failed: " . $Conexion->connect_error);
	} 
	
	//Datos del Emisor
	
	
	
	$sql="SELECT NombreComercial,NombreRepresentante,Cedula,Concat(Telefono1,'/',Telefono2) AS Telefono,Direccion FROM usuario WHERE IDUsuario=$IDUsuario;";
	                     
	$result = $Conexion->query($sql);

	if ($result->num_rows > 0) 
	{
		$row = $result->fetch_assoc();
		
		$NombreComercial=$row['NombreComercial'];
		$NombreRepresentante=$row['NombreRepresentante'];
		$Cedula=$row['Cedula'];
		$Telefono=$row['Telefono'];
		$Direccion=$row['Direccion'];
	}


	$pdf=new PDF_MC_Table('L','mm',array(140,216));
	$pdf->SetTopMargin(5);
	$pdf->SetAutoPageBreak(1, 5);
	$pdf->AddPage();
	//$pdf->SetAutoPageBreak(false);
	$pdf->SetFont('Arial','B',12);

	//$pdf->SetTextColor(255,255,255);  // Establece el color del texto (en este caso es blanco) 
	//$pdf->SetFillColor(0, 0, 0); // establece el color del fondo de la celda (en este caso es AZUL 
	
	$pdf->Cell(70,5,utf8_decode(strtoupper($NombreComercial)),0,0,'C');
	
	if($TipoDocumento=='2')
	{
		$pdf->Cell(115,5,utf8_decode('RECIBO DE DINERO: '.$IDRecibo),0,0,'R');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	else if($TipoDocumento=='3')
	{
		$pdf->Cell(115,5,utf8_decode('NOTA DE DÉBITO: '.$IDRecibo),0,0,'R');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	else if($TipoDocumento=='4')
	{
		$pdf->Cell(115,5,utf8_decode('NOTA DE CRÉDITO: '.$IDRecibo),0,0,'R');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	
	$pdf->SetFont('Arial','',9);
	
	$pdf->Ln();
	$pdf->Cell(70,5,utf8_decode($NombreRepresentante),0,0,'C');
	
	$pdf->Ln();
	$pdf->Cell(70,5,utf8_decode($Cedula),0,0,'C');
	
	$pdf->Ln();
	$pdf->MultiCell(70,5,utf8_decode($Telefono.', '.$Direccion),0,'C');
	
	$pdf->Cell(185,5,utf8_decode('Fecha: '.$Fecha),0,0,'R');
	
	$pdf->Ln();
	$pdf->Cell(185,5,utf8_decode('Cédula: '.$CedulaCliente),0,0,'L');
	
	$pdf->Ln();
	$pdf->Cell(185,5,utf8_decode('Cliente: '.$NombreCliente),0,0,'L');
	
	$pdf->Ln();
	
	$pdf->Ln();
	
	if($TipoDocumento=='2')
	{
		$pdf->Cell(120,5,utf8_decode('Monto del recibo.........................................................................................................:'),0,0,'L');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	else if($TipoDocumento=='3')
	{
		$pdf->Cell(120,5,utf8_decode('Monto de Débito..........................................................................................................:'),0,0,'L');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	else if($TipoDocumento=='4')
	{
		$pdf->Cell(120,5,utf8_decode('Monto de Crédito.........................................................................................................:'),0,0,'L');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	
	$pdf->SetFont('Arial','B',15);
	$pdf->Cell(65,5,utf8_decode(number_format($MontoRecibo,2)),0,0,'C');
	
	$pdf->Ln();
	
	$pdf->SetFont('Arial','',9);
	
	$pdf->Ln();
	$pdf->MultiCell(185,5,utf8_decode('En letras: '.NumeroALetras::convertir($MontoRecibo, 'COLONES', 'CÉNTIMOS')),0,'L');
	
	if($TipoDocumento=='2')
	{
		$pdf->MultiCell(185,5,utf8_decode('Abona/Cancela Factura: '.rtrim($ListaFacturas,' ')),0,'L');	
	}
	else if($TipoDocumento=='3')
	{
		$pdf->MultiCell(185,5,utf8_decode('Concepto: Aplicó Nota de Débito a Facturas: '.rtrim($ListaFacturas,' ')),0,'L');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	else if($TipoDocumento=='4')
	{
		$pdf->MultiCell(185,5,utf8_decode('Concepto: Aplicó Nota de Crédito a Facturas: '.rtrim($ListaFacturas,' ')),0,'L');//si es 2 recibo si es 3 ND si es 4 NC	
	}
	
	
	
	$pdf->SetFont('Arial','',12);
	
	$pdf->Ln();
	$pdf->Cell(35,5,utf8_decode('Saldo Anterior: '),0,0,'L');
	$pdf->Cell(30,5,utf8_decode(number_format($SaldoAnterior,2)),0,0,'R');
	
	$pdf->SetFont('Arial','B',12);
	
	$pdf->Ln();
	
	if($TipoDocumento=='2')
	{
		$pdf->Cell(35,5,utf8_decode('Monto Recibo: '),0,0,'L');	
	}
	else if($TipoDocumento=='3')
	{
		$pdf->Cell(35,5,utf8_decode('Monto Débito: '),0,0,'L');
	}
	else if($TipoDocumento=='4')
	{
		$pdf->Cell(35,5,utf8_decode('Monto Crédito: '),0,0,'L');
	}
	
	$pdf->Cell(30,5,utf8_decode(number_format($MontoRecibo,2)),0,0,'R');
	
	$pdf->SetFont('Arial','',12);
	
	$pdf->Ln();
	$pdf->Cell(35,5,utf8_decode('Saldo Actual: '),0,0,'L');
	$pdf->Cell(30,5,utf8_decode(number_format($SaldoActual,2)),0,0,'R');
	
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
	
	$pdf->SetFont('Arial','',9);
	
	$pdf->Cell(30,5,utf8_decode(''),0,0,'C');
	$pdf->Cell(60,5,utf8_decode('Autorizado por'),'T',0,'C');
	$pdf->Cell(10,5,utf8_decode(''),0,0,'C');
	$pdf->Cell(60,5,utf8_decode('Recibido conforme'),'T',0,'C');
	 
	 
	if($TipoDocumento=='2')
	{ 
		$pdf->Output('Recibo Nº '.$IDRecibo.'.pdf','I');
	}
	else if($TipoDocumento=='3')
	{
		$pdf->Output('Nota Débito Nº '.$IDRecibo.'.pdf','I');
	}
	else if($TipoDocumento=='4')
	{
		$pdf->Output('Nota Crédito Nº '.$IDRecibo.'.pdf','I');
	}
}

function QuitarFormatoNumero($Numero)
{
	//$Numero=number_format((float)$Numero, 2, '.', '');
	
	return str_replace(",", "", $Numero);
}

?>
