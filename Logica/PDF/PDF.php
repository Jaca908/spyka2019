<?php
session_start();

require('mc_table.php');
require('NumeroALetras.php');
require('../../Conexion/Conexion.php');

if(isset($_POST['btnPDF']))
{
	$_SESSION["IDDocumento"]=$_POST['IDDocumento'];
	$_SESSION["TipoDocumento"]=$_POST['TipoDocumento'];
	$_SESSION["CedulaCliente"]=$_POST['CedulaCliente'];
}
else
{
	if(!isset($_SESSION["IDDocumento"],$_SESSION["TipoDocumento"],$_SESSION["CedulaCliente"]))
	{
		echo "<script>window.close();</script>";
	}
	
	$IDDocumento=$_SESSION["IDDocumento"];
	$TipoDocumento=$_SESSION["TipoDocumento"]; 
	$CedulaCliente=$_SESSION["CedulaCliente"];
	$UsuarioID=$_SESSION['IDUsuario'];

	unset($_SESSION["IDDocumento"],$_SESSION["TipoDocumento"],$_SESSION["CedulaCliente"]);

	$TipoDoc=$TipoDocumento;

	//Consultar datos antes de generar PDF

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
	    die("Connection failed: " . $Conexion->connect_error);
	} 

	$sql=SelectSQLEncabezado($TipoDoc,$IDDocumento,$CedulaCliente,$UsuarioID);
	                     
	$result = $Conexion->query($sql);

	if ($result->num_rows > 0) 
	{
		$row = $result->fetch_assoc();
		
		$ID=($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')?$row["IDFactura"]:$row["IDNota"];
		$NoDoc=($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')?$row["NoFactura"]:$row["NoNota"];
		
		$FK_Usuario=$row["FK_Usuario"]; //II
		$Clave=$row["Clave"];//II
		
		/*Datos del CLiente*/

		  $FK_Cliente=$row["FK_Cliente"];//II
		  $NombreCliente=$row["NombreCliente"];//II

		  $Telefono=$row["Telefono"];//II
		  $Email=$row["Email"];//II
		  $Direccion=$row["Direccion"];//II
		  $Zona=$row["Zona"];//II
		  $Exonerado=$row["Exonerado"];
		  
		  /*Datos del Emisor*/
		  
		  $NombreRepEmisor=$row["NombreU"];//II
		  $NombreComEmisor=$row["NombreCU"];//II
		  $CedulaEmisor=$row["CedulaU"];//II
		  $TelefonoEmisor=$row["TelefonoU"];//II
		  $DireccionEmisor=$row["DireccionU"];//II
		  $EmailEmisor=$row["EmailU"];//II
		  $Logo=$row["Logo"];
		  
		  $Fecha=$row["Fecha"];//II
		  $Plazo=$row["Plazo"];//II
		  
		  $MedioPago=$row["MedioPago"];//II
	      $CondicionVenta=$row["CondicionVenta"];//II
	      $Status=$row["Status"];//II
	      $NoOrden=$row["NoOrden"];//II
	      $TipoMoneda=$row["TipoMoneda"];//II
	      $TipoCambio=$row["TipoCambio"];//II
	      
	      $Simbolo='';//II
	      $NombreMoneda='';//II
	      $Moneda_TipoCambio='';//II
	      
	      if($TipoMoneda=='C')
	      {
		  	$Simbolo=html_entity_decode('&#162;');
		  	$NombreMoneda='COLONES';
		  }
		  else if($TipoMoneda=='D')
		  {
		  	$Simbolo=html_entity_decode('&#36;');
		  	$NombreMoneda='DÓLARES';
		  	$Moneda_TipoCambio='Factura en Dólares. Tipo de Cambio: '.$TipoCambio;
		  }
		  else
		  {
		  	$Simbolo=utf8_encode(chr(128));
		  	$NombreMoneda='EUROS';
		  	$Moneda_TipoCambio='Factura en Euros. Tipo de Cambio: '.$TipoCambio;
		  }
	      
	      $Terminal=$row["Terminal"];//II
	      $Sucursal=$row["Sucursal"];//II
	      
	      $Razon=($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')?'':$row["Razon"];//D Solo N
	      
	      $MontoGravado=$row["MontoGravado"];//II
	      $MontoExento=$row["MontoExento"];//II
	      $Descuento=$row["Descuento"];//II
	      $Impuesto=$row["Impuesto"];//II
	      $OtroImpuesto=$row["OtroImpuesto"];//II
	      $ServicioGravado=$row["ServicioGravado"];//II
	      $ServicioExento=$row["ServicioExento"];//II
	      $Subtotal=0.00;//($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')?$row["SubtotalFactura"]:$row["SubtotalNota"];//ID
		  $Total=($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')?$row["TotalFactura"]:$row["TotalNota"];//ID
		
		if($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')
		{	      
		      $FechaVencimiento=ObtenerFechaVencimiento($Fecha,$Plazo);//D Solo FT
		      $Saldo=$row["Saldo"];//D Solo F
		}
		else // Notas
		{
		    $ClaveDocAfectado=$row["NoReferencia"];//Clave Doc Afectado D Solo N
		}
	      
		
		$pdf=new PDF_MC_Table('P','mm','Letter');
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',12);

		$pdf->SetTextColor(255,255,255);  // Establece el color del texto (en este caso es blanco) 
		$pdf->SetFillColor(0, 0, 0); // establece el color del fondo de la celda (en este caso es AZUL 
		
		if($TipoDoc=='Factura')
		{	      
		    $pdf->Cell(195,10,utf8_decode('FACTURA ELECTRÓNICA'),1,0,'C',TRUE);
		}
		else if($TipoDoc=='Tiquete' AND ($NoDoc=='' OR $Clave==''))//es una proforma
		{	      
		    $pdf->Cell(195,10,utf8_decode('FACTURA PROFORMA'),1,0,'C',TRUE);
		}
		else if($TipoDoc=='Tiquete' AND ($NoDoc!='' OR $Clave!=''))
		{	      
		    $pdf->Cell(195,10,utf8_decode('TIQUETE ELECTRÓNICO'),1,0,'C',TRUE);
		}
		else if($TipoDoc=='Nota Credito')
		{	      
		    $pdf->Cell(195,10,utf8_decode('NOTA DE CRÉDITO'),1,0,'C',TRUE);
		}
		else if($TipoDoc=='Nota Debito')
		{	      
		    $pdf->Cell(195,10,utf8_decode('NOTA DE DÉBITO'),1,0,'C',TRUE);
		}
		
		 //$pdf->Cell(195,10,utf8_decode('FACTURA ELECTRÓNICA'),1,0,'C',TRUE);
		 // en orden lo que informan estos parametros es: 
		// ancho 145 
		//alto 20 
		// texto dentro de la celda = LETRERO 
		// ¿Borde?  ==> Si, 
		// ´0´ 
		// C ==> texto centrado 
		// True == > Inidica que el fondo de la celda SI debe dibujarse.   
		//Por default es ´false´ (en tu caso es ´false´ porque tu lo omitiste)  
		//y por lo tanto el fondo de tu celda NO SE ESTA DIBUJANDO.   
		//CAMBIA ESTE VALOR.
		$pdf->SetFont('Arial','',10);
		$pdf->SetTextColor(0,0,0);
		 $pdf->Ln();
		 $pdf->Ln();

		if(!empty($Logo))
		{
			$scale = 0.20;    
		
			list($originalWidth, $originalHeight) = getimagesize($Logo);//debe ser de 140px de alto maximo
		
			$pdf->Image($Logo,10,25,$originalWidth * $scale, $originalHeight * $scale);	
		}

		$pdf->Cell(75,5,' ',0,0,'L',0);   // LTR:LeftTopRigth bordes solo de arriba y lados, 0 no bordes, 1 todos los bordes
		$pdf->SetFont('Arial','B',10);
		
		if($TipoDoc=='Tiquete')
		{
			$pdf->Cell(100,5,utf8_decode($NombreComEmisor),0,0,'L',0);	
		}
		else
		{
			$pdf->Cell(100,5,utf8_decode($NombreRepEmisor),0,0,'L',0);
		}

		$pdf->Ln();
		$pdf->SetFont('Arial','',10);

		$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
		$pdf->Cell(100,5,utf8_decode('Cédula (f/j): '.$CedulaEmisor),0,0,'L',0);
		$pdf->Ln();

		$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
		$pdf->Cell(100,5,utf8_decode('Teléfono: '.$TelefonoEmisor),0,0,'L',0);
		$pdf->Ln();

		$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
		//$pdf->Cell(100,5,utf8_decode('DIRECCION:'),0,0,'L',0);
		$pdf->MultiCell(100,5,utf8_decode($DireccionEmisor),0,'L');
		//$pdf->Ln();

		$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
		$pdf->Cell(100,5,utf8_decode('Correo: '.$EmailEmisor),0,0,'L',0);
		$pdf->Ln();

		$pdf->SetFont('Arial','B',10);

		if($TipoDoc=='Factura')
		{	      
		    $pdf->Cell(150,5,utf8_decode('FACTURA ELECTRÓNICA: '.$NoDoc),0,0,'L');
			$pdf->Ln();
		}
		else if($TipoDoc=='Tiquete' AND ($NoDoc!='' OR $Clave!=''))
		{	      
		    $pdf->Cell(150,5,utf8_decode('TIQUETE ELECTRÓNICO: '.$NoDoc),0,0,'L');
			$pdf->Ln();
		}
		else if($TipoDoc=='Tiquete' AND ($NoDoc=='' OR $Clave==''))//es proforma
		{	      
		    $pdf->Cell(150,5,utf8_decode('FACTURA PROFORMA'),0,0,'L');
			$pdf->Ln();
		}
		else if($TipoDoc=='Nota Credito')
		{	      
		    $pdf->Cell(150,5,utf8_decode('NOTA DE CRÉDITO: '.$NoDoc),0,0,'L');
			$pdf->Ln();
		}
		else if($TipoDoc=='Nota Debito')
		{	      
		    $pdf->Cell(150,5,utf8_decode('NOTA DE DÉBITO: '.$NoDoc),0,0,'L');
			$pdf->Ln();
		}

		$pdf->Cell(150,5,utf8_decode('Número Clave: '.$Clave),0,0,'L');
		$pdf->Ln();

		$pdf->SetFont('Arial','',10);

		$pdf->Cell(150,5,utf8_decode('Fecha del documento: '.$Fecha),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(150,5,utf8_decode('Condición de pago: '.$CondicionVenta),0,0,'L');
		$pdf->Ln();
		
		//Si es Factura o Tiquete Mostrar Fecha Vencimiento
		if($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')
		{	      
		    $pdf->Cell(150,5,utf8_decode('Fecha de vencimiento: '.$FechaVencimiento.' Plazo: '.$Plazo.' días'),0,0,'L');
			$pdf->Ln();
		}
		else //Si NO es Factura o Tiquete Mostrar Clave DOC Afectado
		{
			$pdf->Cell(150,5,utf8_decode('Afecta a Factura/Nota: '.$ClaveDocAfectado),0,0,'L');
			$pdf->Ln();
		}

		$pdf->Cell(150,5,utf8_decode('Num/Fecha de pedido: '.$NoOrden),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(150,5,utf8_decode('Número de referencia: '.$ID),0,0,'L');
		$pdf->Ln();	
		
		$pdf->Cell(150,5,utf8_decode('Código del vendedor: 00. '.$Moneda_TipoCambio),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(195,3,utf8_decode(''),0,0,'L');
		$pdf->Ln();

		$pdf->SetFont('Arial','B',10);

		$pdf->Cell(195,3,utf8_decode(''),'T',0,'L');
		$pdf->Ln();

		$pdf->Cell(195,5,utf8_decode('Cliente: '.$NombreCliente),0,0,'L');
		$pdf->Ln();

		$pdf->SetFont('Arial','',10);

		$pdf->Cell(150,5,utf8_decode('N° de Identificación: '.$FK_Cliente),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(150,5,utf8_decode('Teléfono: '.$Telefono),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(150,5,utf8_decode('Email: '.$Email),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(150,5,utf8_decode('Dirección: '.$Direccion),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(195,3,utf8_decode(''),0,0,'L');
		$pdf->Ln();

		$pdf->SetFont('Arial','B',8);

		$pdf->Cell(195,3,utf8_decode(''),'T',0,'L');
		$pdf->Ln();

		//Detalle de Factura

		$pdf->Cell(20,5,utf8_decode('Código'),0,0,'L',0);
		$pdf->Cell(60,5,utf8_decode('Descripción'),0,0,'L',0);
		$pdf->Cell(20,5,utf8_decode('Cantidad'),0,0,'L',0);
		$pdf->Cell(30,5,utf8_decode('Precio Unidad'),0,0,'L',0);
		$pdf->Cell(25,5,utf8_decode('% IVA'),0,0,'L',0);
		$pdf->Cell(20,5,utf8_decode('Impuesto'),0,0,'L',0);
		$pdf->Cell(30,5,utf8_decode('Total'),0,0,'L',0);

		$pdf->Ln();

		$pdf->SetFont('Arial','',8);

		$pdf->SetWidths(array(20,60,20,30,20,25,30));
		
		//Consultar para obtener el detalle de Notas o FT_INTERNAL
		
		$FinSQL=
		($TipoDoc=='Factura'||$TipoDoc=='Tiquete')
						?"FROM detallefactura WHERE FK_Factura=$IDDocumento AND FK_FK_Usuario=$UsuarioID ORDER BY NoLinea;"
						:"FROM detallenota WHERE FK_Nota=$IDDocumento ORDER BY NoLinea;";
		
		$sql="SELECT 

	            IDDetalle,
	            NombreProducto,
	            Cantidad,
	            Medida,
	            Bonificacion,
	            PrecioVenta,
	            PrecioCosto,
	            Impuesto,
	            Descuento,
	            TotalNeto,
	            NoLinea,
	            FK_Producto ".$FinSQL;

		  $result = $Conexion->query($sql);
	      
	      while($ri =  mysqli_fetch_array($result))
	      {
	      		 $IDDetalle= $ri['IDDetalle'];
		         $IDProducto= $ri['FK_Producto'];
		         $NombreProducto= $ri['NombreProducto'];
		         $Cantidad= $ri['Cantidad'];
		         $PrecioVentaSinIV= $ri['PrecioVenta'];
		         $UnidadMedida= $ri['Medida'];
		         $ImpuestoVenta= $ri['Impuesto'];
		         $DescuentoL= $ri['Descuento'];
		         $PrecioCosto= $ri['PrecioCosto'];
		         $Bonificacion= $ri['Bonificacion'];
		         $TotalNeto= $ri['TotalNeto'];
		         $NoLinea= $ri['NoLinea'];
				 
				 //Camlcular Monto IVA
				 
				 $PrecioTotalBruto=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);//para hacer todos los calculos
				 
				 $Subtotal=bcdiv(($Subtotal+$PrecioTotalBruto),1,2);
				
				 $MontoIV=bcdiv(((($PrecioTotalBruto)-(($PrecioTotalBruto)*($DescuentoL/100)))*($ImpuestoVenta/100)),1,2); 
				 
		
				$pdf->Row(array(utf8_decode($IDProducto),
								utf8_decode($NombreProducto),
								utf8_decode($Cantidad),
								utf8_decode($Simbolo.$PrecioVentaSinIV),
								utf8_decode($ImpuestoVenta),
								utf8_decode($Simbolo.$MontoIV),
								utf8_decode($Simbolo.$TotalNeto)));	
		}

		$pdf->Cell(195,3,utf8_decode(''),0,0,'L');
		$pdf->Ln();

		$pdf->Cell(195,3,utf8_decode(''),'T',0,'L');
		$pdf->Ln();

		//totales
		$pdf->Cell(150,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','B',10);

		$pdf->Cell(15,5,'Subtotal',0,0,'R',0);
		$pdf->Cell(8,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','',10);

		$pdf->Cell(30,5,utf8_decode($Simbolo.$Subtotal),0,0,'L',0);

		$pdf->Ln();

		//-------------------------------------------------------

		$pdf->Cell(150,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','B',10);

		$pdf->Cell(15,5,'Descuento',0,0,'R',0);
		$pdf->Cell(8,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','',10);

		$pdf->Cell(30,5,utf8_decode($Simbolo.$Descuento),0,0,'L',0);

		$pdf->Ln();
		//------------------------------------------------------------

		$pdf->Cell(150,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','B',10);

		$pdf->Cell(15,5,'Impuestos IVA',0,0,'R',0);
		$pdf->Cell(8,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','',10);

		$pdf->Cell(30,5,utf8_decode($Simbolo.$Impuesto),0,0,'L',0);

		$pdf->Ln();
		//------------------------------------------------------------
		  
		$pdf->Cell(150,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','B',10);

		$pdf->Cell(15,5,'Total',0,0,'R',0);
		$pdf->Cell(8,5,' ',0,0,'L',0);

		$pdf->SetFont('Arial','',10);

		$pdf->Cell(30,5,utf8_decode($Simbolo.$Total),0,0,'L',0);

		$pdf->Ln();
		//------------------------------------------------------------

		$pdf->Ln();

		$pdf->MultiCell(195,5,utf8_decode(NumeroALetras::convertir($Total, $NombreMoneda, 'CÉNTIMOS')),0,'L');

		$pdf->Ln();
		$pdf->Ln();

		$pdf->MultiCell(195,5,'Notas: '.$Razon,0,'L');

		$pdf->Ln();

		$pdf->Cell(15,5,utf8_decode('Autorizada mediante resolución DGT-R-033-2019, publicada en La Gaceta, Alcance Nº147 del 27 de junio de 2019'),0,0,'L',0);
		
		$pdf->Ln();
		$pdf->Ln();
		
		$pdf->SetFont('Arial','B',10);
		
		$pdf->Cell(15,5,utf8_decode('Versión del Documento Electrónico: 4.3'),0,0,'L',0);	
		
		/**
		* 
		* @si el cliente es exonerado, agregar pagina nueva con la info de la exoneracion
		* 
		*/
		
		if($Exonerado=='1')
		{
			$PorcentajeExoneracionTP=ObtenerPorcentajeGlobarExoneracion($FK_Cliente);
			$InfoExoneracion=ObtenerInfoExoneracion($FK_Cliente);
	  		    
		    $TipoDocumentoExo=$InfoExoneracion[0]["TipoDocumento"];
			$NumeroDocumentoExo=$InfoExoneracion[0]["NumeroDocumentoExo"];
			$NombreInstitucionExo=$InfoExoneracion[0]["NombreInstitucionExo"];
			$FechaEmisionExo=$InfoExoneracion[0]["FechaEmisionExo"];
			
			
			//$pdf=new PDF_MC_Table('P','mm','Letter');
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',12);

			$pdf->SetTextColor(255,255,255);  // Establece el color del texto (en este caso es blanco) 
			$pdf->SetFillColor(0, 0, 0); // establece el color del fondo de la celda (en este caso es AZUL 
			
			if($TipoDoc=='Factura')
			{	      
			    $pdf->Cell(195,10,utf8_decode('FACTURA ELECTRÓNICA'),1,0,'C',TRUE);
			}
			else if($TipoDoc=='Tiquete' AND ($NoDoc=='' OR $Clave==''))//es una proforma
			{	      
			    $pdf->Cell(195,10,utf8_decode('FACTURA PROFORMA'),1,0,'C',TRUE);
			}
			else if($TipoDoc=='Tiquete' AND ($NoDoc!='' OR $Clave!=''))
			{	      
			    $pdf->Cell(195,10,utf8_decode('TIQUETE ELECTRÓNICO'),1,0,'C',TRUE);
			}
			else if($TipoDoc=='Nota Credito')
			{	      
			    $pdf->Cell(195,10,utf8_decode('NOTA DE CRÉDITO'),1,0,'C',TRUE);
			}
			else if($TipoDoc=='Nota Debito')
			{	      
			    $pdf->Cell(195,10,utf8_decode('NOTA DE DÉBITO'),1,0,'C',TRUE);
			}
			
			 //$pdf->Cell(195,10,utf8_decode('FACTURA ELECTRÓNICA'),1,0,'C',TRUE);
			 // en orden lo que informan estos parametros es: 
			// ancho 145 
			//alto 20 
			// texto dentro de la celda = LETRERO 
			// ¿Borde?  ==> Si, 
			// ´0´ 
			// C ==> texto centrado 
			// True == > Inidica que el fondo de la celda SI debe dibujarse.   
			//Por default es ´false´ (en tu caso es ´false´ porque tu lo omitiste)  
			//y por lo tanto el fondo de tu celda NO SE ESTA DIBUJANDO.   
			//CAMBIA ESTE VALOR.
			$pdf->SetFont('Arial','',10);
			$pdf->SetTextColor(0,0,0);
			 $pdf->Ln();
			 $pdf->Ln();

			if(!empty($Logo))
			{
				$scale = 0.20;    
			
				list($originalWidth, $originalHeight) = getimagesize($Logo);//debe ser de 140px de alto maximo
			
				$pdf->Image($Logo,10,25,$originalWidth * $scale, $originalHeight * $scale);	
			}

			$pdf->Cell(75,5,' ',0,0,'L',0);   // LTR:LeftTopRigth bordes solo de arriba y lados, 0 no bordes, 1 todos los bordes
			$pdf->SetFont('Arial','B',10);
			
			if($TipoDoc=='Tiquete')
			{
				$pdf->Cell(100,5,utf8_decode($NombreComEmisor),0,0,'L',0);	
			}
			else
			{
				$pdf->Cell(100,5,utf8_decode($NombreRepEmisor),0,0,'L',0);
			}

			$pdf->Ln();
			$pdf->SetFont('Arial','',10);

			$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
			$pdf->Cell(100,5,utf8_decode('Cédula (f/j): '.$CedulaEmisor),0,0,'L',0);
			$pdf->Ln();

			$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
			$pdf->Cell(100,5,utf8_decode('Teléfono: '.$TelefonoEmisor),0,0,'L',0);
			$pdf->Ln();

			$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
			//$pdf->Cell(100,5,utf8_decode('DIRECCION:'),0,0,'L',0);
			$pdf->MultiCell(100,5,utf8_decode($DireccionEmisor),0,'L');
			//$pdf->Ln();

			$pdf->Cell(75,5,'',0,0,'C',0);  // cell with left and right borders
			$pdf->Cell(100,5,utf8_decode('Correo: '.$EmailEmisor),0,0,'L',0);
			$pdf->Ln();

			$pdf->SetFont('Arial','B',10);

			if($TipoDoc=='Factura')
			{	      
			    $pdf->Cell(150,5,utf8_decode('FACTURA ELECTRÓNICA: '.$NoDoc),0,0,'L');
				$pdf->Ln();
			}
			else if($TipoDoc=='Tiquete' AND ($NoDoc!='' OR $Clave!=''))
			{	      
			    $pdf->Cell(150,5,utf8_decode('TIQUETE ELECTRÓNICO: '.$NoDoc),0,0,'L');
				$pdf->Ln();
			}
			else if($TipoDoc=='Tiquete' AND ($NoDoc=='' OR $Clave==''))//es proforma
			{	      
			    $pdf->Cell(150,5,utf8_decode('FACTURA PROFORMA'),0,0,'L');
				$pdf->Ln();
			}
			else if($TipoDoc=='Nota Credito')
			{	      
			    $pdf->Cell(150,5,utf8_decode('NOTA DE CRÉDITO: '.$NoDoc),0,0,'L');
				$pdf->Ln();
			}
			else if($TipoDoc=='Nota Debito')
			{	      
			    $pdf->Cell(150,5,utf8_decode('NOTA DE DÉBITO: '.$NoDoc),0,0,'L');
				$pdf->Ln();
			}

			$pdf->Cell(150,5,utf8_decode('Número Clave: '.$Clave),0,0,'L');
			$pdf->Ln();

			$pdf->SetFont('Arial','',10);

			$pdf->Cell(150,5,utf8_decode('Fecha del documento: '.$Fecha),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Condición de pago: '.$CondicionVenta),0,0,'L');
			$pdf->Ln();
			
			//Si es Factura o Tiquete Mostrar Fecha Vencimiento
			if($TipoDoc=='Factura'|| $TipoDoc=='Tiquete')
			{	      
			    $pdf->Cell(150,5,utf8_decode('Fecha de vencimiento: '.$FechaVencimiento.' Plazo: '.$Plazo.' días'),0,0,'L');
				$pdf->Ln();
			}
			else //Si NO es Factura o Tiquete Mostrar Clave DOC Afectado
			{
				$pdf->Cell(150,5,utf8_decode('Afecta a Factura/Nota: '.$ClaveDocAfectado),0,0,'L');
				$pdf->Ln();
			}

			$pdf->Cell(150,5,utf8_decode('Num/Fecha de pedido: '.$NoOrden),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Número de referencia: '.$ID),0,0,'L');
			$pdf->Ln();	
			
			$pdf->Cell(150,5,utf8_decode('Código del vendedor: 00. '.$Moneda_TipoCambio),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(195,3,utf8_decode(''),0,0,'L');
			$pdf->Ln();

			$pdf->SetFont('Arial','B',10);

			$pdf->Cell(195,3,utf8_decode(''),'T',0,'L');
			$pdf->Ln();

			$pdf->Cell(195,5,utf8_decode('Cliente: '.$NombreCliente),0,0,'L');
			$pdf->Ln();

			$pdf->SetFont('Arial','',10);

			$pdf->Cell(150,5,utf8_decode('N° de Identificación: '.$FK_Cliente),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Teléfono: '.$Telefono),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Email: '.$Email),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Dirección: '.$Direccion),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(195,3,utf8_decode(''),0,0,'L');
			$pdf->Ln();

			$pdf->SetFont('Arial','B',8);

			$pdf->Cell(195,3,utf8_decode(''),'T',0,'L');
			
			/**
			* **************************************************
			*/
			
			$pdf->SetFont('Arial','',10);

			$pdf->Cell(195,3,utf8_decode(''),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(195,5,utf8_decode('Este documento está exonerado con base en el documento tipo '.$TipoDocumentoExo),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Número exoneración o autorizacion: '.$NumeroDocumentoExo),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Con fecha de emisión del día '.$FechaEmisionExo),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Emitido por '.$NombreInstitucionExo),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(150,5,utf8_decode('Por un porcentaje del '.$PorcentajeExoneracionTP.'%. Siendo el total de lo exonerado por un monto de '.$Simbolo.$Total),0,0,'L');
			$pdf->Ln();

			$pdf->Cell(195,3,utf8_decode(''),0,0,'L');
			$pdf->Ln();

			$pdf->SetFont('Arial','B',8);

			$pdf->Cell(195,3,utf8_decode(''),'T',0,'L');
			$pdf->Ln();
		}
		
		
		$pdf->Output($Clave.'.pdf','I');
	}
}

function ObtenerFechaVencimiento($Fecha,$DiasAAgregar)
	{
		date_default_timezone_set("America/Costa_Rica");

		$date = date('d-m-Y');
		return date('d-m-Y', strtotime($Fecha. " + $DiasAAgregar days"));	
	}

function SelectSQLEncabezado($TipoDoc,$IDDocumento,$CedulaCliente,$IDUsuario)
{
	$sql='';
	
	if($TipoDoc=='Factura' || $TipoDoc=='Tiquete')
	{
		$sql = "SELECT  F.IDFactura,
						F.FK_Usuario,
						F.NoFactura,
						F.Clave,

						/*Datos del CLiente*/

						F.FK_Cliente,
						F.NombreCliente,

						C.Telefono,
						Concat(F.EmailCliente,'/',C.Email2) as Email,
						C.Direccion,
						C.Zona,
						C.Exonerado,

						/*Datos del UsuarioEmisor*/

						U.NombreRepresentante as NombreU,
						U.NombreComercial as NombreCU,
						U.Cedula as CedulaU,
						Concat(U.Telefono1,'/',U.TeleFono2) as TelefonoU,
						U.Direccion as DireccionU,
						U.Email as EmailU,
						U.Logo,

						DATE_FORMAT(F.Fecha,'%h:%i:%s %d-%m-%Y') as Fecha,
						F.Plazo,
						F.MedioPago,

						CASE F.CondicionVenta 
									WHEN '01'      THEN 'Contado'
								   	WHEN '02'      THEN 'Crédito'
								   	WHEN '04'      THEN 'Apartado'
								       END AS CondicionVenta,
						F.Status,
						F.Clave,
						F.NoOrden,
						F.TipoMoneda,
						F.TipoCambio,
						F.TipoDocumento,
						F.MontoGravado,
						F.MontoExento,
						F.Descuento,
						F.Impuesto,
						F.OtroImpuesto,
						F.ServicioGravado,
						F.ServicioExento,
						F.SubtotalFactura,
						F.TotalFactura,
						F.Saldo,
						F.Terminal,
						F.Sucursal
						FROM factura F 
						INNER JOIN cliente C ON F.FK_Cliente=C.Cedula 
						INNER JOIN usuario U ON F.FK_Usuario=U.IDUsuario
						WHERE F.IDFactura=$IDDocumento AND F.FK_Cliente='$CedulaCliente' AND F.FK_Usuario=$IDUsuario;";
	}
	else // nota
	{
		$sql = "SELECT 

					N.IDNota,
					N.FK_Usuario,
					N.NoNota,
					N.Clave,

					/*Datos del CLiente*/

					N.FK_Cliente,
					N.NombreCliente,

					C.Telefono,
					Concat(N.EmailCliente,'/',C.Email2) as Email,
					C.Direccion,
					C.Zona,
					C.Exonerado,

					/*Datos del UsuarioEmisor*/

					U.NombreRepresentante as NombreU,
					U.Cedula as CedulaU,
					Concat(U.Telefono1,'/',U.TeleFono2) as TelefonoU,
					U.Direccion as DireccionU,
					U.Email as EmailU,
					U.Logo,

					DATE_FORMAT(N.Fecha,'%h:%i:%s %d-%m-%Y') as Fecha,

					N.NoFactura,
					N.NoReferencia,
					N.Razon,
					N.Plazo,
					N.MedioPago,
					
					CASE N.CondicionVenta 
									WHEN '01'      THEN 'Contado'
								   	WHEN '02'      THEN 'Crédito'
								   	WHEN '04'      THEN 'Apartado'
								       END AS CondicionVenta,
					N.Status,
					N.Clave,
					N.NoOrden,
					N.TipoMoneda,
					N.TipoCambio,
					N.TipoDocumento,
					N.TipoDocAfectado,
					N.MontoGravado,
					N.MontoExento,
					N.Descuento,
					N.Impuesto,
					N.OtroImpuesto,
					N.ServicioGravado,
					N.ServicioExento,
					N.SubtotalNota,
					N.TotalNota,
					N.Terminal,
					N.Sucursal
					FROM notacreditodebito N 
					INNER JOIN cliente C ON N.FK_Cliente=C.Cedula 
					INNER JOIN usuario U ON N.FK_Usuario=U.IDUsuario 
					WHERE N.IDNota=$IDDocumento AND N.FK_Cliente='$CedulaCliente' AND N.FK_Usuario=$IDUsuario;";
	}
	
	return $sql;
}

function ObtenerInfoExoneracion($CedulaCliente)
{
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$InfoExoneracion= array();
	
	$sql="SELECT TipoExoneracion,NoExoneracion,NombreInstitucionExoneracion,
	  				 DATE_FORMAT(FechaExoneracion,'%Y-%m-%dT%H:%i:%s%-06:00') AS FechaExoneracion FROM cliente
	  				 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";
	  				 
	  	$result=$Conexion->query($sql);

	    $row = $result->fetch_assoc();
	    
	    $TipoDocumentoExo=$row["TipoExoneracion"];
		$NumeroDocumentoExo=$row["NoExoneracion"];
		$NombreInstitucionExo=$row["NombreInstitucionExoneracion"];
		$FechaEmisionExo=$row["FechaExoneracion"];	
		
		$InfoExoneracion[]=array(
								"TipoDocumento"=>$TipoDocumentoExo,
								"NumeroDocumentoExo"=>$NumeroDocumentoExo,
								"NombreInstitucionExo"=>$NombreInstitucionExo,
								"FechaEmisionExo"=>$FechaEmisionExo
								);
		return $InfoExoneracion;	
}

function ObtenerPorcentajeGlobarExoneracion($CedulaCliente)
{
	//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
    
    $PorcentajeExoneracionTP='';

    $sql="SELECT
		 (
		 CASE 
		     WHEN TodosProductosExonerados=1 THEN PorcentajeExoneracion
		     ELSE 'NO'
		 END
		 ) AS PorcentajeExoneracion
		 FROM cliente
		 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";
    
    $result=$Conexion->query($sql);

    $row = $result->fetch_assoc();
    $PorcentajeExoneracionTP=$row["PorcentajeExoneracion"];	
    
    return $PorcentajeExoneracionTP;
}

?>
