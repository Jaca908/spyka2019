<?php
session_start();

require ("../Conexion/Conexion.php");

$request = $_POST['request'];   // request

// Get username list
if($request == 1){
    $search = $_POST['search'];

    $query = "SELECT * FROM producto WHERE IDProducto like '%".$search."%' AND Activo=1 and FK_Usuario=".$_SESSION['IDUsuario']." and IDProducto NOT IN('000000000000000000')"; 
    
    $result = mysqli_query($con,$query);
    
    while($row = mysqli_fetch_array($result)){
        $response[] = array("value"=>$row['IDProducto'],"label"=>$row['NombreProducto']);
    }

    // encoding array to json format
    echo json_encode($response);
    exit;
}

// Get details
if($request == 2){
    $pid = $_POST['IDp'];
    $TipoCambio= $_POST['TipoCambio'];
    
    $sql = "SELECT *,
    			   CAST((PrecioVenta/$TipoCambio) AS DECIMAL(14,2)) AS PrecioVentaFinal, 
				   CAST(((PrecioVenta/$TipoCambio)/(1+Impuesto/100)/1) AS DECIMAL(14,2)) AS PrecioVentaSinIV
    			   FROM producto WHERE IDProducto like '%".$pid."%' AND Activo=1 and FK_Usuario=".$_SESSION['IDUsuario']." AND IDProducto NOT IN('000000000000000000')";

    $result = mysqli_query($con,$sql);

    $users_arr = array();

    while( $row = mysqli_fetch_array($result)){
        $IDProducto = $row['IDProducto'];
        $NombreProducto = $row['NombreProducto'];
        $Impuesto = $row['Impuesto'];
        $Descuento = $row['Descuento'];
		$UM=$row['UnidadMedida'];
        $SaldoActual=$row['SaldoActual'];
        $PrecioCosto=$row['PrecioCosto'];

        $PrecioVenta = $row['PrecioVenta'];
        
        $PrecioVentaFinal=$row['PrecioVentaFinal'];
        
        $PrecioVentaSinIV=$row['PrecioVentaSinIV'];
        
        //$PrecioVentaFinal=bcdiv(round(($PrecioVenta/$TipoCambio),2),1,2);
        
       // $PrecioVentaSinIV=bcdiv(($PrecioVentaFinal/(1+($Impuesto/100))),1,2);
        
        $users_arr[] = array("IDProducto" => $IDProducto, "NombreProducto" => $NombreProducto,"PrecioVentaSinIV" => $PrecioVentaSinIV, "Impuesto" =>$Impuesto, "Descuento" =>$Descuento, "UM"=>$UM, "SaldoActual"=>$SaldoActual, "PrecioCosto"=>$PrecioCosto);
    }

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
}
?>