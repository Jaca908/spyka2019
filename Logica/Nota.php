<?php 
session_start();
 
require ("../Conexion/Conexion.php");

//Verificar que el usuario haya pulsado el boton de agregar producto
if(isset($_POST['btnAgregarProducto'])) 
{
	AgregarProducto();
}
else if(isset($_POST['btnFactura']))
{
    GrabarNota();
}
else if(isset($_POST['MostrarDatos']))
{
    ConsultarNota();
}
else if(isset($_POST['btnBorrar']))
{
	BorrarNota($_POST['IDDocumento'],$_POST['TipoDocumento']);	
}
else if(isset($_POST['btnEnviarAHacienda']))
{
	EnviarAHacienda();
}
else if(isset($_POST['btnReenviarEmail']))
{
	ReenviarEmail();
}
else if(isset($_POST['btnConsultarEstado']))
{
	ConsultarEstado();
}
else if(isset($_POST['CargarNoDocumento']))
{
    $TipoDocumento=$_POST['TipoDocumento'];

    $NoConsecutivo= ObtenerConsecutivo($TipoDocumento);

        $users_arr[] = array( 
                         "NoConsecutivo"=>$NoConsecutivo,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}



function ObtenerConsecutivo($TipoDocumento)
{
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $sql="SELECT
          LPAD(CAST(".$TipoDocumento." AS CHAR(10)), 10, '0') as NoConsecutivo
          FROM consecutivo
          WHERE FK_Usuario=".$_SESSION['IDUsuario'].";";

    $NoConsecutivo="";
    
    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $NoConsecutivo=$row["NoConsecutivo"];
    }

    return $NoConsecutivo;
}

function ObtenerTipoDocumentoNumerico($TipoDocumento)

/**
* 
* @var /REvisar cuabdo es nota credito o debito si esta pegado o no: sino lo esta
* 
*/

{
    if($TipoDocumento=='Factura')
   {
        $TipoDocumento='01';    
   }
   else if($TipoDocumento=='NotaDebito' || $TipoDocumento=='Nota Debito')
   {
        $TipoDocumento='02';
   }
   else if($TipoDocumento=='NotaCredito' || $TipoDocumento=='Nota Credito')
   {
        $TipoDocumento='03';
   }
   else if($TipoDocumento=='Tiquete')
   {
        $TipoDocumento='04';
   }

   return $TipoDocumento;
}

function ObtenerNombreTipoDocumento($TipoDocumento)
{
    if($TipoDocumento=='01')
   {
        $TipoDocumento='Factura';    
   }
   else if($TipoDocumento=='02')
   {
        $TipoDocumento='NotaDebito';
   }
   else if($TipoDocumento=='03')
   {
        $TipoDocumento='NotaCredito';
   }
   else if($TipoDocumento=='04')
   {
        $TipoDocumento='Tiquete';
   }

   return $TipoDocumento;
}

function AgregarProducto()
{
  	$Linea=$_POST['Linea'];
	$CantVendida=$_POST['CantVendida'];
	$CodProducto=$_POST['CodProducto'];
	$CantADev=$_POST['CantADev'];
	$CantidadDevAnt=($_POST['CantidadDevAnt']=='')?'0.00':$_POST['CantidadDevAnt'];
	$Descripcion=$_POST['Descripcion'];
	$PUSinIV=QuitarFormatoNumero($_POST['PUSinIV']);
	$PorDesc=$_POST['PorDesc'];
	$IV=$_POST['IV'];
	$CantidadDevuelta=$_POST['CantidadDevuelta'];
	$UM=$_POST['UM'];
	$Bonificacion=$_POST['Bonificacion'];
	$PrecioCosto=$_POST['PreComp'];
	
	$ServicioExoneradoL=$_POST['ServicioExoneradoL'];// hay que traer el porcentaje de exoneracion y aplicarlo al servicio de la linea
    $ImpuestoDevueltoL=$_POST['ImpuestoDevueltoL'];//no puede ser con el monto de linea porque puede que no este devolv todo
    $MontoExoneradoL=$_POST['MontoExoneradoL'];//no puede ser con el monto de linea porque puede que no este devolv todo
    
    $ClienteExonerado=$_POST['ClienteExonerado'];//0 no exonerado, 1 exonerado
    $CedulaCliente=$_POST['CedulaCliente'];
                
	$PrecioTotalBrutoADev=bcdiv(($PUSinIV*$CantADev),1,2);
	$PrecioTotalBrutoDevAnt=bcdiv(($PUSinIV*$CantidadDevAnt),1,2);

    /*Campos de Calculos de Factura*/
    
    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
    $MontoExoF=QuitarFormatoNumero($_POST['MontExoF']);
    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
    $MontoSExoF=QuitarFormatoNumero($_POST['MontSExoF']);
    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
    $MontoIVDF=QuitarFormatoNumero($_POST['MontIVDF']);
    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
    $TotF=QuitarFormatoNumero($_POST['MontTF']);

	$MontoSExoneracion=0;
    $MontoExoneracion=0;
    $MontoSExonerado=0;
    $MontoExonerado=0;
    
    if($CantidadDevAnt=='0.00')
    {
			/*Variables para calculos*/
		    $MontoExento=0;
		    $MontoGravado=0;
		    $MontoSExento=0;
		    $MontoSGravado=0;
		    $MontoIV=0;
		    $MontoOtroI=0;
		    $MontoDescuento=0;
		    $SubtotalL=0;
		    $TotalL=0;


		    if(($UM!='sp' AND $UM!='h'AND $UM!='d') AND $IV==0) /*Sino Tiene Impuesto*/
		    {
		      $MontoExento=bcdiv(($PrecioTotalBrutoADev),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
		      $MontoGravado=0; 
		  
		    }
		    else if(($UM!='sp' AND $UM!='h'AND $UM!='d') AND $IV!=0 AND $ClienteExonerado!='1') /*Si Tiene Impuesto*/ 
		    {
		      $MontoExento=0;
		      $MontoGravado=bcdiv(($PrecioTotalBrutoADev),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
		    }
		    else
		    {
			  $MontoExento=0;
			  $MontoGravado=0;	
			}
		    
		    if(($UM=='sp'||$UM=='h'||$UM=='d') AND $IV==0)
		    {
		      $MontoSExento=bcdiv(($PrecioTotalBrutoADev),1,2); /*Precio sin impuesto x Cantidad*/
		      $MontoSGravado=0;
		    }
		    else if(($UM=='sp'||$UM=='h'||$UM=='d') AND $IV!=0 AND $ClienteExonerado!='1')
		  {
		    $MontoSExento=0;/*Precio con impuesto x Cantidad*/
		      $MontoSGravado=bcdiv(($PrecioTotalBrutoADev),1,2);
		  }
		  else
		  {
		    $MontoSExento=0;
		    $MontoSGravado=0;
		  }     

		    $MontoIV=bcdiv(((($PrecioTotalBrutoADev)-(($PrecioTotalBrutoADev)*($PorDesc/100)))*($IV/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
		    $MontoOtroI=0;/*se va a cambiar mas adelante*/
		    $MontoDescuento=bcdiv((($PrecioTotalBrutoADev)*($PorDesc/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


		    if($ClienteExonerado=='1')//es Exonerado
		    {
				//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
				$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

			    $sql="SELECT
					 (
					 CASE 
					     WHEN TodosProductosExonerados=1 THEN PorcentajeExoneracion
					     ELSE 'NO'
					 END
					 ) AS PorcentajeExoneracion
					 FROM cliente
					 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";

			    $Resutado="";
			    
			    $result=$Conexion->query($sql);

		        $row = $result->fetch_assoc();
		        $PorcentajeExoneracionTP=$row["PorcentajeExoneracion"];
		        
		        if($PorcentajeExoneracionTP!='NO')
				{					
					if(($UM!='sp'AND $UM!='d'AND $UM!='h'))
		    		{
						$MontoExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
						
						$MontoExonerado=(bcdiv(($PrecioTotalBrutoADev),1,2));
						
						$SubtotalL=bcdiv(($PrecioTotalBrutoADev)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
			    		
			    		$MontoIV=bcdiv(($MontoIV-$MontoExoneracion),1,2);
					}
					else if(($UM=='sp'OR $UM=='d'OR $UM=='h'))
		    		{
						$MontoSExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
						
						$MontoSExonerado=(bcdiv(($PrecioTotalBrutoADev),1,2));
						
						$SubtotalL=bcdiv(($PrecioTotalBrutoADev)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoSExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
			    		
			    		$MontoIV=bcdiv(($MontoIV-$MontoSExoneracion),1,2);
					}
				}
				else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
				{
					
				}
			}
			else //No es Exonerado
			{
				$SubtotalL=bcdiv(($PrecioTotalBrutoADev)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
		    	$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
			}


		    /*Hacer Calculos de calculos de Campos*/

		    $MontoExF=bcdiv(($MontoExF+$MontoExento),1,2);
		    $MontoGrF=bcdiv(($MontoGrF+$MontoGravado),1,2);
		    $MontoExoF=bcdiv(($MontoExoF+$MontoExonerado),1,2);
		    $MontoSExF=bcdiv(($MontoSExF+$MontoSExento),1,2);
		    $MontoSGrF=bcdiv(($MontoSGrF+$MontoSGravado),1,2);
		    $MontoSExoF=bcdiv(($MontoSExoF+$MontoSExonerado),1,2);
		    $MontoIVF=bcdiv(($MontoIVF+$MontoIV),1,2);
		    $MontoIVDF=bcdiv(($MontoIVDF+$ImpuestoDevueltoL),1,2);
		    $MontoOtroIF=bcdiv(($MontoOtroIF+$MontoOtroI),1,2);
		    $MontoDescF=bcdiv(($MontoDescF+$MontoDescuento),1,2);
		    $SubF=bcdiv(($SubF+$SubtotalL),1,2);
		    $TotF=bcdiv(($TotF+$TotalL),1,2);	
	}
	else //con cantidad devuelta anterior para restar
	{
			/*Variables para calculos*/
		    $MontoExento=0;
		    $MontoGravado=0;
		    $MontoSExento=0;
		    $MontoSGravado=0;
		    $MontoIV=0;
		    $MontoOtroI=0;
		    $MontoDescuento=0;
		    $SubtotalL=0;
		    $TotalL=0;


		    if(($UM!='sp' AND $UM!='h'AND $UM!='d') AND $IV==0) /*Sino Tiene Impuesto*/
		    {
		      $MontoExento=bcdiv(($PrecioTotalBrutoDevAnt),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
		      $MontoGravado=0; 
		  
		    }
		    else if(($UM!='sp' AND $UM!='h'AND $UM!='d') AND $IV!=0 AND $ClienteExonerado!='1') /*Si Tiene Impuesto*/ 
		    {
		      $MontoExento=0;
		      $MontoGravado=bcdiv(($PrecioTotalBrutoDevAnt),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
		    }
		    else
		    {
			  $MontoExento=0;
			  $MontoGravado=0;	
			}
		    
		    if(($UM=='sp'||$UM=='h'||$UM=='d') AND $IV==0)
		    {
		      $MontoSExento=bcdiv(($PrecioTotalBrutoDevAnt),1,2); /*Precio sin impuesto x Cantidad*/
		      $MontoSGravado=0;
		    }
		    else if(($UM=='sp'||$UM=='h'||$UM=='d') AND $IV!=0 AND $ClienteExonerado!='1')
		  {
		    $MontoSExento=0;/*Precio con impuesto x Cantidad*/
		      $MontoSGravado=bcdiv(($PrecioTotalBrutoDevAnt),1,2);
		  }
		  else
		  {
		    $MontoSExento=0;
		    $MontoSGravado=0;
		  }     

		    $MontoIV=bcdiv(((($PrecioTotalBrutoDevAnt)-(($PrecioTotalBrutoDevAnt)*($PorDesc/100)))*($IV/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
		    $MontoOtroI=0;/*se va a cambiar mas adelante*/
		    $MontoDescuento=bcdiv((($PrecioTotalBrutoDevAnt)*($PorDesc/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


		    if($ClienteExonerado=='1')//es Exonerado
			{
				//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
				$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

			    $sql="SELECT
					 (
					 CASE 
					     WHEN TodosProductosExonerados=1 THEN PorcentajeExoneracion
					     ELSE 'NO'
					 END
					 ) AS PorcentajeExoneracion
					 FROM cliente
					 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";

			    $Resutado="";
			    
			    $result=$Conexion->query($sql);

			    $row = $result->fetch_assoc();
			    $PorcentajeExoneracionTP=$row["PorcentajeExoneracion"];
			    
			    if($PorcentajeExoneracionTP!='NO')
				{					
					if(($UM!='sp'AND $UM!='d'AND $UM!='h'))
		    		{
						$MontoExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
						
						$MontoExonerado=(bcdiv(($PrecioTotalBrutoDevAnt),1,2));
						
						$SubtotalL=bcdiv(($PrecioTotalBrutoDevAnt)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
			    		
			    		$MontoIV=bcdiv(($MontoIV-$MontoExoneracion),1,2);
					}
					else if(($UM=='sp'OR $UM=='d'OR $UM=='h'))
		    		{
						$MontoSExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
						
						$MontoSExonerado=(bcdiv(($PrecioTotalBrutoDevAnt),1,2));
						
						$SubtotalL=bcdiv(($PrecioTotalBrutoDevAnt)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoSExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
			    		
			    		$MontoIV=bcdiv(($MontoIV-$MontoSExoneracion),1,2);
					}
				}
				else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
				{
					
				}
			}
			else //No es Exonerado
			{
				$SubtotalL=bcdiv(($PrecioTotalBrutoDevAnt)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
				$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
			}
		    
		    /*Hacer Calculos de calculos de Campos*/
		    
		    $MontoExF=bcdiv(($MontoExF-$MontoExento),1,2);
		    $MontoGrF=bcdiv(($MontoGrF-$MontoGravado),1,2);
		    $MontoExoF=bcdiv(($MontoExoF-$MontoExonerado),1,2);
		    $MontoSExF=bcdiv(($MontoSExF-$MontoSExento),1,2);
		    $MontoSGrF=bcdiv(($MontoSGrF-$MontoSGravado),1,2);
		    $MontoSExoF=bcdiv(($MontoSExoF-$MontoSExonerado),1,2);
		    $MontoIVF=bcdiv(($MontoIVF-$MontoIV),1,2);
		    $MontoIVDF=bcdiv(($MontoIVDF-$ImpuestoDevueltoL),1,2);
		    $MontoOtroIF=bcdiv(($MontoOtroIF-$MontoOtroI),1,2);
		    $MontoDescF=bcdiv(($MontoDescF-$MontoDescuento),1,2);
		    $SubF=bcdiv(($SubF-$SubtotalL),1,2);
		    $TotF=bcdiv(($TotF-$TotalL),1,2);
		    
		    
		    //calculo final con resta ya hecha
		    
		    /*Variables para calculos*/
		    $MontoExento=0;
		    $MontoGravado=0;
		    $MontoSExento=0;
		    $MontoSGravado=0;
		    $MontoIV=0;
		    $MontoOtroI=0;
		    $MontoDescuento=0;
		    $SubtotalL=0;
		    $TotalL=0;


		    if(($UM!='sp' AND $UM!='h'AND $UM!='d') AND $IV==0) /*Sino Tiene Impuesto*/
		    {
		      $MontoExento=bcdiv(($PrecioTotalBrutoADev),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
		      $MontoGravado=0; 
		  
		    }
		    else if(($UM!='sp' AND $UM!='h'AND $UM!='d') AND $IV!=0 AND $ClienteExonerado!='1') /*Si Tiene Impuesto*/ 
		    {
		      $MontoExento=0;
		      $MontoGravado=bcdiv(($PrecioTotalBrutoADev),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
		    }
		    else
			{
			  $MontoExento=0;
			  $MontoGravado=0;	
			}
		    
		    if(($UM=='sp'||$UM=='h'||$UM=='d') AND $IV==0)
		    {
		      $MontoSExento=bcdiv(($PrecioTotalBrutoADev),1,2); /*Precio sin impuesto x Cantidad*/
		      $MontoSGravado=0;
		    }
		    else if(($UM=='sp'||$UM=='h'||$UM=='d') AND $IV!=0 AND $ClienteExonerado!='1')
		  {
		    $MontoSExento=0;/*Precio con impuesto x Cantidad*/
		      $MontoSGravado=bcdiv(($PrecioTotalBrutoADev),1,2);
		  }
		  else
		  {
		    $MontoSExento=0;
		    $MontoSGravado=0;
		  }     

		    $MontoIV=bcdiv(((($PrecioTotalBrutoADev)-(($PrecioTotalBrutoADev)*($PorDesc/100)))*($IV/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
		    $MontoOtroI=0;/*se va a cambiar mas adelante*/
		    $MontoDescuento=bcdiv((($PrecioTotalBrutoADev)*($PorDesc/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


		    if($ClienteExonerado=='1')//es Exonerado
			{
				//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
				$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

			    $sql="SELECT
					 (
					 CASE 
					     WHEN TodosProductosExonerados=1 THEN PorcentajeExoneracion
					     ELSE 'NO'
					 END
					 ) AS PorcentajeExoneracion
					 FROM cliente
					 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";

			    $Resutado="";
			    
			    $result=$Conexion->query($sql);

			    $row = $result->fetch_assoc();
			    $PorcentajeExoneracionTP=$row["PorcentajeExoneracion"];
			    
			    if($PorcentajeExoneracionTP!='NO')
				{					
					if(($UM!='sp'AND $UM!='d'AND $UM!='h'))
		    		{
						$MontoExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
						
						$MontoExonerado=(bcdiv(($PrecioTotalBrutoADev),1,2));
						
						$SubtotalL=bcdiv(($PrecioTotalBrutoADev)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
			    		
			    		$MontoIV=bcdiv(($MontoIV-$MontoExoneracion),1,2);
					}
					else if(($UM=='sp'OR $UM=='d'OR $UM=='h'))
		    		{
						$MontoSExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
						
						$MontoSExonerado=(bcdiv(($PrecioTotalBrutoADev),1,2));
						
						$SubtotalL=bcdiv(($PrecioTotalBrutoADev)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoSExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
			    		
			    		$MontoIV=bcdiv(($MontoIV-$MontoSExoneracion),1,2);
					}
				}
				else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
				{
					
				}
			}
			else //No es Exonerado
			{
				$SubtotalL=bcdiv(($PrecioTotalBrutoADev)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
				$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
			}


		    /*Hacer Calculos de calculos de Campos*/
		    
		    $MontoExF=bcdiv(($MontoExF+$MontoExento),1,2);
		    $MontoGrF=bcdiv(($MontoGrF+$MontoGravado),1,2);
		    $MontoExoF=bcdiv(($MontoExoF+$MontoExonerado),1,2);
		    $MontoSExF=bcdiv(($MontoSExF+$MontoSExento),1,2);
		    $MontoSGrF=bcdiv(($MontoSGrF+$MontoSGravado),1,2);
		    $MontoSExoF=bcdiv(($MontoSExoF+$MontoSExonerado),1,2);
		    $MontoIVF=bcdiv(($MontoIVF+$MontoIV),1,2);
		    $MontoIVDF=bcdiv(($MontoIVDF+$ImpuestoDevueltoL),1,2);
		    $MontoOtroIF=bcdiv(($MontoOtroIF+$MontoOtroI),1,2);
		    $MontoDescF=bcdiv(($MontoDescF+$MontoDescuento),1,2);
		    $SubF=bcdiv(($SubF+$SubtotalL),1,2);
		    $TotF=bcdiv(($TotF+$TotalL),1,2);
		    
	}

    /*Pasar variables a vector para ponerlos en los campos de tabla y de calculos en el formulario*/
    $users_arr[] = array("Linea"=> $Linea,
    					 "IDProducto" => $CodProducto, 
                         "NombreProducto" => $Descripcion,
                         "PrecioVSinIV" => number_format($PUSinIV,2), 
                         "Impuesto" =>$IV, 
                         "Descuento" =>$PorDesc, 
                         "Cantidad"=>$CantVendida,
                         "UM"=>$UM,
                         "PreComp"=>$PrecioCosto,
                         "Bonificacion"=>$Bonificacion,
                         "CantidadDevuelta"=>$CantidadDevuelta,
                         "CantidadADevolver"=>($CantADev=='0.00'?"":$CantADev),
                         
                         "MontoE"=>$MontoExento,
                         "MontoG"=>$MontoGravado,
                         "MontoExo"=>$MontoExoneracion,
                         "MontoSE"=>$MontoSExento,
                         "MontoSG"=>$MontoSGravado,
                         "MontoSExo"=>$MontoSExoneracion,
                         "MontoImpV"=>$MontoIV,
                         "MontoImpVDev"=>$ImpuestoDevueltoL,
                         "MontoOImp"=>$MontoOtroI,
                         "MontoDesc"=>$MontoDescuento,
                         "SubtL"=>number_format($SubtotalL,2),
                         "TotL"=>number_format($TotalL,2),

                         /*Pasar al vector las variables de campo de calculos*/
                         "MontoEF"=>number_format($MontoExF,2),
                         "MontoGF"=>number_format($MontoGrF,2),
                         "MontoExoF"=>number_format($MontoExoF,2),
                         "MontoSEF"=>number_format($MontoSExF,2),
                         "MontoSGF"=>number_format($MontoSGrF,2),
                         "MontoSExoF"=>number_format($MontoSExoF,2),
                         "MontoImpVF"=>number_format($MontoIVF,2),
                         "MontoImpVDevF"=>number_format($MontoIVDF,2),
                         "MontoOImpF"=>number_format($MontoOtroIF,2),
                         "MontoDescF"=>number_format($MontoDescF,2),
                         "SubtF"=>number_format($SubF,2),
                         "TotF"=>number_format($TotF,2),
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}


function GrabarNota()
{
	$GuardarModifcarFactura=$_POST['btnFactura'];
	
	$Respuesta=''; //para mostrar errores o msj de exito al guardar
	$GuarMod='';
	$CantFilasDevTot=0;
	
	if($GuardarModifcarFactura=='GrabarNota')
	{
	   $ActividadEconomica=$_POST["ActividadEconomica"]; //si esta nula no muestra el msj
			   
	   $IDNota=($_POST['IDFactura']!="")?$_POST['IDFactura']:0;
	   $FK_Usuario=$_SESSION['IDUsuario'];
	   $FK_Cliente= $_POST['Cedula'];
	   $NombreCliente= $_POST['Nombre'];
	   $EmailCliente=($_POST['EmailCliente']!='NULL')?$_POST['EmailCliente']:'NULL';
	   $NoReferencia=$_POST['NoReferencia'];//NoFactura/NoNotaCredito
	   $NoClaveAfectada=$_POST['NoClave'];
	   $Razon=$_POST['Razon'];
	   $Fecha= DateTime::createFromFormat('d-m-Y H:i:s', $_POST['Fecha'])->format('Y-m-d H:i:s'); //now()
	   $Plazo= $_POST['Plazo']; //si es contado poner plazo como 0
	   $MedioPago= $_POST['MedioPago']; 
	   $CondicionVenta= $_POST['CondicionVenta'];
	   $Status="";/*Se guarda al mandara a hacienda*/

	   $NoOrden= $_POST['NoOrden']; 
	   $TipoMoneda= $_POST['TipoMoneda']; 
	   $TipoCambio= $_POST['TipoCambio'];

	   $TipoDocumento= ObtenerTipoDocumentoNumerico($_POST['TipoDocumento']); /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/
	   
	   if($TipoDocumento=='02')//ND
	   {
	   		$NoReferenciaFactura=ObtenerNoRefFactura($FK_Usuario,$FK_Cliente,$NoReferencia);//para obtener NoRefFactura
	   
	   	 	//sacar ID de factura: traer el no referencia de factura de la nota afectada para poder sacar el id de factura para poder actualizar la cantidad en el detalle de la factura
	   	   
	   		$IDFactura=ObtenerIDFactura($FK_Usuario,$FK_Cliente,$NoReferenciaFactura);
	   		
	   		//obtener IDNOTACREDITOAfectada
			$IDNotaAfectada=ObtenerIDNotaAfectada($FK_Usuario,$FK_Cliente,$NoReferencia);
	   }
	   else if($TipoDocumento=='03')//NC
	   {
	   	$IDFactura=ObtenerIDFactura($FK_Usuario,$FK_Cliente,$NoReferencia);
	   }
	   //Variables para obtener IDFactura
	   
	   
		
	   $TipoDocumentoAfectado=ObtenerTipoDocumentoNumerico($_POST['TipoDocumentoAfectado']);


	   /*Campos de Calculos*/
	   
	   $MontoGravado= QuitarFormatoNumero($_POST['MontGrF']);
	   $MontoExento= QuitarFormatoNumero($_POST['MontExF']);
	   $MontoExonerado= QuitarFormatoNumero($_POST['MontExoF']);
	   $Descuento= QuitarFormatoNumero($_POST['MontDescF']);
	   $Impuesto= QuitarFormatoNumero($_POST['MontIVF']);
	   $ImpuestoDevuelto= QuitarFormatoNumero($_POST['MontIVDF']);
	   $OtroImpuesto= QuitarFormatoNumero($_POST['MontOImpF']);
	   $ServicioGravado= QuitarFormatoNumero($_POST['MontSGF']);
	   $ServicioExento= QuitarFormatoNumero($_POST['MontSEF']);
	   $ServicioExonerado= QuitarFormatoNumero($_POST['MontSExoF']);

	   $Subtotal= QuitarFormatoNumero($_POST['MontSubF']);
	   $Total= QuitarFormatoNumero($_POST['MontTF']);

		


	   //$Telefono= $_POST['Telefono'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Email= $_POST['Email'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Direccion= $_POST['Direccion'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Zona= $_POST['Zona'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   
	   $Terminal= $_POST['Terminal'];/*1 que hago con esto*/ 
	   $Sucursal= $_POST['Sucursal'];/*2 que hago con esto*/
	   //$Consecutivo= ObtenerConsecutivo($_POST['TipoDocumento']);  
	   
	   $NoNota=""; /* que hago con esto? $Terminal.$Sucursal.$TipoDocumento.$Consecutivo;Hacer una funcion para generarla  Terminal+Sucursal+TipoDocumento+Consecutivo*/ 

	   $Clave="";/*Hacer una funcion para generarla*/ //NoReferencia

	   $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	    if ($Conexion->connect_error) 
	    {
	        die("Connection failed: " . $Conexion->connect_error);
	    } 
	    
   /*guardar Encabezado de nota en la BD*/

	    /*sanitize sql*/
	    if($ActividadEconomica=='NULL')
		{
			if($EmailCliente=='NULL')
	    	{
	    		$sql="INSERT INTO  notacreditodebito(
FK_Usuario,FK_Cliente,NoNota,NombreCliente,NoFactura,NoReferencia,Razon,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,
TipoCambio,TipoDocumento,TipoDocAfectado,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalNota,TotalNota,Terminal,Sucursal)
values($FK_Usuario,'$FK_Cliente','$NoNota','$NombreCliente','$NoReferencia','$NoClaveAfectada','$Razon','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento','$TipoDocumentoAfectado',
$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,'$Terminal','$Sucursal');";
			}
			else
			{
				$sql="INSERT INTO  notacreditodebito(
FK_Usuario,FK_Cliente,NoNota,NombreCliente,NoFactura,NoReferencia,Razon,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,
TipoCambio,TipoDocumento,TipoDocAfectado,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalNota,TotalNota,Terminal,Sucursal,EmailCliente)
values($FK_Usuario,'$FK_Cliente','$NoNota','$NombreCliente','$NoReferencia','$NoClaveAfectada','$Razon','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento','$TipoDocumentoAfectado',
$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,'$Terminal','$Sucursal'.'$EmailCliente');";
			}
			
		}
	    else
	    {
			if($EmailCliente=='NULL')
	    	{
	    		$sql="INSERT INTO  notacreditodebito(
FK_Usuario,FK_Cliente,NoNota,NombreCliente,NoFactura,NoReferencia,Razon,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,
TipoCambio,TipoDocumento,TipoDocAfectado,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalNota,TotalNota,Terminal,Sucursal,FK_ActividadEconomica)
values($FK_Usuario,'$FK_Cliente','$NoNota','$NombreCliente','$NoReferencia','$NoClaveAfectada','$Razon','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento','$TipoDocumentoAfectado',
$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,'$Terminal','$Sucursal','$ActividadEconomica');";
			}
			else
			{
				$sql="INSERT INTO  notacreditodebito(
FK_Usuario,FK_Cliente,NoNota,NombreCliente,NoFactura,NoReferencia,Razon,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,
TipoCambio,TipoDocumento,TipoDocAfectado,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalNota,TotalNota,Terminal,Sucursal,FK_ActividadEconomica,EmailCliente)
values($FK_Usuario,'$FK_Cliente','$NoNota','$NombreCliente','$NoReferencia','$NoClaveAfectada','$Razon','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento','$TipoDocumentoAfectado',
$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,'$Terminal','$Sucursal','$ActividadEconomica','$EmailCliente');";
			}
		}
	                
	    if($Conexion->query($sql) === TRUE) 
	    { 
	        /*Guardar el Detalle de Factura*/

	        /*Obtener IDFactura*/

	        $sql="SELECT max(IDNota) as IDNota FROM notacreditodebito where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente';";

	        $result=$Conexion->query($sql);

	        if($result->num_rows > 0)
	        {
	          $row = $result->fetch_assoc();
	          $IDNota=$row["IDNota"];
	        }
	       
	        $TotalFilasGuardadas=0;
	        $NoLinea=0;

	        /*sacar el vector del JSON y pasarlo a Variable vector*/

	        $DetalleNota=$_POST['DetalleNota'];
	        

	        /*Recorrer el vector con vectores (Matriz) y guardar en la BD*/
	        foreach($DetalleNota as $i => $item) 
	        {	        	
			   if(!empty($DetalleNota[$i]['CantidadADevolver']))
			   {					   	
			   		   $NoLinea++;
			   		   $NoLineaFactura= $DetalleNota[$i]['NoLinea']; 
			           $IDProducto= $DetalleNota[$i]['IDProducto'];
			           $NombreProducto= $DetalleNota[$i]['NombreProducto'];
			           $Cantidad= $DetalleNota[$i]['CantidadADevolver'];
			           $PrecioVentaSinIV= QuitarFormatoNumero($DetalleNota[$i]['PrecioVentaSinIV']);
			           $UnidadMedida= $DetalleNota[$i]['UnidadMedida'];
			           $ImpuestoVentas= $DetalleNota[$i]['ImpuestoVentas'];
			           $Descuento= $DetalleNota[$i]['Descuento'];
			           $PrecioCosto= $DetalleNota[$i]['PrecioCosto'];
			           $Bonificacion= $DetalleNota[$i]['Bonificacion'];
			           $TotalNeto= QuitarFormatoNumero($DetalleNota[$i]['TotalNeto']);
			           $IDDetalle= $DetalleNota[$i]['IDDetalle'];
			           $MontoSExonerado= $DetalleNota[$i]['MontoSExonerado'];
			           $MontoExonerado= $DetalleNota[$i]['MontoExonerado'];
			           $MontoIVDevuelto= $DetalleNota[$i]['MontoIVDevuelto'];

			        // $array[$i] is same as $item

			           $sql="Insert into detallenota(FK_Nota,NombreProducto,Cantidad,Medida,Bonificacion,PrecioVenta,PrecioCosto,Impuesto,Descuento,MontoExonerado,MontoServicioExonerado,MontoImpuestoDevuelto,TotalNeto,NoLinea,NoLineaFactura,FK_Producto)
			values($IDNota,'$NombreProducto',$Cantidad,'$UnidadMedida',$Bonificacion,$PrecioVentaSinIV,$PrecioCosto,$ImpuestoVentas,$Descuento,$MontoExonerado,$MontoSExonerado,$MontoIVDevuelto,$TotalNeto,$NoLinea,$NoLineaFactura,'$IDProducto');";

			          if($Conexion->query($sql) === TRUE) 
			          { 
			            /*Ingrementar LineasGuardadas*/
			              $TotalFilasGuardadas++;
			            //Restar producto de inventario al guardar el detalle si todas las lineas se guardaron
			          }
			   }
			   else
			   {
			   		$TotalFilasGuardadas++;
			   }
			   

	        }
	    
	        //Si TotalFilas y TotalFilasGuardadas son iguales, se guardo la factura y hay que recorrer el arreglo y  restar la cantidad de productos disponibles por linea

	        if(count($DetalleNota)==$TotalFilasGuardadas)
	        {
	        	foreach($DetalleNota as $i => $item) 
		        {
		        	$CantidadADev=(!empty($DetalleNota[$i]['CantidadADevolver']))?$DetalleNota[$i]['CantidadADevolver']:0.00;
		        	$CantDevuelta=(!empty($DetalleNota[$i]['CantidadDevuelta']))?$DetalleNota[$i]['CantidadDevuelta']:0.00;
		        	$Cant= $DetalleNota[$i]['Cantidad'];
		        	
		        	if(($CantidadADev+$CantDevuelta)==$Cant)
		        	{
						$CantFilasDevTot++;
					}		
				}
				
				//Si cantidad de filas en detalle es igual a cantidad de filas con devolucionTotal=ya se devolvio toda la fila
		        if(count($DetalleNota)==$CantFilasDevTot)
		        {
					//actualizar campo de Tipo devolucion como Total en Factura/NotaCredito
					
					if($TipoDocumento=='02')//ND
					{					
						$sql="update notacreditodebito SET TipoDevolucion='Total' WHERE IDNota=$IDNotaAfectada";
				    
				                    
		                  if($Conexion->query($sql) === TRUE) 
			              { 
			                //Ingrementar cantidad y fecha de Prod en las lineas
			                  $Exito="";
			              }	
					}
					else if($TipoDocumento=='03')//NC
				    {
				    	$sql="update factura SET TipoDevolucion='Total', CantidadNotasCredito=CantidadNotasCredito+1 WHERE IDFactura=$IDFactura";
				                    
		                  if($Conexion->query($sql) === TRUE) 
			              { 
			                //Ingrementar cantidad y fecha de Prod en las lineas
			                  $Exito="";
			              }
					}	
				}
				else
				{				
					//actualizar campo de Tipo devolucion como Parcial en Factura/NotaCredito
					if($TipoDocumento=='02')//ND
					{
						$sql="update notacreditodebito SET TipoDevolucion='Parcial' WHERE IDNota=$IDNotaAfectada";
				    
				                    
		                  if($Conexion->query($sql) === TRUE) 
			              { 
			                //Ingrementar cantidad y fecha de Prod en las lineas
			                  $Exito="";
			              }	
					}
					else if($TipoDocumento=='03')//NC
				    {
				    	$sql="update factura SET TipoDevolucion='Parcial', CantidadNotasCredito=CantidadNotasCredito+1 WHERE IDFactura=$IDFactura";
				    
				                    
		                  if($Conexion->query($sql) === TRUE) 
			              { 
			                //Ingrementar cantidad y fecha de Prod en las lineas
			                  $Exito="";
			              }
					}
					
				}

	        	
	        	
	          $FilasAct=0;

	          /*Recorrer el arreglo, actualizar cantidad disponible y al final mostrar msj de exito en un boostrap*/
	          foreach($DetalleNota as $i => $item) 
	          {
	             if(!empty($DetalleNota[$i]['CantidadADevolver']))
			   	 {
			   	 		 $IDProducto= $DetalleNota[$i]['IDProducto'];
			             $Cantidad= $DetalleNota[$i]['CantidadADevolver'];
			             $Bonificacion= $DetalleNota[$i]['Bonificacion'];
			             $Medida= $DetalleNota[$i]['UnidadMedida'];
			             $NoLineaFactura= $DetalleNota[$i]['NoLinea']; 
			             
			             //Modificar Cantidad devuelta en factura si es nota credito
			             if($TipoDocumento=='02')
			             {						        
						        $sql="update detallefactura 
			                    set CantidadDevuelta=CantidadDevuelta-$Cantidad where FK_FK_Usuario=$FK_Usuario and FK_Factura=$IDFactura and FK_Producto='$IDProducto' and NoLinea=$NoLineaFactura;";
			                    
			                    if($Conexion->query($sql) === TRUE) 
				              { 
				                /*Ingrementar cantidad y fecha de Prod en las lineas*/
				                  $Exito="";
				              }	
						 }
						 else if($TipoDocumento=='03')
			             {						        
						        $sql="update detallefactura 
			                    set CantidadDevuelta=CantidadDevuelta+$Cantidad where FK_FK_Usuario=$FK_Usuario and FK_Factura=$IDFactura and FK_Producto='$IDProducto' and NoLinea=$NoLineaFactura;";
			                    
			                    if($Conexion->query($sql) === TRUE) 
				              { 
				                /*Ingrementar cantidad y fecha de Prod en las lineas*/
				                  $Exito="";
				              }	
						 }
						 
			             
			             
			             if(($Medida!='sp'AND $Medida!='h' AND $Medida!='d') && $TipoDocumento=='02')
			             {
						 		$sql="update producto 
			                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual-($Cantidad) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

				              if($Conexion->query($sql) === TRUE) 
				              { 
				                /*Ingrementar cantidad y fecha de Prod en las lineas*/
				                  $FilasAct++;
				              }	
						 }
						 else if(($Medida!='sp' AND $Medida!='h' AND $Medida!='d') && $TipoDocumento=='03')
			             {
						 		$sql="update producto 
			                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual+($Cantidad) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

				              if($Conexion->query($sql) === TRUE) 
				              { 
				                /*Ingrementar cantidad y fecha de Prod en las lineas*/
				                  $FilasAct++;
				              }	
						 }
						 else
						 {
						 	$FilasAct++;
						 }
				 }
	             else
				 {
					$FilasAct++;		
				 }	             
	      
	              
	          }

	          if(count($DetalleNota)==$FilasAct)
	          {
	            
						$Respuesta=($_POST['TipoDocumento']=='NotaCredito')? "Nota de crédito guardada correctamente":'Nota de débito guardada correctamente';
						$GuarMod='Guardo';			
   
	          }

	        }
	        else //sino son iguales entonces borrar todo del detalle y luego borrar el encabezado y mostrar msj de error
	        {
	            $sql="delete from detallenota where FK_Nota=$IDNota;";

	            if($Conexion->query($sql) === TRUE) 
	              { 
	                //Borrar encabezado de factura luego del detalle
	                $sql="delete from notacreditodebito where IDNota=$IDNota;";

	                 if($Conexion->query($sql) === TRUE) 
	                {
	                  //Error al guardar la factura
	                  $Respuesta= ($_POST['TipoDocumento']=='NotaCredito')? "Error al guardar la nota de crédito":"Error al guardar la nota de débito";
	                  $GuarMod='Error';
	                }

	              }
	        }
	    }
	    else
	    {
	      /*No se guardo el encabezado, mostrar error*/
	      $Respuesta= ($_POST['TipoDocumento']=='NotaCredito')? "Error al guardar la nota de crédito":"Error al guardar la nota de débito";
	      $GuarMod='Error';
	    }
	}
		    $users_arr[] = array( 

	                         /*Pasar al vector la Respuesta*/
	                         "Respuesta"=>$Respuesta,
	                         "GuarMod"=>$GuarMod,
	                     );

	    // encoding array to json format
	    echo json_encode($users_arr);
	    exit;
	
}

function ConsultarNota()
{
    $IDFactura=$_POST['IDFactura'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    $TipoDocAGenerar=(!empty($_POST['TipoDocAGenerar']))?$_POST['TipoDocAGenerar']:"";

    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

    $sql = "SELECT 

					N.IDNota,
					N.FK_Usuario,
					N.NoNota,

					/*Datos del CLiente*/

					N.FK_Cliente,
					N.NombreCliente,
					IFNULL(N.EmailCliente,C.Email1) AS EmailCliente,
					
					C.Telefono,
					C.Direccion,
					C.Zona,
					C.Exonerado,

					N.NoFactura,
					N.NoReferencia,
					N.Razon,
					DATE_FORMAT(N.Fecha,'%d-%m-%Y  %h:%i:%s') as Fecha,
					N.Plazo,
					N.MedioPago,
					N.CondicionVenta,
					N.Status,
					N.Clave,
					N.NoOrden,
					N.TipoMoneda,
					N.TipoCambio,
					N.TipoDocumento,
					N.TipoDocAfectado,
					N.MontoGravado,
					N.MontoExento,
					N.MontoExonerado,
					N.Descuento,
					N.Impuesto,
					N.ImpuestoDevuelto,
					N.OtroImpuesto,
					N.ServicioGravado,
					N.ServicioExento,
					N.ServicioExonerado,
					N.SubtotalNota,
					N.TotalNota,
					N.Terminal,
					N.Sucursal,
                    N.FK_ActividadEconomica
					FROM notacreditodebito N INNER JOIN cliente C ON N.FK_Cliente=C.Cedula AND N.FK_Usuario = C.FK_Usuario WHERE N.IDNota=$IDFactura AND N.FK_Cliente='$CedulaCliente' AND N.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDNota=$row["IDNota"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=ObtenerNombreTipoDocumento($row["TipoDocumento"]);
      
      $NoNota=($row["NoNota"]=="") ? ObtenerConsecutivo($TipoDocumento) : $row["NoNota"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
      $NoTipoDocAGenerar=(!empty($TipoDocAGenerar))?ObtenerConsecutivo($TipoDocumento):"";

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];
	  $EmailCliente=$row["EmailCliente"];

      $Telefono=$row["Telefono"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      $Exonerado=$row["Exonerado"];

	  $NoFactura=$row['NoFactura'];
	  $NoReferencia=$row["NoReferencia"];
	  $Razon=$row["Razon"];
      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $Plazo=$row["Plazo"];
      $MedioPago=$row["MedioPago"];
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row["Clave"]; //si es notadebito debe ser el no clave de la notacredito 
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=$row["TipoMoneda"];
      $TipoCambio=$row["TipoCambio"];
      
      $TipoDocAfectado=$row["TipoDocAfectado"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoN=$row["MontoGravado"];
      $MontoExentoN=$row["MontoExento"];
      $MontoExoneradoN=$row["MontoExonerado"];
      $DescuentoN=$row["Descuento"];
      $ImpuestoN=$row["Impuesto"];
      $ImpuestoDevueltoN=$row["ImpuestoDevuelto"];
      $OtroImpuestoN=$row["OtroImpuesto"];
      $ServicioGravadoN=$row["ServicioGravado"];
      $ServicioExentoN=$row["ServicioExento"];
      $ServicioExoneradoN=$row["ServicioExonerado"];
      $SubtotalNotaN=$row["SubtotalNota"];
      $TotalNotaN=$row["TotalNota"];
      $FK_ActividadEconomica=($row["FK_ActividadEconomica"]);

      /*Poner detalle de Factura*/  

      /*Hay que hacer los calculos de montos por fila con los datos del detalle*/  

      $sql="SELECT 

            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            MontoExonerado,
            MontoServicioExonerado,
            MontoImpuestoDevuelto,
            TotalNeto,
            NoLinea,
            FK_Producto

            FROM detallenota WHERE FK_Nota=$IDNota ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
	  
	  $DetalleNota = array();
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $IDDetalle= $ri['IDDetalle'];
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $PrecioVentaSinIV= $ri['PrecioVenta'];
	         $UnidadMedida= $ri['Medida'];
	         $ImpuestoVenta= $ri['Impuesto'];
	         $Descuento= $ri['Descuento'];
	         $MontoExonerado= $ri['MontoExonerado'];
	         $MontoServicioExonerado= $ri['MontoServicioExonerado'];
	         $MontoImpuestoDevuelto= $ri['MontoImpuestoDevuelto'];
	         $PrecioCosto= $ri['PrecioCosto'];
	         $Bonificacion= $ri['Bonificacion'];
	         $TotalNeto= $ri['TotalNeto'];
	         $NoLinea= $ri['NoLinea'];
			
			 $PrecioTotalBruto=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);

	         /*Variables para calculos*/
	          $MontoExento=0;
	          $MontoGravado=0; 
	          $MontoSExento=0;
	          $MontoSGravado=0;
	          $MontoIV=0;
	          $MontoOtroI=0;
	          $MontoDescuento=0;
	          $SubtotalL=0;
	          $TotalL=0;


	          if(($UnidadMedida!='sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h') AND $ImpuestoVenta==0) /*Sino Tiene Impuesto*/
	          {
	            $MontoExento=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	            $MontoGravado=0; 
	        
	          }
	          else if(($UnidadMedida!='sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1') /*Si Tiene Impuesto*/ 
	          {
	            $MontoExento=0;
	            $MontoGravado=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	          }
	          else
		    {
			  $MontoExento=0;
			  $MontoGravado=0;	
			}
	          
	          if(($UnidadMedida=='sp' || $UnidadMedida=='d' || $UnidadMedida=='h') AND $ImpuestoVenta==0)
	          {
	            $MontoSExento=bcdiv(($PrecioTotalBruto),1,2); /*Precio sin impuesto x Cantidad*/
	            $MontoSGravado=0;
	          }
	          else if(($UnidadMedida=='sp' || $UnidadMedida=='d' || $UnidadMedida=='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1')
	        {
	          $MontoSExento=0;/*Precio con impuesto x Cantidad*/
	            $MontoSGravado=bcdiv(($PrecioTotalBruto),1,2);
	        }
	        else
	        {
	          $MontoSExento=0;
	          $MontoSGravado=0;
	        }     

	          $MontoIV=bcdiv(((($PrecioTotalBruto)-(($PrecioTotalBruto)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
	          $MontoOtroI=0;/*se va a cambiar mas adelante*/
	          $MontoDescuento=bcdiv((($PrecioTotalBruto)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


	        if($Exonerado=='1' AND $ImpuestoVenta!=0)//es Exonerado
		    {
		        $PorcentajeExoneracionTP=ObtenerPorcentajeGlobarExoneracion($FK_Cliente);
		        
		        if($PorcentajeExoneracionTP!='NO')
				{
					if(($UnidadMedida!='sp'AND $UnidadMedida!='d'AND $UnidadMedida!='h'))
		    		{						
						$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExonerado),1,2);/*Subtotal-Descuento+Impuesto*/
					}
					else if(($UnidadMedida=='sp'OR $UnidadMedida=='d'OR $UnidadMedida=='h'))
		    		{						
						$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoServicioExonerado),1,2);/*Subtotal-Descuento+Impuesto*/
					}
				}
				else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
				{
					
				}
			}
			else //No es Exonerado
			{
				$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
		    	$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
			}



	         $DetalleNota[] = array(
	                                 "IDDetalle" => $IDDetalle,
	                                 "IDProducto" => $IDProducto, 
	                                 "NombreProducto" => $NombreProducto,
	                                 "PrecioVSinIV" => number_format($PrecioVentaSinIV,2), 
	                                 "Impuesto" =>$ImpuestoVenta, 
	                                 "Descuento" =>$Descuento, 
	                                 "Cantidad"=>$Cantidad,
	                                 "UM"=>$UnidadMedida,
	                                 "PrecioCosto"=>$PrecioCosto,
	                                 "Bonificacion"=>$Bonificacion,
	                                 
	                                 "MontoE"=>$MontoExento,
	                                 "MontoG"=>$MontoGravado,
	                                 "MontoSE"=>$MontoSExento,
	                                 "MontoSG"=>$MontoSGravado,
	                                 "MontoImpV"=>$MontoIV,
	                                 "MontoOImp"=>$MontoOtroI,
	                                 "MontoDesc"=>$MontoDescuento,
	                                 "MontoExonerado"=>$MontoExonerado,
	                                 "MontoServicioExonerado"=>$MontoServicioExonerado,
	                                 "MontoImpuestoDevuelto"=>$MontoImpuestoDevuelto,
	                                 "SubtL"=>number_format($SubtotalL,2),
	                                 "TotL"=>number_format($TotalL,2),
	                                 "NoLinea"=>$NoLinea,
	                                );
	  }


    }

    $users_arr[] = array( 
                            "IDNota"=>$IDNota,
                            "FK_Usuario"=>$FK_Usuario,
                            "NoNota"=>$NoNota,
                            "FK_Cliente"=>$FK_Cliente,
                            "NombreCliente"=>$NombreCliente,
                            "Telefono"=>$Telefono,
                            "Email1"=>$EmailCliente,
                            "Direccion"=>$Direccion,
                            "Zona"=>$Zona,
                            "Exonerado"=>$Exonerado,
                            "NoFacturaAfectada"=>$NoFactura,
                            "NoReferencia"=>$NoReferencia,
                            "Razon"=>$Razon,
                            
                            "Fecha"=>$Fecha,
                            "Plazo"=>$Plazo,
                            "MedioPago"=>$MedioPago,
                            "CondicionVenta"=>$CondicionVenta,
                            "Status"=>$Status,
                            "Clave"=>$Clave,
                            "NoOrden"=>$NoOrden,
                            "TipoMoneda"=>$TipoMoneda,
                            "TipoCambio"=>$TipoCambio,
                            "TipoDocumento"=>$TipoDocumento,
                            "TipoDocAfectado"=>$TipoDocAfectado,
                            "NoTipoDocAGenerar"=>$NoTipoDocAGenerar,

                            /*Campos de Calculo de factura*/
                            "MontoGravado"=>number_format($MontoGravadoN,2),
                            "MontoExento"=>number_format($MontoExentoN,2),
                            "MontoExonerado"=>number_format($MontoExoneradoN,2),
                            "Descuento"=>number_format($DescuentoN,2),
                            "Impuesto"=>number_format($ImpuestoN,2),
                            "ImpuestoDevuelto"=>number_format($ImpuestoDevueltoN,2),
                            "OtroImpuesto"=>number_format($OtroImpuestoN,2),
                            "ServicioGravado"=>number_format($ServicioGravadoN,2),
                            "ServicioExento"=>number_format($ServicioExentoN,2),
                            "ServicioExonerado"=>number_format($ServicioExoneradoN,2),
                            "SubtotalNota"=>number_format($SubtotalNotaN,2),
                            "TotalNota"=>number_format($TotalNotaN,2),
                            /*******************************/
                            "Terminal"=>$Terminal,
                            "Sucursal"=>$Sucursal,
                            "ActividadEconomica"=>$FK_ActividadEconomica,
                            
                            /*DetalleNota*/
                            "DetalleNota"=>$DetalleNota,
                       );

      // encoding array to json format
      echo json_encode($users_arr);
      exit;

}

function BorrarNota($IDDocumento,$TipoDocumento)
{
	$FK_Usuario=$_SESSION['IDUsuario'];
	$FK_Cliente=$_POST['CedulaCliente'];
	
	$Respuesta;
	
	$IDFactura=0;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 
	
	//Obtener IDFactura
	
	if($TipoDocumento=='02')//ND
   {
   		$NoReferenciaNota=ObtenerNoRefNotaCredito($FK_Usuario,$FK_Cliente,$IDDocumento);//Sacar NoRefNota Credito
   	
   		$NoReferenciaFactura=ObtenerNoRefFactura($FK_Usuario,$FK_Cliente,$NoReferenciaNota,'');//obtener NoRefFactura con NR Nota
   	   
   		$IDFactura=ObtenerIDFactura($FK_Usuario,$FK_Cliente,$NoReferenciaFactura);
   }
   else if($TipoDocumento=='03')//NC
   {
   		$NoReferenciaFactura=ObtenerNoRefFactura($FK_Usuario,$FK_Cliente,'',$IDDocumento);//para obtener NoRefFactura con IDNota
   	
   		$IDFactura=ObtenerIDFactura($FK_Usuario,$FK_Cliente,$NoReferenciaFactura);
   }
	
	
	 $TotalFilas=0;
	 $TotalModificaciones=0;
	 
	 //traer el detalle de la nota antes de borrar y restar cantidad del detalle al inventario por cada fila del detalle
	
		$sql="SELECT 

            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            TotalNeto,
            NoLinea,
            NoLineaFactura,
            FK_Producto

            FROM detallenota WHERE FK_Nota=$IDDocumento ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $TotalFilas++;
      	
      		 $NoLineaFactura= $ri['NoLineaFactura'];
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $Bonificacion= $ri['Bonificacion'];
	         $Medida= $ri['Medida'];
	         
	         if($TipoDocumento=='02')
             {						        
			        $sql="update detallefactura 
                    set CantidadDevuelta=CantidadDevuelta+$Cantidad where FK_FK_Usuario=$FK_Usuario and FK_Factura=$IDFactura and FK_Producto='$IDProducto' and NoLinea=$NoLineaFactura;";
                    
                    if($Conexion->query($sql) === TRUE) 
	              { 
	                /*Ingrementar cantidad y fecha de Prod en las lineas*/
	                  $Exito="";
	              }	
			 }
			 else if($TipoDocumento=='03')
             {						        
			        $sql="update detallefactura 
                    set CantidadDevuelta=CantidadDevuelta-$Cantidad where FK_FK_Usuario=$FK_Usuario and FK_Factura=$IDFactura and FK_Producto='$IDProducto' and NoLinea=$NoLineaFactura;";
                    
                    if($Conexion->query($sql) === TRUE) 
	              { 
	                /*Ingrementar cantidad y fecha de Prod en las lineas*/
	                  $Exito="";
	              }	
			 }
	         
	         if(($Medida!='sp' AND $Medida!='d' AND $Medida!='h') && $TipoDocumento=='02')
             {
			 		$sql="update producto 
                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual+($Cantidad) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

	              if($Conexion->query($sql) === TRUE) 
	              { 
	                /*Ingrementar cantidad y fecha de Prod en las lineas*/
	                  $TotalModificaciones++;
	              }	
			 }
			 else if(($Medida!='sp' AND $Medida!='d' AND $Medida!='h') && $TipoDocumento=='03')
             {
			 		$sql="update producto 
                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual-($Cantidad) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

	              if($Conexion->query($sql) === TRUE) 
	              { 
	                /*Ingrementar cantidad y fecha de Prod en las lineas*/
	                  $TotalModificaciones++;
	              }	
			 }
			 else
			 {
			 	$TotalModificaciones++;
			 }
			 		 
	  }
	  
	  if($TotalFilas==$TotalModificaciones)
	  {				
	  		
	  		$sql="delete from detallenota where FK_Nota=$IDDocumento";

			if($Conexion->query($sql))
			{				
				$sql = "SELECT  N.IDNota,N.FK_Usuario,N.NoNota,N.NoFactura,N.TipoDocumento,N.FK_Cliente,N.TotalNota 
				FROM notacreditodebito N WHERE N.IDNota=$IDDocumento AND N.FK_Usuario=$FK_Usuario";
				
				$result = $Conexion->query($sql);

			    if ($result->num_rows > 0) 
			    {
			      $row = $result->fetch_assoc();

			      $IDNota=$row["IDNota"];
			      $NoNotaAfectada=$row["NoFactura"];
			      $FK_Usuario=$row["FK_Usuario"];
			      $FK_Cliente=$row["FK_Cliente"];
			      $TotalNota=$row["TotalNota"];
			      $TipoDocumento=$row["TipoDocumento"];
			      

                  $sql="delete from notacreditodebito where FK_Usuario=$FK_Usuario AND IDNota=$IDNota";
		
					if($Conexion->query($sql))
					{								
						if($TipoDocumento=='02')
						{
							//obtener IDNOTACREDITOAfectada
							$IDNotaAfectada=ObtenerIDNotaAfectada($FK_Usuario,$FK_Cliente,$NoNotaAfectada);
							
							$sql="update notacreditodebito SET TipoDevolucion='' WHERE IDNota=$IDNotaAfectada";
			
							if($Conexion->query($sql))
							{								
								$Respuesta="Nota borrada correctamente";
							}
						}
						else if($TipoDocumento=='03')
						{
							//obtener IDFactura(se obtuvo mas arriba)
							
							//obtener cantNC de Factura
							$CantNC=ObtenerCantNC($IDFactura,$FK_Usuario,$FK_Cliente);
							$TipoDevolucion=ObtenerTipoDevolucion($IDFactura,$FK_Usuario,$FK_Cliente);							
							
							if($CantNC==1 AND ($TipoDevolucion=='Total' OR $TipoDevolucion=='Parcial'))//si solo tiene una nota y es total
							{
								$sql="update factura SET TipoDevolucion='', CantidadNotasCredito=CantidadNotasCredito-1 WHERE IDFactura=$IDFactura";
								if($Conexion->query($sql))
								{								
									$Respuesta="Nota borrada correctamente";
								}
							}
							else if($CantNC>1)//si tiene mas de una nota y es total
							{
								$sql="update factura SET TipoDevolucion='Parcial', CantidadNotasCredito=CantidadNotasCredito-1 WHERE IDFactura=$IDFactura";
								if($Conexion->query($sql))
								{								
									$Respuesta="Nota borrada correctamente";
								}
							}
						}
					}
					else
					{
						$Respuesta="Error al borrar la nota";
					}
				}
			}	
	  }
	  else
	  {
	  		$Respuesta="Error al borrar la nota";	
	  }
	
	  $users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
}

function ConsultarEstadoAntesDeEnvio($IDapi,$NoClaveLarga)
{
//Enviar a hacienda	    

$JSONConsulta='{
			  "data": {
			    "nombre_usuario": "'.$IDapi.'",
			    "clavelarga": "'.$NoClaveLarga.'"
			  }
			}';
			            
//API URL Envio
$urlEnvio = "http://35.170.39.96/api/server/consultar";

//create a new cURL resource
$ch = curl_init($urlEnvio);

//poner JSON en curl
curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONConsulta);

//set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

//return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute the POST request
$result = curl_exec($ch);

$Respuesta= $result;

//close cURL resource
curl_close($ch);	
                
//guardar el estado de la respuesta

$ResultadoEstado = '';

if( strpos( $Respuesta,'rechazado' ) !== false) 
{
$ResultadoEstado='rechazado';
}
else if( strpos( $Respuesta,'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
{
$ResultadoEstado='aceptado';
}
else if( strpos( $Respuesta,'procesando' ) !== false) 
{
$ResultadoEstado='procesando';
}
else if( strpos( $Respuesta,'El documento ha sido enviado a Hacienda pero Hacienda no ha dado respuesta' ) !== false) 
{
$ResultadoEstado='sin respuesta';
}
else
{
	$ResultadoEstado='no enviado';
}

return $ResultadoEstado; 

}

function ConsultarEstado()
{
	$IDDoc=$_POST['IDDocumento'];
	$FK_Cliente=$_POST['CedulaCliente'];
	$FK_Usuario=$_SESSION['IDUsuario'];
	
	
	$IDapi=ObtenerID_API($FK_Usuario);
	$NoClaveLarga=ObtenerClaveLarga($IDDoc,$FK_Usuario,$FK_Cliente);
	
	//Enviar a hacienda	    
	
	$JSONConsulta='{
				  "data": {
				    "nombre_usuario": "'.$IDapi.'",
				    "clavelarga": "'.$NoClaveLarga.'"
				  }
				}';
				            
	//API URL Envio
	$urlEnvio = "http://35.170.39.96/api/NuevoServer/consultar";

	//create a new cURL resource
	$ch = curl_init($urlEnvio);
	
	//poner JSON en curl
	curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONConsulta);

	//set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

	//return response instead of outputting
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	//execute the POST request
	$result = curl_exec($ch);

	$Respuesta= $result;

	//close cURL resource
	curl_close($ch);	
	                
	//guardar el estado de la respuesta
			
	$ResultadoEstado= '';
				
	if( strpos( $Respuesta,'rechazado' ) !== false) 
    {
    	$ResultadoEstado='rechazado';
	}
	else if( strpos( $Respuesta,'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
    {
    	$ResultadoEstado='aceptado';
	}
	else if( strpos( $Respuesta,'procesando' ) !== false) 
    {
    	$ResultadoEstado='procesando';
	}
	else if( strpos( $Respuesta,'El documento ya ha sido registado anteriormente' ) !== false) 
    {
    	$ResultadoEstado=rtrim(substr($Respuesta, strpos($Respuesta , 'es ')+ 2),'"}');
	}
	else if( strpos( $Respuesta,'sin_enviar' ) !== false || 
		   strpos( $Respuesta,'"status":500' ) !== false ||
		   strpos( $Respuesta,'"status":"500"' ) !== false) 
    {
    	$ResultadoEstado='en espera';
	}
	else if( strpos( $Respuesta,'no dio ninguna respuesta' ) !== false) 
    {
    	$ResultadoEstado='sin respuesta';
	}
	else
	{
		$ResultadoEstado='no enviado';
	}

				/*
				$IniMsj = '"message":{"0":"';
				$FinalMsj = '"},"clave":';
				$ResultadoMsj = ObtenerEstatusYMensajeHacienda($Respuesta,$IniMsj,$FinalMsj);
		*/

	$sql="UPDATE notacreditodebito SET Status='$ResultadoEstado', MensajeHacienda='' WHERE IDNota=$IDDoc and FK_Usuario=$FK_Usuario;";

$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  if($Conexion->query($sql) === TRUE) 
  { 
      $Exito='';
  }

	
  	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
}

function EnviarAHacienda()
{
	$IDDocumento=$_POST['IDDocumento'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    //variables por si el cliente es exonerado
    $TipoDocumentoExo="";
	$NumeroDocumentoExo="";
	$NombreInstitucionExo="";
	$FechaEmisionExo="";
    
    $Respuesta='';
    
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 
    
    $sql = "SELECT 

					N.IDNota,
					N.FK_Usuario,
					N.NoNota,
					
				    /*Datos del Usuario*/
			        U.ID_API,
					LPAD(CAST(U.Cedula AS CHAR(12)), 12, '0') AS CedulaUsuario,

					/*Datos del CLiente*/

					N.FK_Cliente,
					N.NombreCliente,

					C.TipoCedula,
					C.Telefono,
					C.Email1,
					C.Email2,
					C.Direccion,
					C.Zona,
					C.Exonerado,

					N.NoFactura,
					N.NoReferencia,
					N.Razon,
					
					DATE_FORMAT(N.Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha,
			        DATE_FORMAT(N.Fecha,'%d%m%y') AS FechaPClave,
			        DATE_FORMAT(N.Fecha,'%Y-%m-%d  %h:%i:%s') as FechaPCliente,
					
					N.Plazo,
					N.MedioPago,
					N.CondicionVenta,
					N.Status,
					N.Clave,
					N.NoOrden,
					N.TipoMoneda,
					N.TipoCambio,
					N.TipoDocumento,
					N.TipoDocAfectado,
					N.MontoGravado,
					N.MontoExento,
					N.MontoExonerado,
					N.Descuento,
					N.Impuesto,
					N.ImpuestoDevuelto,
					N.OtroImpuesto,
					N.ServicioGravado,
					N.ServicioExento,
					N.ServicioExonerado,
					N.SubtotalNota,
					N.TotalNota,
					N.Terminal,
					N.Sucursal,
					LPAD(CAST(N.FK_ActividadEconomica AS CHAR(6)), 6, '0') as FK_ActividadEconomica
					FROM notacreditodebito N
					INNER JOIN usuario U ON N.FK_Usuario=U.IDUsuario 
					INNER JOIN cliente C ON N.FK_Cliente=C.Cedula
					AND N.FK_Usuario = C.FK_Usuario 
					WHERE N.IDNota=$IDDocumento AND N.FK_Cliente='$CedulaCliente' AND N.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDNota=$row["IDNota"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=$row["TipoDocumento"];
      $NombreTipoDoc=ObtenerNombreTipoDocumento($TipoDocumento);
      
      $NoNota=($row["NoNota"]=="") ? ObtenerConsecutivo($NombreTipoDoc) : $row["NoNota"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
      $ID_API=$row["ID_API"];
	  $TipoCedula=ObternerTipoIdentificacionNumerica($row['TipoCedula']);
	  $CedulaUsuario=$row["CedulaUsuario"];

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];

      $Telefono=$row["Telefono"];
      $Email1=$row["Email1"];
      $Email2=$row["Email2"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      $Exonerado=$row["Exonerado"];
      
      //Partir la Zona
      $Provincia=substr($Zona,0,1);
      $Canton=substr($Zona,1,2);
      $Distrito=substr($Zona,3,4);

	  $NoFactura=$row['NoFactura']; //NoDocAfectado
	  $NoReferencia=$row["NoReferencia"];//Clave larga DocAfectado
	  $Razon=$row["Razon"];
	  
      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $FechaPClave=$row["FechaPClave"];
      $FechaPCliente=$row["FechaPCliente"];
      
      $Plazo=$row["Plazo"];
      $MedioPago= MedioParaJSON($row["MedioPago"]);
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row["Clave"]; //si es notadebito debe ser el no clave de la notacredito 
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=ObtenerCodigoMoneda($row["TipoMoneda"]);
      $TipoCambio=$row["TipoCambio"];
      
      $TipoDocAfectado=$row["TipoDocAfectado"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoN=$row["MontoGravado"];
      $MontoExentoN=$row["MontoExento"];
      $MontoExoneradoN=$row["MontoExonerado"];
      $DescuentoN=$row["Descuento"];
      $ImpuestoN=$row["Impuesto"];
      $ImpuestoDevueltoN=$row["ImpuestoDevuelto"];
      $OtroImpuestoN=$row["OtroImpuesto"];
      $ServicioGravadoN=$row["ServicioGravado"];
      $ServicioExentoN=$row["ServicioExento"];
      $ServicioExoneradoN=$row["ServicioExonerado"];
      $SubtotalNotaN=$row["SubtotalNota"];
      $TotalNotaN=$row["TotalNota"];
      $FK_ActividadEconomica=($row["FK_ActividadEconomica"]);
      
      /*Variables que hay que guardar*/
      $Consecutivo=($row["NoNota"]=="") ? $Terminal.$Sucursal.$TipoDocumento.$NoNota : $row["NoNota"];
      $ClaveLarga=($Clave=='')?"506".$FechaPClave.$CedulaUsuario.$Consecutivo."1".'00000000':$Clave;
      
      //Si es exonerado el cliente sacar los datos de la exoneracion y ponerlo en las constantes
      if($Exonerado=='1')
      {
	  	$InfoExoneracion=ObtenerInfoExoneracion($FK_Cliente);
	  		    
	    $TipoDocumentoExo=$InfoExoneracion[0]["TipoDocumento"];
		$NumeroDocumentoExo=$InfoExoneracion[0]["NumeroDocumentoExo"];
		$NombreInstitucionExo=$InfoExoneracion[0]["NombreInstitucionExo"];
		$FechaEmisionExo=$InfoExoneracion[0]["FechaEmisionExo"];			 
	  }
      
      $DetNot=DetalleNotaParaJSON($IDNota,$FK_Cliente,$Exonerado,$TipoDocumentoExo,$NumeroDocumentoExo,$NombreInstitucionExo,$FechaEmisionExo);
      
	  if($NombreTipoDoc=='NotaCredito')
      {
      	//Sacar FechaFactura
      	$IDFact=ObtenerIDFactura($FK_Usuario,$FK_Cliente,$NoFactura);
      	$FechaDocAfectado=ObtenerFechaFacturaAfectada($IDFact,$FK_Usuario,$FK_Cliente);

		if($FK_Cliente!='000000000')
		{
			$JSONEnvio=
		      			'{
						  "data": {
						    "tipo": "3",
						    "nombre_usuario": "'.$ID_API.'",
						    "situacion": "normal",
						    "consecutivo": "'.$Consecutivo.'",
						    "clavelarga": "'.$ClaveLarga.'",
						    "terminal": "'.$Sucursal.'",
						    "sucursal": "'.$Terminal.'",
						    "fechafactura": "'.$Fecha.'",
						    "afectafactura": "'.$NoReferencia.'",
						    "codigoActividad": "'.$FK_ActividadEconomica.'",
						    "otro_telefono": "",
						    "receptor": {
						      "tipoIdentificacion": "'.$TipoCedula.'",
						      "numeroIdentificacion": "'.$FK_Cliente.'",
						      "nombre": "'.$NombreCliente.'",
						      "provincia": "'.$Provincia.'",
						      "canton": "'.$Canton.'",
						      "distrito": "'.$Distrito.'",
						      "otrasSenas": "'.$Direccion.'",
						      "codigoPais": "506",
						      "telefono": "'.$Telefono.'",
						      "website": "",
						      "correo": "'.$Email1.'",
						      "nombreComercial": "",
						      "correo_gastos": "'.$Email2.'"
						    },
						    "condicionVenta": "'.$CondicionVenta.'",
						    "plazoCredito": "'.$Plazo.'",
						    "numFecha": "'.$Fecha.'",
						    "numReferencia": "'.$IDNota.'",
						    "codigoVendedor": "00",
						    "medioPago": [
						        '.$MedioPago.'
						    ],
						    "detalles": [
						      '.$DetNot.'
							],
						    "codigoTipoMoneda": {
						      "tipoMoneda": "'.$TipoMoneda.'",
						      "tipoCambio": "'.$TipoCambio.'"
						    },
						    "totalServGravados": "'.$ServicioGravadoN.'",
						    "totalServExentos": "'.$ServicioExentoN.'",
						    "totalServExonerado": "'.$ServicioExoneradoN.'",
						    "totalMercanciasGravados": "'.$MontoGravadoN.'",
						    "totalMercanciasExentos": "'.$MontoExentoN.'",
						    "totalMercExonerada": "'.$MontoExoneradoN.'",
						    "totalGravados": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN),1,2).'",
						    "totalExentos": "'.bcdiv(($ServicioExentoN+$MontoExentoN),1,2).'",
						    "totalExonerado": "'.bcdiv(($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalVentas": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN)+($ServicioExentoN+$MontoExentoN)+($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalDescuentos": "'.$DescuentoN.'",
						    "totalVentasNetas": "'.$SubtotalNotaN.'",
						    "totalImpuestos": "'.bcdiv(($ImpuestoN+$OtroImpuestoN),1,2).'",
						    "totalIVADevuelto": "'.$ImpuestoDevueltoN.'",
						    "totalOtrosCargos": "0.00000",
						    "totalComprobantes": "'.$TotalNotaN.'",
						    "otrosCargos": {
						      "tipoDocumento": "",
						      "numeroIdentidadTercero": "",
						      "nombreTercero": "",
						      "detalle": "",
						      "porcentaje": "0.00000",
						      "montoCargo": "0.00000"
						    },
						    "referencia": {
						      "tipoDocumento": "'.$TipoDocAfectado.'",
						      "numeroReferencia": "'.$NoReferencia.'",
						      "fecha": "'.$FechaDocAfectado.'",
						      "codigo": "03",
						      "razon": "ajuste completo o parcial de factura electronica"
						    },
						    "otros": {
						      "otroTexto": "devolucion por ajuste en factura ventas. Tipo de Cambio:. '.$TipoCambio.'",
						      "otroContenido": ""
						    }
						  }
						}';
		}
		else
		{
			$JSONEnvio=
		      			'{
						  "data": {
						    "tipo": "3",
						    "nombre_usuario": "'.$ID_API.'",
						    "situacion": "normal",
						    "consecutivo": "'.$Consecutivo.'",
						    "clavelarga": "'.$ClaveLarga.'",
						    "terminal": "'.$Sucursal.'",
						    "sucursal": "'.$Terminal.'",
						    "fechafactura": "'.$Fecha.'",
						    "afectafactura": "'.$NoReferencia.'",
						    "codigoActividad": "'.$FK_ActividadEconomica.'",
						    "otro_telefono": "",
						    "condicionVenta": "'.$CondicionVenta.'",
						    "plazoCredito": "'.$Plazo.'",
						    "numFecha": "'.$Fecha.'",
						    "numReferencia": "'.$IDNota.'",
						    "codigoVendedor": "00",
						    "medioPago": [
						        '.$MedioPago.'
						    ],
						    "detalles": [
						      '.$DetNot.'
							],
						    "codigoTipoMoneda": {
						      "tipoMoneda": "'.$TipoMoneda.'",
						      "tipoCambio": "'.$TipoCambio.'"
						    },
						    "totalServGravados": "'.$ServicioGravadoN.'",
						    "totalServExentos": "'.$ServicioExentoN.'",
						    "totalServExonerado": "'.$ServicioExoneradoN.'",
						    "totalMercanciasGravados": "'.$MontoGravadoN.'",
						    "totalMercanciasExentos": "'.$MontoExentoN.'",
						    "totalMercExonerada": "'.$MontoExoneradoN.'",
						    "totalGravados": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN),1,2).'",
						    "totalExentos": "'.bcdiv(($ServicioExentoN+$MontoExentoN),1,2).'",
						    "totalExonerado": "'.bcdiv(($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalVentas": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN)+($ServicioExentoN+$MontoExentoN)+($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalDescuentos": "'.$DescuentoN.'",
						    "totalVentasNetas": "'.$SubtotalNotaN.'",
						    "totalImpuestos": "'.bcdiv(($ImpuestoN+$OtroImpuestoN),1,2).'",
						    "totalIVADevuelto": "'.$ImpuestoDevueltoN.'",
						    "totalOtrosCargos": "0.00000",
						    "totalComprobantes": "'.$TotalNotaN.'",
						    "otrosCargos": {
						      "tipoDocumento": "",
						      "numeroIdentidadTercero": "",
						      "nombreTercero": "",
						      "detalle": "",
						      "porcentaje": "0.00000",
						      "montoCargo": "0.00000"
						    },
						    "referencia": {
						      "tipoDocumento": "'.$TipoDocAfectado.'",
						      "numeroReferencia": "'.$NoReferencia.'",
						      "fecha": "'.$FechaDocAfectado.'",
						      "codigo": "03",
						      "razon": "ajuste completo o parcial de factura electronica"
						    },
						    "otros": {
						      "otroTexto": "devolucion por ajuste en factura ventas. Tipo de Cambio:. '.$TipoCambio.'",
						      "otroContenido": ""
						    }
						  }
						}';
		}
      			
      			
      	
	  }
	  else if($NombreTipoDoc=='NotaDebito')
	  {
	  	//SacarFecha NotaCredito
	  	$IDNotaAfectada=ObtenerIDNotaAfectada($FK_Usuario,$FK_Cliente,$NoFactura);
      	$FechaDocAfectado=ObtenerFechaNotaCreditoAfectada($IDNotaAfectada,$FK_Usuario,$FK_Cliente);
      	
      	if($FK_Cliente!='000000000')
      	{
			$JSONEnvio=
		      			'{
						  "data": {
						    "tipo": "2",
						    "nombre_usuario": "'.$ID_API.'",
						    "situacion": "normal",
						    "consecutivo": "'.$Consecutivo.'",
						    "clavelarga": "'.$ClaveLarga.'",
						    "terminal": "'.$Sucursal.'",
						    "sucursal": "'.$Terminal.'",
						    "fechafactura": "'.$Fecha.'",
						    "afectafactura": "'.$NoReferencia.'",
						    "codigoActividad": "'.$FK_ActividadEconomica.'",
						    "otro_telefono": "",
						    "receptor": {
						      "tipoIdentificacion": "'.$TipoCedula.'",
						      "numeroIdentificacion": "'.$FK_Cliente.'",
						      "nombre": "'.$NombreCliente.'",
						      "provincia": "'.$Provincia.'",
						      "canton": "'.$Canton.'",
						      "distrito": "'.$Distrito.'",
						      "otrasSenas": "'.$Direccion.'",
						      "codigoPais": "506",
						      "telefono": "'.$Telefono.'",
						      "website": "",
						      "correo": "'.$Email1.'",
						      "nombreComercial": "",
						      "correo_gastos": "'.$Email2.'"
						    },
						    "condicionVenta": "'.$CondicionVenta.'",
						    "plazoCredito": "'.$Plazo.'",
						    "numFecha": "'.$Fecha.'",
						    "numReferencia": "'.$IDNota.'",
						    "codigoVendedor": "00",
						    "medioPago": [
						        '.$MedioPago.'
						    ],
						    "detalles": [
						      '.$DetNot.'
							],
						    "codigoTipoMoneda": {
						      "tipoMoneda": "'.$TipoMoneda.'",
						      "tipoCambio": "'.$TipoCambio.'"
						    },
						    "totalServGravados": "'.$ServicioGravadoN.'",
						    "totalServExentos": "'.$ServicioExentoN.'",
						    "totalServExonerado": "'.$ServicioExoneradoN.'",
						    "totalMercanciasGravados": "'.$MontoGravadoN.'",
						    "totalMercanciasExentos": "'.$MontoExentoN.'",
						    "totalMercExonerada": "'.$MontoExoneradoN.'",
						    "totalGravados": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN),1,2).'",
						    "totalExentos": "'.bcdiv(($ServicioExentoN+$MontoExentoN),1,2).'",
						    "totalExonerado": "'.bcdiv(($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalVentas": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN)+($ServicioExentoN+$MontoExentoN)+($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalDescuentos": "'.$DescuentoN.'",
						    "totalVentasNetas": "'.$SubtotalNotaN.'",
						    "totalImpuestos": "'.bcdiv(($ImpuestoN+$OtroImpuestoN),1,2).'",
						    "totalIVADevuelto": "'.$ImpuestoDevueltoN.'",
						    "totalOtrosCargos": "0.00000",
						    "totalComprobantes": "'.$TotalNotaN.'",
						    "otrosCargos": {
						      "tipoDocumento": "",
						      "numeroIdentidadTercero": "",
						      "nombreTercero": "",
						      "detalle": "",
						      "porcentaje": "0.00000",
						      "montoCargo": "0.00000"
						    },
						    "referencia": {
						      "tipoDocumento": "'.$TipoDocAfectado.'",
						      "numeroReferencia": "'.$NoReferencia.'",
						      "fecha": "'.$FechaDocAfectado.'",
						      "codigo": "03",
						      "razon": "ajuste completo o parcial de factura electronica"
						    },
						    "otros": {
						      "otroTexto": "devolucion por ajuste en factura ventas. Tipo de Cambio:. '.$TipoCambio.'",
						      "otroContenido": ""
						    }
						  }
						}';
		}
      	else
      	{
			$JSONEnvio=
		      			'{
						  "data": {
						    "tipo": "2",
						    "nombre_usuario": "'.$ID_API.'",
						    "situacion": "normal",
						    "consecutivo": "'.$Consecutivo.'",
						    "clavelarga": "'.$ClaveLarga.'",
						    "terminal": "'.$Sucursal.'",
						    "sucursal": "'.$Terminal.'",
						    "fechafactura": "'.$Fecha.'",
						    "afectafactura": "'.$NoReferencia.'",
						    "codigoActividad": "'.$FK_ActividadEconomica.'",
						    "otro_telefono": "",
						    "condicionVenta": "'.$CondicionVenta.'",
						    "plazoCredito": "'.$Plazo.'",
						    "numFecha": "'.$Fecha.'",
						    "numReferencia": "'.$IDNota.'",
						    "codigoVendedor": "00",
						    "medioPago": [
						        '.$MedioPago.'
						    ],
						    "detalles": [
						      '.$DetNot.'
							],
						    "codigoTipoMoneda": {
						      "tipoMoneda": "'.$TipoMoneda.'",
						      "tipoCambio": "'.$TipoCambio.'"
						    },
						    "totalServGravados": "'.$ServicioGravadoN.'",
						    "totalServExentos": "'.$ServicioExentoN.'",
						    "totalServExonerado": "'.$ServicioExoneradoN.'",
						    "totalMercanciasGravados": "'.$MontoGravadoN.'",
						    "totalMercanciasExentos": "'.$MontoExentoN.'",
						    "totalMercExonerada": "'.$MontoExoneradoN.'",
						    "totalGravados": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN),1,2).'",
						    "totalExentos": "'.bcdiv(($ServicioExentoN+$MontoExentoN),1,2).'",
						    "totalExonerado": "'.bcdiv(($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalVentas": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN)+($ServicioExentoN+$MontoExentoN)+($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
						    "totalDescuentos": "'.$DescuentoN.'",
						    "totalVentasNetas": "'.$SubtotalNotaN.'",
						    "totalImpuestos": "'.bcdiv(($ImpuestoN+$OtroImpuestoN),1,2).'",
						    "totalIVADevuelto": "'.$ImpuestoDevueltoN.'",
						    "totalOtrosCargos": "0.00000",
						    "totalComprobantes": "'.$TotalNotaN.'",
						    "otrosCargos": {
						      "tipoDocumento": "",
						      "numeroIdentidadTercero": "",
						      "nombreTercero": "",
						      "detalle": "",
						      "porcentaje": "0.00000",
						      "montoCargo": "0.00000"
						    },
						    "referencia": {
						      "tipoDocumento": "'.$TipoDocAfectado.'",
						      "numeroReferencia": "'.$NoReferencia.'",
						      "fecha": "'.$FechaDocAfectado.'",
						      "codigo": "03",
						      "razon": "ajuste completo o parcial de factura electronica"
						    },
						    "otros": {
						      "otroTexto": "devolucion por ajuste en factura ventas. Tipo de Cambio:. '.$TipoCambio.'",
						      "otroContenido": ""
						    }
						  }
						}';
		}
	  }
	  
	  
			//actualizar clave y consecutivo de la nota
			
			  $sql="UPDATE notacreditodebito SET NoNota='$Consecutivo', Clave='$ClaveLarga' WHERE IDNota=$IDNota and FK_Usuario=$FK_Usuario;";

	          if($Conexion->query($sql) === TRUE) 
	          { 
	          	 //Actualizar consecutivo del documento sumar 1	solo si es una factura nueva
		          	 if($Clave=='' || $row["NoNota"]=="")
		          	 {
					 	$sql="UPDATE consecutivo SET $NombreTipoDoc=$NombreTipoDoc+1 WHERE FK_Usuario=$FK_Usuario;";
			
						if($Conexion->query($sql))
						{								
							$Exito="";
						}
					 }
	  	      }
			
      			
      		  //Enviar a hacienda	    
			            
			//API URL Envio
			$urlEnvio = $urlEnvio =(($Clave!=''||$row["NoNota"]!="") && ($row["Status"]=='sin respuesta'||$row["Status"]=='no enviado'))
				//es un documento ya enviado que no llego a hacienda=>reenviar
						   ?'http://35.170.39.96/api/NuevoServer/reenviarfactura'
						   :'http://35.170.39.96/api/NuevoServer/enviar';//es un documento nuevo, enviar

			//create a new cURL resource
			$ch = curl_init($urlEnvio);
			
			//poner JSON en curl
			curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

			//set the content type to application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

			//return response instead of outputting
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			//execute the POST request
			$result = curl_exec($ch);

			$Respuesta= $result;

			//close cURL resource
			curl_close($ch);
			
			//guardar el estado de la respuesta
			
				//guardar el estado de la respuesta
			
			$ResultadoEstado = '';
				
			if( strpos( $Respuesta,'rechazado' ) !== false) 
		    {
		    	$ResultadoEstado='rechazado';
			}
			else if( strpos( $Respuesta,'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
		    {
		    	$ResultadoEstado='aceptado';
			}
			else if( strpos( $Respuesta,'procesando' ) !== false) 
		    {
		    	$ResultadoEstado='procesando';
			}
			else if( strpos( $Respuesta,'El documento ya ha sido registado anteriormente' ) !== false) 
		    {
		    	$ResultadoEstado=rtrim(substr($Respuesta, strpos($Respuesta , 'es ')+ 2),'"}');
			}
			else if( strpos( $Respuesta,'sin_enviar' ) !== false || 
				   strpos( $Respuesta,'"status":500' ) !== false ||
				   strpos( $Respuesta,'"status":"500"' ) !== false) 
		    {
		    	$ResultadoEstado='en espera';
			}
			else if( strpos( $Respuesta,'no dio ninguna respuesta' ) !== false) 
		    {
		    	$ResultadoEstado='sin respuesta';
			}
			else
			{
				$ResultadoEstado='no enviado';
			}

				/*
				$IniMsj = '"message":{"0":"';
				$FinalMsj = '"},"clave":';
				$ResultadoMsj = ObtenerEstatusYMensajeHacienda($Respuesta,$IniMsj,$FinalMsj);
		*/
	
			$sql="UPDATE notacreditodebito SET Status='$ResultadoEstado', MensajeHacienda='' WHERE IDNota=$IDNota and FK_Usuario=$FK_Usuario;";

		  if($Conexion->query($sql) === TRUE) 
		  { 
		      $Exito='';
		  }

			
			//actualizar Totales de venta del cliente
			if($NombreTipoDoc=='NotaCredito' && $FK_Cliente!='000000000' && ($Clave=='' || $row["NoNota"]==""))
			{
				  $sql="UPDATE cliente SET TotalCreditos=TotalCreditos+$TotalNotaN WHERE Cedula='$FK_Cliente' and FK_Usuario=$FK_Usuario;";

				  if($Conexion->query($sql) === TRUE) 
				  { 

				      $Exito='';
				  }	
			}
			else if($NombreTipoDoc=='NotaDebito' && $FK_Cliente!='000000000' && ($Clave=='' || $row["NoNota"]==""))
			{
				  $sql="UPDATE cliente SET TotalDebitos=TotalDebitos+$TotalNotaN WHERE Cedula='$FK_Cliente' and FK_Usuario=$FK_Usuario;";

				  if($Conexion->query($sql) === TRUE) 
				  { 

				      $Exito='';
				  }	
			}
	}

	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
	
}

function ReenviarEmail()
{
	$IDDocumento=$_POST['IDDocumento'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    //variables por si el cliente es exonerado
    $TipoDocumentoExo="";
	$NumeroDocumentoExo="";
	$NombreInstitucionExo="";
	$FechaEmisionExo="";
    
    $Respuesta='';
    
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 
    
    $sql = "SELECT 

					N.IDNota,
					N.FK_Usuario,
					N.NoNota,
					
				    /*Datos del Usuario*/
			        U.ID_API,
					LPAD(CAST(U.Cedula AS CHAR(12)), 12, '0') AS CedulaUsuario,

					/*Datos del CLiente*/

					N.FK_Cliente,
					N.NombreCliente,

					C.TipoCedula,
					C.Telefono,
					C.Email1,
					C.Email2,
					C.Direccion,
					C.Zona,
					C.Exonerado,

					N.NoFactura,
					N.NoReferencia,
					N.Razon,
					
					DATE_FORMAT(N.Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha,
			        DATE_FORMAT(N.Fecha,'%d%m%y') AS FechaPClave,
			        DATE_FORMAT(N.Fecha,'%Y-%m-%d  %h:%i:%s') as FechaPCliente,
					
					N.Plazo,
					N.MedioPago,
					N.CondicionVenta,
					N.Status,
					N.Clave,
					N.NoOrden,
					N.TipoMoneda,
					N.TipoCambio,
					N.TipoDocumento,
					N.TipoDocAfectado,
					N.MontoGravado,
					N.MontoExento,
					N.MontoExonerado,
					N.Descuento,
					N.Impuesto,
					N.ImpuestoDevuelto,
					N.OtroImpuesto,
					N.ServicioGravado,
					N.ServicioExento,
					N.ServicioExonerado,
					N.SubtotalNota,
					N.TotalNota,
					N.Terminal,
					N.Sucursal,
					LPAD(CAST(N.FK_ActividadEconomica AS CHAR(6)), 6, '0') as FK_ActividadEconomica
					FROM notacreditodebito N
					INNER JOIN usuario U ON N.FK_Usuario=U.IDUsuario 
					INNER JOIN cliente C ON N.FK_Cliente=C.Cedula 
					AND N.FK_Usuario = C.FK_Usuario
					WHERE N.IDNota=$IDDocumento AND N.FK_Cliente='$CedulaCliente' AND N.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDNota=$row["IDNota"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=$row["TipoDocumento"];
      $NombreTipoDoc=ObtenerNombreTipoDocumento($TipoDocumento);
      
      $NoNota=($row["NoNota"]=="") ? ObtenerConsecutivo($NombreTipoDoc) : $row["NoNota"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
      $ID_API=$row["ID_API"];
	  $TipoCedula=ObternerTipoIdentificacionNumerica($row['TipoCedula']);
	  $CedulaUsuario=$row["CedulaUsuario"];

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];

      $Telefono=$row["Telefono"];
      $Email1=$row["Email1"];
      $Email2=$row["Email2"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      $Exonerado=$row["Exonerado"];
      
      //Partir la Zona
      $Provincia=substr($Zona,0,1);
      $Canton=substr($Zona,1,2);
      $Distrito=substr($Zona,3,4);

	  $NoFactura=$row['NoFactura']; //NoDocAfectado
	  $NoReferencia=$row["NoReferencia"];//Clave larga DocAfectado
	  $Razon=$row["Razon"];
	  
      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $FechaPClave=$row["FechaPClave"];
      $FechaPCliente=$row["FechaPCliente"];
      
      $Plazo=$row["Plazo"];
      $MedioPago= MedioParaJSON($row["MedioPago"]);
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row["Clave"]; //si es notadebito debe ser el no clave de la notacredito 
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=ObtenerCodigoMoneda($row["TipoMoneda"]);
      $TipoCambio=$row["TipoCambio"];
      
      $TipoDocAfectado=$row["TipoDocAfectado"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoN=$row["MontoGravado"];
      $MontoExentoN=$row["MontoExento"];
      $MontoExoneradoN=$row["MontoExonerado"];
      $DescuentoN=$row["Descuento"];
      $ImpuestoN=$row["Impuesto"];
      $ImpuestoDevueltoN=$row["ImpuestoDevuelto"];
      $OtroImpuestoN=$row["OtroImpuesto"];
      $ServicioGravadoN=$row["ServicioGravado"];
      $ServicioExentoN=$row["ServicioExento"];
      $ServicioExoneradoN=$row["ServicioExonerado"];
      $SubtotalNotaN=$row["SubtotalNota"];
      $TotalNotaN=$row["TotalNota"];
      
      $FK_ActividadEconomica=($row["FK_ActividadEconomica"]);
      
      /*Variables que hay que guardar*/
      $Consecutivo=($row["NoNota"]=="") ? $Terminal.$Sucursal.$TipoDocumento.$NoNota : $row["NoNota"];
      $ClaveLarga=($Clave=='')?"506".$FechaPClave.$CedulaUsuario.$Consecutivo."1".'00000000':$Clave;
      
      //Si es exonerado el cliente sacar los datos de la exoneracion y ponerlo en las constantes
      if($Exonerado=='1')
      {
	  	$InfoExoneracion=ObtenerInfoExoneracion($FK_Cliente);
	  		    
	    $TipoDocumentoExo=$InfoExoneracion[0]["TipoDocumento"];
		$NumeroDocumentoExo=$InfoExoneracion[0]["NumeroDocumentoExo"];
		$NombreInstitucionExo=$InfoExoneracion[0]["NombreInstitucionExo"];
		$FechaEmisionExo=$InfoExoneracion[0]["FechaEmisionExo"];			 
	  }
      
      $DetNot=DetalleNotaParaJSON($IDNota,$FK_Cliente,$Exonerado,$TipoDocumentoExo,$NumeroDocumentoExo,$NombreInstitucionExo,$FechaEmisionExo);
      
      if($Status=='aceptado')
      {
      		if($NombreTipoDoc=='NotaCredito')
	      	{
	      	//Sacar FechaFactura
	      	$IDFact=ObtenerIDFactura($FK_Usuario,$FK_Cliente,$NoFactura);
	      	$FechaDocAfectado=ObtenerFechaFacturaAfectada($IDFact,$FK_Usuario,$FK_Cliente);

			if($FK_Cliente!='000000000')
			{
				$JSONEnvio=
			      			'{
							  "data": {
							    "tipo": "3",
							    "nombre_usuario": "'.$ID_API.'",
							    "situacion": "normal",
							    "consecutivo": "'.$Consecutivo.'",
							    "clavelarga": "'.$ClaveLarga.'",
							    "sucursal": "'.$Sucursal.'",
							    "fechafactura": "'.$Fecha.'",
							    "afectafactura": "'.$NoReferencia.'",
							    "codigoActividad": "'.$FK_ActividadEconomica.'",
							    "terminal": "'.$Terminal.'",
							    "otro_telefono": "",
							    "receptor": {
							      "tipoIdentificacion": "'.$TipoCedula.'",
							      "numeroIdentificacion": "'.$FK_Cliente.'",
							      "nombre": "'.$NombreCliente.'",
							      "provincia": "'.$Provincia.'",
							      "canton": "'.$Canton.'",
							      "distrito": "'.$Distrito.'",
							      "otrasSenas": "'.$Direccion.'",
							      "codigoPais": "506",
							      "telefono": "'.$Telefono.'",
							      "website": "",
							      "correo": "'.$Email1.'",
							      "correo_gastos": "'.$Email2.'"
							    },
							    "condicionVenta": "'.$CondicionVenta.'",
							    "plazoCredito": "'.$Plazo.'",
							    "numFecha": "",
							    "numReferencia": "'.$IDNota.'",
							    "codigoVendedor": "00",
							    "medioPago": [
							        '.$MedioPago.'
							    ],
							    "detalles": [
							      '.$DetNot.'
								],
							    "codigoTipoMoneda": {
							      "tipoMoneda": "'.$TipoMoneda.'",
							      "tipoCambio": "'.$TipoCambio.'"
							    },
							    "totalServGravados": "'.$ServicioGravadoN.'",
							    "totalServExentos": "'.$ServicioExentoN.'",
							    "totalServExonerado": "'.$ServicioExoneradoN.'",
							    "totalMercanciasGravados": "'.$MontoGravadoN.'",
							    "totalMercanciasExentos": "'.$MontoExentoN.'",
							    "totalMercExonerada": "'.$MontoExoneradoN.'",
							    "totalGravados": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN),1,2).'",
							    "totalExentos": "'.bcdiv(($ServicioExentoN+$MontoExentoN),1,2).'",
							    "totalExonerado": "'.bcdiv(($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
							    "totalVentas": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN)+($ServicioExentoN+$MontoExentoN)+($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
							    "totalDescuentos": "'.$DescuentoN.'",
							    "totalVentasNetas": "'.$SubtotalNotaN.'",
							    "totalImpuestos": "'.bcdiv(($ImpuestoN+$OtroImpuestoN),1,2).'",
							    "totalIVADevuelto": "'.$ImpuestoDevueltoN.'",
							    "totalOtrosCargos": "0.00000",
							    "totalComprobantes": "'.$TotalNotaN.'",
							    "otrosCargos": {
							      "tipoDocumento": "",
							      "numeroIdentidadTercero": "",
							      "nombreTercero": "",
							      "detalle": "",
							      "porcentaje": "0.00000",
							      "montoCargo": "0.00000"
							    },
							    "referencia": {
							      "tipoDocumento": "'.$TipoDocAfectado.'",
							      "numeroReferencia": "'.$NoReferencia.'",
							      "fecha": "'.$FechaDocAfectado.'",
							      "codigo": "03",
							      "razon": "ajuste completo o parcial de factura electronica"
							    },
							    "otros": {
							      "otroTexto": "devolucion por ajuste en factura ventas. Tipo de Cambio:. '.$TipoCambio.'",
							      "otroContenido": ""
							    }
							  }
							}';
			}
			
		  }
		  else if($NombreTipoDoc=='NotaDebito')
		  {
		  	//SacarFecha NotaCredito
		  	$IDNotaAfectada=ObtenerIDNotaAfectada($FK_Usuario,$FK_Cliente,$NoFactura);
	      	$FechaDocAfectado=ObtenerFechaNotaCreditoAfectada($IDNotaAfectada,$FK_Usuario,$FK_Cliente);
	      	
	      	if($FK_Cliente!='000000000')
	      	{
				$JSONEnvio=
			      			'{
							  "data": {
							    "tipo": "2",
							    "nombre_usuario": "'.$ID_API.'",
							    "situacion": "normal",
							    "consecutivo": "'.$Consecutivo.'",
							    "clavelarga": "'.$ClaveLarga.'",
							    "sucursal": "'.$Sucursal.'",
							    "fechafactura": "'.$Fecha.'",
							    "afectafactura": "'.$NoReferencia.'",
							    "codigoActividad": "'.$FK_ActividadEconomica.'",
							    "terminal": "'.$Terminal.'",
							    "otro_telefono": "",
							    "receptor": {
							      "tipoIdentificacion": "'.$TipoCedula.'",
							      "numeroIdentificacion": "'.$FK_Cliente.'",
							      "nombre": "'.$NombreCliente.'",
							      "provincia": "'.$Provincia.'",
							      "canton": "'.$Canton.'",
							      "distrito": "'.$Distrito.'",
							      "otrasSenas": "'.$Direccion.'",
							      "codigoPais": "506",
							      "telefono": "'.$Telefono.'",
							      "website": "",
							      "correo": "'.$Email1.'",
							      "correo_gastos": "'.$Email2.'"
							    },
							    "condicionVenta": "'.$CondicionVenta.'",
							    "plazoCredito": "'.$Plazo.'",
							    "numFecha": "",
							    "numReferencia": "'.$IDNota.'",
							    "codigoVendedor": "00",
							    "medioPago": [
							        '.$MedioPago.'
							    ],
							    "detalles": [
							      '.$DetNot.'
								],
							    "codigoTipoMoneda": {
							      "tipoMoneda": "'.$TipoMoneda.'",
							      "tipoCambio": "'.$TipoCambio.'"
							    },
							    "totalServGravados": "'.$ServicioGravadoN.'",
							    "totalServExentos": "'.$ServicioExentoN.'",
							    "totalServExonerado": "'.$ServicioExoneradoN.'",
							    "totalMercanciasGravados": "'.$MontoGravadoN.'",
							    "totalMercanciasExentos": "'.$MontoExentoN.'",
							    "totalMercExonerada": "'.$MontoExoneradoN.'",
							    "totalGravados": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN),1,2).'",
							    "totalExentos": "'.bcdiv(($ServicioExentoN+$MontoExentoN),1,2).'",
							    "totalExonerado": "'.bcdiv(($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
							    "totalVentas": "'.bcdiv(($ServicioGravadoN+$MontoGravadoN)+($ServicioExentoN+$MontoExentoN)+($ServicioExoneradoN+$MontoExoneradoN),1,2).'",
							    "totalDescuentos": "'.$DescuentoN.'",
							    "totalVentasNetas": "'.$SubtotalNotaN.'",
							    "totalImpuestos": "'.bcdiv(($ImpuestoN+$OtroImpuestoN),1,2).'",
							    "totalIVADevuelto": "'.$ImpuestoDevueltoN.'",
							    "totalOtrosCargos": "0.00000",
							    "totalComprobantes": "'.$TotalNotaN.'",
							    "otrosCargos": {
							      "tipoDocumento": "",
							      "numeroIdentidadTercero": "",
							      "nombreTercero": "",
							      "detalle": "",
							      "porcentaje": "0.00000",
							      "montoCargo": "0.00000"
							    },
							    "referencia": {
							      "tipoDocumento": "'.$TipoDocAfectado.'",
							      "numeroReferencia": "'.$NoReferencia.'",
							      "fecha": "'.$FechaDocAfectado.'",
							      "codigo": "03",
							      "razon": "ajuste completo o parcial de factura electronica"
							    },
							    "otros": {
							      "otroTexto": "devolucion por ajuste en factura ventas. Tipo de Cambio:. '.$TipoCambio.'",
							      "otroContenido": ""
							    }
							  }
							}';
			}
		  }				
		 
	  }		
      			
      	//Enviar email    
				
				if($FK_Cliente!='000000000')
	      		{
				
					//Regenerar PDF por si no esta	
					
					$urlEnvio = "http://35.170.39.96/api/NuevoServer/generar_pdf";

					//create a new cURL resource
					$ch = curl_init($urlEnvio);
					
					//poner JSON en curl
					curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

					//set the content type to application/json
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

					//return response instead of outputting
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					//execute the POST request
					$result = curl_exec($ch);

					$Respuesta= $result;/**poner Msj enviado en negrita*/

					//close cURL resource
					curl_close($ch);
				
				}
				
				//enviar 
							            
				//API URL Envio
				$urlEnvio = "http://35.170.39.96/api/NuevoServer/enviar_correo";

				//create a new cURL resource
				$ch = curl_init($urlEnvio);
				
				//poner JSON en curl
				curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

				//set the content type to application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

				//return response instead of outputting
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				//execute the POST request
				$result = curl_exec($ch);

				$Respuesta= $result;/**poner Msj enviado en negrita*/

				//close cURL resource
				curl_close($ch);
			
			
	  }
      
	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;      	
			

}
	


function QuitarFormatoNumero($Numero)
{
	//$Numero=number_format((float)$Numero, 2, '.', '');
	
	return str_replace(",", "", $Numero);
}

function ObternerTipoIdentificacionNumerica($TipoCedula)
{
	$TipoDocumento;
	
	if($TipoCedula=='F')
	{
		$TipoDocumento='01';
	}
	else if($TipoCedula=='J')
	{
		$TipoDocumento='02';
	}
	else if($TipoCedula=='D')
	{
		$TipoDocumento='03';
	}
	else if($TipoCedula=='E')
	{
		$TipoDocumento='05';
	}
	else//N
	{
		$TipoDocumento='04';
	}
	
	return $TipoDocumento;
}

function ObtenerCodigoMoneda($TipoMoneda)
{
	$CodMoneda='';
	
	if($TipoMoneda=='C')
	{
		$CodMoneda='CRC';
	}
	else if($TipoMoneda=='D')
	{
		$CodMoneda='USD';
	}
	else if($TipoMoneda=='E')
	{
		$CodMoneda='EUR';
	}
	
	return $CodMoneda;
}

function MedioParaJSON($MedioPago)
{
	$Medio='';
	
	$MedioPago=str_replace('0','',$MedioPago);
	
	$chars = str_split($MedioPago);
	foreach($chars as $char)
	{
    	if($char=='1')
    	{
			$Medio.='{"codigo": "01"},'.PHP_EOL;
		}
	    else if($char=='2')
    	{
			$Medio.='{"codigo": "02"},'.PHP_EOL;
		}
		else if($char=='3')
    	{
			$Medio.='{"codigo": "03"},'.PHP_EOL;
		}
		else if($char=='4')
    	{
			$Medio.='{"codigo": "04"},'.PHP_EOL;
		}
		else if($char=='5')
    	{
			$Medio.='{"codigo": "05"},'.PHP_EOL;
		} 	
	}
		
	return rtrim($Medio, ',' . PHP_EOL);
}

function DetalleNotaParaJSON($IDNota,$FK_Cliente,$Exonerado,$TipoDocumentoExo,$NumeroDocumentoExo,$NombreInstitucionExo,$FechaEmisionExo)
{
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 
        
       $sql="SELECT 

            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            MontoExonerado,
            MontoServicioExonerado,
            MontoImpuestoDevuelto,
            TotalNeto,
            NoLinea,
            FK_Producto

            FROM detallenota WHERE FK_Nota=$IDNota ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
	  
	  $DetalleNota = '';
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $IDDetalle= $ri['IDDetalle'];
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $PrecioVentaSinIV= $ri['PrecioVenta'];
	         $UnidadMedida= ConvertirUnidades($ri['Medida']);
	         $ImpuestoVenta= $ri['Impuesto'];
	         $Descuento= $ri['Descuento'];
	         $PrecioCosto= $ri['PrecioCosto'];
	         $Bonificacion= $ri['Bonificacion'];
	         $TotalNeto= $ri['TotalNeto'];
	         $NoLinea= $ri['NoLinea'];
	         
	         $MontoExonerado= $ri['MontoExonerado'];
	         $MontoServicioExonerado= $ri['MontoServicioExonerado'];
	         $MontoImpuestoDevuelto= $ri['MontoImpuestoDevuelto'];

			 $PrecioTotalBruto=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);
			 
	         /*Variables para calculos*/
	          $MontoExento=0;
	          $MontoGravado=0; 
	          $MontoSExento=0;
	          $MontoSGravado=0;
	          $MontoIV=0;
	          $MontoOtroI=0;
	          $MontoDescuento=0;
	          $SubtotalL=0;
	          $TotalL=0;


	          if(($UnidadMedida!='Sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h') AND $ImpuestoVenta==0) /*Sino Tiene Impuesto*/
	          {
	            $MontoExento=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	            $MontoGravado=0; 
	        
	          }
	          else if(($UnidadMedida!='Sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1') /*Si Tiene Impuesto*/ 
	          {
	            $MontoExento=0;
	            $MontoGravado=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	          }
	          else
		    {
			  $MontoExento=0;
			  $MontoGravado=0;	
			}
	          
	          if(($UnidadMedida=='Sp' || $UnidadMedida=='d' || $UnidadMedida=='h') AND $ImpuestoVenta==0)
	          {
	            $MontoSExento=bcdiv(($PrecioTotalBruto),1,2); /*Precio sin impuesto x Cantidad*/
	            $MontoSGravado=0;
	          }
	          else if(($UnidadMedida=='Sp' || $UnidadMedida=='d' || $UnidadMedida=='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1')
	        {
	          $MontoSExento=0;/*Precio con impuesto x Cantidad*/
	            $MontoSGravado=bcdiv(($PrecioTotalBruto),1,2);
	        }
	        else
	        {
	          $MontoSExento=0;
	          $MontoSGravado=0;
	        }     

	          $MontoIV=bcdiv(((($PrecioTotalBruto)-(($PrecioTotalBruto)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
	          $MontoOtroI=0;/*se va a cambiar mas adelante*/
	          $MontoDescuento=bcdiv((($PrecioTotalBruto)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


	          if(($Exonerado=='1' AND $ImpuestoVenta!=0)OR($Exonerado=='1' AND $ImpuestoVenta==0))//es Exonerado
			    {
					$PorcentajeExoneracionTP=ObtenerPorcentajeGlobarExoneracion($FK_Cliente);
			        
			        if($PorcentajeExoneracionTP!='NO')
					{
						if(($UnidadMedida!='Sp'||$UnidadMedida!='Sp') AND $UnidadMedida!='d'AND $UnidadMedida!='h')
			    		{						
							$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
				    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExonerado),1,2);/*Subtotal-Descuento+Impuesto*/
						}
						else if(($UnidadMedida=='Sp'||$UnidadMedida=='Sp') OR $UnidadMedida=='d'OR $UnidadMedida=='h')
			    		{						
							$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
				    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoServicioExonerado),1,2);/*Subtotal-Descuento+Impuesto*/
						}
					}
					else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
					{
						
					}
				}
				else //No es Exonerado
				{
					$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    	$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
				}
				
			    /*
			    $NodoIVA= ($Exonerado=='1')? 	'"impuesto": [
											       {
											         "codigoTarifa": "08",
										             "codigo": "01",
										             "tarifa": "'.$ImpuestoVenta.'",
										             "monto": "'.$MontoIV.'",
										             "factorIVA": "",
										             "exoneracion": {
									                  "Tipodocumento": "'.$TipoDocumentoExo.'",
										              "NumeroDocumento": "'.$NumeroDocumentoExo.'",
										              "NombreInstitucion": "'.$NombreInstitucionExo.'",
										              "FechaEmision": "'.$FechaEmisionExo.'",
										              "porcentajeExoneracion": "'.$PorcentajeExoneracionTP.'",
										              "montoExoneracion": "'.bcdiv(($MontoExonerado+$MontoServicioExonerado),1,2).'"
										            }
											       }
											     ],'
									     :"";
	          */
	          
	          if($ImpuestoVenta=='0.00')//Es producto exento sin importar el cliente
	          {
			 $DetalleNota.='{
						         "numero": "'.$NoLinea.'",
						         "cantidad": "'.$Cantidad.'",
						         "codigoComercial": [
						           {
						           	 "tipo": "01",
						             "codigo": "'.$IDProducto.'"
						           }
						         ],
						         "unidadMedidaComercial": "",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioTotalBruto).'",
						         "descuento": [
					               {
					               	 "monto": "'.$MontoDescuento.'",
					            	 "descripcionDescuento": "Descuento"
					               }
					             ],
						         "subtotal": "'.$SubtotalL.'",
						         "impuestoNeto": "'.bcdiv(($MontoIV-($MontoExonerado+$MontoServicioExonerado)),1,2).'",
						         "totalLinea": "'.$TotalL.'",
						         "partidaArancelaria": "",
        						 "baseImponible": "0.00000"
						       },';
			  }
			  else if($ImpuestoVenta!='0.00' AND $Exonerado!='1')//No Es producto exento con cliente no exonerado
			  {
			 $DetalleNota.='{
						         "numero": "'.$NoLinea.'",
						         "cantidad": "'.$Cantidad.'",
						         "codigoComercial": [
						           {
						           	 "tipo": "01",
						             "codigo": "'.$IDProducto.'"
						           }
						         ],
						         "unidadMedidaComercial": "",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioTotalBruto).'",
						         "descuento": [
					               {
					               	 "monto": "'.$MontoDescuento.'",
					            	 "descripcionDescuento": "Descuento"
					               }
					             ],
						         "subtotal": "'.$SubtotalL.'",
						         "impuesto": [
							       {
							         "codigoTarifa": "08",
						             "codigo": "01",
						             "tarifa": "'.$ImpuestoVenta.'",
						             "monto": "'.$MontoIV.'",
						             "factorIVA": ""
							       }
							     ],
							     "impuestoNeto": "'.bcdiv(($MontoIV-($MontoExonerado+$MontoServicioExonerado)),1,2).'",
						         "totalLinea": "'.$TotalL.'",
						         "partidaArancelaria": "",
        						 "baseImponible": "0.00000"

						       },';
			  }
			  else if($ImpuestoVenta!='0.00' AND $Exonerado=='1')//no es producto exento con cliente exonerado
	          {
	         $DetalleNota.='{
						         "numero": "'.$NoLinea.'",
						         "cantidad": "'.$Cantidad.'",
						         "codigoComercial": [
						           {
						           	 "tipo": "01",
						             "codigo": "'.$IDProducto.'"
						           }
						         ],
						         "unidadMedidaComercial": "",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioTotalBruto).'",
						         "descuento": [
					               {
					               	 "monto": "'.$MontoDescuento.'",
					            	 "descripcionDescuento": "Descuento"
					               }
					             ],
						         "subtotal": "'.$SubtotalL.'",
						         "impuesto": [
							       {
							         "codigoTarifa": "08",
						             "codigo": "01",
						             "tarifa": "'.$ImpuestoVenta.'",
						             "monto": "'.$MontoIV.'",
						             "factorIVA": "",
						             "exoneracion": {
					                  "Tipodocumento": "'.$TipoDocumentoExo.'",
						              "NumeroDocumento": "'.$NumeroDocumentoExo.'",
						              "NombreInstitucion": "'.$NombreInstitucionExo.'",
						              "FechaEmision": "'.$FechaEmisionExo.'",
						              "porcentajeExoneracion": "'.$PorcentajeExoneracionTP.'",
						              "montoExoneracion": "'.bcdiv(($MontoExonerado+$MontoServicioExonerado),1,2).'"
						            }
							       }
							     ],
							     "impuestoNeto": "'.bcdiv(($MontoIV-($MontoExonerado+$MontoServicioExonerado)),1,2).'",
						         "totalLinea": "'.$TotalL.'",
						         "partidaArancelaria": "",
        						 "baseImponible": "0.00000"
						       },';
			  }
	         
	  }
	  
	  return rtrim($DetalleNota,',');
}

function ObtenerIDFactura($FK_Usuario,$FK_Cliente,$NoReferencia)
{
	$IDFactura;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$sql="SELECT IDFactura FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and NoFactura='$NoReferencia';";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $IDFactura=$row["IDFactura"];
    }
    
	return $IDFactura;
}

function ObtenerIDNotaAfectada($FK_Usuario,$FK_Cliente,$NumeroDoc)
{
	$IDFactura;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$sql="SELECT IDNota FROM notacreditodebito where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and NoNota='$NumeroDoc';";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $IDFactura=$row["IDNota"];
    }
    
	return $IDFactura;
}

function ObtenerNoRefFactura($FK_Usuario,$FK_Cliente,$NoReferencia='',$IDNota='')
{
	$NoFactura;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	if($NoReferencia!='')
	{
		$sql="SELECT NoFactura FROM notacreditodebito where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and NoNota='$NoReferencia';";
	}
	else if($IDNota!='')
	{
		$sql="SELECT NoFactura FROM notacreditodebito where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDNota=$IDNota;";
	}

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $NoFactura=$row["NoFactura"];
    }
    
	return $NoFactura;
}

function ObtenerNoRefNotaCredito($FK_Usuario,$FK_Cliente,$IDNotaDebito)
{
	$NoRefND;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$sql="SELECT NoFactura FROM notacreditodebito where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDNota=$IDNotaDebito;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $NoRefND=$row["NoFactura"];
    }
    
	return $NoRefND;
}

function ObtenerCantNC($IDFactura,$FK_Usuario,$FK_Cliente)
{
	$CantNC=0;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$sql="SELECT CantidadNotasCredito FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDFactura=$IDFactura;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $CantNC=(int)$row["CantidadNotasCredito"];
    }
    
    return $CantNC;
}

function ObtenerTipoDevolucion($IDFactura,$FK_Usuario,$FK_Cliente)
{
	$TipoDevolucion='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$sql="SELECT TipoDevolucion FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDFactura=$IDFactura;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $TipoDevolucion=$row["TipoDevolucion"];
    }
    
    return $TipoDevolucion;
}

function ObtenerFechaFacturaAfectada($IDFactura,$FK_Usuario,$FK_Cliente)
{
	$FechaFactura;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$sql="SELECT DATE_FORMAT(Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDFactura=$IDFactura;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $FechaFactura=$row["Fecha"];
    }
    
	return $FechaFactura;
}

function ObtenerFechaNotaCreditoAfectada($IDNota,$FK_Usuario,$FK_Cliente)
{
	$FechaNota;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$sql="SELECT DATE_FORMAT(Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha FROM notacreditodebito where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDNota=$IDNota;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $FechaNota=$row["Fecha"];
    }
    
	return $FechaNota;
}

function ObtenerID_API($FK_Usuario)
{
	$ID_API='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

	$sql="SELECT ID_API FROM usuario where IDUsuario=$FK_Usuario;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $ID_API=$row["ID_API"];
    }
    
	return $ID_API;			
}

function ObtenerClaveLarga($IDDocumento,$FK_Usuario,$FK_Cliente)
{
	$ClaveLarga='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

	$sql="SELECT Clave FROM notacreditodebito where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDNota=$IDDocumento;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $ClaveLarga=$row["Clave"];
    }
    
	return $ClaveLarga;			
} 

function ConvertirUnidades($UnidadMedida)
{
	$UM='';

	switch ($UnidadMedida) 
	{
	    case "Unidad":
	        $UM='Unid';
	        break;
	    case "Kg":
	        $UM='kg';
	        break;
	    case "oz":
	        $UM='Oz';
	        break;
	    case "l":
	        $UM='L';
	        break;
	    case "gal":
	        $UM='Gal';
	        break;
	    case "m":
	        $UM='m';
	        break;
	    case "min":
	        $UM='min';
	        break;
	    case "h":
	        $UM='h';
	        break;
	    case "d":
	        $UM='d';
	        break;
	    case "ml":
	        $UM='mL';
	        break;
	    case "g":
	        $UM='g';
	        break;
	    case "t":
	        $UM='t';
	        break;
	    case "sp":
	        $UM='Sp';
	        break;
	    default:
	        $UM='';
	}
	
	return $UM;	
}

function ObtenerPorcentajeGlobarExoneracion($CedulaCliente)
{
	//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
    
    $PorcentajeExoneracionTP='';

    $sql="SELECT
		 (
		 CASE 
		     WHEN TodosProductosExonerados=1 THEN PorcentajeExoneracion
		     ELSE 'NO'
		 END
		 ) AS PorcentajeExoneracion
		 FROM cliente
		 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";

    $Resutado="";
    
    $result=$Conexion->query($sql);

    $row = $result->fetch_assoc();
    $PorcentajeExoneracionTP=$row["PorcentajeExoneracion"];	
    
    return $PorcentajeExoneracionTP;
}

function ObtenerInfoExoneracion($CedulaCliente)
{
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$InfoExoneracion= array();
	
	$sql="SELECT TipoExoneracion,NoExoneracion,NombreInstitucionExoneracion,
	  				 DATE_FORMAT(FechaExoneracion,'%Y-%m-%dT%H:%i:%s%-06:00') AS FechaExoneracion FROM cliente
	  				 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";
	  				 
	  	$result=$Conexion->query($sql);

	    $row = $result->fetch_assoc();
	    
	    $TipoDocumentoExo=$row["TipoExoneracion"];
		$NumeroDocumentoExo=$row["NoExoneracion"];
		$NombreInstitucionExo=$row["NombreInstitucionExoneracion"];
		$FechaEmisionExo=$row["FechaExoneracion"];	
		
		$InfoExoneracion[]=array(
								"TipoDocumento"=>$TipoDocumentoExo,
								"NumeroDocumentoExo"=>$NumeroDocumentoExo,
								"NombreInstitucionExo"=>$NombreInstitucionExo,
								"FechaEmisionExo"=>$FechaEmisionExo
								);
		return $InfoExoneracion;	
}

?>