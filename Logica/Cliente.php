<?php
	
session_start();
require ("../Conexion/Conexion.php");


//Comprbar si el usuario presiono el boton de Login

if(isset($_POST["btnEnviarCliente"]))
{
	GuardarOModificarCliente();
}
else if(isset($_POST['MostrarDatos']))/*Para consultar y llenar los campos*/
{
	ConsultarCliente();
}


//Funcion para Guardar o Modificar

function GuardarOModificarCliente()
{
	$GuardarModificar=$_POST["GuardarModificar"];
	
	$Respuesta='';
	$GuarMod='';

	#ver si es guardar o Modificar
	
	if($GuardarModificar=="Guardar")
	{
		$Cedula=$_POST["txtCedula"];
		$FK_Usuario=$_SESSION['IDUsuario'];
		$TipoCedula=$_POST["cbmTipoCedula"];
		$Nombre=$_POST["txtNombre"];
		$Direccion=$_POST["txtDireccion"];
		$Telefono=$_POST["txtTelefono"];
		$Email1=$_POST["txtEmail1"];
		$Email2=$_POST["txtEmail2"];
		
		$FechaIngreso='now()';
		$FechaUltimaFacturacion='null';
		
		$Provincia=$_POST["cbmProvincia"];
		$Canton=$_POST["cbmCanton"];
		$Distrito=$_POST["cbmDistrito"];
		$Barrio=$_POST["cbmBarrio"];
		
		$Zona=$Provincia.$Canton.$Distrito.$Barrio;
		$Exonerado=$_POST["cbmExonerado"];
		
		if($Exonerado=='1')
		{
			$DescripcionExoneracion="'".$_POST['txtDescExoneracion']."'";	
			$TipoExoneracion="'".$_POST["cbmTipoExoneracion"]."'";	
			$NoExoneracion="'".$_POST["txtNoExoneracion"]."'";	
			$NombreInstitucionExoneracion="'".$_POST["txtNombreInstitucion"]."'";
			$FechaExoneracion="'".DateTime::createFromFormat('d-m-Y',$_POST["txtFechaExoneracion"])->format('Y-m-d H:i:s')."'";	
			$PorcentajeExoneracion=($_POST["txtPorcExoneracion"]!='')?"".$_POST["txtPorcExoneracion"]."":'null';	
			$PeriodoVigenciaExoneracion="".$_POST["VigenciaExoneracion"]."";	
			$TodosProductosExoneracion="".$_POST["TodosProductosExonerados"]."";	
		}
		else
		{
			$DescripcionExoneracion="null";	
			$TipoExoneracion="null";	
			$NoExoneracion="null";	
			$NombreInstitucionExoneracion="null";
			$FechaExoneracion="null";	
			$PorcentajeExoneracion="null";	
			$PeriodoVigenciaExoneracion="null";	
			$TodosProductosExoneracion="null";
		}

		$Expediente=$_POST["txtExpediente"];
		
		$Activo=$_POST["cbmEstado"];
		
		
		# guardar...

		#sino existe: verificar la cedula del campo txtCedula en la BD
		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		} 

		$sql = "SELECT Cedula FROM cliente WHERE Cedula = '" .$Cedula."' AND FK_Usuario='".$FK_Usuario."'";
							
		$result = $Conexion->query($sql);

		if ($result->num_rows > 0) 
		{
		$Respuesta = "El cliente que intenta guardar ya existe en la Base de Datos";
		$GuarMod='Error';  
		}
		else 
		{			
			//sanitize el sql
		    $sql = "insert into cliente
					(Cedula,FK_Usuario,Nombre,Direccion,Telefono,FechaIngreso,FechaUltimaFacturacion,
					TipoCedula,Email1,Email2,Zona,Exonerado,Activo,Expediente,DescripcionExoneracion,
					TipoExoneracion,NoExoneracion,NombreInstitucionExoneracion,FechaExoneracion,
					PorcentajeExoneracion,VigenciaExoneracion,TodosProductosExonerados)
					values('$Cedula',$FK_Usuario,'$Nombre','$Direccion','$Telefono',$FechaIngreso,$FechaUltimaFacturacion,
					'$TipoCedula','$Email1','$Email2','$Zona',$Exonerado,$Activo,'$Expediente',$DescripcionExoneracion,
					$TipoExoneracion,$NoExoneracion,$NombreInstitucionExoneracion,$FechaExoneracion,
					$PorcentajeExoneracion,$PeriodoVigenciaExoneracion,$TodosProductosExoneracion);";
							
			if($Conexion->query($sql) === TRUE) 
			{  
			  $Respuesta = "Cliente guardado exitosamente";
			  $GuarMod='Guardo';  			  
			} 
			else 
			{
	    		 $Respuesta = "Error al guardar el cliente";
	    		 $GuarMod='Error'; 
			}

		}
	}
	else if($GuardarModificar=="Modificar")
	{
		# modificar...
		$Cedula=$_POST["txtCedula"];
		$FK_Usuario=$_SESSION['IDUsuario'];
		$TipoCedula=$_POST["cbmTipoCedula"];
		$Nombre=$_POST["txtNombre"];
		$Direccion=$_POST["txtDireccion"];
		$Telefono=$_POST["txtTelefono"];
		$Email1=$_POST["txtEmail1"];
		$Email2=$_POST["txtEmail2"];
		
		$FechaIngreso=DateTime::createFromFormat('d-m-Y', $_POST['txtFechaIngreso'])->format('Y-m-d');
		$FechaUltimaFacturacion=($_POST["txtFechaUltimaFacturacion"]!="") ? DateTime::createFromFormat('d-m-Y H:i:s', $_POST["txtFechaUltimaFacturacion"])->format('Y-m-d H:i:s') : 'null';
		
		$Provincia=$_POST["cbmProvincia"];
		$Canton=$_POST["cbmCanton"];
		$Distrito=$_POST["cbmDistrito"];
		$Barrio=$_POST["cbmBarrio"];
		
		$Zona=$Provincia.$Canton.$Distrito.$Barrio;
		$Exonerado=$_POST["cbmExonerado"];
		
		if($Exonerado=='1')
		{
			$DescripcionExoneracion="'".$_POST['txtDescExoneracion']."'";	
			$TipoExoneracion="'".$_POST["cbmTipoExoneracion"]."'";	
			$NoExoneracion="'".$_POST["txtNoExoneracion"]."'";	
			$NombreInstitucionExoneracion="'".$_POST["txtNombreInstitucion"]."'";
			$FechaExoneracion="'".DateTime::createFromFormat('d-m-Y',$_POST["txtFechaExoneracion"])->format('Y-m-d H:i:s')."'";	
			$PorcentajeExoneracion=($_POST["txtPorcExoneracion"]!='')?"".$_POST["txtPorcExoneracion"]."":'null';	
			$PeriodoVigenciaExoneracion="".$_POST["VigenciaExoneracion"]."";	
			$TodosProductosExoneracion="".$_POST["TodosProductosExonerados"]."";	
		}
		else
		{
			$DescripcionExoneracion="null";	
			$TipoExoneracion="null";	
			$NoExoneracion="null";	
			$NombreInstitucionExoneracion="null";
			$FechaExoneracion="null";	
			$PorcentajeExoneracion="null";	
			$PeriodoVigenciaExoneracion="null";	
			$TodosProductosExoneracion="null";
		}

		$Expediente=$_POST["txtExpediente"];
		
		$Activo=$_POST["cbmEstado"];
		 
		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		} 	
			//sanitize el sql
	    
		if($FechaUltimaFacturacion!='null')
		{
			$sql = "UPDATE cliente SET Nombre='$Nombre',Direccion='$Direccion',Telefono='$Telefono',FechaIngreso='$FechaIngreso',
			        FechaUltimaFacturacion='$FechaUltimaFacturacion',TipoCedula='$TipoCedula',Email1='$Email1',Email2='$Email2',
			        Zona='$Zona',Exonerado=$Exonerado,Activo=$Activo,Expediente='$Expediente', 
			        DescripcionExoneracion=$DescripcionExoneracion,TipoExoneracion=$TipoExoneracion,NoExoneracion=$NoExoneracion,
			        NombreInstitucionExoneracion=$NombreInstitucionExoneracion,FechaExoneracion=$FechaExoneracion,
			        PorcentajeExoneracion=$PorcentajeExoneracion,VigenciaExoneracion=$PeriodoVigenciaExoneracion,
			        TodosProductosExonerados=$TodosProductosExoneracion
					WHERE Cedula='$Cedula' AND FK_Usuario=$FK_Usuario;";
		}
		else
		{
			$sql = "UPDATE cliente SET Nombre='$Nombre',Direccion='$Direccion',Telefono='$Telefono',FechaIngreso='$FechaIngreso',
					FechaUltimaFacturacion=null,TipoCedula='$TipoCedula',Email1='$Email1',Email2='$Email2',Zona='$Zona',
					Exonerado=$Exonerado,Activo=$Activo,Expediente='$Expediente',DescripcionExoneracion=$DescripcionExoneracion,
					TipoExoneracion=$TipoExoneracion,NoExoneracion=$NoExoneracion,
					NombreInstitucionExoneracion=$NombreInstitucionExoneracion,FechaExoneracion=$FechaExoneracion,
			        PorcentajeExoneracion=$PorcentajeExoneracion,VigenciaExoneracion=$PeriodoVigenciaExoneracion,
			        TodosProductosExonerados=$TodosProductosExoneracion
					WHERE Cedula='$Cedula' AND FK_Usuario=$FK_Usuario;";
		}
						
		if($Conexion->query($sql) === TRUE) 
		{  
		  $Respuesta = "Cliente modificado exitosamente";
		  $GuarMod='Modifico';  			  
		} 
		else 
		{
    		 $Respuesta = "Error al modificar el cliente";
    		 $GuarMod='Error'; 
		}
	}
	
	$users_arr[] = array( 
                            "Respuesta"=>$Respuesta,
                            "GuarMod"=>$GuarMod,
                       );

      // encoding array to json format
      echo json_encode($users_arr);
      exit;
	
	  	/*si existe: modifica revisa que los cmb no esten seleccionado el campo Provincia canton o distrito, sin cambiar la cedula

	 		
	 		si la cedula existe: error ya hay un usuario en la base de datos con el numero de cedula ingresar
	 		*/
}

function ConsultarCliente()
{
	$CedulaCliente=$_POST['CedulaCliente'];
	$FK_Usuario=$_SESSION['IDUsuario'];

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	if ($Conexion->connect_error) 
	{
		die("Connection failed: " . $Conexion->connect_error);
	} 

/*no */
	$sql = "SELECT Cedula,Nombre,TotalVentas,TotalDebitos,TotalCreditos,Direccion,Telefono,DATE_FORMAT(FechaIngreso,'%d-%m-%Y') as FechaIngreso,DATE_FORMAT(FechaUltimaFacturacion,'%d-%m-%Y  %h:%i:%s') as FechaUltimaFacturacion,TipoCedula,
				   Email1,Email2,Zona,Exonerado,Activo,Expediente,DescripcionExoneracion,
					TipoExoneracion,NoExoneracion,NombreInstitucionExoneracion,DATE_FORMAT(FechaExoneracion,'%d-%m-%Y') as FechaExoneracion,
					PorcentajeExoneracion,VigenciaExoneracion,TodosProductosExonerados
			FROM cliente WHERE Cedula='$CedulaCliente' AND FK_USuario=$FK_Usuario;";
						
	$result = $Conexion->query($sql);

	if ($result->num_rows > 0) 
	{
		$row = $result->fetch_assoc();

		$Cedula= $row["Cedula"];
		$Nombre= $row["Nombre"];
		$TotalVentas= $row["TotalVentas"];
		$TotalDebitos= $row["TotalDebitos"];
		$TotalCreditos= $row["TotalCreditos"];
		$Direccion= $row["Direccion"];
		$Telefono= $row["Telefono"];
		$FechaIngreso= $row["FechaIngreso"];
		$FechaUltimaFacturacion= $row["FechaUltimaFacturacion"];
		$TipoCedula= $row["TipoCedula"];
		$Email1= $row["Email1"];
		$Email2= $row["Email2"];
		$Zona= $row["Zona"];
		$Exonerado= $row["Exonerado"];
		$Activo= $row["Activo"];
		$Expediente= $row["Expediente"];
		
		$DescripcionExoneracion=$row['DescripcionExoneracion'];	
		$TipoExoneracion=$row["TipoExoneracion"];	
		$NoExoneracion=$row["NoExoneracion"];	
		$NombreInstitucionExoneracion=$row["NombreInstitucionExoneracion"];
		$FechaExoneracion=$row["FechaExoneracion"];	
		$PorcentajeExoneracion=$row["PorcentajeExoneracion"];	
		$VigenciaExoneracion=$row["VigenciaExoneracion"];	
		$TodosProductosExonerados=$row["TodosProductosExonerados"];

	}
 
	$users_arr[] = array( 
						"Cedula"=>$Cedula,
						"Nombre"=>$Nombre,
						"TotalVentas"=>$TotalVentas,
						"TotalDebitos"=>$TotalDebitos,
						"TotalCreditos"=>$TotalCreditos,
						"Direccion"=>$Direccion,
						"Telefono"=>$Telefono,
						"FechaIngreso"=>$FechaIngreso,
						"FechaUltimaFacturacion"=>$FechaUltimaFacturacion,
						"TipoCedula"=>$TipoCedula,
						"Email1"=>$Email1,
						"Email2"=>$Email2,
						"Zona"=>$Zona,
						"Exonerado"=>$Exonerado,
						"Activo"=>$Activo,
						"Expediente"=>$Expediente,
						
						"DescripcionExoneracion"=>$DescripcionExoneracion,	
						"TipoExoneracion"=>$TipoExoneracion,	
						"NoExoneracion"=>$NoExoneracion,	
						"NombreInstitucionExoneracion"=>$NombreInstitucionExoneracion,
						"FechaExoneracion"=>$FechaExoneracion,	
						"PorcentajeExoneracion"=>$PorcentajeExoneracion,	
						"VigenciaExoneracion"=>$VigenciaExoneracion,	
						"TodosProductosExonerados"=>$TodosProductosExonerados
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}
?>
