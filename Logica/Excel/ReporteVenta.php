<?php session_start();?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
				    <!--Librerias para el modal -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<title></title>
	</head>
	<body>
		<div class="modal fade" id="ModalMSJ" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                      <h4 class="modal-title" style="font-weight: bold; color:black;" id="exampleModalLabel">Reporte de Ventas</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div class="modal-body" style="color:black;" id="MSJ">
                      </div>
                      <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                    </div>
                  </div> 
	</body>
	
	  <script>
	
$('#ModalMSJ').on('hide.bs.modal', function (e) {
	window.close();
});
	
</script>
	
	
</html>	
<?php
/** Include PHPExcel */
require ('Classes/PHPExcel.php');
require('../../Conexion/Conexion.php');

//Comprbar si el usuario presiono el boton de EnviarGrupo
if(isset($_POST["btnGenerarReporteVentas"]))/*Para guardar o modificar*/
{
	$RangoFechas= $_POST['RangoFechas'];
	
	$FechaInicio= substr($RangoFechas,0,10);
	$FechaFin= substr($RangoFechas,13,24);
	
	$FechaInicio = DateTime::createFromFormat('d/m/Y', $FechaInicio)->format('Y-m-d');
	$FechaFin = DateTime::createFromFormat('d/m/Y', $FechaFin)->format('Y-m-d'); 
	
    $CedulaCliente= $_POST['CedulaCliente'];
    $Estado = EstadoPConsulta($_POST['Estado']);
    
    $Factura= $_POST['Factura'];
    $Tiquete= $_POST['Tiquete'];
    $NC= $_POST['NC'];
    $ND= $_POST['ND'];
    
    $_SESSION['FechaInicioRV']=$FechaInicio;
    $_SESSION['FechaFinRV']=$FechaFin;
    $_SESSION['CedulaClienteRV']=$CedulaCliente;
    $_SESSION['EstadoRV']=$Estado;
    $_SESSION['FacturaRV']=$Factura;
    $_SESSION['TiqueteRV']=$Tiquete;
    $_SESSION['NCRV']=$NC;
    $_SESSION['NDRV']=$ND;
	
}
else
{
	ReporteVentas();
}

function ReporteVentas()
{		
	$FechaInicio=$_SESSION['FechaInicioRV'];
    $FechaFin=$_SESSION['FechaFinRV'];
    $CedulaCliente=$_SESSION['CedulaClienteRV'];
    $Estado=$_SESSION['EstadoRV'];
    $Factura=$_SESSION['FacturaRV'];
    $Tiquete=$_SESSION['TiqueteRV'];
    $NC=$_SESSION['NCRV'];
    $ND=$_SESSION['NDRV'];
    
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    unset($_SESSION['FechaInicioRV'],$_SESSION['FechaFinRV'],$_SESSION['CedulaClienteRV'],$_SESSION['EstadoRV'],
          $_SESSION['FacturaRV'],$_SESSION['TiqueteRV'],$_SESSION['NCRV'],$_SESSION['NDRV']);
          
    $DatosInexistentes=0;
    $CantidadDoc=0;
    
    	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($Conexion->connect_error) 
		{
			die("Connection failed: " . $Conexion->connect_error);
		}
     		
		if(!empty($Factura))
		{
			$CantidadDoc++;
			
			$sqlFactura=TodasFacturas($Estado,$CedulaCliente,$FK_Usuario,$FechaInicio,$FechaFin);
			
			$resultFactura = $Conexion->query($sqlFactura); 
			
			if(mysqli_num_rows($resultFactura) == 0)
			{
				$DatosInexistentes++;
			}	
		}
		
		if(!empty($Tiquete))
		{
			$CantidadDoc++;
			
			$sqlTiquete=TodosTiquetes($Estado,$CedulaCliente,$FK_Usuario,$FechaInicio,$FechaFin);
			
			$resultTiquete = $Conexion->query($sqlTiquete); 
			
			if(mysqli_num_rows($resultTiquete)==0) 
			{
				$DatosInexistentes++;
			}	
		}
		
		if(!empty($ND))
		{
			$CantidadDoc++;
			
			$sqlNotasDebito=TodasNotasDebito($Estado,$CedulaCliente,$FK_Usuario,$FechaInicio,$FechaFin);
			
			$resultND = $Conexion->query($sqlNotasDebito); 
			
			if(mysqli_num_rows($resultND)==0) 
			{
				$DatosInexistentes++;
			}	
		}
		
		if(!empty($NC))
		{
			$CantidadDoc++;
			
			$sqlNotasCredito=TodasNotasCredito($Estado,$CedulaCliente,$FK_Usuario,$FechaInicio,$FechaFin);
			
			$resultNC = $Conexion->query($sqlNotasCredito); 
			
			if(mysqli_num_rows($resultNC)==0) 
			{
				$DatosInexistentes++;
			}	
		}
		
		if($DatosInexistentes==$CantidadDoc)//no hay datos porque la cant de doc a consultar ninguno tiene datos
		{
			echo "<script>";
			echo "$('#MSJ').html('Error: No hay datos para generar el reporte');";
            echo "$('#ModalMSJ').modal('show');";
            echo "</script>";
		}
		else//hay datos, generar el reporte
		{
			error_reporting(E_ALL);
			ini_set('display_errors', TRUE);
			ini_set('display_startup_errors', TRUE);

			// Create new PHPExcel object

			$objPHPExcel = new PHPExcel();

			// Set document properties

			$objPHPExcel->getProperties()->setCreator("Spyka")
										 ->setLastModifiedBy("Spyka")
										 ->setTitle("Reporte de Ventas")
										 ->setSubject("Reporte de Ventas")
										 ->setDescription("Reporte de Compras");

			// Add some data

			$objPHPExcel->setActiveSheetIndex(0)

						->setCellValue('F1', "Reporte de ventas del ".DateTime::createFromFormat('Y-m-d', $FechaInicio)->format('d-m-Y')." al ".DateTime::createFromFormat('Y-m-d', $FechaFin)->format('d-m-Y'))
						->setCellValue('A3', 'Tipo')
						->setCellValue('B3', '#Documento')
						->setCellValue('C3', 'Local')
						->setCellValue('D3', 'Terminal')
						->setCellValue('E3', 'Consecutivo')
						->setCellValue('F3', 'Clave')
						->setCellValue('G3', 'Mes')
						->setCellValue('H3', 'Año')
						->setCellValue('I3', 'Fecha')
						->setCellValue('J3', 'ID Emisor')
						->setCellValue('K3', 'Nombre Emisor')
						->setCellValue('L3', 'ID Receptor')
						->setCellValue('M3', 'Nombre Receptor')
						->setCellValue('N3', 'Estado DGT')
						->setCellValue('O3', 'Condición')
						->setCellValue('P3', 'Plazo')
						->setCellValue('Q3', 'Moneda')
						->setCellValue('R3', 'Tipo Cambio')
						->setCellValue('S3', 'Servicios Gravados')
						->setCellValue('T3', 'Servicios Exentos')
						->setCellValue('U3', 'Mercancias Gravadas')
						->setCellValue('V3', 'Mercancias Exentas')
						->setCellValue('W3', 'Subtotal Gravado')
						->setCellValue('X3', 'Subtotal Exento')
						->setCellValue('Y3', 'Subtotal Venta')
						->setCellValue('Z3', 'Descuento')
						->setCellValue('AA3', 'Venta Neta')
						->setCellValue('AB3', 'Impuesto')
						->setCellValue('AC3', 'Total Venta');
						
			$styleArrayTitulos = array(
			    'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => '3366FF'),
			        'size'  => 12,
			        'name'  => 'Arial'
			    ));
			    
			$styleArrayDatos = array(
			    'font'  => array(
			        'bold'  => true,
			        'size'  => 11
			    ));
						
			$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArrayTitulos);
			$objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A3:AC3')->applyFromArray($styleArrayTitulos);
			$objPHPExcel->getActiveSheet()->getStyle('A3:AC3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$rowCount = 5;//empezar de la fila 5 
			// Iterate through each result from the SQL query in turn
			// We fetch each database result row into $row in turn
			while($ri =  mysqli_fetch_array($resultFactura))
			{ 
			    // Set cell An to the "name" column from the database (assuming you have a column called name)
			    //    where n is the Excel row number (ie cell A1 in the first row)
			    $Fecha=DateTime::createFromFormat('Y-m-d H:i:s', $ri['Fecha'])->format('d-m-Y');//dar formato
			    $Mes=substr($Fecha, 3, 2);
			    $Ano=substr($Fecha, 6, 4);
			    
			    $CeldaInicio='A'.$rowCount;
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $ri['TipoDocumento']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $ri['#Documento']);
			    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $ri['Local']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $ri['Terminal']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $ri['Consecutivo']);
			    $objPHPExcel->getActiveSheet()->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );			    
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $ri['Clave']);
			    $objPHPExcel->getActiveSheet()->getStyle('F'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $Mes); 
			    $objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $Ano);
			    $objPHPExcel->getActiveSheet()->getStyle('H'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $Fecha); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $ri['ID Emisor']);
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $ri['NombreEmisor']);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $ri['ID Receptor']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $ri['NombreCliente']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $ri['Estado DGT']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $ri['Condicion']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $ri['Plazo']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $ri['Moneda']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $ri['TipoCambio']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $ri['Servicios Gravados']);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, $ri['Servicios Exentos']);
			    $objPHPExcel->getActiveSheet()->getStyle('T'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, $ri['Mercancias Gravadas']);
			    $objPHPExcel->getActiveSheet()->getStyle('U'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, $ri['Mercancias Exentas']);
			    $objPHPExcel->getActiveSheet()->getStyle('V'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, $ri['Subtotal Gravado']);
			    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, $ri['Subtotal Exento']);
			    $objPHPExcel->getActiveSheet()->getStyle('X'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, $ri['Subtotal']);
			    $objPHPExcel->getActiveSheet()->getStyle('Y'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount, $ri['Descuento']);
			    $objPHPExcel->getActiveSheet()->getStyle('Z'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount, $ri['Venta Neta']);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount, $ri['Impuesto']);
			    $objPHPExcel->getActiveSheet()->getStyle('AB'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$rowCount, $ri['Total']);
			    $objPHPExcel->getActiveSheet()->getStyle('AC'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);  
			    
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':AC'.$rowCount)->applyFromArray($styleArrayDatos);
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':R'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount.':AC'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
 
			    // Increment the Excel row counter
			    $rowCount++; 
			}
			
			$rowCount++;
			
			while($ri =  mysqli_fetch_array($resultTiquete))
			{ 
			    // Set cell An to the "name" column from the database (assuming you have a column called name)
			    //    where n is the Excel row number (ie cell A1 in the first row)
			    
			    $Fecha=DateTime::createFromFormat('Y-m-d H:i:s', $ri['Fecha'])->format('d-m-Y');//dar formato
			    $Mes=substr($Fecha, 3, 2);
			    $Ano=substr($Fecha, 6, 4);
			    
			    $CeldaInicio='A'.$rowCount;
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $ri['TipoDocumento']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $ri['#Documento']);
			    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $ri['Local']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $ri['Terminal']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $ri['Consecutivo']);
			    $objPHPExcel->getActiveSheet()->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );			    
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $ri['Clave']);
			    $objPHPExcel->getActiveSheet()->getStyle('F'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $Mes); 
			    $objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $Ano);
			    $objPHPExcel->getActiveSheet()->getStyle('H'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $Fecha); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $ri['ID Emisor']);
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $ri['NombreEmisor']);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $ri['ID Receptor']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $ri['NombreCliente']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $ri['Estado DGT']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $ri['Condicion']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $ri['Plazo']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $ri['Moneda']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $ri['TipoCambio']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $ri['Servicios Gravados']);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, $ri['Servicios Exentos']);
			    $objPHPExcel->getActiveSheet()->getStyle('T'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, $ri['Mercancias Gravadas']);
			    $objPHPExcel->getActiveSheet()->getStyle('U'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, $ri['Mercancias Exentas']);
			    $objPHPExcel->getActiveSheet()->getStyle('V'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, $ri['Subtotal Gravado']);
			    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, $ri['Subtotal Exento']);
			    $objPHPExcel->getActiveSheet()->getStyle('X'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, $ri['Subtotal']);
			    $objPHPExcel->getActiveSheet()->getStyle('Y'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount, $ri['Descuento']);
			    $objPHPExcel->getActiveSheet()->getStyle('Z'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount, $ri['Venta Neta']);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount, $ri['Impuesto']);
			    $objPHPExcel->getActiveSheet()->getStyle('AB'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$rowCount, $ri['Total']);
			    $objPHPExcel->getActiveSheet()->getStyle('AC'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);  
			    
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':AC'.$rowCount)->applyFromArray($styleArrayDatos);
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':R'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount.':AC'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
 
			    // Increment the Excel row counter
			    $rowCount++; 
			}
			
			$rowCount++;
			
			while($ri =  mysqli_fetch_array($resultND))
			{ 
			    // Set cell An to the "name" column from the database (assuming you have a column called name)
			    //    where n is the Excel row number (ie cell A1 in the first row)
			    
			    $Fecha=DateTime::createFromFormat('Y-m-d H:i:s', $ri['Fecha'])->format('d-m-Y');//dar formato
			    $Mes=substr($Fecha, 3, 2);
			    $Ano=substr($Fecha, 6, 4);
			    
			    $CeldaInicio='A'.$rowCount;
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $ri['TipoDocumento']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $ri['#Documento']);
			    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $ri['Local']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $ri['Terminal']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $ri['Consecutivo']);
			    $objPHPExcel->getActiveSheet()->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );			    
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $ri['Clave']);
			    $objPHPExcel->getActiveSheet()->getStyle('F'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $Mes); 
			    $objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $Ano);
			    $objPHPExcel->getActiveSheet()->getStyle('H'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $Fecha); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $ri['ID Emisor']);
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $ri['NombreEmisor']);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $ri['ID Receptor']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $ri['NombreCliente']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $ri['Estado DGT']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $ri['Condicion']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $ri['Plazo']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $ri['Moneda']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $ri['TipoCambio']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $ri['Servicios Gravados']);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, $ri['Servicios Exentos']);
			    $objPHPExcel->getActiveSheet()->getStyle('T'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, $ri['Mercancias Gravadas']);
			    $objPHPExcel->getActiveSheet()->getStyle('U'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, $ri['Mercancias Exentas']);
			    $objPHPExcel->getActiveSheet()->getStyle('V'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, $ri['Subtotal Gravado']);
			    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, $ri['Subtotal Exento']);
			    $objPHPExcel->getActiveSheet()->getStyle('X'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, $ri['Subtotal']);
			    $objPHPExcel->getActiveSheet()->getStyle('Y'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount, $ri['Descuento']);
			    $objPHPExcel->getActiveSheet()->getStyle('Z'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount, $ri['Venta Neta']);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount, $ri['Impuesto']);
			    $objPHPExcel->getActiveSheet()->getStyle('AB'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$rowCount, $ri['Total']);
			    $objPHPExcel->getActiveSheet()->getStyle('AC'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);  
			    
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':AC'.$rowCount)->applyFromArray($styleArrayDatos);
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':R'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount.':AC'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
 
			    // Increment the Excel row counter
			    $rowCount++; 
			}
			
			$rowCount++;
			
			while($ri =  mysqli_fetch_array($resultNC))
			{ 
			    // Set cell An to the "name" column from the database (assuming you have a column called name)
			    //    where n is the Excel row number (ie cell A1 in the first row)
			    
			    $Fecha=DateTime::createFromFormat('Y-m-d H:i:s', $ri['Fecha'])->format('d-m-Y');//dar formato
			    $Mes=substr($Fecha, 3, 2);
			    $Ano=substr($Fecha, 6, 4);
			    
			    $CeldaInicio='A'.$rowCount;
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $ri['TipoDocumento']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $ri['#Documento']);
			    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $ri['Local']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $ri['Terminal']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $ri['Consecutivo']);
			    $objPHPExcel->getActiveSheet()->getStyle('E'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );			    
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $ri['Clave']);
			    $objPHPExcel->getActiveSheet()->getStyle('F'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $Mes); 
			    $objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $Ano);
			    $objPHPExcel->getActiveSheet()->getStyle('H'.$rowCount)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $Fecha); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $ri['ID Emisor']);
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $ri['NombreEmisor']);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $ri['ID Receptor']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $ri['NombreCliente']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $ri['Estado DGT']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $ri['Condicion']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $ri['Plazo']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $ri['Moneda']); 
			    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $ri['TipoCambio']); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $ri['Servicios Gravados']);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1); 
			    
			    $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, $ri['Servicios Exentos']);
			    $objPHPExcel->getActiveSheet()->getStyle('T'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, $ri['Mercancias Gravadas']);
			    $objPHPExcel->getActiveSheet()->getStyle('U'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, $ri['Mercancias Exentas']);
			    $objPHPExcel->getActiveSheet()->getStyle('V'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, $ri['Subtotal Gravado']);
			    $objPHPExcel->getActiveSheet()->getStyle('W'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, $ri['Subtotal Exento']);
			    $objPHPExcel->getActiveSheet()->getStyle('X'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, $ri['Subtotal']);
			    $objPHPExcel->getActiveSheet()->getStyle('Y'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount, $ri['Descuento']);
			    $objPHPExcel->getActiveSheet()->getStyle('Z'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount, $ri['Venta Neta']);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			    $objPHPExcel->getActiveSheet()->getStyle('AA'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount, $ri['Impuesto']);
			    $objPHPExcel->getActiveSheet()->getStyle('AB'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			     
			    $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$rowCount, $ri['Total']);
			    $objPHPExcel->getActiveSheet()->getStyle('AC'.$rowCount)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);  
			    
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':AC'.$rowCount)->applyFromArray($styleArrayDatos);
			    $objPHPExcel->getActiveSheet()->getStyle($CeldaInicio.':R'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			    $objPHPExcel->getActiveSheet()->getStyle('S'.$rowCount.':AC'.$rowCount)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
 
			    // Increment the Excel row counter
			    $rowCount++; 
			}
			
			$rowCount++;
			
			foreach(array_merge(range('A','Z'), ['AA','AB','AC']) as $columnID) 
			{
    			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			}
			
			// Rename worksheet

			$objPHPExcel->getActiveSheet()->setTitle('Reporte de Ventas');


			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);


			// Save Excel 2007 file

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			ob_end_clean();
			// We'll be outputting an excel file
			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="Reporte de ventas del '.DateTime::createFromFormat('Y-m-d', $FechaInicio)->format('d-m-Y').' al '.DateTime::createFromFormat('Y-m-d', $FechaFin)->format('d-m-Y').'.xlsx');
			$objWriter->save('php://output');
			
		}
		
}

function EstadoPConsulta($Estado)
{
	$Condicion='';

	if($Estado=='SoloAceptado')
	{
		$Condicion="AND Status='aceptado'";
	}
    else if($Estado=='SoloNoAceptado')
	{
		$Condicion="AND Status!='aceptado'";
	}
	else
	{
		$Condicion='';
	}
	
	
	return $Condicion;	
}

function TodasFacturas($Estado,$Cedula,$FK_Usuario,$FechaInicio,$FechaFin)/*Todo de facturas sin clientes ni tipo doc*/
{
	$Fechas=($FechaInicio==$FechaFin)?"AND DATE(Fecha)='$FechaInicio' ":"AND Fecha BETWEEN '$FechaInicio' AND '$FechaFin'";
	
	$AgregadoSQL=(!empty($Cedula))?"AND FK_Cliente='$Cedula' ":"";
	
	$sql="SELECT

			CASE TipoDocumento 
									WHEN '01'      THEN 'Factura'
								   	WHEN '02'      THEN 'Nota Debito'
								  	WHEN '03'      THEN 'Nota Credito'
								   	WHEN '04'      THEN 'Tiquete'
								       END AS TipoDocumento,
			IDFactura AS  '#Documento',
			Terminal AS 'Local',
			Sucursal AS 'Terminal',
			NoFactura AS Consecutivo,
			factura.Clave AS Clave,
			Fecha,
			U.Cedula AS 'ID Emisor',
			U.NombreComercial AS NombreEmisor,
			FK_Cliente AS 'ID Receptor',
			C.Nombre AS NombreCliente,
			Status AS 'Estado DGT',
			CASE CondicionVenta 
									WHEN '01'      THEN 'Contado'
								   	WHEN '02'      THEN 'Credito'
								   	WHEN '04'      THEN 'Apartado'
								       END AS Condicion,
			Plazo,
			CASE TipoMoneda 
								WHEN 'C'      THEN 'CRC'
							   	WHEN 'D'      THEN 'USD'
							  	WHEN 'E'      THEN 'EUR'
							       END AS Moneda,
			TipoCambio,
			FORMAT(ServicioGravado,2) AS 'Servicios Gravados',
			FORMAT(ServicioExento,2) AS 'Servicios Exentos',
			FORMAT(MontoGravado,2) AS 'Mercancias Gravadas',
			FORMAT(MontoExento,2) AS 'Mercancias Exentas',
			FORMAT((ServicioGravado+MontoGravado),2) AS 'Subtotal Gravado',
			FORMAT((ServicioExento+MontoExento),2) AS 'Subtotal Exento',
			FORMAT(((ServicioGravado+MontoGravado)+(ServicioExento+MontoExento)),2) AS Subtotal,
			FORMAT(Descuento,2) AS Descuento,
			FORMAT((SubtotalFactura),2) as 'Venta Neta',
			FORMAT(Impuesto,2) AS Impuesto,
			FORMAT(TotalFactura,2) AS Total

			FROM factura INNER JOIN usuario U ON factura.FK_Usuario=U.IDUsuario
			             INNER JOIN cliente C ON factura.FK_Cliente=C.Cedula
			WHERE (TipoDocumento='01') $AgregadoSQL $Estado $Fechas AND factura.FK_Usuario=$FK_Usuario ORDER BY Fecha ASC";
			
	return $sql;
}

function TodosTiquetes($Estado,$Cedula,$FK_Usuario,$FechaInicio,$FechaFin)/*Todo de tiquetes sin clientes ni tipo doc*/
{	
	$Fechas=($FechaInicio==$FechaFin)?"AND DATE(Fecha)='$FechaInicio' ":"AND Fecha BETWEEN '$FechaInicio' AND '$FechaFin'";

	$sql="SELECT

			CASE TipoDocumento 
									WHEN '01'      THEN 'Factura'
								   	WHEN '02'      THEN 'Nota Debito'
								  	WHEN '03'      THEN 'Nota Credito'
								   	WHEN '04'      THEN 'Tiquete'
								       END AS TipoDocumento,
			IDFactura AS  '#Documento',
			Terminal AS 'Local',
			Sucursal AS 'Terminal',
			NoFactura AS Consecutivo,
			factura.Clave AS Clave,
			Fecha,
			U.Cedula AS 'ID Emisor',
			U.NombreComercial AS NombreEmisor,
			FK_Cliente AS 'ID Receptor',
			C.Nombre AS NombreCliente,
			Status AS 'Estado DGT',
			CASE CondicionVenta 
									WHEN '01'      THEN 'Contado'
								   	WHEN '02'      THEN 'Credito'
								   	WHEN '04'      THEN 'Apartado'
								       END AS Condicion,
			Plazo,
			CASE TipoMoneda 
								WHEN 'C'      THEN 'CRC'
							   	WHEN 'D'      THEN 'USD'
							  	WHEN 'E'      THEN 'EUR'
							       END AS Moneda,
			TipoCambio,
			FORMAT(ServicioGravado,2) AS 'Servicios Gravados',
			FORMAT(ServicioExento,2) AS 'Servicios Exentos',
			FORMAT(MontoGravado,2) AS 'Mercancias Gravadas',
			FORMAT(MontoExento,2) AS 'Mercancias Exentas',
			FORMAT((ServicioGravado+MontoGravado),2) AS 'Subtotal Gravado',
			FORMAT((ServicioExento+MontoExento),2) AS 'Subtotal Exento',
			FORMAT(((ServicioGravado+MontoGravado)+(ServicioExento+MontoExento)),2) AS Subtotal,
			FORMAT(Descuento,2) AS Descuento,
			FORMAT((SubtotalFactura),2) as 'Venta Neta',
			FORMAT(Impuesto,2) AS Impuesto,
			FORMAT(TotalFactura,2) AS Total

			FROM factura INNER JOIN usuario U ON factura.FK_Usuario=U.IDUsuario
			             INNER JOIN cliente C ON factura.FK_Cliente=C.Cedula
			WHERE (TipoDocumento='04') $Estado $Fechas AND factura.FK_Usuario=$FK_Usuario ORDER BY Fecha ASC";
			
	return $sql;
}

function TodasNotasDebito($Estado,$Cedula,$FK_Usuario,$FechaInicio,$FechaFin)/*Todo de notas sin clientes ni tipo doc*/
{
	$Fechas=($FechaInicio==$FechaFin)?"AND DATE(Fecha)='$FechaInicio' ":"AND Fecha BETWEEN '$FechaInicio' AND '$FechaFin'";
	
	$AgregadoSQL=(!empty($Cedula))?"AND FK_Cliente='$Cedula' ":"";
	
	$sql="SELECT

			CASE TipoDocumento 
									WHEN '01'      THEN 'Factura'
								   	WHEN '02'      THEN 'Nota Debito'
								  	WHEN '03'      THEN 'Nota Credito'
								   	WHEN '04'      THEN 'Tiquete'
								       END AS TipoDocumento,
			IDNota AS  '#Documento',
			Terminal AS 'Local',
			Sucursal AS 'Terminal',
			NoNota AS Consecutivo,
			notacreditodebito.Clave AS Clave,
			Fecha,
			U.Cedula AS 'ID Emisor',
			U.NombreComercial AS NombreEmisor,
			FK_Cliente AS 'ID Receptor',
			C.Nombre AS NombreCliente,
			Status AS 'Estado DGT',
			CASE CondicionVenta 
									WHEN '01'      THEN 'Contado'
								   	WHEN '02'      THEN 'Credito'
								   	WHEN '04'      THEN 'Apartado'
								       END AS Condicion,
			Plazo,
			CASE TipoMoneda 
								WHEN 'C'      THEN 'CRC'
							   	WHEN 'D'      THEN 'USD'
							  	WHEN 'E'      THEN 'EUR'
							       END AS Moneda,
			TipoCambio,
			FORMAT(ServicioGravado,2) AS 'Servicios Gravados',
			FORMAT(ServicioExento,2) AS 'Servicios Exentos',
			FORMAT(MontoGravado,2) AS 'Mercancias Gravadas',
			FORMAT(MontoExento,2) AS 'Mercancias Exentas',
			FORMAT((ServicioGravado+MontoGravado),2) AS 'Subtotal Gravado',
			FORMAT((ServicioExento+MontoExento),2) AS 'Subtotal Exento',
			FORMAT(((ServicioGravado+MontoGravado)+(ServicioExento+MontoExento)),2) AS Subtotal,
			FORMAT(Descuento,2) AS Descuento,
			FORMAT((SubtotalNota),2) as 'Venta Neta',
			FORMAT(Impuesto,2) AS Impuesto,
			FORMAT(TotalNota,2) AS Total

			FROM notacreditodebito INNER JOIN usuario U ON notacreditodebito.FK_Usuario=U.IDUsuario
			                       INNER JOIN cliente C ON notacreditodebito.FK_Cliente=C.Cedula
			WHERE (TipoDocumento='02') $AgregadoSQL $Estado $Fechas AND notacreditodebito.FK_Usuario=$FK_Usuario ORDER BY Fecha ASC";
			
	return $sql;
}

function TodasNotasCredito($Estado,$Cedula,$FK_Usuario,$FechaInicio,$FechaFin)/*Todo de notas sin clientes ni tipo doc*/
{
	$Fechas=($FechaInicio==$FechaFin)?"AND DATE(Fecha)='$FechaInicio' ":"AND Fecha BETWEEN '$FechaInicio' AND '$FechaFin'";
	
	$AgregadoSQL=(!empty($Cedula))?"AND FK_Cliente='$Cedula' ":"";
	
	$sql="SELECT

			CASE TipoDocumento 
									WHEN '01'      THEN 'Factura'
								   	WHEN '02'      THEN 'Nota Debito'
								  	WHEN '03'      THEN 'Nota Credito'
								   	WHEN '04'      THEN 'Tiquete'
								       END AS TipoDocumento,
			IDNota AS  '#Documento',
			Terminal AS 'Local',
			Sucursal AS 'Terminal',
			NoNota AS Consecutivo,
			notacreditodebito.Clave AS Clave,
			Fecha,
			U.Cedula AS 'ID Emisor',
			U.NombreComercial AS NombreEmisor,
			FK_Cliente AS 'ID Receptor',
			C.Nombre AS NombreCliente,
			Status AS 'Estado DGT',
			CASE CondicionVenta 
									WHEN '01'      THEN 'Contado'
								   	WHEN '02'      THEN 'Credito'
								   	WHEN '04'      THEN 'Apartado'
								       END AS Condicion,
			Plazo,
			CASE TipoMoneda 
								WHEN 'C'      THEN 'CRC'
							   	WHEN 'D'      THEN 'USD'
							  	WHEN 'E'      THEN 'EUR'
							       END AS Moneda,
			TipoCambio,
			FORMAT(ServicioGravado,2) AS 'Servicios Gravados',
			FORMAT(ServicioExento,2) AS 'Servicios Exentos',
			FORMAT(MontoGravado,2) AS 'Mercancias Gravadas',
			FORMAT(MontoExento,2) AS 'Mercancias Exentas',
			FORMAT((ServicioGravado+MontoGravado),2) AS 'Subtotal Gravado',
			FORMAT((ServicioExento+MontoExento),2) AS 'Subtotal Exento',
			FORMAT(((ServicioGravado+MontoGravado)+(ServicioExento+MontoExento)),2) AS Subtotal,
			FORMAT(Descuento,2) AS Descuento,
			FORMAT((SubtotalNota),2) as 'Venta Neta',
			FORMAT(Impuesto,2) AS Impuesto,
			FORMAT(TotalNota,2) AS Total

			FROM notacreditodebito INNER JOIN usuario U ON notacreditodebito.FK_Usuario=U.IDUsuario
			                       INNER JOIN cliente C ON notacreditodebito.FK_Cliente=C.Cedula
			WHERE (TipoDocumento='03') $AgregadoSQL $Estado $Fechas AND notacreditodebito.FK_Usuario=$FK_Usuario ORDER BY Fecha  ASC";
			
	return $sql;
}

?>
