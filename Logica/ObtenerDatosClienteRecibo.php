<?php
session_start();

require ("../Conexion/Conexion.php");

$request = $_POST['request'];   // request

// Get username list
if($request == 1){
    $search = $_POST['search'];

    $query = "SELECT * FROM cliente WHERE Cedula like '%".$search."%' AND Activo=1 and FK_Usuario=".$_SESSION['IDUsuario']." and Cedula NOT IN('000000000')"; /*Falta indicar el usuario*/
    $result = mysqli_query($con,$query);
    
    while($row = mysqli_fetch_array($result) ){
        $response[] = array("value"=>$row['Cedula'],"label"=>$row['Nombre']);
    }

    // encoding array to json format
    echo json_encode($response);
    exit;
}

// Get details
if($request == 2){
    $cid = $_POST['IDc'];
    $sql = "SELECT Cedula,Nombre FROM cliente WHERE Cedula like '%".$cid."%' and FK_Usuario=".$_SESSION['IDUsuario']." and Cedula NOT IN('000000000')";

    $result = mysqli_query($con,$sql);

    while( $row = mysqli_fetch_array($result)){
        $Cedula= $row['Cedula'];
        $Nombre= $row['Nombre'];
    }
    
    $sql="SELECT SUM(Saldo) AS SaldoActual FROM factura
		  WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND FK_Cliente='$cid' AND CondicionVenta='02' AND Saldo!=0.00;";
		  
	$result = mysqli_query($con,$sql);

    while( $row = mysqli_fetch_array($result)){
        $SaldoActual= number_format($row['SaldoActual'],2);
    }
    
    $sql="SELECT 
		 'Factura' AS TipoDocumento,
		 IDFactura, 
		 NoFactura, 
		 Fecha, 
		 Plazo, 
		 DATE_ADD(Fecha, INTERVAL Plazo DAY) AS 'Vence',
		 CAST((TotalFactura*TipoCambio) AS DECIMAL(14,2)) as TotalFactura,
		 0.00 AS Abono,
		 Saldo AS Saldo  
		 FROM factura
		 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND FK_Cliente='$cid' AND TipoDocumento='01' AND CondicionVenta='02'
		 AND Saldo!=0.00
		 ORDER BY Fecha DESC";
		  
	$result = mysqli_query($con,$sql);
	
	$FacturasCredito= array();

    while( $row = mysqli_fetch_array($result)){
        $TipoDocumento=$row['TipoDocumento'];
        $IDFactura=$row['IDFactura'];
        $NoFactura=$row['NoFactura'];
        $Fecha=DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha'])->format('d-m-Y');//dar formato
        $Plazo=$row['Plazo'].' días';
        $Vence=DateTime::createFromFormat('Y-m-d H:i:s', $row['Vence'])->format('d-m-Y');//dar formato
        $TotalFactura=number_format($row['TotalFactura'],2);
        $Abono=$row['Abono'];
        $Saldo=number_format($row['Saldo'],2);
        
	     $FacturasCredito[] = array(
	                             "TipoDocumento" => $TipoDocumento,
	                             "IDFactura" => $IDFactura,
	                             "NoFactura" => $NoFactura, 
	                             "Fecha" => $Fecha,
	                             "Plazo" => $Plazo, 
	                             "Vence" =>$Vence, 
	                             "TotalFactura" =>$TotalFactura, 
	                             "Abono"=>$Abono,//"<input type='text' class='abonos' value='$Abono'>",
	                             "Saldo"=>$Saldo,
	                            );
        
    }
    
    $sql="SELECT SUM(Saldo) AS SaldoVencido FROM factura
		  WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND FK_Cliente='$cid' AND CondicionVenta='02'
		  AND Saldo!=0.00
		  AND CAST(DATE_ADD(Fecha, INTERVAL Plazo DAY) AS DATE)<CAST(NOW() AS DATE);";
		  
	$result = mysqli_query($con,$sql);

    while( $row = mysqli_fetch_array($result)){
        $SaldoVencido= number_format($row['SaldoVencido'],2);
    }
    
    $sql="SELECT SUM(Saldo) AS SaldoSinVencer FROM factura
		  WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND FK_Cliente='$cid' AND CondicionVenta='02'
		  AND Saldo!=0.00
		  AND CAST(DATE_ADD(Fecha, INTERVAL Plazo DAY) AS DATE)>=CAST(NOW() AS DATE);";
		  
	$result = mysqli_query($con,$sql);

    while( $row = mysqli_fetch_array($result)){
        $SaldoSinVencer= number_format($row['SaldoSinVencer'],2);
    }
    
    /*obtener saldo anterior del cliente*/
    $sql="SELECT SaldoTotalAnterior
    	  FROM reciboabono
		  WHERE 
		  IDRecibo IN (SELECT MAX(IDRecibo) FROM reciboabono WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND FK_Cliente='$cid') LIMIT 1;";
		  
	$result = mysqli_query($con,$sql);
	
	if(mysqli_num_rows($result)==0) 
	{
		$SaldoAnterior=$SaldoActual;
	}
	else if(mysqli_num_rows($result)>0) 
	{
		while( $row = mysqli_fetch_array($result))
		{
        	$SaldoAnterior= number_format($row['SaldoTotalAnterior'],2);
    	}
	}
    
    $users_arr = array();
    
    $users_arr[] = 
    array("Cedula" => $Cedula, "Nombre" => $Nombre, "SaldoActual" => $SaldoActual, "FacturasCredito" => $FacturasCredito, "SaldoVencido" => $SaldoVencido, "SaldoSinVencer" => $SaldoSinVencer, "SaldoAnterior" => $SaldoAnterior);

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
}
?>