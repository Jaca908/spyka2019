<?php
session_start();

require ("../Conexion/Conexion.php");


if(isset($_POST['ObtenerNoRecibo'])) 
{
	ObtenerNoRecibo();
}
else if(isset($_POST['btnAbonar'])) 
{
	CalcularAbono();
}
else if(isset($_POST['btnAbonarMultiplesFacturas']))
{
	CalcularAbonoAutomaticoMultiple();
}
else if(isset($_POST['btnGrabarRecibo']))
{
	GrabarRecibo();
}
else if(isset($_POST['ConsultarEstadoCuentaCliente']))
{
	ConsultarEstadoCuentaCliente();
}
else if(isset($_POST['ReimprimirRecibo']))
{
	ObtenerDatosParaReimprimirRecibo();
}


function ObtenerNoRecibo()
{
	$FK_Usuario=$_SESSION['IDUsuario'];
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $sql="SELECT 
		  IFNULL((MAX(IDRecibo)+1),1) AS 'IDRecibo' 
		  FROM reciboabono
	 	  WHERE FK_Usuario=$FK_Usuario;";

    $NoRecibo="";
    
    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $NoRecibo=$row["IDRecibo"];
    }

    /*Pasar al vector las variables*/
    $users_arr[] = array( 
                         "NoRecibo"=>$NoRecibo,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
}

function CalcularAbono()
{
	$MontoAbono=QuitarFormatoNumero($_POST['MontoAbono']);
    $TotalFinalAbono=QuitarFormatoNumero($_POST['TotalFinalAbono']);
    $TotalSaldoActual=QuitarFormatoNumero($_POST['TotalSaldoActual']);
    $TipDoc=$_POST['TipDoc'];
	
	$Accion=$_POST['Accion'];
	
	if($Accion=='AbonoIndividual')//es una abono a una factura
	{
		if($TipDoc=='2' || $TipDoc=='4')//es abono o Nota Credito: suma al monto de abono/credito resta total saldo actual
		{
			$TotalFinalAbono=bcdiv(($TotalFinalAbono+$MontoAbono),1,2);
		    $TotalSaldoActual=bcdiv(($TotalSaldoActual-$MontoAbono),1,2);
		}
		else//Nota Debito: suma al monto de abono/credito suma total saldo actual
		{
			$TotalFinalAbono=bcdiv(($TotalFinalAbono+$MontoAbono),1,2);
		    $TotalSaldoActual=bcdiv(($TotalSaldoActual+$MontoAbono),1,2);
		}
		
		$users_arr[] = array( 
                         		"MontoAbono"=>number_format($MontoAbono,2),
                         		"TotalFinalAbono"=>number_format($TotalFinalAbono,2),
                         		"TotalSaldoActual"=>number_format($TotalSaldoActual,2),
                     		);
	}
	else if($Accion=='BorrarAbono')//borrar abono a una factura
	{
		if($TipDoc=='2' || $TipDoc=='4')//es abono o Nota Credito: suma al monto de abono/credito resta total saldo actual
		{
			$TotalFinalAbono=bcdiv(($TotalFinalAbono-$MontoAbono),1,2);
			$TotalSaldoActual=bcdiv(($TotalSaldoActual+$MontoAbono),1,2);
		}
		else//Nota Debito: suma al monto de abono/credito suma total saldo actual
		{
			$TotalFinalAbono=bcdiv(($TotalFinalAbono-$MontoAbono),1,2);
		    $TotalSaldoActual=bcdiv(($TotalSaldoActual-$MontoAbono),1,2);
		}
		
		$users_arr[] = array( 
                         		"MontoAbono"=>number_format('0.00',2),
                         		"TotalFinalAbono"=>number_format($TotalFinalAbono,2),
                         		"TotalSaldoActual"=>number_format($TotalSaldoActual,2),
                     		);
	}


    // encoding array to json format
    echo json_encode($users_arr);
    exit;
}

function CalcularAbonoAutomaticoMultiple()
{
	$MontoAbonoCancAut=QuitarFormatoNumero($_POST['MontoAbonoCancAut']);
    $TotalFinalAbono=QuitarFormatoNumero($_POST['TotalFinalAbono']);
    $TotalSaldoActual=QuitarFormatoNumero($_POST['TotalSaldoActual']);
    $Facturas=$_POST['FacturasData'];
    $TipDoc=$_POST['TipDoc'];
    
    $TotalFinalAbono=$MontoAbonoCancAut;
    
    if($TipDoc=='2' || $TipDoc=='4')//es abono o Nota Credito
	{
		$TotalSaldoActual=bcdiv(($TotalSaldoActual-$MontoAbonoCancAut),1,2);	
	}
	
	$FacturasAbonadas=array();
    
    foreach($Facturas as $i => $item) 
	{
		$TipoDoc=$Facturas[$i]["TipoDoc"];	
	    $IDFactura=$Facturas[$i]["IDFactura"];
	    $NoFacturaElect=$Facturas[$i]["NoFacturaElect"];
	    $Fecha=$Facturas[$i]["Fecha"];
	    $Plazo=$Facturas[$i]["Plazo"];
	    $Vence=$Facturas[$i]["Vence"];
	    $Monto=$Facturas[$i]["Monto"];
	    $Abono=$Facturas[$i]["Abono"];
	    
		$SaldoActualFactura= QuitarFormatoNumero($Facturas[$i]['SaldoActual']);
		
		if($TipDoc=='2' || $TipDoc=='4')//es abono o Nota Credito
		{
			if($SaldoActualFactura<=$MontoAbonoCancAut && $MontoAbonoCancAut>0)//si el saldo es menor o igual al monto que queda del monto a abonar y el monta a abonar es > 0
			{
				$MontoAbonoCancAut=bcdiv(($MontoAbonoCancAut-$SaldoActualFactura),1,2);//restar
				$Abono=bcdiv($SaldoActualFactura,1,2);
			}
			else if($SaldoActualFactura>$MontoAbonoCancAut && $MontoAbonoCancAut>0) //el monto a abonar disponible es menor al saldo de la factura
			{
				$Abono=bcdiv($MontoAbonoCancAut,1,2);
				$MontoAbonoCancAut=bcdiv(($MontoAbonoCancAut-$Abono),1,2);
			}	
		}
		
		$FacturasAbonadas[] = array(
	                                 "TipoDoc" => $TipoDoc,	
									 "IDFactura" => $IDFactura,
									 "NoFacturaElect" => $NoFacturaElect,
									 "Fecha" => $Fecha,
									 "Plazo" => $Plazo,
									 "Vence" => $Vence,
									 "Monto" => $Monto,
									 "Abono" => number_format($Abono,2),
									 "SaldoActual" => number_format($SaldoActualFactura,2),
	                               );
	
	}
	
	$users_arr[] = array( 
                     		"TotalFinalAbono"=>number_format($TotalFinalAbono,2),
                     		"TotalSaldoActual"=>number_format($TotalSaldoActual,2),
                     		"FacturasAbonadas"=>$FacturasAbonadas,
                 		);
                 		
    echo json_encode($users_arr);
    exit;            		
}

function QuitarFormatoNumero($Numero)
{
	//$Numero=number_format((float)$Numero, 2, '.', '');
	
	return str_replace(",", "", $Numero);
}

function GrabarRecibo()
{
	$Respuesta='';
	$GuarMod='';
	
	$IDRecibo=$_POST['IDRecibo'];
    $FechaRecibo=DateTime::createFromFormat('d-m-Y H:i:s', $_POST['FechaRecibo'])->format('Y-m-d H:i:s');
    $TipoDocAGenerar=$_POST['TipDoc'];
	$FK_Cliente=$_POST['CedulaCliente'];
    $TotalFinalAbono=QuitarFormatoNumero($_POST['TotalFinalAbono']);
    $TotalSaldoActual=QuitarFormatoNumero($_POST['TotalSaldoActual']);
    $TotalSaldoAnterior=QuitarFormatoNumero($_POST['TotalSaldoAnterior']);
    $Facturas=$_POST['FacturasData'];
    
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    $TotalRecibosAGuardar=0;
    $TotalRecibosAplicados=0;
    $TotalSaldosRestados=0;
    $TotalSaldosSumados=0;
    
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
        die("Connection failed: " . $Conexion->connect_error);
    }
    
    foreach($Facturas as $i => $item) 
	{
		$TipoDoc=$Facturas[$i]["TipoDoc"];	
	    $IDFactura=$Facturas[$i]["IDFactura"];
	    $NoFacturaElect=$Facturas[$i]["NoFacturaElect"];
	    $Fecha=$Facturas[$i]["Fecha"];
	    $Plazo=$Facturas[$i]["Plazo"];
	    $Vence=$Facturas[$i]["Vence"];
	    $Monto=QuitarFormatoNumero($Facturas[$i]["Monto"]);
	    $Abono=QuitarFormatoNumero($Facturas[$i]["Abono"]);
	    $SaldoAntesDeRecibo=QuitarFormatoNumero($Facturas[$i]['SaldoActual']);
	    $SaldoAnteriorFactura=$SaldoAntesDeRecibo;
	    
	    if($TipoDocAGenerar=='2'||$TipoDocAGenerar=='4')//es abono o Nota Credito restan al saldo de factura
	    {
			$SaldoActualFactura=($SaldoAntesDeRecibo-$Abono);	
		}
		else//es una nota de debito
		{
			$SaldoActualFactura=($SaldoAntesDeRecibo+$Abono);
		}
	    
	    $sql="SELECT IDRecibo FROM reciboabono WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND FK_Factura=$IDFactura AND IDRecibo=$IDRecibo;";

		$result=$Conexion->query($sql);

		if($result->num_rows==0 AND $Abono>0)
		{
			$TotalRecibosAGuardar++;
			
			//Guardar el recibo por cada factura que tenga abono que no sea 0.00
			$sql="Insert into reciboabono
		(IDRecibo,FK_Factura,FK_Cliente,FK_Usuario,TipoDocumento,Fecha,NoDocAfectado,Monto,SaldoAnteriorFactura,SaldoActualFactura,SaldoTotalAnterior)
 values($IDRecibo,$IDFactura,'$FK_Cliente',$FK_Usuario,'$TipoDocAGenerar','$FechaRecibo','$NoFacturaElect',$Abono,$SaldoAnteriorFactura,$SaldoActualFactura,$TotalSaldoAnterior);";

	        if($Conexion->query($sql) === TRUE) 
	        {
	        	$TotalRecibosAplicados++;
			}
		}
	}
    
    if($TotalRecibosAGuardar==$TotalRecibosAplicados)//Todo se guardo correctamente
    {
    	$FacturasSinAbono= array();
    	
		foreach($Facturas as $i => $item) 
		{	
		    $IDFactura=$Facturas[$i]["IDFactura"];
		    $Abono=QuitarFormatoNumero($Facturas[$i]["Abono"]);	
		
			if($Abono>0)
			{
				if($TipoDocAGenerar=='2' || $TipoDocAGenerar=='4')//Si el Documento a Generar es un Recibo o NotaCredito resta al saldo
				{
					//restar el monto del abono al saldo de la factura
					$sql="UPDATE factura SET Saldo=(Saldo-$Abono) WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND IDFactura=$IDFactura;";

			        if($Conexion->query($sql) === TRUE) 
			        {
			        	$TotalSaldosRestados++;
			        	$FacturasSinAbono[]=array($IDFactura);
					}
				}
				else //Si el Documento a Generar es una Nota Debito suma al saldo
				{
					$sql="UPDATE factura SET Saldo=(Saldo+$Abono) WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND IDFactura=$IDFactura;";

			        if($Conexion->query($sql) === TRUE) 
			        {
			        	$TotalSaldosRestados++;
			        	$FacturasSinAbono[]=array($IDFactura);
					}
				}
					
			}
		}
		
		if($TotalSaldosRestados==$TotalRecibosAplicados)
		{
			//Mostrar MSJ de exito
			$Respuesta='Datos guardados correctamente';			 
			$GuarMod='Guardo';
		}
		else
		{
			foreach($Facturas as $i => $item) 
			{	
			    $IDFactura=$Facturas[$i]["IDFactura"];
			    $Abono=QuitarFormatoNumero($Facturas[$i]["Abono"]);	
			
				if($Abono>0)
				{
					foreach($FacturasSinAbono as $FSA)
					{
						if($FSA==$IDFactura)
						{
							if($TipoDocAGenerar=='2'|| $TipoDocAGenerar=='4')//Si el Documento a Generar es un Recibo o NotaCredito suma al saldo si hay un error
							{
								//sumar el monto del abono al saldo de la factura
								$sql="UPDATE factura SET Saldo=(Saldo+$Abono) WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND IDFactura=$IDFactura;";

						        if($Conexion->query($sql) === TRUE) 
						        {
						        	$TotalSaldosSumados++;
								}	
							}
							else//Si el Documento a Generar es una Nota Debito resta al saldo si hay un error
							{
								$sql="UPDATE factura SET Saldo=(Saldo-$Abono) WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND IDFactura=$IDFactura;";

						        if($Conexion->query($sql) === TRUE) 
						        {
						        	$TotalSaldosSumados++;
								}
							}
						}
					}	
				}
			}
			
			if($TotalSaldosSumados==$FacturasSinAbono.count())
			{
				$sql="DELETE FROM reciboabono WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND IDRecibo=$IDRecibo;";

		        if($Conexion->query($sql) === TRUE) 
		        {
		        	//Mostrar MSJ de Error
		        	$Respuesta='Error al guardar los datos';			 
					$GuarMod='Error';
				}
			}
		}
	}
	else//borrar los recibos que se guardaron porque no se guardaron todos
	{
		$sql="DELETE FROM reciboabono WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND IDRecibo=$IDRecibo;";

        if($Conexion->query($sql) === TRUE) 
        {
        	//Mostrar MSJ de Error
        	$Respuesta='Error al guardar los datos';			 
			$GuarMod='Error';
		}
	}
    
    $users_arr[] = array( 

	                         /*Pasar al vector la Respuesta*/
	                         "Respuesta"=>$Respuesta,
	                         "GuarMod"=>$GuarMod,
	                     );

	    // encoding array to json format
	    echo json_encode($users_arr);
	    exit;
    
}

function ConsultarEstadoCuentaCliente()
{
	$FK_Cliente=$_POST['CedulaCliente'];
	$FK_Usuario=$_SESSION['IDUsuario'];
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
        die("Connection failed: " . $Conexion->connect_error);
    }
	
$sql=
"SELECT TipoDocumento,NoDoc,IDRecibo,Fecha,Vence,Monto,Saldo
FROM
(SELECT
CASE TipoDocumento 
	WHEN '01' THEN 'Factura'
END AS TipoDocumento,
NoFactura AS NoDoc,
'' AS IDRecibo,/*NoRecibo*/
Fecha,
DATE_ADD(Fecha, INTERVAL Plazo DAY) AS 'Vence',
CAST((TotalFactura*TipoCambio) AS DECIMAL(14,2)) AS Monto,
Saldo AS Saldo,
1 AS pos2
FROM factura
WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente' AND TipoDocumento='01' AND CondicionVenta='02'

UNION ALL

SELECT
CASE TipoDocumento 
   WHEN '2' THEN 'Recibo'
	WHEN '3' THEN 'Nota Debito'
  	WHEN '4' THEN 'Nota Credito'
	END AS TipoDocumento,
NoDocAfectado AS NoDoc,
IDRecibo,/*NoRecibo*/
Fecha,
'',
Monto,
'',
2
FROM reciboabono
WHERE FK_Usuario=$FK_Usuario AND FK_Cliente='$FK_Cliente') t
ORDER BY
    NoDoc,
    pos2,
	Fecha;
;";

	$result=$Conexion->query($sql);  

	$Documentos= array();
	
	while( $row = mysqli_fetch_array($result))
	{		
		$TipoDocumento=$row['TipoDocumento'];
		$NoDoc=($TipoDocumento!='Factura')?'':$row['NoDoc'];
		$IDRecibo=$row['IDRecibo'];
		$Fecha=DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha'])->format('d-m-Y');//dar formato
		$Vence=($row['Vence']=='')?'':DateTime::createFromFormat('Y-m-d H:i:s', $row['Vence'])->format('d-m-Y');//dar formato$row['Vence'];
		$Monto=number_format($row['Monto'],2);
		$Saldo=($row['Saldo']==NULL)?'':number_format($row['Saldo'],2);
		
		$Documentos[]=array(
							"TipoDocumento"=>$TipoDocumento,
							"NoDoc"=>$NoDoc,
							"IDRecibo"=>$IDRecibo,
							"Fecha"=>$Fecha,
							"Vence"=>$Vence,
							"Monto"=>$Monto,
							"Saldo"=>$Saldo,
							);
	}
	
	$users_arr = array();
    
    $users_arr[] = 
    array("Documentos" => $Documentos);

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
	
}

function ObtenerDatosParaReimprimirRecibo()
{
	$NoRecibo=$_POST['NoRecibo'];
	$CedulaCliente=$_POST['CedulaCliente'];
	$MontoTotalRecibo=QuitarFormatoNumero($_POST['MontoTotalRecibo']);
	$FK_Usuario=$_SESSION['IDUsuario'];
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
        die("Connection failed: " . $Conexion->connect_error);
    }
	
	//Trer la lista de comprobantes
	
	$sql="SELECT NoDocAfectado,Monto as Abono
		  FROM reciboabono
		  WHERE FK_Usuario=$FK_Usuario AND IDRecibo=$NoRecibo AND FK_Cliente='$CedulaCliente'
		  ORDER BY Fecha DESC";
	
	$result=$Conexion->query($sql);

	$Comprobantes= array();
	
	while( $row = mysqli_fetch_array($result))
	{
		$Comprobantes[] = array(
	                                 "NoFacturaElect" => $row["NoDocAfectado"],
	                                 "Abono" => $row["Abono"],
	                           );
	}
	
	$sql="SELECT DISTINCT 
	      C.Nombre,R.Fecha,R.TipoDocumento,CAST(R.SaldoTotalAnterior AS DECIMAL(14,2)) AS SaldoTotalAnterior
		  FROM reciboabono R INNER JOIN cliente C ON R.FK_Cliente=C.Cedula
		  WHERE R.FK_Usuario=$FK_Usuario AND IDRecibo=$NoRecibo AND FK_Cliente='$CedulaCliente'";
	
	$result=$Conexion->query($sql);
	
	while( $row = mysqli_fetch_array($result))
	{
		$NombreCliente=$row['Nombre'];
		$Fecha=DateTime::createFromFormat('Y-m-d H:i:s', $row['Fecha'])->format('d-m-Y H:i:s');
		$TipoDocumento=$row['TipoDocumento'];
		$SaldoTotalAnterior=$row['SaldoTotalAnterior'];
		
		if($TipoDocumento=='2' || $TipoDocumento=='4')//es un recibo o NotaCredito
		{
			$SaldoTotalActual=($row['SaldoTotalAnterior']-$MontoTotalRecibo);
		}
		else//es una nota de debito
		{
			$SaldoTotalActual=($row['SaldoTotalAnterior']+$MontoTotalRecibo);
		}
	}
	
		$users_arr = array();
    
    $users_arr[] = 
    array("IDRecibo" => $NoRecibo,"CedulaCliente"=>$CedulaCliente,"MontoTotalRecibo"=>$MontoTotalRecibo,"NombreCliente"=>$NombreCliente,"Fecha"=>$Fecha,"TipoDocumento"=>$TipoDocumento,"SaldoTotalAnterior"=>$SaldoTotalAnterior,"SaldoTotalActual"=>$SaldoTotalActual,"Facturas"=>$Comprobantes);

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
}

?>