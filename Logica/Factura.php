<?php 
session_start();

require ("../Conexion/Conexion.php");

//Verificar que el usuario haya pulsado el boton de agregar producto
if(isset($_POST['btnAgregarProducto'])) 
{
	AgregarProducto();
}
else if(isset($_POST['btnFactura']))
{
    GrabarFactura();//Probar tiquete en modificar y guardar:listo
}
else if(isset($_POST['MostrarDatos']))
{
    ConsultarFactura();
}
else if(isset($_POST['btnBorrarFilaDetalle']))
{
    BorrarFilaDetalleFactura();
}
else if(isset($_POST['btnBorrarFilaDetalleFactExist']))
{
    BorrarFilaDetalleFacturaExistente();
}
else if(isset($_POST['btnBorrar']))
{
	BorrarFactura($_POST['IDDocumento']);//Tiquete listo	
}
else if(isset($_POST['btnEnviarAHacienda']))
{
	EnviarAHacienda();
}
else if(isset($_POST['btnReenviarEmail']))
{
	ReenviarEmail();
}
else if(isset($_POST['btnConsultarEstado']))
{
	ConsultarEstado($_POST['IDDocumento'],$_POST['CedulaCliente'],$_SESSION['IDUsuario']);
}
else if(isset($_POST['CargarNoDocumento']))
{
    $TipoDocumento=$_POST['TipoDocumento'];

    $NoConsecutivo= ObtenerConsecutivo($TipoDocumento);
    $Terminal= ObtenerTerminal();
    $Sucursal= ObtenerSucursal();

        $users_arr[] = array( 
                         "NoConsecutivo"=>$NoConsecutivo,
                         "Terminal"=>$Terminal,
                         "Sucursal"=>$Sucursal,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}



function ObtenerConsecutivo($TipoDocumento)
{
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $sql="SELECT
          LPAD(CAST(".$TipoDocumento." AS CHAR(10)), 10, '0') as NoConsecutivo
          FROM consecutivo
          WHERE FK_Usuario=".$_SESSION['IDUsuario'].";";

    $NoConsecutivo="";
    
    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $NoConsecutivo=$row["NoConsecutivo"];
    }

    return $NoConsecutivo;
}

function ObtenerSucursal()
{
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $sql="SELECT
          LPAD(CAST(Sucursal AS CHAR(5)), 5, '0') AS Sucursal
          FROM consecutivo
          WHERE FK_Usuario=".$_SESSION['IDUsuario'].";";

    $Sucursal="";
    
    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $Sucursal=$row["Sucursal"];
    }

    return $Sucursal;
}

function ObtenerTerminal()
{
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $sql="SELECT
          LPAD(CAST(Terminal AS CHAR(3)), 3, '0') AS Terminal
          FROM consecutivo
          WHERE FK_Usuario=".$_SESSION['IDUsuario'].";";

    $Terminal="";
    
    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $Terminal=$row["Terminal"];
    }

    return $Terminal;
}

function ObtenerTipoDocumentoNumerico($TipoDocumento)
{
    if($TipoDocumento=='Factura')
   {
        $TipoDocumento='01';    
   }
   else if($TipoDocumento=='NotaDebito')
   {
        $TipoDocumento='02';
   }
   else if($TipoDocumento=='NotaCredito')
   {
        $TipoDocumento='03';
   }
   else if($TipoDocumento=='Tiquete')
   {
        $TipoDocumento='04';
   }

   return $TipoDocumento;
}

function ObtenerNombreTipoDocumento($TipoDocumento)
{
    if($TipoDocumento=='01')
   {
        $TipoDocumento='Factura';    
   }
   else if($TipoDocumento=='02')
   {
        $TipoDocumento='NotaDebito';
   }
   else if($TipoDocumento=='03')
   {
        $TipoDocumento='NotaCredito';
   }
   else if($TipoDocumento=='04')
   {
        $TipoDocumento='Tiquete';
   }

   return $TipoDocumento;
}

function BorrarFilaDetalleFacturaExistente()
{
	$FK_Usuario=$_SESSION['IDUsuario'];
	$IDFactura= $_POST['IDFactura'];
   	$IDDetalleFactura= $_POST['IDetalleFactura'];
	$IDProducto= $_POST['IDProducto'];
	$Cantidad= $_POST['Cantidad'];
	$Bonificacion= $_POST['Bonificacion'];
	$UnidadMedida= $_POST['UM'];
	
	$Respuesta;
	
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
        die("Connection failed: " . $Conexion->connect_error);
    }

	/*Eliminar DetalleFactura en la bd*/

	$sql="DELETE FROM detallefactura WHERE FK_Factura=$IDFactura AND FK_FK_Usuario=$FK_Usuario AND IDDetalle=$IDDetalleFactura;";
    
    if($Conexion->query($sql) === TRUE) 
	{
		 /*Modificar CantProducto en la bd*/
	
		if($UnidadMedida!='sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h')
		{
			$sql="UPDATE producto 
	    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual+($Cantidad+$Bonificacion) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

			if($Conexion->query($sql) === TRUE) 
			{
				 	$MontoExentoT=$_POST['MET'];
				    $MontoGravadoT=$_POST['MGT'];
				    $MontoExoneradoT=$_POST['MExoT'];
				    $MontoSExentoT=$_POST['MSET'];
				    $MontoSGravadoT=$_POST['MSGT'];
				    $MontoSExoneradoT=$_POST['MSExoT'];
				    $MontoIVT=$_POST['MIVT'];
				    $MontoIVDT=$_POST['MIVDT'];
				    $MontoOtroIT=$_POST['MOIT'];
				    $MontoDescuentoT=$_POST['MDT'];
				    $SubtotalT=QuitarFormatoNumero($_POST['ST']);
				    $TotalT=QuitarFormatoNumero($_POST['TT']);
				    
				    //para borrar fila detalle cuando el producto tiene exoneracion
	
					if(($UnidadMedida=='sp' || $UnidadMedida=='d' || $UnidadMedida=='h') && $MontoSExoneradoT!=0)
					{
						$MontoExoneracion=0.00;
						$MontoSExoneracion=bcdiv(($SubtotalT+$MontoDescuentoT),1,2);
						$MontoIVAExoneracion=bcdiv(($MontoIVT-$MontoSExoneradoT),1,2);
					}
					else if(($UnidadMedida!='sp' || $UnidadMedida!='d' || $UnidadMedida!='h') && $MontoExoneradoT!=0)
					{
						$MontoExoneracion=bcdiv(($SubtotalT+$MontoDescuentoT),1,2);
						$MontoSExoneracion=0.00;
						$MontoIVAExoneracion=bcdiv(($MontoIVT-$MontoExoneradoT),1,2);
					}
					else
					{
						$MontoExoneracion=0.00;
						$MontoSExoneracion=0.00;
						$MontoIVAExoneracion=0.00;
					}

				    /*Campos de Calculos de Factura*/

				    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
				    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
				    $MontoExoF=QuitarFormatoNumero($_POST['MontExoF']);
				    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
				    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
				    $MontoSExoF=QuitarFormatoNumero($_POST['MontSExoF']);
				    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
				    $MontoIVDF=QuitarFormatoNumero($_POST['MontIVDF']);
				    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
				    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
				    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
				    $TotF=QuitarFormatoNumero($_POST['MontTF']);

					$MontoExF=bcdiv(($MontoExF-$MontoExentoT),1,2);
				    $MontoGrF=bcdiv(($MontoGrF-$MontoGravadoT),1,2);
				    $MontoExoF=bcdiv(($MontoExoF-$MontoExoneracion),1,2);
				    $MontoSExF=bcdiv(($MontoSExF-$MontoSExentoT),1,2);
				    $MontoSGrF=bcdiv(($MontoSGrF-$MontoSGravadoT),1,2);
				    $MontoSExoF=bcdiv(($MontoSExoF-$MontoSExoneracion),1,2);
				    $MontoIVF=bcdiv(($MontoIVF-$MontoIVAExoneracion),1,2);
				    $MontoIVDF=bcdiv(($MontoIVDF-$MontoIVDT),1,2);
				    $MontoOtroIF=bcdiv(($MontoOtroIF-$MontoOtroIT),1,2);
				    $MontoDescF=bcdiv(($MontoDescF-$MontoDescuentoT),1,2);
				    $SubF=bcdiv(($SubF-$SubtotalT),1,2);
				    $TotF=bcdiv(($TotF-$TotalT),1,2);
				    $Saldo=0;/*para mas adelante trabajar con abonos*/
				    
				    //Actualizar en bd los totales de la factura/tiquete
				    
				    $sql =
			    "UPDATE factura SET 
			    				 MontoGravado=$MontoGrF,
								 MontoExento=$MontoExF,
								 MontoExonerado=$MontoExoF,
								 Descuento=$MontoDescF,
								 Impuesto=$MontoIVF,
								 ImpuestoDevuelto=$MontoIVDF,
								 OtroImpuesto=$MontoOtroIF,
								 ServicioGravado=$MontoSGrF,
								 ServicioExento=$MontoSExF,
								 ServicioExonerado=$MontoSExoF,
								 SubtotalFactura=$SubF,
								 TotalFactura=$TotF,
								 Saldo=$Saldo
				WHERE FK_Usuario= $FK_Usuario AND IDFactura=$IDFactura";

				if($Conexion->query($sql) === TRUE) 
				{ 	    
			    	$Respuesta="Producto eliminado de la factura correctamente";
				}
				else
				{
					$Respuesta="Error: no se pudo eliminar el producto de la factura";
				}
			}
		}
		else
		{
			$MontoExentoT=$_POST['MET'];
		    $MontoGravadoT=$_POST['MGT'];
		    $MontoExoneradoT=$_POST['MExoT'];
		    $MontoSExentoT=$_POST['MSET'];
		    $MontoSGravadoT=$_POST['MSGT'];
		    $MontoSExoneradoT=$_POST['MSExoT'];
		    $MontoIVT=$_POST['MIVT'];
		    $MontoIVDT=$_POST['MIVDT'];
		    $MontoOtroIT=$_POST['MOIT'];
		    $MontoDescuentoT=$_POST['MDT'];
		    $SubtotalT=QuitarFormatoNumero($_POST['ST']);
		    $TotalT=QuitarFormatoNumero($_POST['TT']);
		    
		    //para borrar fila detalle cuando el producto tiene exoneracion
			if(($UnidadMedida=='sp' || $UnidadMedida=='d' || $UnidadMedida=='h') && $MontoSExoneradoT!=0)
			{
				$MontoExoneracion=0.00;
				$MontoSExoneracion=bcdiv(($SubtotalT+$MontoDescuentoT),1,2);
				$MontoIVAExoneracion=bcdiv(($MontoIVT-$MontoSExoneradoT),1,2);
			}
			else if(($UnidadMedida!='sp' || $UnidadMedida!='d' || $UnidadMedida!='h') && $MontoExoneradoT!=0)
			{
				$MontoExoneracion=bcdiv(($SubtotalT+$MontoDescuentoT),1,2);
				$MontoSExoneracion=0.00;
				$MontoIVAExoneracion=bcdiv(($MontoIVT-$MontoExoneradoT),1,2);
			}
			else
			{
				$MontoExoneracion=0.00;
				$MontoSExoneracion=0.00;
				$MontoIVAExoneracion=0.00;
			}

		    /*Campos de Calculos de Factura*/

		    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
		    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
		    $MontoExoF=QuitarFormatoNumero($_POST['MontExoF']);
		    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
		    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
		    $MontoSExoF=QuitarFormatoNumero($_POST['MontSExoF']);
		    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
		    $MontoIVDF=QuitarFormatoNumero($_POST['MontIVDF']);
		    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
		    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
		    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
		    $TotF=QuitarFormatoNumero($_POST['MontTF']);

			$MontoExF=bcdiv(($MontoExF-$MontoExentoT),1,2);
		    $MontoGrF=bcdiv(($MontoGrF-$MontoGravadoT),1,2);
		    $MontoExoF=bcdiv(($MontoExoF-$MontoExoneracion),1,2);
		    $MontoSExF=bcdiv(($MontoSExF-$MontoSExentoT),1,2);
		    $MontoSGrF=bcdiv(($MontoSGrF-$MontoSGravadoT),1,2);
		    $MontoSExoF=bcdiv(($MontoSExoF-$MontoSExoneracion),1,2);
		    $MontoIVF=bcdiv(($MontoIVF-$MontoIVAExoneracion),1,2);
		    $MontoIVDF=bcdiv(($MontoIVDF-$MontoIVDT),1,2);
		    $MontoOtroIF=bcdiv(($MontoOtroIF-$MontoOtroIT),1,2);
		    $MontoDescF=bcdiv(($MontoDescF-$MontoDescuentoT),1,2);
		    $SubF=bcdiv(($SubF-$SubtotalT),1,2);
		    $TotF=bcdiv(($TotF-$TotalT),1,2);
		    $Saldo=0;/*para mas adelante trabajar con abonos*/
				    
				    //Actualizar en bd los totales de la factura/tiquete
				    
				    $sql =
			    "UPDATE factura SET 
			    				 MontoGravado=$MontoGrF,
								 MontoExento=$MontoExF,
								 MontoExonerado=$MontoExoF,
								 Descuento=$MontoDescF,
								 Impuesto=$MontoIVF,
								 ImpuestoDevuelto=$MontoIVDF,
								 OtroImpuesto=$MontoOtroIF,
								 ServicioGravado=$MontoSGrF,
								 ServicioExento=$MontoSExF,
								 ServicioExonerado=$MontoSExoF,
								 SubtotalFactura=$SubF,
								 TotalFactura=$TotF,
								 Saldo=$Saldo
				WHERE FK_Usuario= $FK_Usuario AND IDFactura=$IDFactura";

				if($Conexion->query($sql) === TRUE) 
				{ 	    
			    	$Respuesta="Producto eliminado de la factura correctamente";
				}
				else
				{
					$Respuesta="Error: no se pudo eliminar el producto de la factura";
				}
		}
	}
	else
	{
		$Respuesta="Error: no se pudo eliminar el producto de la factura";
	}

    /*Pasar al vector las variables de campo de calculos*/
    $users_arr[] = array( 

                         /*Pasar al vector las variables de campo de calculos*/
                         "MontoEF"=>number_format($MontoExF,2),
                         "MontoGF"=>number_format($MontoGrF,2),
                         "MontoExoF"=>number_format($MontoExoF,2),
                         "MontoSEF"=>number_format($MontoSExF,2),
                         "MontoSGF"=>number_format($MontoSGrF,2),
                         "MontoSExoF"=>number_format($MontoSExoF,2),
                         "MontoImpVF"=>number_format($MontoIVF,2),
                         "MontoImpVDF"=>number_format($MontoIVDF,2),
                         "MontoOImpF"=>number_format($MontoOtroIF,2),
                         "MontoDescF"=>number_format($MontoDescF,2),
                         "SubtF"=>number_format($SubF,2),
                         "TotF"=>number_format($TotF,2),
                         "Respuesta"=>$Respuesta,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
}

function BorrarFilaDetalleFactura()
{

    /*Campos de la tabla*/
    $IDProducto=$_POST['IDp'];
    $NombreProducto=$_POST['NombreP'];
    $PrecioVentaSinIV=QuitarFormatoNumero($_POST['PV']);
    $ImpuestoVenta=$_POST['IV'];
    $Descuento=$_POST['Des'];
    $Cantidad=$_POST['Cant'];
    $UnidadMedida=$_POST['UM'];

    $MontoExentoT=$_POST['MET'];
    $MontoGravadoT=$_POST['MGT'];
    $MontoExoneradoT=$_POST['MExoT'];
    $MontoSExentoT=$_POST['MSET'];
    $MontoSGravadoT=$_POST['MSGT'];
    $MontoSExoneradoT=$_POST['MSExoT'];
    $MontoIVT=$_POST['MIVT'];
    $MontoIVDT=$_POST['MIVDT'];
    $MontoOtroIT=$_POST['MOIT'];
    $MontoDescuentoT=$_POST['MDT'];
    $SubtotalT=QuitarFormatoNumero($_POST['ST']);
    $TotalT=QuitarFormatoNumero($_POST['TT']);
    
	//para borrar fila detalle cuando el producto tiene exoneracion
	if(($UnidadMedida=='sp' || $UnidadMedida=='d' || $UnidadMedida=='h') && $MontoSExoneradoT!=0)
	{
		$MontoExoneracion=0.00;
		$MontoSExoneracion=bcdiv(($SubtotalT+$MontoDescuentoT),1,2);
		$MontoIVAExoneracion=bcdiv(($MontoIVT-$MontoSExoneradoT),1,2);
	}
	else if(($UnidadMedida!='sp' || $UnidadMedida!='d' || $UnidadMedida!='h') && $MontoExoneradoT!=0)
	{
		$MontoExoneracion=bcdiv(($SubtotalT+$MontoDescuentoT),1,2);
		$MontoSExoneracion=0.00;
		$MontoIVAExoneracion=bcdiv(($MontoIVT-$MontoExoneradoT),1,2);
	}
	else
	{
		$MontoExoneracion=0.00;
		$MontoSExoneracion=0.00;
		$MontoIVAExoneracion=0.00;
	}
	

    /*Campos de Calculos de Factura*/

    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
    $MontoExoF=QuitarFormatoNumero($_POST['MontExoF']);
    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
    $MontoSExoF=QuitarFormatoNumero($_POST['MontSExoF']);
    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
    $MontoIVDF=QuitarFormatoNumero($_POST['MontIVDF']);
    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
    $TotF=QuitarFormatoNumero($_POST['MontTF']);

  	$MontoExF=bcdiv(($MontoExF-$MontoExentoT),1,2);
    $MontoGrF=bcdiv(($MontoGrF-$MontoGravadoT),1,2);
    $MontoExoF=bcdiv(($MontoExoF-$MontoExoneracion),1,2);//-precio sin IV porque es el montobruto lo qye va en esta cas
    $MontoSExF=bcdiv(($MontoSExF-$MontoSExentoT),1,2);
    $MontoSGrF=bcdiv(($MontoSGrF-$MontoSGravadoT),1,2);
    $MontoSExoF=bcdiv(($MontoSExoF-$MontoSExoneracion),1,2);//
    $MontoIVF=bcdiv(($MontoIVF-$MontoIVT),1,2);
    $MontoIVDF=bcdiv(($MontoIVDF-$MontoIVDT),1,2);
    $MontoOtroIF=bcdiv(($MontoOtroIF-$MontoOtroIT),1,2);
    $MontoDescF=bcdiv(($MontoDescF-$MontoDescuentoT),1,2);
    $SubF=bcdiv(($SubF-$SubtotalT),1,2);
    $TotF=bcdiv(($TotF-$TotalT),1,2);


    /*Pasar al vector las variables de campo de calculos*/
    $users_arr[] = array( 

                         /*Pasar al vector las variables de campo de calculos*/
                         "MontoEF"=>number_format($MontoExF,2),
                         "MontoGF"=>number_format($MontoGrF,2),
                         "MontoExoF"=>number_format($MontoExoF,2),
                         "MontoSEF"=>number_format($MontoSExF,2),
                         "MontoSGF"=>number_format($MontoSGrF,2),
                         "MontoSExoF"=>number_format($MontoSExoF,2),
                         "MontoImpVF"=>number_format($MontoIVF,2),
                         "MontoImpVDF"=>number_format($MontoIVDF,2),
                         "MontoOImpF"=>number_format($MontoOtroIF,2),
                         "MontoDescF"=>number_format($MontoDescF,2),
                         "SubtF"=>number_format($SubF,2),
                         "TotF"=>number_format($TotF,2),
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}

function AgregarProducto()
{
  	$IDProducto=$_POST['IDp'];
  	$NombreProducto=$_POST['NombreP'];
  	$PrecioVentaSinIV=QuitarFormatoNumero($_POST['PV']);
    $ImpuestoVenta=$_POST['IV'];
    $Descuento=$_POST['Des'];
    $Cantidad=$_POST['Cant'];
    $UnidadMedida=$_POST['UM'];
    $PreComp=$_POST['PreComp'];
    $Bonificacion=$_POST['Bonificacion'];
    $Moneda=$_POST['Moneda'];
    $TipoCambio=$_POST['TipoCambio'];
    
    $ClienteExonerado=$_POST['ClienteExonerado'];//0 no exonerado, 1 exonerado
    $CedulaCliente=$_POST['CedulaCliente'];
	    
    $PrecioTotalBruto=bcdiv((round(($PrecioVentaSinIV),2)*$Cantidad),1,2);//para hacer todos los calculos

    /*Campos de Calculos de Factura*/

    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
    $MontoExoF=QuitarFormatoNumero($_POST['MontExoF']);
    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
    $MontoSExoF=QuitarFormatoNumero($_POST['MontSExoF']);
    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
    $MontoIVDF=QuitarFormatoNumero($_POST['MontIVDF']);
    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
    $TotF=QuitarFormatoNumero($_POST['MontTF']);

    /*Variables para calculos*/
    $MontoExento=0;
    $MontoGravado=0;
    
    $MontoExonerado=0;
    
    $MontoSExento=0;
    $MontoSGravado=0;
    
    $MontoSExoneracion=0;
    $MontoExoneracion=0;
    
    $MontoSExonerado=0;
    
    $MontoIV=0;
    
    $MontoIVDev=0;
    
    $MontoOtroI=0;
    $MontoDescuento=0;
    $SubtotalL=0;
    $TotalL=0;

    if(($UnidadMedida!='sp' AND $UnidadMedida!='d'AND $UnidadMedida!='h') AND $ImpuestoVenta==0) /*Sino Tiene Impuesto*/
    {
      $MontoExento=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
      $MontoGravado=0; 
  
    }
    else if(($UnidadMedida!='sp'AND $UnidadMedida!='d'AND $UnidadMedida!='h') AND $ImpuestoVenta!=0 AND $ClienteExonerado!='1')/*Si Tiene Impuesto*/ 
    {
      $MontoExento=0;
      $MontoGravado=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
    }
    else
    {
	  $MontoExento=0;
	  $MontoGravado=0;	
	}
    
    if(($UnidadMedida=='sp'||$UnidadMedida=='d'||$UnidadMedida=='h') AND $ImpuestoVenta==0)
    {
      $MontoSExento=bcdiv(($PrecioTotalBruto),1,2); /*Precio sin impuesto x Cantidad*/
      $MontoSGravado=0;
    }
    else if(($UnidadMedida=='sp'||$UnidadMedida=='d'||$UnidadMedida=='h') AND $ImpuestoVenta!=0 AND $ClienteExonerado!='1')
  {
    $MontoSExento=0;/*Precio con impuesto x Cantidad*/
      $MontoSGravado=bcdiv(($PrecioTotalBruto),1,2);
  }
  else
  {
    $MontoSExento=0;
    $MontoSGravado=0;
  }     

    $MontoIV=bcdiv(((($PrecioTotalBruto)-(($PrecioTotalBruto)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
    $MontoOtroI=0;/*se va a cambiar mas adelante*/
    $MontoDescuento=bcdiv((($PrecioTotalBruto)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/

	/**
	* 
	* Exoneracion
	* 
	*/

	if($ClienteExonerado=='1' AND $ImpuestoVenta!=0)//es Exonerado y tiene IV
    {
		//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
		$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	    $sql="SELECT
			 (
			 CASE 
			     WHEN TodosProductosExonerados=1 THEN PorcentajeExoneracion
			     ELSE 'NO'
			 END
			 ) AS PorcentajeExoneracion
			 FROM cliente
			 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";

	    $Resutado="";
	    
	    $result=$Conexion->query($sql);

        $row = $result->fetch_assoc();
        $PorcentajeExoneracionTP=$row["PorcentajeExoneracion"];
        
        if($PorcentajeExoneracionTP!='NO')
		{
			if(($UnidadMedida!='sp'AND $UnidadMedida!='d'AND $UnidadMedida!='h'))
    		{
				$MontoExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
				
				$MontoExonerado=(bcdiv(($PrecioTotalBruto),1,2));
				
				$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
	    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
	    		
	    		$MontoIV=bcdiv(($MontoIV-$MontoExoneracion),1,2);//si es 100% da 0
			}
			else if(($UnidadMedida=='sp'OR $UnidadMedida=='d'OR $UnidadMedida=='h'))
    		{
				$MontoSExoneracion=bcdiv($MontoIV*($PorcentajeExoneracionTP/100),1,2);
				
				$MontoSExonerado=(bcdiv(($PrecioTotalBruto),1,2));
				
				$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
	    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoSExoneracion),1,2);/*Subtotal-Descuento+Impuesto*/
	    		
	    		$MontoIV=bcdiv(($MontoIV-$MontoSExoneracion),1,2);
			}
		}
		else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
		{
			
		}
	}
	else //No es Exonerado
	{
		$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
    	$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
	}

    


    /*Hacer Calculos de calculos de Campos*/

    $MontoExF=bcdiv(($MontoExF+$MontoExento),1,2);
    $MontoGrF=bcdiv(($MontoGrF+$MontoGravado),1,2);
    
    $MontoExoF=bcdiv(($MontoExoF+$MontoExonerado),1,2);
    
    $MontoSExF=bcdiv(($MontoSExF+$MontoSExento),1,2);
    $MontoSGrF=bcdiv(($MontoSGrF+$MontoSGravado),1,2);
    
    $MontoSExoF=bcdiv(($MontoSExoF+$MontoSExonerado),1,2);
    
    $MontoIVF=bcdiv(($MontoIVF+$MontoIV),1,2);
    
    $MontoIVDF=bcdiv(($MontoIVDF+$MontoIVDev),1,2);
    
    $MontoOtroIF=bcdiv(($MontoOtroIF+$MontoOtroI),1,2);
    $MontoDescF=bcdiv(($MontoDescF+$MontoDescuento),1,2);
    $SubF=bcdiv(($SubF+$SubtotalL),1,2);
    $TotF=bcdiv(($TotF+$TotalL),1,2);



    /*Pasar variables a vector para ponerlos en los campos de tabla y de calculos en el formulario*/
    $users_arr[] = array("IDProducto" => $IDProducto, 
                         "NombreProducto" => $NombreProducto,
                         "PrecioVSinIV" => number_format(round(($PrecioVentaSinIV),2),2), 
                         "Impuesto" =>$ImpuestoVenta, 
                         "Descuento" =>$Descuento, 
                         "Cantidad"=>$Cantidad,
                         "UM"=>$UnidadMedida,
                         "PreComp"=>$PreComp,
                         "Bonificacion"=>$Bonificacion,
                         
                         "MontoE"=>$MontoExento,
                         "MontoG"=>$MontoGravado,
                         "MontoExo"=>$MontoExoneracion,
                         "MontoSE"=>$MontoSExento,
                         "MontoSG"=>$MontoSGravado,
                         "MontoSExo"=>$MontoSExoneracion,
                         "MontoImpV"=>$MontoIV,
                         "MontoImpVD"=>$MontoIVDev,
                         "MontoOImp"=>$MontoOtroI,
                         "MontoDesc"=>$MontoDescuento,
                         "SubtL"=>number_format($SubtotalL,2),
                         "TotL"=>number_format($TotalL,2),

                         /*Pasar al vector las variables de campo de calculos*/
                         "MontoEF"=>number_format($MontoExF,2),
                         "MontoGF"=>number_format($MontoGrF,2),
                         "MontoExoF"=>number_format($MontoExoF,2),
                         "MontoSEF"=>number_format($MontoSExF,2),
                         "MontoSGF"=>number_format($MontoSGrF,2),
                         "MontoSExoF"=>number_format($MontoSExoF,2),
                         "MontoImpVF"=>number_format($MontoIVF,2),
                         "MontoImpVDF"=>number_format($MontoIVDF,2),
                         "MontoOImpF"=>number_format($MontoOtroIF,2),
                         "MontoDescF"=>number_format($MontoDescF,2),
                         "SubtF"=>number_format($SubF,2),
                         "TotF"=>number_format($TotF,2),
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}


function GrabarFactura()
{
	$GuardarModifcarFactura=$_POST['btnFactura'];
	
	$GuarMod='';
	$Respuesta=''; //para mostrar errores o msj de exito al guardar
	
	if($GuardarModifcarFactura=='GrabarFactura')
	{
		/*Guardar el encabezado de factura*/  
		
	   $ActividadEconomica=$_POST["ActividadEconomica"];  

	   $IDFactura=($_POST['IDFactura']!="")?$_POST['IDFactura']:0;
	   $FK_Usuario=$_SESSION['IDUsuario'];
	   $FK_Cliente= $_POST['Cedula'];
	   $NombreCliente= $_POST['Nombre'];
	   $EmailCliente=($_POST['EmailCliente']!='NULL')?$_POST['EmailCliente']:'NULL';
	   $NoReferencia="";/*Hay que hacer los campos en el formulario primero*/
	   $Razon="";/*Hay que hacer los campos en el formulario primero*/
	   $Fecha= DateTime::createFromFormat('d-m-Y H:i:s', $_POST['Fecha'])->format('Y-m-d H:i:s'); 
	   $Plazo= $_POST['Plazo']; //si es contado poner plazo como 0
	   $MedioPago= $_POST['MedioPago']; 
	   $CondicionVenta= $_POST['CondicionVenta'];
	   $Status="";/*Se guarda al mandara a hacienda*/

	   $NoOrden= $_POST['NoOrden']; 
	   $TipoMoneda= $_POST['TipoMoneda']; 
	   $TipoCambio= $_POST['TipoCambio'];

	   $TipoDocumento= ObtenerTipoDocumentoNumerico($_POST['TipoDocumento']); /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   $TipoDocumentoAfectado=""; /*Hay que hacer los campos en el formulario primero*/ /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   /*Campos de Calculos*/
	   $MontoGravado= QuitarFormatoNumero($_POST['MontGrF']);
	   $MontoExento= QuitarFormatoNumero($_POST['MontExF']);
	   $MontoExonerado= QuitarFormatoNumero($_POST['MontExoF']);
	   $Descuento= QuitarFormatoNumero($_POST['MontDescF']);
	   $Impuesto= QuitarFormatoNumero($_POST['MontIVF']);
	   $ImpuestoDevuelto= QuitarFormatoNumero($_POST['MontIVDF']);
	   $OtroImpuesto= QuitarFormatoNumero($_POST['MontOImpF']);
	   $ServicioGravado= QuitarFormatoNumero($_POST['MontSGF']);
	   $ServicioExento= QuitarFormatoNumero($_POST['MontSEF']);
	   $ServicioExonerado= QuitarFormatoNumero($_POST['MontSExoF']);

	   $Subtotal= QuitarFormatoNumero($_POST['MontSubF']);
	   $Total= QuitarFormatoNumero($_POST['MontTF']);

	   $Saldo= QuitarFormatoNumero($_POST['Saldo']);/*para mas adelante trabajar con abonos*/


	   //$Telefono= $_POST['Telefono'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Email= $_POST['Email'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Direccion= $_POST['Direccion'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Zona= $_POST['Zona'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   
	   $Terminal= $_POST['Terminal'];/*1 que hago con esto*/ 
	   $Sucursal= $_POST['Sucursal'];/*2 que hago con esto*/
	   //$Consecutivo= ObtenerConsecutivo($_POST['TipoDocumento']);  
	   
	   $NoFactura=""; /* que hago con esto? $Terminal.$Sucursal.$TipoDocumento.$Consecutivo;Hacer una funcion para generarla  Terminal+Sucursal+TipoDocumento+Consecutivo*/ 

	   $Clave="";/*Hacer una funcion para generarla*/ 

	   /*guardar Encabezado de factura en la BD*/

	   $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	    if ($Conexion->connect_error) 
	    {
	        die("Connection failed: " . $Conexion->connect_error);
	    } 

	    /*sanitize sql*/
	    
	    if($ActividadEconomica=='NULL')
	    {
	    	if($EmailCliente=='NULL')
	    	{
				$sql="INSERT INTO factura(FK_Usuario,NoFactura,FK_Cliente,NombreCliente,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,TipoCambio,TipoDocumento,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalFactura,TotalFactura,Saldo,Terminal,Sucursal)
	    values($FK_Usuario,'$NoFactura','$FK_Cliente','$NombreCliente','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento',$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,($Saldo*$TipoCambio),'$Terminal','$Sucursal');";
			}
	    	else
	    	{
				$sql="INSERT INTO factura(FK_Usuario,NoFactura,FK_Cliente,NombreCliente,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,TipoCambio,TipoDocumento,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalFactura,TotalFactura,Saldo,Terminal,Sucursal,EmailCliente)
	    values($FK_Usuario,'$NoFactura','$FK_Cliente','$NombreCliente','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento',$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,($Saldo*$TipoCambio),'$Terminal','$Sucursal','$EmailCliente');";
			}	
		}
	    else
	    {
			if($EmailCliente=='NULL')
			{
				$sql="INSERT INTO factura(FK_Usuario,NoFactura,FK_Cliente,NombreCliente,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,TipoCambio,TipoDocumento,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalFactura,TotalFactura,Saldo,Terminal,Sucursal,FK_ActividadEconomica)
	    values($FK_Usuario,'$NoFactura','$FK_Cliente','$NombreCliente','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento',$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,($Saldo*$TipoCambio),'$Terminal','$Sucursal','$ActividadEconomica');";
			}
			else
			{
				$sql="INSERT INTO factura(FK_Usuario,NoFactura,FK_Cliente,NombreCliente,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,TipoCambio,TipoDocumento,MontoGravado,MontoExento,MontoExonerado,Descuento,Impuesto,ImpuestoDevuelto,OtroImpuesto,ServicioGravado,ServicioExento,ServicioExonerado,SubtotalFactura,TotalFactura,Saldo,Terminal,Sucursal,FK_ActividadEconomica,EmailCliente)
	    values($FK_Usuario,'$NoFactura','$FK_Cliente','$NombreCliente','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento',$MontoGravado,$MontoExento,$MontoExonerado,$Descuento,$Impuesto,$ImpuestoDevuelto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$ServicioExonerado,$Subtotal,$Total,($Saldo*$TipoCambio),'$Terminal','$Sucursal','$ActividadEconomica','$EmailCliente');";
			}
		}
	                
	    if($Conexion->query($sql) === TRUE) 
	    { 
	        /*Guardar el Detalle de Factura*/

	        /*Obtener IDFactura*/

	        $sql="SELECT max(IDFactura) as IDFactura FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente';";

	        $result=$Conexion->query($sql);

	        if($result->num_rows > 0)
	        {
	          $row = $result->fetch_assoc();
	          $IDFactura=$row["IDFactura"];
	        }
	       
	        $TotalFilasGuardadas=0;

	        /*sacar el vector del JSON y pasarlo a Variable vector*/

	        $DetalleFactura=$_POST['DetalleFactura'];

	        /*Recorrer el vector con vectores (Matriz) y guardar en la BD*/
	        foreach($DetalleFactura as $i => $item) 
	        {
			   $NoLinea= $DetalleFactura[$i]['NoLinea'];
	           $IDProducto= $DetalleFactura[$i]['IDProducto'];
	           $NombreProducto= $DetalleFactura[$i]['NombreProducto'];
	           $Cantidad= $DetalleFactura[$i]['Cantidad'];
	           $PrecioVentaSinIV= QuitarFormatoNumero($DetalleFactura[$i]['PrecioVentaSinIV']);
	           $UnidadMedida= $DetalleFactura[$i]['UnidadMedida'];
	           $ImpuestoVentas= $DetalleFactura[$i]['ImpuestoVentas'];
	           $Descuento= $DetalleFactura[$i]['Descuento'];
	           $PrecioCosto= $DetalleFactura[$i]['PrecioCosto'];
	           $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
	           $TotalNeto= QuitarFormatoNumero($DetalleFactura[$i]['TotalNeto']);
	           $IDDetalle= $DetalleFactura[$i]['IDDetalle'];
	           $MontoSExonerado= $DetalleFactura[$i]['MontoSExonerado'];
	           $MontoExonerado= $DetalleFactura[$i]['MontoExonerado'];
	           $MontoIVDevuelto= $DetalleFactura[$i]['MontoIVDevuelto'];

	        // $array[$i] is same as $item

	           $sql="Insert into detallefactura(FK_FK_Usuario,FK_Factura,NombreProducto,Cantidad,Medida,Bonificacion,PrecioVenta,PrecioCosto,Impuesto,Descuento,MontoExonerado,MontoServicioExonerado,MontoImpuestoDevuelto,TotalNeto,NoLinea,FK_Producto)
	values($FK_Usuario,$IDFactura,'$NombreProducto',$Cantidad,'$UnidadMedida',$Bonificacion,$PrecioVentaSinIV,$PrecioCosto,$ImpuestoVentas,$Descuento,$MontoExonerado,$MontoSExonerado,$MontoIVDevuelto,$TotalNeto,$NoLinea,'$IDProducto');";

	          if($Conexion->query($sql) === TRUE) 
	          { 
	            /*Ingrementar LineasGuardadas*/
	              $TotalFilasGuardadas++;
	            //Restar producto de inventario al guardar el detalle si todas las lineas se guardaron
	          }

	        }

	        //Si TotalFilas y TotalFilasGuardadas son iguales, se guardo la factura y hay que recorrer el arreglo y  restar la cantidad de productos disponibles por linea

	        if(count($DetalleFactura)==$TotalFilasGuardadas)
	        {
	          $FilasAct=0;

	          /*Recorrer el arreglo, actualizar cantidad disponible y al final mostrar msj de exito en un boostrap*/
	          foreach($DetalleFactura as $i => $item) 
	          {
	             $IDProducto= $DetalleFactura[$i]['IDProducto'];
	             $Cantidad= $DetalleFactura[$i]['Cantidad'];
	             $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
	             $Medida= $DetalleFactura[$i]['UnidadMedida'];
	             
	             if((($Medida!='sp'AND $Medida!='d'AND $Medida!='h') AND $IDProducto!='000000000000000000'))
	             {
				 		$sql="update producto 
	                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual-($Cantidad+$Bonificacion), UltimaVenta='$Fecha'
	                    where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

		              if($Conexion->query($sql) === TRUE) 
		              { 
		                /*Ingrementar cantidad y fecha de Prod en las lineas*/
		                  $FilasAct++;
		              }	
				 }
				 else
				 {
				 	$FilasAct++;
				 }
	      
	              
	          }

	          if(count($DetalleFactura)==$FilasAct)
	          {

						$Respuesta=($_POST['TipoDocumento']=='Factura')? "Factura guardada correctamente":'Tiquete guardado correctamente';			 
						$GuarMod='Guardo';
	              
	          }

	        }
	        else //sino son iguales entonces borrar todo del detalle y luego borrar el encabezado y mostrar msj de error
	        {
	            $sql="delete from detallefactura where FK_Factura=$IDFactura;";

	            if($Conexion->query($sql) === TRUE) 
	              { 
	                //Borrar encabezado de factura luego del detalle
	                $sql="delete from factura where IDFactura=$IDFactura;";

	                 if($Conexion->query($sql) === TRUE) 
	                {
	                  //Error al guardar la factura
	                  $Respuesta= ($_POST['TipoDocumento']=='Factura')? "Error al guardar la factura":"Error al guardar el tiquete";
	                  $GuarMod='Error';
	                }

	              }
	        }
	    }
	    else
	    {
	      /*No se guardo el encabezado, mostrar error*/
	      $Respuesta= ($_POST['TipoDocumento']=='Factura')? "Error al guardar la factura":"Error al guardar el tiquete";
	      $GuarMod='Error';
	    }
	}
	else //Modifcar
	{
		/**
		* Sacar cedula de cliente aneterior para restar el total de la factura(sacarlo tambien con el id) anterior 
		* Cuando borre, sumar la cantidad a cantidad disponible del producto en cuestion
		*/
		
		/*Modifcar el encabezado de factura*/ 
		
	   $ActividadEconomica=$_POST["ActividadEconomica"];   

	   $IDFactura=($_POST['IDFactura']!="")?$_POST['IDFactura']:0;
	   $FK_Usuario=$_SESSION['IDUsuario'];
	   $FK_Cliente= $_POST['Cedula'];
	   $NombreCliente= $_POST['Nombre'];

	   $NoReferencia="";/*Hay que hacer los campos en el formulario primero*/
	   $Razon="";/*Hay que hacer los campos en el formulario primero*/
	   $Fecha= DateTime::createFromFormat('d-m-Y H:i:s', $_POST['Fecha'])->format('Y-m-d H:i:s'); 
	   $Plazo= $_POST['Plazo']; //si es contado poner plazo como 0
	   $MedioPago= $_POST['MedioPago']; 
	   $CondicionVenta= $_POST['CondicionVenta'];
	   $Status="";/*Se guarda al mandara a hacienda*/

	   $NoOrden= $_POST['NoOrden']; 
	   $TipoMoneda= $_POST['TipoMoneda']; 
	   $TipoCambio= $_POST['TipoCambio'];

	   $TipoDocumento= ObtenerTipoDocumentoNumerico($_POST['TipoDocumento']); /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   $TipoDocumentoAfectado=""; /*Hay que hacer los campos en el formulario primero*/ /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   /*Campos de Calculos*/
	   $MontoGravado= QuitarFormatoNumero($_POST['MontGrF']);
	   $MontoExento= QuitarFormatoNumero($_POST['MontExF']);
	   $MontoExonerado= QuitarFormatoNumero($_POST['MontExoF']);
	   $Descuento= QuitarFormatoNumero($_POST['MontDescF']);
	   $Impuesto= QuitarFormatoNumero($_POST['MontIVF']);
	   $ImpuestoDevuelto= QuitarFormatoNumero($_POST['MontIVDF']);
	   $OtroImpuesto= QuitarFormatoNumero($_POST['MontOImpF']);
	   $ServicioGravado= QuitarFormatoNumero($_POST['MontSGF']);
	   $ServicioExento= QuitarFormatoNumero($_POST['MontSEF']);
	   $ServicioExonerado= QuitarFormatoNumero($_POST['MontSExoF']);

	   $Subtotal= QuitarFormatoNumero($_POST['MontSubF']);
	   $Total= QuitarFormatoNumero($_POST['MontTF']);

	   $Saldo=QuitarFormatoNumero($_POST['Saldo']);/*para mas adelante trabajar con abonos*/


	   //$Telefono= $_POST['Telefono'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Email= $_POST['Email'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Direccion= $_POST['Direccion'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Zona= $_POST['Zona'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   
	   $Terminal= $_POST['Terminal'];/*1 que hago con esto*/ 
	   $Sucursal= $_POST['Sucursal'];/*2 que hago con esto*/
	   //$Consecutivo= ObtenerConsecutivo($_POST['TipoDocumento']);  
	   
	   $NoFactura=""; /* que hago con esto? $Terminal.$Sucursal.$TipoDocumento.$Consecutivo;Hacer una funcion para generarla  Terminal+Sucursal+TipoDocumento+Consecutivo*/ 

	   $Clave="";/*Hacer una funcion para generarla*/ 
	   
	   $IDDetalles=array();

	   /*guardar Encabezado de factura en la BD*/

	   $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	    if ($Conexion->connect_error) 
	    {
	        die("Connection failed: " . $Conexion->connect_error);
	    } 
	    
	    /*Obtener Datos de Cliente Factura Anterior y factura para actualizar(restar) datos de totalfacturado si son diferentes
	    Sis on el mismo Restar total de factura actual con el de fatura anterior(o al reves) y restarlo o sumarlo al total facturado  */
	    $FK_ClienteAntFact;
      	$TotalFactAnt;
	    
	    $sql="SELECT IDFactura,FK_Cliente,TotalFactura FROM factura WHERE FK_Usuario=$FK_Usuario AND IDFactura=$IDFactura;";
	    
	    $result = $Conexion->query($sql);

	    if ($result->num_rows > 0) 
	    {
      		$row = $result->fetch_assoc();

      		$FK_ClienteAntFact=$row["FK_Cliente"];
      		$TotalFactAnt=$row["TotalFactura"];
		}

		//Actualizar encabezado de factura    

	    /*sanitize sql*/
	    $sql =
	    		($ActividadEconomica=='NULL')
	    		?
			    "UPDATE factura SET FK_Usuario=$FK_Usuario,
								 NoFactura='$NoFactura',
								 FK_Cliente='$FK_Cliente',
								 NombreCliente='$NombreCliente',
								 Fecha='$Fecha',
								 Plazo=$Plazo,
								 MedioPago='$MedioPago',
								 CondicionVenta='$CondicionVenta',
								 Status='$Status',
								 Clave='$Clave',
								 NoOrden='$NoOrden',
								 TipoMoneda='$TipoMoneda',
								 TipoCambio=$TipoCambio,
								 TipoDocumento='$TipoDocumento',
								 MontoGravado=$MontoGravado,
								 MontoExento=$MontoExento,
								 MontoExonerado=$MontoExonerado,
								 Descuento=$Descuento,
								 Impuesto=$Impuesto,
								 ImpuestoDevuelto=$ImpuestoDevuelto,
								 OtroImpuesto=$OtroImpuesto,
								 ServicioGravado=$ServicioGravado,
								 ServicioExento=$ServicioExento,
								 ServicioExonerado=$ServicioExonerado,
								 SubtotalFactura=$Subtotal,
								 TotalFactura=$Total,
								 Saldo=($Saldo*$TipoCambio),
								 Terminal='$Terminal',
								 Sucursal='$Sucursal'
				WHERE FK_Usuario= $FK_Usuario AND IDFactura=$IDFactura"
				:
				"UPDATE factura SET FK_Usuario=$FK_Usuario,
								 NoFactura='$NoFactura',
								 FK_Cliente='$FK_Cliente',
								 NombreCliente='$NombreCliente',
								 Fecha='$Fecha',
								 Plazo=$Plazo,
								 MedioPago='$MedioPago',
								 CondicionVenta='$CondicionVenta',
								 Status='$Status',
								 Clave='$Clave',
								 NoOrden='$NoOrden',
								 TipoMoneda='$TipoMoneda',
								 TipoCambio=$TipoCambio,
								 TipoDocumento='$TipoDocumento',
								 MontoGravado=$MontoGravado,
								 MontoExento=$MontoExento,
								 MontoExonerado=$MontoExonerado,
								 Descuento=$Descuento,
								 Impuesto=$Impuesto,
								 ImpuestoDevuelto=$ImpuestoDevuelto,
								 OtroImpuesto=$OtroImpuesto,
								 ServicioGravado=$ServicioGravado,
								 ServicioExento=$ServicioExento,
								 ServicioExonerado=$ServicioExonerado,
								 SubtotalFactura=$Subtotal,
								 TotalFactura=$Total,
								 Saldo=($Saldo*$TipoCambio),
								 Terminal='$Terminal',
								 Sucursal='$Sucursal',
								 FK_ActividadEconomica='$ActividadEconomica'
				WHERE FK_Usuario= $FK_Usuario AND IDFactura=$IDFactura"
				;
	                
	    if($Conexion->query($sql) === TRUE) 
	    { 
	    	/*Actualizar el detalle fila a fila teniendo en cuenta que si la fila no tiene IDdetalle se guarda y si tiene no se hace nada)*/
	    	
	    	$TotalFilasAGuardar=0;
	    	$TotalFilasGuardadas=0;

	        /*sacar el vector del JSON y pasarlo a Variable vector*/

	        $DetalleFactura=$_POST['DetalleFactura'];

	        /*Recorrer el vector con vectores (Matriz) y guardar en la BD*/
	        foreach($DetalleFactura as $i => $item) 
	        {
			   $NoLinea= $DetalleFactura[$i]['NoLinea'];
	           $IDProducto= $DetalleFactura[$i]['IDProducto'];
	           $NombreProducto= $DetalleFactura[$i]['NombreProducto'];
	           $Cantidad= $DetalleFactura[$i]['Cantidad'];
	           $PrecioVentaSinIV= QuitarFormatoNumero($DetalleFactura[$i]['PrecioVentaSinIV']);
	           $UnidadMedida= $DetalleFactura[$i]['UnidadMedida'];
	           $ImpuestoVentas= $DetalleFactura[$i]['ImpuestoVentas'];
	           $Descuento= $DetalleFactura[$i]['Descuento'];
	           $PrecioCosto= $DetalleFactura[$i]['PrecioCosto'];
	           $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
	           $TotalNeto= QuitarFormatoNumero($DetalleFactura[$i]['TotalNeto']);
	           $IDDetalle= $DetalleFactura[$i]['IDDetalle'];
	           $MontoSExonerado= $DetalleFactura[$i]['MontoSExonerado'];
	           $MontoExonerado= $DetalleFactura[$i]['MontoExonerado'];
	           $MontoIVDevuelto= $DetalleFactura[$i]['MontoIVDevuelto'];
	           
	           //Si Iddetalle=="" guarde
	            if( $IDDetalle=="")
				{
		        	$TotalFilasAGuardar++; 

	           		 $sql="Insert into detallefactura(FK_FK_Usuario,FK_Factura,NombreProducto,Cantidad,Medida,Bonificacion,PrecioVenta,PrecioCosto,Impuesto,Descuento,MontoExonerado,MontoServicioExonerado,MontoImpuestoDevuelto,TotalNeto,NoLinea,FK_Producto)
	values($FK_Usuario,$IDFactura,'$NombreProducto',$Cantidad,'$UnidadMedida',$Bonificacion,$PrecioVentaSinIV,$PrecioCosto,$ImpuestoVentas,$Descuento,$MontoExonerado,$MontoSExonerado,$MontoIVDevuelto,$TotalNeto,$NoLinea,'$IDProducto');";

			          if($Conexion->query($sql) === TRUE) 
			          { 
			            /*Guardar el IDDetalle en un vector por si hay que borrarlo*/
			            
			            $sql="SELECT MAX(IDDetalle) as IDDetalle FROM detallefactura where FK_FK_Usuario=$FK_Usuario and FK_Factura=$IDFactura;";

				        $result=$Conexion->query($sql);

				        if($result->num_rows > 0)
				        {
				          $row = $result->fetch_assoc();
				          $IDDetalles[]=$row["IDDetalle"];
				        }
			            
			            /*Ingrementar LineasGuardadas*/
			              $TotalFilasGuardadas++;
			          }
				}

	        }	        

	        //Si TotalFilas y TotalFilasGuardadas son iguales, se guardo la factura y hay que recorrer el arreglo y  restar la cantidad de productos disponibles por linea

	        if($TotalFilasAGuardar==$TotalFilasGuardadas)
	        {
		          $FilasAct=0;

		          /*Recorrer el arreglo, actualizar cantidad disponible y al final mostrar msj de exito en un boostrap*/
		          foreach($DetalleFactura as $i => $item) 
		          {
		             $IDProducto= $DetalleFactura[$i]['IDProducto'];
		             $Cantidad= $DetalleFactura[$i]['Cantidad'];
		             $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
		             $IDDetalle= $DetalleFactura[$i]['IDDetalle'];
		             $UnidadMedida= $DetalleFactura[$i]['UnidadMedida'];
		      
		      		 if($IDDetalle=="" AND ($UnidadMedida!='sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h'))
		      		 {
					 		$sql="update producto 
		                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual-($Cantidad+$Bonificacion), UltimaVenta='$Fecha'
		                    where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

			              if($Conexion->query($sql) === TRUE) 
			              { 
			                /*Ingrementar cantidad y fecha de Prod en las lineas*/
			                  $FilasAct++;
			              }	
					 }
					 else if($IDDetalle=="" && ($UnidadMedida=='sp' OR $UnidadMedida=='d' OR $UnidadMedida=='h'))
					 {
					 	$FilasAct++;	
					 }
		          }

		          if($TotalFilasAGuardar==$FilasAct)
		          {
	
						$Respuesta=($_POST['TipoDocumento']=='Factura')? "Factura modificada correctamente":'Tiquete modificado correctamente';
						$GuarMod='Modifico';
  
				  }
				}
				else
				{
					//error al modificar fact borrar Los detalles ingresados	
					
					foreach ($IDDetalles as $IDDet) {
    					
    					$sql="delete from detallefactura where FK_FK_Usuario=$FK_Usuario AND FK_Factura and=$IDFactura AND IDDetalle=$IDDet;";

	            		$Conexion->query($sql);
					}
					
					$Respuesta=($_POST['TipoDocumento']=='Factura')? "Error al modificar la factura":"Error al modificar el tiquete";
					$GuarMod='Error';
					
				}
		}
		else //total anterior y actual son iguales
		{
			//error al modificar fact mostrar error(no se ha gardado el detalle)
			$Respuesta=($_POST['TipoDocumento']=='Factura')? "Error al modificar la factura":"Error al modificar el tiquete";				$GuarMod='Error';
		}
	}
	
		    $users_arr[] = array( 

	                         /*Pasar al vector la Respuesta*/
	                         "Respuesta"=>$Respuesta,
	                         "GuarMod"=>$GuarMod,
	                     );

	    // encoding array to json format
	    echo json_encode($users_arr);
	    exit;
	
}

function ConsultarFactura()
{
    $IDFactura=$_POST['IDFactura'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    $TipoDocAGenerar=(!empty($_POST['TipoDocAGenerar']))?$_POST['TipoDocAGenerar']:"";
    
    $OmitirComentario='';
    
    if(!empty($TipoDocAGenerar))
    {
		$OmitirComentario="AND FK_Producto NOT IN('000000000000000000')";
	}

    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

    $sql = "SELECT  F.IDFactura,
                    F.FK_Usuario,
                    F.NoFactura,

                    /*Datos del CLiente*/

                    F.FK_Cliente,
                    F.NombreCliente,
					IFNULL(F.EmailCliente,C.Email1) AS EmailCliente,

                    C.Telefono,
                    C.Direccion,
                    C.Zona,
                    C.Exonerado,

                    DATE_FORMAT(F.Fecha,'%d-%m-%Y  %h:%i:%s') as Fecha,
                    F.Plazo,
                    F.MedioPago,
                    F.CondicionVenta,
                    F.Status,
                    F.Clave,
                    F.NoOrden,
                    F.TipoMoneda,
                    F.TipoCambio,
                    F.TipoDocumento,
                    F.MontoGravado,
                    F.MontoExento,
                    F.MontoExonerado,
                    F.Descuento,
                    F.Impuesto,
                    F.ImpuestoDevuelto,
                    F.OtroImpuesto,
                    F.ServicioGravado,
                    F.ServicioExento,
                    F.ServicioExonerado,
                    F.SubtotalFactura,
                    F.TotalFactura,
                    F.Saldo,
                    F.Terminal,
                    F.Sucursal,
                    F.FK_ActividadEconomica
                    FROM factura F INNER JOIN cliente C ON F.FK_Cliente=C.Cedula AND F.FK_Usuario = C.FK_Usuario WHERE F.IDFactura=$IDFactura AND F.FK_Cliente='$CedulaCliente' AND F.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDFactura=$row["IDFactura"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=ObtenerNombreTipoDocumento($row["TipoDocumento"]);
      
      $NoFactura=($row["NoFactura"]=="") ? ObtenerConsecutivo($TipoDocumento) : $row["NoFactura"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
      $NoTipoDocAGenerar=(!empty($TipoDocAGenerar))?ObtenerConsecutivo($TipoDocumento):"";

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];
	  $EmailCliente=$row["EmailCliente"];
      $Telefono=$row["Telefono"];
      
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      $Exonerado=$row["Exonerado"];

      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $Plazo=$row["Plazo"];
      $MedioPago=$row["MedioPago"];
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row['Clave'];
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=$row["TipoMoneda"];
      $TipoCambio=$row["TipoCambio"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoF=$row["MontoGravado"];
      $MontoExentoF=$row["MontoExento"];
      $MontoExoneradoF=$row["MontoExonerado"];
      $DescuentoF=$row["Descuento"];
      $ImpuestoF=$row["Impuesto"];
      $ImpuestoDevueltoF=$row["ImpuestoDevuelto"];
      $OtroImpuestoF=$row["OtroImpuesto"];
      $ServicioGravadoF=$row["ServicioGravado"];
      $ServicioExentoF=$row["ServicioExento"];
      $ServicioExoneradoF=$row["ServicioExonerado"];
      $SubtotalFacturaF=$row["SubtotalFactura"];
      $TotalFacturaF=$row["TotalFactura"];
      $SaldoF=$row["Saldo"];
      $FK_ActividadEconomica=($row["FK_ActividadEconomica"]);

      /*Poner detalle de Factura*/  

      /*Hay que hacer los calculos de montos por fila con los datos del detalle*/  

      $sql="SELECT 

            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            MontoExonerado,
            MontoServicioExonerado,
            MontoImpuestoDevuelto,
            TotalNeto,
            NoLinea,
            CantidadDevuelta,
            FK_Producto

            FROM detallefactura WHERE FK_Factura=$IDFactura AND FK_FK_Usuario=$FK_Usuario $OmitirComentario ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
	  
	  $DetalleFactura = array();
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $IDDetalle= $ri['IDDetalle'];
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $PrecioVentaSinIV= $ri['PrecioVenta'];
	         $UnidadMedida= $ri['Medida'];
	         $ImpuestoVenta= $ri['Impuesto'];
	         $Descuento= $ri['Descuento'];
	         $MontoExonerado= $ri['MontoExonerado'];
	         $MontoServicioExonerado= $ri['MontoServicioExonerado'];
	         $MontoImpuestoDevuelto= $ri['MontoImpuestoDevuelto'];
	         $PrecioCosto= $ri['PrecioCosto'];
	         $Bonificacion= $ri['Bonificacion'];
	         $TotalNeto= $ri['TotalNeto'];
	         $NoLinea= $ri['NoLinea'];
			 $CantidadDevuelta=$ri['CantidadDevuelta'];

			 $PrecioTotalBruto=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);//para hacer todos los calculos

	         /*Variables para calculos*/
	          $MontoExento=0;
	          $MontoGravado=0;
	          
	          //$MontoExonerado=0;
	           
	          $MontoSExento=0;
	          $MontoSGravado=0;
	          
	          //$MontoSExonerado=0;
	          
	          $MontoIV=0;
	          $MontoOtroI=0;
	          $MontoDescuento=0;
	          $SubtotalL=0;
	          $TotalL=0;


	          if(($UnidadMedida!='sp' AND $UnidadMedida!='d'AND $UnidadMedida!='h') AND $ImpuestoVenta==0) /*Sino Tiene Impuesto*/
	          {
	            $MontoExento=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	            $MontoGravado=0; 
	        
	          }
	          else if(($UnidadMedida!='sp' AND $UnidadMedida!='d'AND $UnidadMedida!='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1') /*Si Tiene Impuesto*/ 
	          {
	            $MontoExento=0;
	            $MontoGravado=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	          }
	          else
		      {
			    $MontoExento=0;
			    $MontoGravado=0;	
			  }
	          
	          if(($UnidadMedida=='sp'||$UnidadMedida=='d'||$UnidadMedida=='h') AND $ImpuestoVenta==0)
	          {
	            $MontoSExento=bcdiv(($PrecioTotalBruto),1,2); /*Precio sin impuesto x Cantidad*/
	            $MontoSGravado=0;
	          }
	          else if(($UnidadMedida=='sp'||$UnidadMedida=='d'||$UnidadMedida=='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1')
	        {
	          $MontoSExento=0;/*Precio con impuesto x Cantidad*/
	            $MontoSGravado=bcdiv(($PrecioTotalBruto),1,2);
	        }
	        else
	        {
	          $MontoSExento=0;
	          $MontoSGravado=0;
	        }     

	          $MontoIV=bcdiv(((($PrecioTotalBruto)-(($PrecioTotalBruto)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
	          $MontoOtroI=0;/*se va a cambiar mas adelante*/
	          $MontoDescuento=bcdiv((($PrecioTotalBruto)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


	        if($Exonerado=='1' AND $ImpuestoVenta!=0)//es Exonerado
		    {
				$PorcentajeExoneracionTP=ObtenerPorcentajeGlobarExoneracion($FK_Cliente);
		        
		        if($PorcentajeExoneracionTP!='NO')
				{
					if(($UnidadMedida!='sp'AND $UnidadMedida!='d'AND $UnidadMedida!='h'))
		    		{						
						$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);//Precio sin impuesto x Cantidad 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExonerado),1,2);//Subtotal-Descuento+Impuesto
					}
					else //if(($UnidadMedida=='sp'OR $UnidadMedida=='d'OR $UnidadMedida=='h'))
		    		{						
						$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);//Precio sin impuesto x Cantidad 
			    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoServicioExonerado),1,2);//Subtotal-Descuento+Impuesto
					}
				}
				else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
				{
					
				}
			}
			else //No es Exonerado
			{
				$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
		    	$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
			}



	         $DetalleFactura[] = array(
	                                 "IDDetalle" => $IDDetalle,
	                                 "IDProducto" => $IDProducto, 
	                                 "NombreProducto" => $NombreProducto,
	                                 "PrecioVSinIV" => number_format($PrecioVentaSinIV,2), 
	                                 "Impuesto" =>$ImpuestoVenta, 
	                                 "Descuento" =>$Descuento, 
	                                 "Cantidad"=>$Cantidad,
	                                 "UM"=>$UnidadMedida,
	                                 "PrecioCosto"=>$PrecioCosto,
	                                 "Bonificacion"=>$Bonificacion,
	                                 
	                                 "MontoE"=>$MontoExento,
	                                 "MontoG"=>$MontoGravado,
	                                 "MontoSE"=>$MontoSExento,
	                                 "MontoSG"=>$MontoSGravado,
	                                 "MontoImpV"=>$MontoIV,
	                                 "MontoOImp"=>$MontoOtroI,
	                                 "MontoDesc"=>$MontoDescuento,
	                                 "MontoExonerado"=>$MontoExonerado,
	                                 "MontoServicioExonerado"=>$MontoServicioExonerado,
	                                 "MontoImpuestoDevuelto"=>$MontoImpuestoDevuelto,
	                                 "SubtL"=>number_format($SubtotalL,2),
	                                 "TotL"=>number_format($TotalL,2),
	                                 "NoLinea"=>$NoLinea,
	                                 "CantidadDevuelta"=>$CantidadDevuelta,
	                                );
	  }


    }

    $users_arr[] = array( 
                            "IDFactura"=>$IDFactura,
                            "FK_Usuario"=>$FK_Usuario,
                            "NoFactura"=>$NoFactura,
                            "FK_Cliente"=>$FK_Cliente,
                            "NombreCliente"=>$NombreCliente,
                            "Telefono"=>$Telefono,
                            "Email1"=>$EmailCliente,
                            "Direccion"=>$Direccion,
                            "Zona"=>$Zona,
                            "Exonerado"=>$Exonerado,
                            "Fecha"=>$Fecha,
                            "Plazo"=>$Plazo,
                            "MedioPago"=>$MedioPago,
                            "CondicionVenta"=>$CondicionVenta,
                            "Clave"=>$Clave,
                            "Status"=>$Status,
                            "NoOrden"=>$NoOrden,
                            "TipoMoneda"=>$TipoMoneda,
                            "TipoCambio"=>$TipoCambio,
                            "TipoDocumento"=>$TipoDocumento,
                            "NoTipoDocAGenerar"=>$NoTipoDocAGenerar,

                            /*Campos de Calculo de factura*/
                            "MontoGravado"=>number_format($MontoGravadoF,2),
                            "MontoExento"=>number_format($MontoExentoF,2),
                            "MontoExonerado"=>number_format($MontoExoneradoF,2),
                            "Descuento"=>number_format($DescuentoF,2),
                            "Impuesto"=>number_format($ImpuestoF,2),
                            "ImpuestoDevuelto"=>number_format($ImpuestoDevueltoF,2),
                            "OtroImpuesto"=>number_format($OtroImpuestoF,2),
                            "ServicioGravado"=>number_format($ServicioGravadoF,2),
                            "ServicioExento"=>number_format($ServicioExentoF,2),
                            "ServicioExonerado"=>number_format($ServicioExoneradoF,2),
                            "SubtotalFactura"=>number_format($SubtotalFacturaF,2),
                            "TotalFactura"=>number_format($TotalFacturaF,2),
                            /*******************************/
                            "Saldo"=>$SaldoF,
                            "Terminal"=>$Terminal,
                            "Sucursal"=>$Sucursal,
                            "ActividadEconomica"=>$FK_ActividadEconomica,
                            
                            /*DetalleFactura*/
                            "DetalleFactura"=>$DetalleFactura,
                       );

      // encoding array to json format
      echo json_encode($users_arr);
      exit;

}

function BorrarFactura($IDDocumento)
{
	$FK_Usuario=$_SESSION['IDUsuario'];
	
	$Respuesta;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 
	
	 $TotalFilas=0;
	 $TotalModificaciones=0;
	 
	 //trer el detalle de la factura antes de borrar y sumar cantidad del detalle al inventario por cada fila del detalle
	
		$sql="SELECT 
		
            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            TotalNeto,
            NoLinea,
            FK_Producto

            FROM detallefactura WHERE FK_Factura=$IDDocumento AND FK_FK_Usuario=$FK_Usuario ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $TotalFilas++;
      	
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $Bonificacion= $ri['Bonificacion'];
	         $Medida= $ri['Medida'];
	         
	         if($Medida!='sp'AND $Medida!='h'AND $Medida!='h')
	         {
			 	$sql="UPDATE producto 
	    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual+($Cantidad+$Bonificacion) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";
	         
		         if($Conexion->query($sql))
				 {
					$TotalModificaciones++;	
				 }	
			 }
			 else
			 {
			 	$TotalModificaciones++;	
			 }
	  }
	  
	  if($TotalFilas==$TotalModificaciones)
	  {
	  		$sql="delete from detallefactura where FK_FK_Usuario=$FK_Usuario AND FK_Factura=$IDDocumento";

			if($Conexion->query($sql))
			{
				  	$sql="delete from factura where FK_Usuario=$FK_Usuario AND IDFactura=$IDDocumento";
				
							if($Conexion->query($sql))
							{								
								$Respuesta="Factura borrada correctamente";
							}
							else
							{
								$Respuesta="Error al borrar la factura";
							}
				
			}	
	  }
	  else
	  {
	  		$Respuesta="Error al borrar la factura";	
	  }
	
	  $users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
}

function ConsultarEstadoAntesDeEnvio($IDapi,$NoClaveLarga)
{
//Enviar a hacienda	    

$JSONConsulta='{
			  "data": {
			    "nombre_usuario": "'.$IDapi.'",
			    "clavelarga": "'.$NoClaveLarga.'"
			  }
			}';
			            
//API URL Envio
$urlEnvio = "http://35.170.39.96/api/NuevoServer/consultar";

//create a new cURL resource
$ch = curl_init($urlEnvio);

//poner JSON en curl
curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONConsulta);

//set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

//return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute the POST request
$result = curl_exec($ch);

$Respuesta= $result;

//close cURL resource
curl_close($ch);	
                
//guardar el estado de la respuesta

$ResultadoEstado = '';

if( strpos( $Respuesta,'rechazado' ) !== false) 
{
$ResultadoEstado='rechazado';
}
else if( strpos( $Respuesta,'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
{
$ResultadoEstado='aceptado';
}
else if( strpos( $Respuesta,'procesando' ) !== false) 
{
$ResultadoEstado='procesando';
}
else if( strpos( $Respuesta,'El documento ha sido enviado a Hacienda pero Hacienda no ha dado respuesta' ) !== false) 
{
$ResultadoEstado='sin respuesta';
}
else
{
	$ResultadoEstado='no enviado';
}

return $ResultadoEstado; 

}

function ConsultarEstado($IDDoc,$FK_Cliente,$FK_Usuario)
{
	$IDapi=ObtenerID_API($FK_Usuario);
	$NoClaveLarga=ObtenerClaveLarga($IDDoc,$FK_Usuario,$FK_Cliente);
	
	//Enviar a hacienda	    
	
	$JSONConsulta='{
				  "data": {
				    "nombre_usuario": "'.$IDapi.'",
				    "clavelarga": "'.$NoClaveLarga.'"
				  }
				}';
				            
	//API URL Envio
	$urlEnvio = "http://35.170.39.96/api/NuevoServer/consultar";

	//create a new cURL resource
	$ch = curl_init($urlEnvio);
	
	//poner JSON en curl
	curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONConsulta);

	//set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

	//return response instead of outputting
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	//execute the POST request
	$result = curl_exec($ch);

	$Respuesta= $result;

	//close cURL resource
	curl_close($ch);	
	                
	//guardar el estado de la respuesta
		
		$ResultadoEstado= '';
				
		if( strpos( $Respuesta,'rechazado' ) !== false) 
	    {
	    	$ResultadoEstado='rechazado';
		}
		else if( strpos( $Respuesta,'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
	    {
	    	$ResultadoEstado='aceptado';
		}
		else if( strpos( $Respuesta,'procesando' ) !== false) 
	    {
	    	$ResultadoEstado='procesando';
		}
		else if( strpos( $Respuesta,'El documento ya ha sido registado anteriormente' ) !== false) 
	    {
	    	$ResultadoEstado=rtrim(substr($Respuesta, strpos($Respuesta , 'es ')+ 2),'"}');
		}
		else if( strpos( $Respuesta,'sin_enviar' ) !== false || 
			   strpos( $Respuesta,'"status":500' ) !== false ||
			   strpos( $Respuesta,'"status":"500"' ) !== false) 
	    {
	    	$ResultadoEstado='en espera';
		}
		else if( strpos( $Respuesta,'no dio ninguna respuesta' ) !== false) 
	    {
	    	$ResultadoEstado='sin respuesta';
		}
		else
		{
			$ResultadoEstado='no enviado';
		}

				/*
				$IniMsj = '"message":{"0":"';
				$FinalMsj = '"},"clave":';
				$ResultadoMsj = ObtenerEstatusYMensajeHacienda($Respuesta,$IniMsj,$FinalMsj);
		*/
		
	$sql="UPDATE factura SET Status='$ResultadoEstado', MensajeHacienda='' WHERE IDFactura=$IDDoc and FK_Usuario=$FK_Usuario;";

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  if($Conexion->query($sql) === TRUE) 
  { 
      $Exito='';
  }
   
  	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
  
}

function EnviarAHacienda()
{
	$IDDocumento=$_POST['IDDocumento'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    //variables por si el cliente es exonerado
    $TipoDocumentoExo="";
	$NumeroDocumentoExo="";
	$NombreInstitucionExo="";
	$FechaEmisionExo="";
    
    $Respuesta='';
    
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

    $sql = "SELECT  F.IDFactura,
			        F.FK_Usuario,
			        F.NoFactura,
			        
			        /*Datos del Usuario*/
			        U.ID_API,
					LPAD(CAST(U.Cedula AS CHAR(12)), 12, '0') AS CedulaUsuario,

			        /*Datos del CLiente*/

			        F.FK_Cliente,
			        F.NombreCliente,

					C.TipoCedula,
			        C.Telefono,
			        C.Email1,
			        C.Email2,
			        C.Direccion,
			        C.Zona,
			        C.Exonerado,

			        DATE_FORMAT(F.Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha,
			        DATE_FORMAT(F.Fecha,'%d%m%y') AS FechaPClave,
			        DATE_FORMAT(F.Fecha,'%Y-%m-%d  %h:%i:%s') as FechaPCliente,
			        F.Plazo,
                    F.MedioPago,
                    F.CondicionVenta,
                    F.Status,
                    F.Clave,
                    F.NoOrden,
                    F.TipoMoneda,
                    F.TipoCambio,
                    F.TipoDocumento,
                    F.MontoGravado,
                    F.MontoExento,
                    F.MontoExonerado,
                    F.Descuento,
                    F.Impuesto,
                    F.ImpuestoDevuelto,
                    F.OtroImpuesto,
                    F.ServicioGravado,
                    F.ServicioExento,
                    F.ServicioExonerado,
                    F.SubtotalFactura,
                    F.TotalFactura,
                    F.Saldo,
                    F.Terminal,
                    F.Sucursal,
			        LPAD(CAST(F.FK_ActividadEconomica AS CHAR(6)), 6, '0') as FK_ActividadEconomica
			        FROM factura F 
					  INNER JOIN usuario U ON F.FK_Usuario=U.IDUsuario
					  INNER JOIN cliente C ON F.FK_Cliente=C.Cedula
					  AND F.FK_Usuario = C.FK_Usuario 
					  WHERE F.IDFactura=$IDDocumento AND F.FK_Cliente='$CedulaCliente' AND F.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDFactura=$row["IDFactura"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=$row["TipoDocumento"];
      $NombreTipoDoc=ObtenerNombreTipoDocumento($TipoDocumento);
      
      $NoFactura=($row["NoFactura"]=="") ? ObtenerConsecutivo($NombreTipoDoc) : $row["NoFactura"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
	  $ID_API=$row["ID_API"];
	  $TipoCedula=ObternerTipoIdentificacionNumerica($row['TipoCedula']);
	  $CedulaUsuario=$row["CedulaUsuario"];

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];

      $Telefono=$row["Telefono"];
      $Email1=$row["Email1"];
      $Email2=$row["Email2"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      $Exonerado=$row["Exonerado"];
      
      //Partir la Zona
      $Provincia=substr($Zona,0,1);
      $Canton=substr($Zona,1,2);
      $Distrito=substr($Zona,3,4);

      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $FechaPClave=$row["FechaPClave"];
      $FechaPCliente=$row["FechaPCliente"];
      $Plazo=$row["Plazo"];
      $MedioPago= MedioParaJSON($row["MedioPago"]);
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row['Clave'];
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=ObtenerCodigoMoneda($row["TipoMoneda"]);
      $TipoCambio=$row["TipoCambio"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoF=$row["MontoGravado"];
      $MontoExentoF=$row["MontoExento"];
      $MontoExoneradoF=$row["MontoExonerado"];
      $DescuentoF=$row["Descuento"];
      $ImpuestoF=$row["Impuesto"];
      $ImpuestoDevueltoF=$row["ImpuestoDevuelto"];
      $OtroImpuestoF=$row["OtroImpuesto"];
      $ServicioGravadoF=$row["ServicioGravado"];
      $ServicioExentoF=$row["ServicioExento"];
      $ServicioExoneradoF=$row["ServicioExonerado"];
      $SubtotalFacturaF=$row["SubtotalFactura"];
      $TotalFacturaF=$row["TotalFactura"];
      $SaldoF=$row["Saldo"];
      $FK_ActividadEconomica=($row["FK_ActividadEconomica"]);
      
      /*Variables que hay que guardar*/
      $Consecutivo=($row["NoFactura"]=="") ? $Terminal.$Sucursal.$TipoDocumento.$NoFactura : $row["NoFactura"];
      $ClaveLarga=($Clave=='')?"506".$FechaPClave.$CedulaUsuario.$Consecutivo."1".'00000000':$Clave;
      
      //Si es exonerado el cliente sacar los datos de la exoneracion y ponerlo en las constantes
      if($Exonerado=='1')
      {
	  	$InfoExoneracion=ObtenerInfoExoneracion($FK_Cliente);
	  		    
	    $TipoDocumentoExo=$InfoExoneracion[0]["TipoDocumento"];
		$NumeroDocumentoExo=$InfoExoneracion[0]["NumeroDocumentoExo"];
		$NombreInstitucionExo=$InfoExoneracion[0]["NombreInstitucionExo"];
		$FechaEmisionExo=$InfoExoneracion[0]["FechaEmisionExo"];			 
	  }
      
      $DetFact=DetalleFacturaParaJSON($IDFactura,$FK_Usuario,$FK_Cliente,$Exonerado,$TipoDocumentoExo,$NumeroDocumentoExo,$NombreInstitucionExo,$FechaEmisionExo);
      
      //poner las constantes en "" porque ya no se ocupan
      
      
      
	  	  if($NombreTipoDoc=='Factura')
	      {
		  		$JSONEnvio=
	      			'{
					  "data": {
					    "tipo": "1",
					    "nombre_usuario": "'.$ID_API.'",
					    "situacion": "normal",
					    "consecutivo": "'.$Consecutivo.'",
					    "clavelarga": "'.$ClaveLarga.'",
					    "terminal": "'.$Sucursal.'",
					    "sucursal": "'.$Terminal.'",
					    "fechafactura": "'.$Fecha.'",
					    "codigoActividad": "'.$FK_ActividadEconomica.'",
					    "otro_telefono": "",
					    "receptor": {
					      "tipoIdentificacion": "'.$TipoCedula.'",
					      "numeroIdentificacion": "'.$FK_Cliente.'",
					      "nombre": "'.$NombreCliente.'",
					      "provincia": "'.$Provincia.'",
					      "canton": "'.$Canton.'",
					      "distrito": "'.$Distrito.'",
					      "otrasSenas": "'.$Direccion.'",
					      "codigoPais": "506",
					      "telefono": "'.$Telefono.'",
					      "website": "",
					      "correo": "'.$Email1.'",
					      "nombreComercial": "",
					      "correo_gastos": "'.$Email2.'"
					    },
					    "condicionVenta": "'.$CondicionVenta.'",
					    "plazoCredito": "'.$Plazo.'",
					    "numFecha": "'.$Fecha.'",
					    "numReferencia": "'.$IDFactura.'",
					    "codigoVendedor": "00",
					    "medioPago": [
					        '.$MedioPago.'
					    ],
					    "detalles": [
					      '.$DetFact.'
						],
						"codigoTipoMoneda": {
					      "tipoMoneda": "'.$TipoMoneda.'",
					      "tipoCambio": "'.$TipoCambio.'"
					    },					    
					    "totalServGravados": "'.$ServicioGravadoF.'",
					    "totalServExentos": "'.$ServicioExentoF.'",
					    "totalServExonerado": "'.$ServicioExoneradoF.'",
					    "totalMercanciasGravados": "'.$MontoGravadoF.'",
					    "totalMercanciasExentos": "'.$MontoExentoF.'",
					    "totalMercExonerada": "'.$MontoExoneradoF.'",
					    "totalGravados": "'.bcdiv(($ServicioGravadoF+$MontoGravadoF),1,2).'",
					    "totalExentos": "'.bcdiv(($ServicioExentoF+$MontoExentoF),1,2).'",
					    "totalExonerado": "'.bcdiv(($ServicioExoneradoF+$MontoExoneradoF),1,2).'",
					    "totalVentas": "'.bcdiv(($ServicioGravadoF+$MontoGravadoF)+($ServicioExentoF+$MontoExentoF)+($ServicioExoneradoF+$MontoExoneradoF),1,2).'",
					    "totalDescuentos": "'.$DescuentoF.'",
					    "totalVentasNetas": "'.$SubtotalFacturaF.'",
					    "totalImpuestos": "'.bcdiv(($ImpuestoF+$OtroImpuestoF),1,2).'",
					    "totalIVADevuelto": "'.$ImpuestoDevueltoF.'",
					    "totalOtrosCargos": "0.00000",
					    "totalComprobantes": "'.$TotalFacturaF.'",
					    "otrosCargos": {
					      "tipoDocumento": "",
					      "numeroIdentidadTercero": "",
					      "nombreTercero": "",
					      "detalle": "",
					      "porcentaje": "0.00000",
					      "montoCargo": "0.00000"
					    },
					    "referencia": "",
					    "otros": {
					      "otroTexto": "Tipo de Cambio:. '.$TipoCambio.'",
					      "otroContenido": ""
					    }
					  }
					}';
		  }
		  else if($NombreTipoDoc=='Tiquete')
		  {
		  		$JSONEnvio=
	      			'{
					  "data": {
					    "tipo": "4",
					    "nombre_usuario": "'.$ID_API.'",
					    "situacion": "normal",
					    "consecutivo": "'.$Consecutivo.'",
					    "clavelarga": "'.$ClaveLarga.'",
					    "terminal": "'.$Sucursal.'",
					    "sucursal": "'.$Terminal.'",
					    "fechafactura": "'.$Fecha.'",
					    "codigoActividad": "'.$FK_ActividadEconomica.'",
					    "otro_telefono": "",
					    "condicionVenta": "'.$CondicionVenta.'",
					    "plazoCredito": "'.$Plazo.'",
					    "numFecha": "'.$Fecha.'",
					    "numReferencia": "'.$IDFactura.'",
					    "codigoVendedor": "00",
					    "medioPago": [
					        '.$MedioPago.'
					    ],
					    "detalles": [
					      '.$DetFact.'
						],
					    "codigoTipoMoneda": {
					      "tipoMoneda": "'.$TipoMoneda.'",
					      "tipoCambio": "'.$TipoCambio.'"
					    },
					    "totalServGravados": "'.$ServicioGravadoF.'",
					    "totalServExentos": "'.$ServicioExentoF.'",
					    "totalServExonerado": "'.$ServicioExoneradoF.'",
					    "totalMercanciasGravados": "'.$MontoGravadoF.'",
					    "totalMercanciasExentos": "'.$MontoExentoF.'",
					    "totalMercExonerada": "'.$MontoExoneradoF.'",
					    "totalGravados": "'.bcdiv(($ServicioGravadoF+$MontoGravadoF),1,2).'",
					    "totalExentos": "'.bcdiv(($ServicioExentoF+$MontoExentoF),1,2).'",
					    "totalExonerado": "'.bcdiv(($ServicioExoneradoF+$MontoExoneradoF),1,2).'",
					    "totalVentas": "'.bcdiv(($ServicioGravadoF+$MontoGravadoF)+($ServicioExentoF+$MontoExentoF)+($ServicioExoneradoF+$MontoExoneradoF),1,2).'",
					    "totalDescuentos": "'.$DescuentoF.'",
					    "totalVentasNetas": "'.$SubtotalFacturaF.'",
					    "totalImpuestos": "'.bcdiv(($ImpuestoF+$OtroImpuestoF),1,2).'",
					    "totalIVADevuelto": "'.$ImpuestoDevueltoF.'",
					    "totalOtrosCargos": "0.00000",
					    "totalComprobantes": "'.$TotalFacturaF.'",
					    "otrosCargos": {
					      "tipoDocumento": "",
					      "numeroIdentidadTercero": "",
					      "nombreTercero": "",
					      "detalle": "",
					      "porcentaje": "0.00000",
					      "montoCargo": "0.00000"
					    },
					    "referencia": "",
					    "otros": {
					      "otroTexto": "Tipo de Cambio:. '.$TipoCambio.'",
					      "otroContenido": ""
					    }
					  }
					}';
		  }
				
				//actualizar clave y consecutivo de la factura
				
				  $sql="UPDATE factura SET NoFactura='$Consecutivo', Clave='$ClaveLarga' WHERE IDFactura=$IDFactura and FK_Usuario=$FK_Usuario;";

		          if($Conexion->query($sql) === TRUE) 
		          { 
		          	 //Actualizar consecutivo del documento sumar 1	solo si es una factura nueva
		          	 if($Clave=='' || $row["NoFactura"]=="")
		          	 {
					 	$sql="UPDATE consecutivo SET $NombreTipoDoc=$NombreTipoDoc+1 WHERE FK_Usuario=$FK_Usuario;";
			
						if($Conexion->query($sql))
						{								
							$Exito="";
						}
					 }
		          	 
		  	      }
				
				//Enviar a hacienda	    
				            
				//API URL Envio si el error es 403 nada mas
				$urlEnvio =(($Clave!=''||$row["NoFactura"]!="") && ($row["Status"]=='sin respuesta'||$row["Status"]=='no enviado'))
				//es un documento ya enviado que no llego a hacienda=>reenviar
						   ?'http://35.170.39.96/api/NuevoServer/reenviarfactura'
						   :'http://35.170.39.96/api/NuevoServer/enviar';//es un documento nuevo, enviar
						   
						   //envio directo: http://35.170.39.96/api/NuevoServer/enviar_directo
				
				//$urlEnvio ='http://35.170.39.96/api/NuevoServer/enviar_prueba';

				//create a new cURL resource
				$ch = curl_init($urlEnvio);
				
				//poner JSON en curl
				curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

				//set the content type to application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

				//return response instead of outputting
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				//execute the POST request
				$result = curl_exec($ch);

				$Respuesta= $result;

				//close cURL resource
				curl_close($ch);	
				                
				//guardar el estado de la respuesta
				
				 $ResultadoEstado= '';
				
				if( strpos( $Respuesta,'rechazado' ) !== false) 
			    {
			    	$ResultadoEstado='rechazado';
				}
				else if( strpos( $Respuesta,'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
			    {
			    	$ResultadoEstado='aceptado';
				}
				else if( strpos( $Respuesta,'procesando' ) !== false) 
			    {
			    	$ResultadoEstado='procesando';
				}
				else if( strpos( $Respuesta,'El documento ya ha sido registado anteriormente' ) !== false) 
			    {
			    	$ResultadoEstado=rtrim(substr($Respuesta, strpos($Respuesta , 'es ')+ 2),'"}');
				}
				else if( strpos( $Respuesta,'sin_enviar' ) !== false || 
					   strpos( $Respuesta,'"status":500' ) !== false ||
					   strpos( $Respuesta,'"status":"500"' ) !== false) 
			    {
			    	$ResultadoEstado='en espera';
				}
				else if( strpos( $Respuesta,'no dio ninguna respuesta' ) !== false) 
			    {
			    	$ResultadoEstado='sin respuesta';
				}
				else
				{
					$ResultadoEstado='no enviado';
				}
				/*
				$IniMsj = '"message":{"0":"';
				$FinalMsj = '"},"clave":';
				$ResultadoMsj = ObtenerEstatusYMensajeHacienda($Respuesta,$IniMsj,$FinalMsj);
		*/
				$sql="UPDATE factura SET Status='$ResultadoEstado', MensajeHacienda='' WHERE IDFactura=$IDFactura and FK_Usuario=$FK_Usuario;";

			  if($Conexion->query($sql) === TRUE) 
			  { 
			      $Exito='';
			  }
		
				if($NombreTipoDoc=='Factura' && ($Clave=='' || $row["NoFactura"]==""))
				{
					  $sql="UPDATE cliente SET FechaUltimaFacturacion='$FechaPCliente', TotalVentas=TotalVentas+$TotalFacturaF WHERE Cedula='$FK_Cliente' and FK_Usuario=$FK_Usuario;";

					  if($Conexion->query($sql) === TRUE) 
					  { 

					      $Exito='';
					  }	
				}			    
	}
	
	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
    
}

function ReenviarEmail()
{
	$IDDocumento=$_POST['IDDocumento'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    //variables por si el cliente es exonerado
    $TipoDocumentoExo="";
	$NumeroDocumentoExo="";
	$NombreInstitucionExo="";
	$FechaEmisionExo="";
    
    $Respuesta='';
    
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

    $sql = "SELECT  F.IDFactura,
			        F.FK_Usuario,
			        F.NoFactura,
			        
			        /*Datos del Usuario*/
			        U.ID_API,
					LPAD(CAST(U.Cedula AS CHAR(12)), 12, '0') AS CedulaUsuario,

			        /*Datos del CLiente*/

			        F.FK_Cliente,
			        F.NombreCliente,

					C.TipoCedula,
			        C.Telefono,
			        C.Email1,
			        C.Email2,
			        C.Direccion,
			        C.Zona,
			        C.Exonerado,

			        DATE_FORMAT(F.Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha,
			        DATE_FORMAT(F.Fecha,'%d%m%y') AS FechaPClave,
			        DATE_FORMAT(F.Fecha,'%Y-%m-%d  %h:%i:%s') as FechaPCliente,
			        F.Plazo,
                    F.MedioPago,
                    F.CondicionVenta,
                    F.Status,
                    F.Clave,
                    F.NoOrden,
                    F.TipoMoneda,
                    F.TipoCambio,
                    F.TipoDocumento,
                    F.MontoGravado,
                    F.MontoExento,
                    F.MontoExonerado,
                    F.Descuento,
                    F.Impuesto,
                    F.ImpuestoDevuelto,
                    F.OtroImpuesto,
                    F.ServicioGravado,
                    F.ServicioExento,
                    F.ServicioExonerado,
                    F.SubtotalFactura,
                    F.TotalFactura,
                    F.Saldo,
                    F.Terminal,
                    F.Sucursal,
			        LPAD(CAST(F.FK_ActividadEconomica AS CHAR(6)), 6, '0') as FK_ActividadEconomica
			        FROM factura F 
					  INNER JOIN usuario U ON F.FK_Usuario=U.IDUsuario
					  INNER JOIN cliente C ON F.FK_Cliente=C.Cedula 
					  AND F.FK_Usuario = C.FK_Usuario
					  WHERE F.IDFactura=$IDDocumento AND F.FK_Cliente='$CedulaCliente' AND F.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDFactura=$row["IDFactura"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=$row["TipoDocumento"];
      $NombreTipoDoc=ObtenerNombreTipoDocumento($TipoDocumento);
      
      $NoFactura=($row["NoFactura"]=="") ? ObtenerConsecutivo($NombreTipoDoc) : $row["NoFactura"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
	  $ID_API=$row["ID_API"];
	  $TipoCedula=ObternerTipoIdentificacionNumerica($row['TipoCedula']);
	  $CedulaUsuario=$row["CedulaUsuario"];

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];

      $Telefono=$row["Telefono"];
      $Email1=$row["Email1"];
      $Email2=$row["Email2"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      $Exonerado=$row["Exonerado"];
      
      //Partir la Zona
      $Provincia=substr($Zona,0,1);
      $Canton=substr($Zona,1,2);
      $Distrito=substr($Zona,3,4);

      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $FechaPClave=$row["FechaPClave"];
      $FechaPCliente=$row["FechaPCliente"];
      $Plazo=$row["Plazo"];
      $MedioPago= MedioParaJSON($row["MedioPago"]);
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row['Clave'];
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=ObtenerCodigoMoneda($row["TipoMoneda"]);
      $TipoCambio=$row["TipoCambio"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoF=$row["MontoGravado"];
      $MontoExentoF=$row["MontoExento"];
      $MontoExoneradoF=$row["MontoExonerado"];
      $DescuentoF=$row["Descuento"];
      $ImpuestoF=$row["Impuesto"];
      $ImpuestoDevueltoF=$row["ImpuestoDevuelto"];
      $OtroImpuestoF=$row["OtroImpuesto"];
      $ServicioGravadoF=$row["ServicioGravado"];
      $ServicioExentoF=$row["ServicioExento"];
      $ServicioExoneradoF=$row["ServicioExonerado"];
      $SubtotalFacturaF=$row["SubtotalFactura"];
      $TotalFacturaF=$row["TotalFactura"];
      $SaldoF=$row["Saldo"];
      
      $FK_ActividadEconomica=($row["FK_ActividadEconomica"]);
      
      /*Variables que hay que guardar*/
      $Consecutivo=($row["NoFactura"]=="") ? $Terminal.$Sucursal.$TipoDocumento.$NoFactura : $row["NoFactura"];
      $ClaveLarga=($Clave=='')?"506".$FechaPClave.$CedulaUsuario.$Consecutivo."1".'00000000':$Clave;
      
      //Si es exonerado el cliente sacar los datos de la exoneracion y ponerlo en las constantes
      if($Exonerado=='1')
      {
	  	$InfoExoneracion=ObtenerInfoExoneracion($FK_Cliente);
	  		    
	    $TipoDocumentoExo=$InfoExoneracion[0]["TipoDocumento"];
		$NumeroDocumentoExo=$InfoExoneracion[0]["NumeroDocumentoExo"];
		$NombreInstitucionExo=$InfoExoneracion[0]["NombreInstitucionExo"];
		$FechaEmisionExo=$InfoExoneracion[0]["FechaEmisionExo"];			 
	  }
      
      $DetFact=DetalleFacturaParaJSON($IDFactura,$FK_Usuario,$FK_Cliente,$Exonerado,$TipoDocumentoExo,$NumeroDocumentoExo,$NombreInstitucionExo,$FechaEmisionExo);
      
      if($Status=='aceptado')
      {
	  	  if($NombreTipoDoc=='Factura')
	      {
		  		$JSONEnvio=
	      			'{
					  "data": {
					    "tipo": "1",
					    "nombre_usuario": "'.$ID_API.'",
					    "situacion": "normal",
					    "consecutivo": "'.$Consecutivo.'",
					    "clavelarga": "'.$ClaveLarga.'",
					    "terminal": "'.$Sucursal.'",
					    "sucursal": "'.$Terminal.'",
					    "fechafactura": "'.$Fecha.'",
					    "codigoActividad": "'.$FK_ActividadEconomica.'",
					    "otro_telefono": "",
					    "receptor": {
					      "tipoIdentificacion": "'.$TipoCedula.'",
					      "numeroIdentificacion": "'.$FK_Cliente.'",
					      "nombre": "'.$NombreCliente.'",
					      "provincia": "'.$Provincia.'",
					      "canton": "'.$Canton.'",
					      "distrito": "'.$Distrito.'",
					      "otrasSenas": "'.$Direccion.'",
					      "codigoPais": "506",
					      "telefono": "'.$Telefono.'",
					      "website": "",
					      "correo": "'.$Email1.'",
					      "nombreComercial": "",
					      "correo_gastos": "'.$Email2.'"
					    },
					    "condicionVenta": "'.$CondicionVenta.'",
					    "plazoCredito": "'.$Plazo.'",
					    "numFecha": "'.$Fecha.'",
					    "numReferencia": "'.$IDFactura.'",
					    "codigoVendedor": "00",
					    "medioPago": [
					        '.$MedioPago.'
					    ],
					    "detalles": [
					      '.$DetFact.'
						],
						"codigoTipoMoneda": {
					      "tipoMoneda": "'.$TipoMoneda.'",
					      "tipoCambio": "'.$TipoCambio.'"
					    },					    
					    "totalServGravados": "'.$ServicioGravadoF.'",
					    "totalServExentos": "'.$ServicioExentoF.'",
					    "totalServExonerado": "'.$ServicioExoneradoF.'",
					    "totalMercanciasGravados": "'.$MontoGravadoF.'",
					    "totalMercanciasExentos": "'.$MontoExentoF.'",
					    "totalMercExonerada": "'.$MontoExoneradoF.'",
					    "totalGravados": "'.bcdiv(($ServicioGravadoF+$MontoGravadoF),1,2).'",
					    "totalExentos": "'.bcdiv(($ServicioExentoF+$MontoExentoF),1,2).'",
					    "totalExonerado": "'.bcdiv(($ServicioExoneradoF+$MontoExoneradoF),1,2).'",
					    "totalVentas": "'.bcdiv(($ServicioGravadoF+$MontoGravadoF)+($ServicioExentoF+$MontoExentoF)+($ServicioExoneradoF+$MontoExoneradoF),1,2).'",
					    "totalDescuentos": "'.$DescuentoF.'",
					    "totalVentasNetas": "'.$SubtotalFacturaF.'",
					    "totalImpuestos": "'.bcdiv(($ImpuestoF+$OtroImpuestoF),1,2).'",
					    "totalIVADevuelto": "'.$ImpuestoDevueltoF.'",
					    "totalOtrosCargos": "0.00000",
					    "totalComprobantes": "'.$TotalFacturaF.'",
					    "otrosCargos": {
					      "tipoDocumento": "",
					      "numeroIdentidadTercero": "",
					      "nombreTercero": "",
					      "detalle": "",
					      "porcentaje": "0.00000",
					      "montoCargo": "0.00000"
					    },
					    "referencia": "",
					    "otros": {
					      "otroTexto": "Tipo de Cambio:. '.$TipoCambio.'",
					      "otroContenido": ""
					    }
					  }
					}';
					
					
					//Regenerar PDF por si no esta	
				
				$urlEnvio = "http://35.170.39.96/api/NuevoServer/generar_pdf";

				//create a new cURL resource
				$ch = curl_init($urlEnvio);
				
				//poner JSON en curl
				curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

				//set the content type to application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

				//return response instead of outputting
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				//execute the POST request
				$result = curl_exec($ch);

				$Respuesta= $result;

				//close cURL resource
				curl_close($ch);
					
					//Enviar email    
				            
				//API URL Envio
				$urlEnvio = "http://35.170.39.96/api/NuevoServer/enviar_correo";

				//create a new cURL resource
				$ch = curl_init($urlEnvio);
				
				//poner JSON en curl
				curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

				//set the content type to application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

				//return response instead of outputting
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				//execute the POST request
				$result = curl_exec($ch);

				$Respuesta= $result;

				//close cURL resource
				curl_close($ch);
		  }
		  	    
		}
	}
	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
    
}

function ObternerTipoIdentificacionNumerica($TipoCedula)
{
	$TipoDocumento;
	
	if($TipoCedula=='F')
	{
		$TipoDocumento='01';
	}
	else if($TipoCedula=='J')
	{
		$TipoDocumento='02';
	}
	else if($TipoCedula=='D')
	{
		$TipoDocumento='03';
	}
	else if($TipoCedula=='E')
	{
		$TipoDocumento='05';
	}
	else//N
	{
		$TipoDocumento='04';
	}
	
	return $TipoDocumento;
}

function ObtenerCodigoMoneda($TipoMoneda)
{
	$CodMoneda='';
	
	if($TipoMoneda=='C')
	{
		$CodMoneda='CRC';
	}
	else if($TipoMoneda=='D')
	{
		$CodMoneda='USD';
	}
	else if($TipoMoneda=='E')
	{
		$CodMoneda='EUR';
	}
	
	return $CodMoneda;
}

function QuitarFormatoNumero($Numero)
{
	//$Numero=number_format((float)$Numero, 2, '.', '');
	
	return str_replace(",", "", $Numero);
}

function MedioParaJSON($MedioPago)
{
	$Medio='';
	
	$MedioPago=str_replace('0','',$MedioPago);
	
	$chars = str_split($MedioPago);
	foreach($chars as $char)
	{
    	if($char=='1')
    	{
			$Medio.='{"codigo": "01"},'.PHP_EOL;
		}
	    else if($char=='2')
    	{
			$Medio.='{"codigo": "02"},'.PHP_EOL;
		}
		else if($char=='3')
    	{
			$Medio.='{"codigo": "03"},'.PHP_EOL;
		}
		else if($char=='4')
    	{
			$Medio.='{"codigo": "04"},'.PHP_EOL;
		}
		else if($char=='5')
    	{
			$Medio.='{"codigo": "05"},'.PHP_EOL;
		} 	
	}
		
	return rtrim($Medio, ',' . PHP_EOL);
}

function DetalleFacturaParaJSON($IDFactura,$FK_Usuario,$FK_Cliente,$Exonerado,$TipoDocumentoExo,$NumeroDocumentoExo,$NombreInstitucionExo,$FechaEmisionExo)
{
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

	$sql="SELECT 

            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            MontoExonerado,
            MontoServicioExonerado,
            MontoImpuestoDevuelto,
            TotalNeto,
            NoLinea,
            FK_Producto

            FROM detallefactura WHERE FK_Factura=$IDFactura AND FK_FK_Usuario=$FK_Usuario ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
	  
	  $rowcount=mysqli_num_rows($result);
	  
	  $DetalleFactura = '';
      
      while($ri =  mysqli_fetch_array($result))
      {      		 
      		 $IDDetalle= $ri['IDDetalle'];
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $PrecioVentaSinIV= $ri['PrecioVenta'];
	         $UnidadMedida= ConvertirUnidades($ri['Medida']);
	         $ImpuestoVenta= $ri['Impuesto'];
	         $Descuento= $ri['Descuento'];
	         $PrecioCosto= $ri['PrecioCosto'];
	         $Bonificacion= $ri['Bonificacion'];
	         $TotalNeto= $ri['TotalNeto'];
	         $NoLinea= $ri['NoLinea'];
	         
	         $MontoExonerado= $ri['MontoExonerado'];
	         $MontoServicioExonerado= $ri['MontoServicioExonerado'];
	         $MontoImpuestoDevuelto= $ri['MontoImpuestoDevuelto'];

			 $PrecioTotalBruto=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);//para hacer todos los calculos
	         /*Variables para calculos*/
	          $MontoExento=0;
	          $MontoGravado=0; 
	          $MontoSExento=0;
	          $MontoSGravado=0;
	          $MontoIV=0;
	          $MontoOtroI=0;
	          $MontoDescuento=0;
	          $SubtotalL=0;
	          $TotalL=0;


	          if(($UnidadMedida!='Sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h') AND $ImpuestoVenta==0) /*Sino Tiene Impuesto*/
	          {
	            $MontoExento=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	            $MontoGravado=0; 
	        
	          }
	          else if(($UnidadMedida!='Sp' AND $UnidadMedida!='d' AND $UnidadMedida!='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1') /*Si Tiene Impuesto*/ 
	          {
	            $MontoExento=0;
	            $MontoGravado=bcdiv(($PrecioTotalBruto),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	          }
	          else
			    {
				  $MontoExento=0;
				  $MontoGravado=0;	
				}
	          
	          if(($UnidadMedida=='Sp' || $UnidadMedida=='d' || $UnidadMedida=='h') AND $ImpuestoVenta==0)
	          {
	            $MontoSExento=bcdiv(($PrecioTotalBruto),1,2); /*Precio sin impuesto x Cantidad*/
	            $MontoSGravado=0;
	          }
	          else if(($UnidadMedida=='Sp' || $UnidadMedida=='d' || $UnidadMedida=='h') AND $ImpuestoVenta!=0 AND $Exonerado!='1')
	        {
	          $MontoSExento=0;/*Precio con impuesto x Cantidad*/
	            $MontoSGravado=bcdiv(($PrecioTotalBruto),1,2);
	        }
	        else
	        {
	          $MontoSExento=0;
	          $MontoSGravado=0;
	        }     

	          $MontoIV=bcdiv(((($PrecioTotalBruto)-(($PrecioTotalBruto)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
	          $MontoOtroI=0;/*se va a cambiar mas adelante*/
	          $MontoDescuento=bcdiv((($PrecioTotalBruto)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


	          	if(($Exonerado=='1' AND $ImpuestoVenta!=0)OR($Exonerado=='1' AND $ImpuestoVenta==0))//es Exonerado
			    {
					//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
					$PorcentajeExoneracionTP=ObtenerPorcentajeGlobarExoneracion($FK_Cliente);
			        
			        if($PorcentajeExoneracionTP!='NO')
					{
						if($UnidadMedida!='Sp' AND $UnidadMedida!='d'AND $UnidadMedida!='h')
			    		{						
							$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
				    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoExonerado),1,2);/*Subtotal-Descuento+Impuesto*/
						}
						else if($UnidadMedida=='Sp' OR $UnidadMedida=='d'OR $UnidadMedida=='h')
			    		{						
							$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
				    		$TotalL=bcdiv(($SubtotalL+$MontoIV-$MontoServicioExonerado),1,2);/*Subtotal-Descuento+Impuesto*/
						}
					}
					else //no tiene impuesto para todos los productos, ir a la tabla de ExoneracionProductoXCliente
					{
						
					}
				}
				else //No es Exonerado
				{
					$SubtotalL=bcdiv(($PrecioTotalBruto)-$MontoDescuento,1,2);/*Precio sin impuesto x Cantidad*/ 
			    	$TotalL=bcdiv(($SubtotalL+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
				}
				
				/*
				$NodoIVA= ($Exonerado=='1')? 	'"impuesto": [
											       {
											         "codigoTarifa": "08",
										             "codigo": "01",
										             "tarifa": "'.$ImpuestoVenta.'",
										             "monto": "'.$MontoIV.'",
										             "factorIVA": "",
										             "exoneracion": {
									                  "Tipodocumento": "'.$TipoDocumentoExo.'",
										              "NumeroDocumento": "'.$NumeroDocumentoExo.'",
										              "NombreInstitucion": "'.$NombreInstitucionExo.'",
										              "FechaEmision": "'.$FechaEmisionExo.'",
										              "porcentajeExoneracion": "'.$PorcentajeExoneracionTP.'",
										              "montoExoneracion": "'.bcdiv(($MontoExonerado+$MontoServicioExonerado),1,2).'"
										            }
											       }
											     ],'
									     	:"";
	          */
	          
	          if($ImpuestoVenta=='0.00')//Es producto exento sin importar el cliente
	          {
			 $DetalleFactura.='{
						         "numero": "'.$NoLinea.'",
						         "cantidad": "'.$Cantidad.'",
						         "codigoComercial": [
						           {
						           	 "tipo": "01",
						             "codigo": "'.$IDProducto.'"
						           }
						         ],
						         "unidadMedidaComercial": "",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioTotalBruto).'",
						         "descuento": [
					               {
					               	 "monto": "'.$MontoDescuento.'",
					            	 "descripcionDescuento": "Descuento"
					               }
					             ],
						         "subtotal": "'.$SubtotalL.'",
						         "impuestoNeto": "'.bcdiv(($MontoIV-($MontoExonerado+$MontoServicioExonerado)),1,2).'",
						         "totalLinea": "'.$TotalL.'",
						         "partidaArancelaria": "",
        						 "baseImponible": "0.00000"
						       },';
			  }
			  else if($ImpuestoVenta!='0.00' AND $Exonerado!='1')//No Es producto exento con cliente no exonerado
			  {
			 $DetalleFactura.='{
						         "numero": "'.$NoLinea.'",
						         "cantidad": "'.$Cantidad.'",
						         "codigoComercial": [
						           {
						           	 "tipo": "01",
						             "codigo": "'.$IDProducto.'"
						           }
						         ],
						         "unidadMedidaComercial": "",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioTotalBruto).'",
						         "descuento": [
					               {
					               	 "monto": "'.$MontoDescuento.'",
					            	 "descripcionDescuento": "Descuento"
					               }
					             ],
						         "subtotal": "'.$SubtotalL.'",
						         "impuesto": [
							       {
							         "codigoTarifa": "08",
						             "codigo": "01",
						             "tarifa": "'.$ImpuestoVenta.'",
						             "monto": "'.$MontoIV.'",
						             "factorIVA": ""
							       }
							     ],
							     "impuestoNeto": "'.bcdiv(($MontoIV-($MontoExonerado+$MontoServicioExonerado)),1,2).'",
						         "totalLinea": "'.$TotalL.'",
						         "partidaArancelaria": "",
        						 "baseImponible": "0.00000"
						       },';
			  }
			  else if($ImpuestoVenta!='0.00' AND $Exonerado=='1')//no es producto exento con cliente exonerado
	          {
	         $DetalleFactura.='{
						         "numero": "'.$NoLinea.'",
						         "cantidad": "'.$Cantidad.'",
						         "codigoComercial": [
						           {
						           	 "tipo": "01",
						             "codigo": "'.$IDProducto.'"
						           }
						         ],
						         "unidadMedidaComercial": "",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioTotalBruto).'",
						         "descuento": [
					               {
					               	 "monto": "'.$MontoDescuento.'",
					            	 "descripcionDescuento": "Descuento"
					               }
					             ],
						         "subtotal": "'.$SubtotalL.'",
						         "impuesto": [
							       {
							         "codigoTarifa": "08",
						             "codigo": "01",
						             "tarifa": "'.$ImpuestoVenta.'",
						             "monto": "'.$MontoIV.'",
						             "factorIVA": "",
						             "exoneracion": {
					                  "Tipodocumento": "'.$TipoDocumentoExo.'",
						              "NumeroDocumento": "'.$NumeroDocumentoExo.'",
						              "NombreInstitucion": "'.$NombreInstitucionExo.'",
						              "FechaEmision": "'.$FechaEmisionExo.'",
						              "porcentajeExoneracion": "'.$PorcentajeExoneracionTP.'",
						              "montoExoneracion": "'.bcdiv(($MontoExonerado+$MontoServicioExonerado),1,2).'"
						            }
							       }
							     ],
							     "impuestoNeto": "'.bcdiv(($MontoIV-($MontoExonerado+$MontoServicioExonerado)),1,2).'",
						         "totalLinea": "'.$TotalL.'",
						         "partidaArancelaria": "",
        						 "baseImponible": "0.00000"
						       },';
			  }
	}
	
	/*esto va abajo de Factor iva cuando el cliente es exonerado
	
*/
	
	return rtrim($DetalleFactura,',');
}

function ObtenerID_API($FK_Usuario)
{
	$ID_API='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

	$sql="SELECT ID_API FROM Usuario where IDUsuario=$FK_Usuario;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $ID_API=$row["ID_API"];
    }
    
	return $ID_API;			
}


function ObtenerClaveLarga($IDDocumento,$FK_Usuario,$FK_Cliente)
{
	$ClaveLarga='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

	$sql="SELECT Clave FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDFactura=$IDDocumento;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $ClaveLarga=$row["Clave"];
    }
    
	return $ClaveLarga;		
	
		
}

function ConvertirUnidades($UnidadMedida)
{
	$UM='';

	switch ($UnidadMedida) 
	{
	    case "Unidad":
	        $UM='Unid';
	        break;
	    case "Kg":
	        $UM='kg';
	        break;
	    case "oz":
	        $UM='Oz';
	        break;
	    case "l":
	        $UM='L';
	        break;
	    case "gal":
	        $UM='Gal';
	        break;
	    case "m":
	        $UM='m';
	        break;
	    case "min":
	        $UM='min';
	        break;
	    case "h":
	        $UM='h';
	        break;
	    case "d":
	        $UM='d';
	        break;
	    case "ml":
	        $UM='mL';
	        break;
	    case "g":
	        $UM='g';
	        break;
	    case "t":
	        $UM='t';
	        break;
	    case "sp":
	        $UM='Sp';
	        break;
	    default:
	        $UM='';
	}
	
	return $UM;	
}

function ObtenerPorcentajeGlobarExoneracion($CedulaCliente)
{
	//ir a tabla de cliente a ver si tiene exoneracion para todos los productos
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
    
    $PorcentajeExoneracionTP='';

    $sql="SELECT
		 (
		 CASE 
		     WHEN TodosProductosExonerados=1 THEN PorcentajeExoneracion
		     ELSE 'NO'
		 END
		 ) AS PorcentajeExoneracion
		 FROM cliente
		 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";
    
    $result=$Conexion->query($sql);

    $row = $result->fetch_assoc();
    $PorcentajeExoneracionTP=$row["PorcentajeExoneracion"];	
    
    return $PorcentajeExoneracionTP;
}

function ObtenerInfoExoneracion($CedulaCliente)
{
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }
	
	$InfoExoneracion= array();
	
	$sql="SELECT TipoExoneracion,NoExoneracion,NombreInstitucionExoneracion,
	  				 DATE_FORMAT(FechaExoneracion,'%Y-%m-%dT%H:%i:%s%-06:00') AS FechaExoneracion FROM cliente
	  				 WHERE FK_Usuario=".$_SESSION['IDUsuario']." AND Cedula='$CedulaCliente';";
	  				 
	  	$result=$Conexion->query($sql);

	    $row = $result->fetch_assoc();
	    
	    $TipoDocumentoExo=$row["TipoExoneracion"];
		$NumeroDocumentoExo=$row["NoExoneracion"];
		$NombreInstitucionExo=$row["NombreInstitucionExoneracion"];
		$FechaEmisionExo=$row["FechaExoneracion"];	
		
		$InfoExoneracion[]=array(
								"TipoDocumento"=>$TipoDocumentoExo,
								"NumeroDocumentoExo"=>$NumeroDocumentoExo,
								"NombreInstitucionExo"=>$NombreInstitucionExo,
								"FechaEmisionExo"=>$FechaEmisionExo
								);
		return $InfoExoneracion;	
}

?>