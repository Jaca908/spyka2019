<?php 
session_start();

require ("../Conexion/Conexion.php");

//Verificar que el usuario haya pulsado el boton de agregar producto
if(isset($_POST['btnAgregarProducto'])) 
{
	AgregarProducto();
}
else if(isset($_POST['btnFactura']))
{
    GrabarFactura();//Probar tiquete en modificar y guardar:listo
}
else if(isset($_POST['MostrarDatos']))
{
    ConsultarFactura();
}
else if(isset($_POST['btnBorrarFilaDetalle']))
{
    BorrarFilaDetalleFactura();
}
else if(isset($_POST['btnBorrarFilaDetalleFactExist']))
{
    BorrarFilaDetalleFacturaExistente();
}
else if(isset($_POST['btnBorrar']))
{
	BorrarFactura($_POST['IDDocumento']);//Tiquete listo	
}
else if(isset($_POST['btnEnviarAHacienda']))
{
	//EnviarAHacienda();
}
else if(isset($_POST['btnReenviarEmail']))
{
	ReenviarEmail();
}
else if(isset($_POST['btnConsultarEstado']))
{
	ConsultarEstado($_POST['IDDocumento'],$_POST['CedulaCliente'],$_SESSION['IDUsuario']);
}
else if(isset($_POST['btnEnviar']))
{
	EnvioMasivo();
}
else if(isset($_POST['CargarNoDocumento']))
{
    $TipoDocumento=$_POST['TipoDocumento'];

    $NoConsecutivo= ObtenerConsecutivo($TipoDocumento);

        $users_arr[] = array( 
                         "NoConsecutivo"=>$NoConsecutivo,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}

function EnvioMasivo()
{
	$FacturasCompra=$_POST['FacturasCompras'];
	$MensajeEstado='';
	
	if($FacturasCompra!=null)
	{
		foreach($FacturasCompra as $i => $item) 
	    {
		   if($FacturasCompra[$i]!='undefined')
		   {		        
		   		
		   	$NumeroElectronico=$FacturasCompra[$i]['NumeroElectronico'];
		   	$NombreEmisor=$FacturasCompra[$i]['NombreEmisor'];
		   	
		   	$FK_Usuario=$_SESSION['IDUsuario'];
		   	$Aceptacion='';
		   	$Mensaje='';
		   	
		   	if($FacturasCompra[$i]['Aceptacion']=='Total'){$Aceptacion='05'; $Mensaje='01';}
		   	else if($FacturasCompra[$i]['Aceptacion']=='Parcial'){$Aceptacion='06'; $Mensaje='02';}
		   	else if($FacturasCompra[$i]['Aceptacion']=='Rechazo'){$Aceptacion='07'; $Mensaje='03';}
		   	
		   	
		   	$sql="SELECT
		   			Co.IDCompra,
					Co.ClaveHacienda,
					Co.ClaveLarga,
					Co.Secuencia,
					Co.FK_Cliente AS CedulaEmisor,
					Cl.TipoCedula AS TipoCedulaEmisor,
					Co.TotalComprobante,
					DATE_FORMAT(Co.Fecha,'%d%m%y') AS FechaPClave,
					U.ID_API,
					U.MsjCompraPHacienda,
					U.Cedula as CedulaReceptor,
					U.TipoCedula as TipoCedulaReceptor,
					LPAD(CAST(U.Cedula AS CHAR(12)), 12, '0') AS CedulaReceptorPClave,
					Co.TotalImpuesto

					FROM compra Co 
					INNER JOIN cliente Cl ON Co.FK_Cliente=Cl.Cedula
					INNER JOIN usuario U ON Co.FK_Usuario=U.IDUsuario

					WHERE Co.FK_Usuario=$FK_Usuario AND Co.NumeroConsecutivoHacienda='$NumeroElectronico';";
					
			$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            if ($Conexion->connect_error) 
            {
                die("Connection failed: " . $Conexion->connect_error);
            } 
            
            $result = $Conexion->query($sql);

		    if ($result->num_rows > 0) 
		    {
		      	$row = $result->fetch_assoc();
		      	
		      	//Select datos
		      	$IDCompra=$row["IDCompra"];
		      	
			   	$ID_API=$row["ID_API"];
			   	$MsjCompraPHacienda=$row["MsjCompraPHacienda"];
			   	
			   	$Fecha=$row["FechaPClave"];
			   	$Clave=$row["ClaveLarga"];
			   	$Secuencia=$row["Secuencia"];
			   	$CedulaReceptorPClave=$row["CedulaReceptorPClave"];
			   	$ClaveHacienda=$row["ClaveHacienda"];//la del XML
			   	$CedulaEmisor=$row["CedulaEmisor"];
			   	$TipoCedulaEmisor=FormatoTipoCedulaNumerico($row["TipoCedulaEmisor"]);
			   	$MontoTotal=$row["TotalComprobante"];
			   	$ConsecutivoInterno=ObtenerConsecutivo('Compra');
			   	$Consecutivo=($Secuencia=='')?'00100001'.$Aceptacion.$ConsecutivoInterno:$Secuencia;
			   	$ClaveLarga=($Clave=='')?'506'.$Fecha.$CedulaReceptorPClave.$Consecutivo.'100000000':$Clave;//05 Total 06 Parcial 07 Rechazo
			   	$CedulaReceptor=$row["CedulaReceptor"];
			   	$TipoCedulaReceptor=FormatoTipoCedulaNumerico($row["TipoCedulaReceptor"]);
			   	$MontoImpuesto=$row["TotalImpuesto"];
			   	
			   	 $JSONEnvio=
	      			'{
  "data": {
    "nombre_usuario": "'.$ID_API.'",
    "claveHacienda": "'.$ClaveHacienda.'",
    "clavelarga": "'.$ClaveLarga.'",
    "emisor": {
      "tipoIdentificacion": "'.$TipoCedulaEmisor.'",
      "numeroIdentificacion": "'.$CedulaEmisor.'"
    },
    "mensaje": "'.$Mensaje.'",
    "detalle": "'.$MsjCompraPHacienda.'",
    "total": "'.$MontoTotal.'",
    "consecutivo": "'.$Consecutivo.'",
    "receptor": {
      "tipoIdentificacion": "'.$TipoCedulaReceptor.'",
      "numeroIdentificacion": "'.$CedulaReceptor.'"
    },
    "montoImpuesto": "'.$MontoImpuesto.'"
  }
}';
			   	 
			   	 	$sql="UPDATE compra SET Secuencia='$Consecutivo', ClaveLarga='$ClaveLarga' WHERE IDCompra=$IDCompra and FK_Usuario=$FK_Usuario;";

			          if($Conexion->query($sql) === TRUE) 
			          { 
			          	 //Actualizar consecutivo del documento sumar 1	solo si es una factura nueva
			          	 if($Clave=='' || $row["Secuencia"]=="")
			          	 {
						 	$sql="UPDATE consecutivo SET Compra=Compra+1 WHERE FK_Usuario=$FK_Usuario;";
				
							if($Conexion->query($sql))
							{								
								$Exito="";
							}
						 }
			          	 
			  	      }
				
					//Enviar a hacienda	    
					            
					//API URL Envio
					$urlEnvio = 'http://35.170.39.96/api/server/mensajereceptor';

					//create a new cURL resource
					$ch = curl_init($urlEnvio);
					
					//poner JSON en curl
					curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

					//set the content type to application/json
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

					//return response instead of outputting
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					//execute the POST request
					$result = curl_exec($ch);

					$Respuesta= $result;

					//close cURL resource
					curl_close($ch);	
					                
					//guardar el estado de la respuesta
					
					 $ResultadoEstado= '';
					
					if( strpos( strtolower($Respuesta),'rechazado' ) !== false) 
				    {
				    	$ResultadoEstado='rechazado';
					}
					else if( strpos( strtolower($Respuesta),'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
				    {
				    	$ResultadoEstado='aceptado';
					}
					else if( strpos( strtolower($Respuesta),'procesando' ) !== false) 
				    {
				    	$ResultadoEstado='procesando';
					}
					else if( strpos( strtolower($Respuesta),'El documento ha sido enviado a Hacienda pero Hacienda no ha dado respuesta' ) !== false) 
				    {
				    	$ResultadoEstado='sin respuesta';
					}
					else
					{
						$ResultadoEstado='no enviado';
					}
		      
		      		$sql="UPDATE compra SET Status='$ResultadoEstado', Mensaje='$Respuesta' WHERE IDCompra=$IDCompra and FK_Usuario=$FK_Usuario;";

					  if($Conexion->query($sql) === TRUE) 
					  { 
					      $Exito='';
					  }
			
					$MensajeEstado.="El documento Nº $ClaveHacienda del Emisor $NombreEmisor, Cédula $CedulaEmisor se encuentra:$ResultadoEstado<br>";		
			
				}
		   	
		   	
		   }
	    }
	}

      echo $MensajeEstado;
      exit;
}

function ObtenerConsecutivo($TipoDocumento)
{
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $sql="SELECT
          LPAD(CAST(".$TipoDocumento." AS CHAR(10)), 10, '0') as NoConsecutivo
          FROM consecutivo
          WHERE FK_Usuario=".$_SESSION['IDUsuario'].";";

    $NoConsecutivo="";
    
    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $NoConsecutivo=$row["NoConsecutivo"];
    }

    return $NoConsecutivo;
}

function ObtenerTipoDocumentoNumerico($TipoDocumento)
{
    if($TipoDocumento=='Factura')
   {
        $TipoDocumento='01';    
   }
   else if($TipoDocumento=='NotaDebito')
   {
        $TipoDocumento='02';
   }
   else if($TipoDocumento=='NotaCredito')
   {
        $TipoDocumento='03';
   }
   else if($TipoDocumento=='Tiquete')
   {
        $TipoDocumento='04';
   }

   return $TipoDocumento;
}

function ObtenerNombreTipoDocumento($TipoDocumento)
{
    if($TipoDocumento=='01')
   {
        $TipoDocumento='Factura';    
   }
   else if($TipoDocumento=='02')
   {
        $TipoDocumento='NotaDebito';
   }
   else if($TipoDocumento=='03')
   {
        $TipoDocumento='NotaCredito';
   }
   else if($TipoDocumento=='04')
   {
        $TipoDocumento='Tiquete';
   }

   return $TipoDocumento;
}

function BorrarFilaDetalleFacturaExistente()
{
	$FK_Usuario=$_SESSION['IDUsuario'];
	$IDFactura= $_POST['IDFactura'];
   	$IDDetalleFactura= $_POST['IDetalleFactura'];
	$IDProducto= $_POST['IDProducto'];
	$Cantidad= $_POST['Cantidad'];
	$Bonificacion= $_POST['Bonificacion'];
	$UnidadMedida= $_POST['UM'];
	
	$Respuesta;
	
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
        die("Connection failed: " . $Conexion->connect_error);
    }

	/*Eliminar DetalleFactura en la bd*/

	$sql="DELETE FROM detallefactura WHERE FK_Factura=$IDFactura AND FK_FK_Usuario=$FK_Usuario AND IDDetalle=$IDDetalleFactura;";
    
    if($Conexion->query($sql) === TRUE) 
	{
		 /*Modificar CantProducto en la bd*/
	
		if($UnidadMedida!='sp')
		{
			$sql="UPDATE producto 
	    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual+($Cantidad+$Bonificacion) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

			if($Conexion->query($sql) === TRUE) 
			{
				 	$MontoExentoT=$_POST['MET'];
				    $MontoGravadoT=$_POST['MGT'];
				    $MontoSExentoT=$_POST['MSET'];
				    $MontoSGravadoT=$_POST['MSGT'];
				    $MontoIVT=$_POST['MIVT'];
				    $MontoOtroIT=$_POST['MOIT'];
				    $MontoDescuentoT=$_POST['MDT'];
				    $SubtotalT=QuitarFormatoNumero($_POST['ST']);
				    $TotalT=QuitarFormatoNumero($_POST['TT']);

				    /*Campos de Calculos de Factura*/

				    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
				    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
				    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
				    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
				    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
				    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
				    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
				    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
				    $TotF=QuitarFormatoNumero($_POST['MontTF']);

					$MontoExF=bcdiv(($MontoExF-$MontoExentoT),1,2);
				    $MontoGrF=bcdiv(($MontoGrF-$MontoGravadoT),1,2);
				    $MontoSExF=bcdiv(($MontoSExF-$MontoSExentoT),1,2);
				    $MontoSGrF=bcdiv(($MontoSGrF-$MontoSGravadoT),1,2);
				    $MontoIVF=bcdiv(($MontoIVF-$MontoIVT),1,2);
				    $MontoOtroIF=bcdiv(($MontoOtroIF-$MontoOtroIT),1,2);
				    $MontoDescF=bcdiv(($MontoDescF-$MontoDescuentoT),1,2);
				    $SubF=bcdiv(($SubF-$SubtotalT),1,2);
				    $TotF=bcdiv(($TotF-$TotalT),1,2);
				    $Saldo=0;/*para mas adelante trabajar con abonos*/
				    
				    //Actualizar en bd los totales de la factura/tiquete
				    
				    $sql =
			    "UPDATE factura SET 
			    				 MontoGravado=$MontoGrF,
								 MontoExento=$MontoExF,
								 Descuento=$MontoDescF,
								 Impuesto=$MontoIVF,
								 OtroImpuesto=$MontoOtroIF,
								 ServicioGravado=$MontoSGrF,
								 ServicioExento=$MontoSExF,
								 SubtotalFactura=$SubF,
								 TotalFactura=$TotF,
								 Saldo=$Saldo
				WHERE FK_Usuario= $FK_Usuario AND IDFactura=$IDFactura";;

				if($Conexion->query($sql) === TRUE) 
				{ 	    
			    	$Respuesta="Producto eliminado de la factura correctamente";
				}
				else
				{
					$Respuesta="Error: no se pudo eliminar el producto de la factura";
				}
			}
		}
		else
		{
			$MontoExentoT=$_POST['MET'];
		    $MontoGravadoT=$_POST['MGT'];
		    $MontoSExentoT=$_POST['MSET'];
		    $MontoSGravadoT=$_POST['MSGT'];
		    $MontoIVT=$_POST['MIVT'];
		    $MontoOtroIT=$_POST['MOIT'];
		    $MontoDescuentoT=$_POST['MDT'];
		    $SubtotalT=QuitarFormatoNumero($_POST['ST']);
		    $TotalT=QuitarFormatoNumero($_POST['TT']);

		    /*Campos de Calculos de Factura*/

		    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
		    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
		    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
		    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
		    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
		    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
		    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
		    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
		    $TotF=QuitarFormatoNumero($_POST['MontTF']);

			$MontoExF=bcdiv(($MontoExF-$MontoExentoT),1,2);
		    $MontoGrF=bcdiv(($MontoGrF-$MontoGravadoT),1,2);
		    $MontoSExF=bcdiv(($MontoSExF-$MontoSExentoT),1,2);
		    $MontoSGrF=bcdiv(($MontoSGrF-$MontoSGravadoT),1,2);
		    $MontoIVF=bcdiv(($MontoIVF-$MontoIVT),1,2);
		    $MontoOtroIF=bcdiv(($MontoOtroIF-$MontoOtroIT),1,2);
		    $MontoDescF=bcdiv(($MontoDescF-$MontoDescuentoT),1,2);
		    $SubF=bcdiv(($SubF-$SubtotalT),1,2);
		    $TotF=bcdiv(($TotF-$TotalT),1,2);			
		    $Saldo=0;/*para mas adelante trabajar con abonos*/
				    
				    //Actualizar en bd los totales de la factura/tiquete
				    
				    $sql =
			    "UPDATE factura SET 
			    				 MontoGravado=$MontoGrF,
								 MontoExento=$MontoExF,
								 Descuento=$MontoDescF,
								 Impuesto=$MontoIVF,
								 OtroImpuesto=$MontoOtroIF,
								 ServicioGravado=$MontoSGrF,
								 ServicioExento=$MontoSExF,
								 SubtotalFactura=$SubF,
								 TotalFactura=$TotF,
								 Saldo=$Saldo
				WHERE FK_Usuario= $FK_Usuario AND IDFactura=$IDFactura";;

				if($Conexion->query($sql) === TRUE) 
				{ 	    
			    	$Respuesta="Producto eliminado de la factura correctamente";
				}
				else
				{
					$Respuesta="Error: no se pudo eliminar el producto de la factura";
				}
		}
	}
	else
	{
		$Respuesta="Error: no se pudo eliminar el producto de la factura";
	}

    /*Pasar al vector las variables de campo de calculos*/
    $users_arr[] = array( 

                         /*Pasar al vector las variables de campo de calculos*/
                         "MontoEF"=>number_format($MontoExF,2),
                         "MontoGF"=>number_format($MontoGrF,2),
                         "MontoSEF"=>number_format($MontoSExF,2),
                         "MontoSGF"=>number_format($MontoSGrF,2),
                         "MontoImpVF"=>number_format($MontoIVF,2),
                         "MontoOImpF"=>number_format($MontoOtroIF,2),
                         "MontoDescF"=>number_format($MontoDescF,2),
                         "SubtF"=>number_format($SubF,2),
                         "TotF"=>number_format($TotF,2),
                         "Respuesta"=>$Respuesta,
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit;
}

function BorrarFilaDetalleFactura()
{

    /*Campos de la tabla*/
    $IDProducto=$_POST['IDp'];
    $NombreProducto=$_POST['NombreP'];
    $PrecioVentaSinIV=QuitarFormatoNumero($_POST['PV']);
    $ImpuestoVenta=$_POST['IV'];
    $Descuento=$_POST['Des'];
    $Cantidad=$_POST['Cant'];
    $UnidadMedida=$_POST['UM'];

    $MontoExentoT=$_POST['MET'];
    $MontoGravadoT=$_POST['MGT'];
    $MontoSExentoT=$_POST['MSET'];
    $MontoSGravadoT=$_POST['MSGT'];
    $MontoIVT=$_POST['MIVT'];
    $MontoOtroIT=$_POST['MOIT'];
    $MontoDescuentoT=$_POST['MDT'];
    $SubtotalT=QuitarFormatoNumero($_POST['ST']);
    $TotalT=QuitarFormatoNumero($_POST['TT']);

    /*Campos de Calculos de Factura*/

    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
    $TotF=QuitarFormatoNumero($_POST['MontTF']);

  $MontoExF=bcdiv(($MontoExF-$MontoExentoT),1,2);
    $MontoGrF=bcdiv(($MontoGrF-$MontoGravadoT),1,2);
    $MontoSExF=bcdiv(($MontoSExF-$MontoSExentoT),1,2);
    $MontoSGrF=bcdiv(($MontoSGrF-$MontoSGravadoT),1,2);
    $MontoIVF=bcdiv(($MontoIVF-$MontoIVT),1,2);
    $MontoOtroIF=bcdiv(($MontoOtroIF-$MontoOtroIT),1,2);
    $MontoDescF=bcdiv(($MontoDescF-$MontoDescuentoT),1,2);
    $SubF=bcdiv(($SubF-$SubtotalT),1,2);
    $TotF=bcdiv(($TotF-$TotalT),1,2);


    /*Pasar al vector las variables de campo de calculos*/
    $users_arr[] = array( 

                         /*Pasar al vector las variables de campo de calculos*/
                         "MontoEF"=>number_format($MontoExF,2),
                         "MontoGF"=>number_format($MontoGrF,2),
                         "MontoSEF"=>number_format($MontoSExF,2),
                         "MontoSGF"=>number_format($MontoSGrF,2),
                         "MontoImpVF"=>number_format($MontoIVF,2),
                         "MontoOImpF"=>number_format($MontoOtroIF,2),
                         "MontoDescF"=>number_format($MontoDescF,2),
                         "SubtF"=>number_format($SubF,2),
                         "TotF"=>number_format($TotF,2),
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}

function AgregarProducto()
{
  $IDProducto=$_POST['IDp'];
  $NombreProducto=$_POST['NombreP'];
  $PrecioVentaSinIV=QuitarFormatoNumero($_POST['PV']);
    $ImpuestoVenta=$_POST['IV'];
    $Descuento=$_POST['Des'];
    $Cantidad=$_POST['Cant'];
    $UnidadMedida=$_POST['UM'];
    $PreComp=$_POST['PreComp'];
    $Bonificacion=$_POST['Bonificacion'];

    /*Campos de Calculos de Factura*/

    $MontoExF=QuitarFormatoNumero($_POST['MontExF']);
    $MontoGrF=QuitarFormatoNumero($_POST['MontGrF']);
    $MontoSExF=QuitarFormatoNumero($_POST['MontSEF']);
    $MontoSGrF=QuitarFormatoNumero($_POST['MontSGF']);
    $MontoIVF=QuitarFormatoNumero($_POST['MontIVF']);
    $MontoOtroIF=QuitarFormatoNumero($_POST['MontOImpF']);
    $MontoDescF=QuitarFormatoNumero($_POST['MontDescF']);
    $SubF=QuitarFormatoNumero($_POST['MontSubF']);
    $TotF=QuitarFormatoNumero($_POST['MontTF']);

    /*Variables para calculos*/
    $MontoExento=0;
    $MontoGravado=0;
    $MontoSExento=0;
    $MontoSGravado=0;
    $MontoIV=0;
    $MontoOtroI=0;
    $MontoDescuento=0;
    $SubtotalL=0;
    $TotalL=0;


    if($ImpuestoVenta==0) /*Sino Tiene Impuesto*/
    {
      $MontoExento=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
      $MontoGravado=0; 
  
    }
    else /*Si Tiene Impuesto*/ 
    {
      $MontoExento=0;
      $MontoGravado=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
    }
    
    if($UnidadMedida=='sp' AND $ImpuestoVenta==0)
    {
      $MontoSExento=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2); /*Precio sin impuesto x Cantidad*/
      $MontoSGravado=0;
    }
    else if($UnidadMedida=='sp' AND $ImpuestoVenta!=0)
  {
    $MontoSExento=0;/*Precio con impuesto x Cantidad*/
      $MontoSGravado=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);
  }
  else
  {
    $MontoSExento=0;
    $MontoSGravado=0;
  }     

    $MontoIV=bcdiv(((($PrecioVentaSinIV*$Cantidad)-(($PrecioVentaSinIV*$Cantidad)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
    $MontoOtroI=0;/*se va a cambiar mas adelante*/
    $MontoDescuento=bcdiv((($PrecioVentaSinIV*$Cantidad)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


    $SubtotalL=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);/*Precio sin impuesto x Cantidad*/ 
    $TotalL=bcdiv(($SubtotalL-$MontoDescuento+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/


    /*Hacer Calculos de calculos de Campos*/

    $MontoExF=bcdiv(($MontoExF+$MontoExento),1,2);
    $MontoGrF=bcdiv(($MontoGrF+$MontoGravado),1,2);
    $MontoSExF=bcdiv(($MontoSExF+$MontoSExento),1,2);
    $MontoSGrF=bcdiv(($MontoSGrF+$MontoSGravado),1,2);
    $MontoIVF=bcdiv(($MontoIVF+$MontoIV),1,2);
    $MontoOtroIF=bcdiv(($MontoOtroIF+$MontoOtroI),1,2);
    $MontoDescF=bcdiv(($MontoDescF+$MontoDescuento),1,2);
    $SubF=bcdiv(($SubF+$SubtotalL),1,2);
    $TotF=bcdiv(($TotF+$TotalL),1,2);



    /*Pasar variables a vector para ponerlos en los campos de tabla y de calculos en el formulario*/
    $users_arr[] = array("IDProducto" => $IDProducto, 
                         "NombreProducto" => $NombreProducto,
                         "PrecioVSinIV" => number_format($PrecioVentaSinIV,2), 
                         "Impuesto" =>$ImpuestoVenta, 
                         "Descuento" =>$Descuento, 
                         "Cantidad"=>$Cantidad,
                         "UM"=>$UnidadMedida,
                         "PreComp"=>$PreComp,
                         "Bonificacion"=>$Bonificacion,
                         
                         "MontoE"=>$MontoExento,
                         "MontoG"=>$MontoGravado,
                         "MontoSE"=>$MontoSExento,
                         "MontoSG"=>$MontoSGravado,
                         "MontoImpV"=>$MontoIV,
                         "MontoOImp"=>$MontoOtroI,
                         "MontoDesc"=>$MontoDescuento,
                         "SubtL"=>number_format($SubtotalL,2),
                         "TotL"=>number_format($TotalL,2),

                         /*Pasar al vector las variables de campo de calculos*/
                         "MontoEF"=>number_format($MontoExF,2),
                         "MontoGF"=>number_format($MontoGrF,2),
                         "MontoSEF"=>number_format($MontoSExF,2),
                         "MontoSGF"=>number_format($MontoSGrF,2),
                         "MontoImpVF"=>number_format($MontoIVF,2),
                         "MontoOImpF"=>number_format($MontoOtroIF,2),
                         "MontoDescF"=>number_format($MontoDescF,2),
                         "SubtF"=>number_format($SubF,2),
                         "TotF"=>number_format($TotF,2),
                     );

    // encoding array to json format
    echo json_encode($users_arr);
    exit; 
}


function GrabarFactura()
{
	$GuardarModifcarFactura=$_POST['btnFactura'];
	
	$GuarMod='';
	$Respuesta=''; //para mostrar errores o msj de exito al guardar
	
	if($GuardarModifcarFactura=='GrabarFactura')
	{
		/*Guardar el encabezado de factura*/    

	   $IDFactura=($_POST['IDFactura']!="")?$_POST['IDFactura']:0;
	   $FK_Usuario=$_SESSION['IDUsuario'];
	   $FK_Cliente= $_POST['Cedula'];
	   $NombreCliente= $_POST['Nombre'];

	   $NoReferencia="";/*Hay que hacer los campos en el formulario primero*/
	   $Razon="";/*Hay que hacer los campos en el formulario primero*/
	   $Fecha= DateTime::createFromFormat('d-m-Y H:i:s', $_POST['Fecha'])->format('Y-m-d H:i:s'); 
	   $Plazo= $_POST['Plazo']; //si es contado poner plazo como 0
	   $MedioPago= $_POST['MedioPago']; 
	   $CondicionVenta= $_POST['CondicionVenta'];
	   $Status="";/*Se guarda al mandara a hacienda*/

	   $NoOrden= $_POST['NoOrden']; 
	   $TipoMoneda= $_POST['TipoMoneda']; 
	   $TipoCambio= $_POST['TipoCambio'];

	   $TipoDocumento= ObtenerTipoDocumentoNumerico($_POST['TipoDocumento']); /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   $TipoDocumentoAfectado=""; /*Hay que hacer los campos en el formulario primero*/ /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   /*Campos de Calculos*/
	   $MontoGravado= QuitarFormatoNumero($_POST['MontGrF']);
	   $MontoExento= QuitarFormatoNumero($_POST['MontExF']);
	   $Descuento= QuitarFormatoNumero($_POST['MontDescF']);
	   $Impuesto= QuitarFormatoNumero($_POST['MontIVF']);
	   $OtroImpuesto= QuitarFormatoNumero($_POST['MontOImpF']);
	   $ServicioGravado= QuitarFormatoNumero($_POST['MontSGF']);
	   $ServicioExento= QuitarFormatoNumero($_POST['MontSEF']);

	   $Subtotal= QuitarFormatoNumero($_POST['MontSubF']);
	   $Total= QuitarFormatoNumero($_POST['MontTF']);

	   $Saldo=0;/*para mas adelante trabajar con abonos*/


	   //$Telefono= $_POST['Telefono'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Email= $_POST['Email'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Direccion= $_POST['Direccion'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Zona= $_POST['Zona'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   
	   $Terminal= $_POST['Terminal'];/*1 que hago con esto*/ 
	   $Sucursal= $_POST['Sucursal'];/*2 que hago con esto*/
	   //$Consecutivo= ObtenerConsecutivo($_POST['TipoDocumento']);  
	   
	   $NoFactura=""; /* que hago con esto? $Terminal.$Sucursal.$TipoDocumento.$Consecutivo;Hacer una funcion para generarla  Terminal+Sucursal+TipoDocumento+Consecutivo*/ 

	   $Clave="";/*Hacer una funcion para generarla*/ 

	   /*guardar Encabezado de factura en la BD*/

	   $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	    if ($Conexion->connect_error) 
	    {
	        die("Connection failed: " . $Conexion->connect_error);
	    } 

	    /*sanitize sql*/
	    $sql =
	    "INSERT INTO factura(FK_Usuario,NoFactura,FK_Cliente,NombreCliente,Fecha,Plazo,MedioPago,CondicionVenta,Status,Clave,NoOrden,TipoMoneda,TipoCambio,TipoDocumento,MontoGravado,MontoExento,Descuento,Impuesto,OtroImpuesto,ServicioGravado,ServicioExento,SubtotalFactura,TotalFactura,Saldo,Terminal,Sucursal)
	    values($FK_Usuario,'$NoFactura','$FK_Cliente','$NombreCliente','$Fecha',$Plazo,'$MedioPago','$CondicionVenta','$Status','$Clave','$NoOrden','$TipoMoneda',$TipoCambio,'$TipoDocumento',$MontoGravado,$MontoExento,$Descuento,$Impuesto,$OtroImpuesto,$ServicioGravado,$ServicioExento,$Subtotal,$Total,$Saldo,'$Terminal','$Sucursal');";
	                
	    if($Conexion->query($sql) === TRUE) 
	    { 
	        /*Guardar el Detalle de Factura*/

	        /*Obtener IDFactura*/

	        $sql="SELECT max(IDFactura) as IDFactura FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente';";

	        $result=$Conexion->query($sql);

	        if($result->num_rows > 0)
	        {
	          $row = $result->fetch_assoc();
	          $IDFactura=$row["IDFactura"];
	        }
	       
	        $TotalFilasGuardadas=0;

	        /*sacar el vector del JSON y pasarlo a Variable vector*/

	        $DetalleFactura=$_POST['DetalleFactura'];

	        /*Recorrer el vector con vectores (Matriz) y guardar en la BD*/
	        foreach($DetalleFactura as $i => $item) 
	        {
			   $NoLinea= $DetalleFactura[$i]['NoLinea'];
	           $IDProducto= $DetalleFactura[$i]['IDProducto'];
	           $NombreProducto= $DetalleFactura[$i]['NombreProducto'];
	           $Cantidad= $DetalleFactura[$i]['Cantidad'];
	           $PrecioVentaSinIV= QuitarFormatoNumero($DetalleFactura[$i]['PrecioVentaSinIV']);
	           $UnidadMedida= $DetalleFactura[$i]['UnidadMedida'];
	           $ImpuestoVentas= $DetalleFactura[$i]['ImpuestoVentas'];
	           $Descuento= $DetalleFactura[$i]['Descuento'];
	           $PrecioCosto= $DetalleFactura[$i]['PrecioCosto'];
	           $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
	           $TotalNeto= QuitarFormatoNumero($DetalleFactura[$i]['TotalNeto']);
	           $IDDetalle= $DetalleFactura[$i]['IDDetalle'];

	        // $array[$i] is same as $item

	           $sql="Insert into detallefactura(FK_FK_Usuario,FK_Factura,NombreProducto,Cantidad,Medida,Bonificacion,PrecioVenta,PrecioCosto,Impuesto,Descuento,TotalNeto,NoLinea,FK_Producto)
	values($FK_Usuario,$IDFactura,'$NombreProducto',$Cantidad,'$UnidadMedida',$Bonificacion,$PrecioVentaSinIV,$PrecioCosto,$ImpuestoVentas,$Descuento,$TotalNeto,$NoLinea,'$IDProducto');";

	          if($Conexion->query($sql) === TRUE) 
	          { 
	            /*Ingrementar LineasGuardadas*/
	              $TotalFilasGuardadas++;
	            //Restar producto de inventario al guardar el detalle si todas las lineas se guardaron
	          }

	        }

	        //Si TotalFilas y TotalFilasGuardadas son iguales, se guardo la factura y hay que recorrer el arreglo y  restar la cantidad de productos disponibles por linea

	        if(count($DetalleFactura)==$TotalFilasGuardadas)
	        {
	          $FilasAct=0;

	          /*Recorrer el arreglo, actualizar cantidad disponible y al final mostrar msj de exito en un boostrap*/
	          foreach($DetalleFactura as $i => $item) 
	          {
	             $IDProducto= $DetalleFactura[$i]['IDProducto'];
	             $Cantidad= $DetalleFactura[$i]['Cantidad'];
	             $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
	             $Medida= $DetalleFactura[$i]['UnidadMedida'];
	             
	             if($Medida!='sp')
	             {
				 		$sql="update producto 
	                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual-($Cantidad+$Bonificacion), UltimaVenta='$Fecha'
	                    where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

		              if($Conexion->query($sql) === TRUE) 
		              { 
		                /*Ingrementar cantidad y fecha de Prod en las lineas*/
		                  $FilasAct++;
		              }	
				 }
				 else
				 {
				 	$FilasAct++;
				 }
	      
	              
	          }

	          if(count($DetalleFactura)==$FilasAct)
	          {

						$Respuesta=($_POST['TipoDocumento']=='Factura')? "Factura guardada correctamente":'Tiquete guardado correctamente';			 
						$GuarMod='Guardo';
	              
	          }

	        }
	        else //sino son iguales entonces borrar todo del detalle y luego borrar el encabezado y mostrar msj de error
	        {
	            $sql="delete from detallefactura where FK_Factura=$IDFactura;";

	            if($Conexion->query($sql) === TRUE) 
	              { 
	                //Borrar encabezado de factura luego del detalle
	                $sql="delete from factura where IDFactura=$IDFactura;";

	                 if($Conexion->query($sql) === TRUE) 
	                {
	                  //Error al guardar la factura
	                  $Respuesta= ($_POST['TipoDocumento']=='Factura')? "Error al guardar la factura":"Error al guardar el tiquete";
	                  $GuarMod='Error';
	                }

	              }
	        }
	    }
	    else
	    {
	      /*No se guardo el encabezado, mostrar error*/
	      $Respuesta= ($_POST['TipoDocumento']=='Factura')? "Error al guardar la factura":"Error al guardar el tiquete";
	      $GuarMod='Error';
	    }
	}
	else //Modifcar
	{
		/**
		* Sacar cedula de cliente aneterior para restar el total de la factura(sacarlo tambien con el id) anterior 
		* Cuando borre, sumar la cantidad a cantidad disponible del producto en cuestion
		*/
		
		/*Modifcar el encabezado de factura*/    

	   $IDFactura=($_POST['IDFactura']!="")?$_POST['IDFactura']:0;
	   $FK_Usuario=$_SESSION['IDUsuario'];
	   $FK_Cliente= $_POST['Cedula'];
	   $NombreCliente= $_POST['Nombre'];

	   $NoReferencia="";/*Hay que hacer los campos en el formulario primero*/
	   $Razon="";/*Hay que hacer los campos en el formulario primero*/
	   $Fecha= DateTime::createFromFormat('d-m-Y H:i:s', $_POST['Fecha'])->format('Y-m-d H:i:s'); 
	   $Plazo= $_POST['Plazo']; //si es contado poner plazo como 0
	   $MedioPago= $_POST['MedioPago']; 
	   $CondicionVenta= $_POST['CondicionVenta'];
	   $Status="";/*Se guarda al mandara a hacienda*/

	   $NoOrden= $_POST['NoOrden']; 
	   $TipoMoneda= $_POST['TipoMoneda']; 
	   $TipoCambio= $_POST['TipoCambio'];

	   $TipoDocumento= ObtenerTipoDocumentoNumerico($_POST['TipoDocumento']); /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   $TipoDocumentoAfectado=""; /*Hay que hacer los campos en el formulario primero*/ /*01=Factura 02=NotaDebito 03=Nota Credito 04=Tiquete*/

	   /*Campos de Calculos*/
	   $MontoGravado= QuitarFormatoNumero($_POST['MontGrF']);
	   $MontoExento= QuitarFormatoNumero($_POST['MontExF']);
	   $Descuento= QuitarFormatoNumero($_POST['MontDescF']);
	   $Impuesto= QuitarFormatoNumero($_POST['MontIVF']);
	   $OtroImpuesto= QuitarFormatoNumero($_POST['MontOImpF']);
	   $ServicioGravado= QuitarFormatoNumero($_POST['MontSGF']);
	   $ServicioExento= QuitarFormatoNumero($_POST['MontSEF']);

	   $Subtotal= QuitarFormatoNumero($_POST['MontSubF']);
	   $Total= QuitarFormatoNumero($_POST['MontTF']);

	   $Saldo=0;/*para mas adelante trabajar con abonos*/


	   //$Telefono= $_POST['Telefono'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Email= $_POST['Email'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Direccion= $_POST['Direccion'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   //$Zona= $_POST['Zona'];/*Se jalan del cliente en vez de guardalo en la BD*/
	   
	   $Terminal= $_POST['Terminal'];/*1 que hago con esto*/ 
	   $Sucursal= $_POST['Sucursal'];/*2 que hago con esto*/
	   //$Consecutivo= ObtenerConsecutivo($_POST['TipoDocumento']);  
	   
	   $NoFactura=""; /* que hago con esto? $Terminal.$Sucursal.$TipoDocumento.$Consecutivo;Hacer una funcion para generarla  Terminal+Sucursal+TipoDocumento+Consecutivo*/ 

	   $Clave="";/*Hacer una funcion para generarla*/ 
	   
	   $IDDetalles=array();

	   /*guardar Encabezado de factura en la BD*/

	   $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	    if ($Conexion->connect_error) 
	    {
	        die("Connection failed: " . $Conexion->connect_error);
	    } 
	    
	    /*Obtener Datos de Cliente Factura Anterior y factura para actualizar(restar) datos de totalfacturado si son diferentes
	    Sis on el mismo Restar total de factura actual con el de fatura anterior(o al reves) y restarlo o sumarlo al total facturado  */
	    $FK_ClienteAntFact;
      	$TotalFactAnt;
	    
	    $sql="SELECT IDFactura,FK_Cliente,TotalFactura FROM factura WHERE FK_Usuario=$FK_Usuario AND IDFactura=$IDFactura;";
	    
	    $result = $Conexion->query($sql);

	    if ($result->num_rows > 0) 
	    {
      		$row = $result->fetch_assoc();

      		$FK_ClienteAntFact=$row["FK_Cliente"];
      		$TotalFactAnt=$row["TotalFactura"];
		}

		//Actualizar encabezado de factura    

	    /*sanitize sql*/
	    $sql =
			    "UPDATE factura SET FK_Usuario=$FK_Usuario,
								 NoFactura='$NoFactura',
								 FK_Cliente='$FK_Cliente',
								 NombreCliente='$NombreCliente',
								 Fecha='$Fecha',
								 Plazo=$Plazo,
								 MedioPago='$MedioPago',
								 CondicionVenta='$CondicionVenta',
								 STATUS='$Status',
								 Clave='$Clave',
								 NoOrden='$NoOrden',
								 TipoMoneda='$TipoMoneda',
								 TipoCambio=$TipoCambio,
								 TipoDocumento='$TipoDocumento',
								 MontoGravado=$MontoGravado,
								 MontoExento=$MontoExento,
								 Descuento=$Descuento,
								 Impuesto=$Impuesto,
								 OtroImpuesto=$OtroImpuesto,
								 ServicioGravado=$ServicioGravado,
								 ServicioExento=$ServicioExento,
								 SubtotalFactura=$Subtotal,
								 TotalFactura=$Total,
								 Saldo=$Saldo,
								 Terminal='$Terminal',
								 Sucursal='$Sucursal'
				WHERE FK_Usuario= $FK_Usuario AND IDFactura=$IDFactura";
	                
	    if($Conexion->query($sql) === TRUE) 
	    { 
	    	/*Actualizar el detalle fila a fila teniendo en cuenta que si la fila no tiene IDdetalle se guarda y si tiene no se hace nada)*/
	    	
	    	$TotalFilasAGuardar=0;
	    	$TotalFilasGuardadas=0;

	        /*sacar el vector del JSON y pasarlo a Variable vector*/

	        $DetalleFactura=$_POST['DetalleFactura'];

	        /*Recorrer el vector con vectores (Matriz) y guardar en la BD*/
	        foreach($DetalleFactura as $i => $item) 
	        {
			   $NoLinea= $DetalleFactura[$i]['NoLinea'];
	           $IDProducto= $DetalleFactura[$i]['IDProducto'];
	           $NombreProducto= $DetalleFactura[$i]['NombreProducto'];
	           $Cantidad= $DetalleFactura[$i]['Cantidad'];
	           $PrecioVentaSinIV= QuitarFormatoNumero($DetalleFactura[$i]['PrecioVentaSinIV']);
	           $UnidadMedida= $DetalleFactura[$i]['UnidadMedida'];
	           $ImpuestoVentas= $DetalleFactura[$i]['ImpuestoVentas'];
	           $Descuento= $DetalleFactura[$i]['Descuento'];
	           $PrecioCosto= $DetalleFactura[$i]['PrecioCosto'];
	           $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
	           $TotalNeto= QuitarFormatoNumero($DetalleFactura[$i]['TotalNeto']);
	           $IDDetalle= $DetalleFactura[$i]['IDDetalle'];
	           
	           //Si Iddetalle=="" guarde
	            if( $IDDetalle=="")
				{
		        	$TotalFilasAGuardar++;

	           		$sql="Insert into detallefactura(FK_FK_Usuario,FK_Factura,NombreProducto,Cantidad,Medida,Bonificacion,PrecioVenta,PrecioCosto,Impuesto,Descuento,TotalNeto,NoLinea,FK_Producto)
	values($FK_Usuario,$IDFactura,'$NombreProducto',$Cantidad,'$UnidadMedida',$Bonificacion,$PrecioVentaSinIV,$PrecioCosto,$ImpuestoVentas,$Descuento,$TotalNeto,$NoLinea,'$IDProducto');";

			          if($Conexion->query($sql) === TRUE) 
			          { 
			            /*Guardar el IDDetalle en un vector por si hay que borrarlo*/
			            
			            $sql="SELECT MAX(IDDetalle) as IDDetalle FROM detallefactura where FK_FK_Usuario=$FK_Usuario and FK_Factura=$IDFactura;";

				        $result=$Conexion->query($sql);

				        if($result->num_rows > 0)
				        {
				          $row = $result->fetch_assoc();
				          $IDDetalles[]=$row["IDDetalle"];
				        }
			            
			            /*Ingrementar LineasGuardadas*/
			              $TotalFilasGuardadas++;
			          }
				}

	        }	        

	        //Si TotalFilas y TotalFilasGuardadas son iguales, se guardo la factura y hay que recorrer el arreglo y  restar la cantidad de productos disponibles por linea

	        if($TotalFilasAGuardar==$TotalFilasGuardadas)
	        {
		          $FilasAct=0;

		          /*Recorrer el arreglo, actualizar cantidad disponible y al final mostrar msj de exito en un boostrap*/
		          foreach($DetalleFactura as $i => $item) 
		          {
		             $IDProducto= $DetalleFactura[$i]['IDProducto'];
		             $Cantidad= $DetalleFactura[$i]['Cantidad'];
		             $Bonificacion= $DetalleFactura[$i]['Bonificacion'];
		             $IDDetalle= $DetalleFactura[$i]['IDDetalle'];
		             $UnidadMedida= $DetalleFactura[$i]['UnidadMedida'];
		      
		      		 if($IDDetalle=="" && $UnidadMedida!='sp')
		      		 {
					 		$sql="update producto 
		                    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual-($Cantidad+$Bonificacion), UltimaVenta='$Fecha'
		                    where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";

			              if($Conexion->query($sql) === TRUE) 
			              { 
			                /*Ingrementar cantidad y fecha de Prod en las lineas*/
			                  $FilasAct++;
			              }	
					 }
					 else if($IDDetalle=="" && $UnidadMedida=='sp')
					 {
					 	$FilasAct++;	
					 }
		          }

		          if($TotalFilasAGuardar==$FilasAct)
		          {
	
						$Respuesta=($_POST['TipoDocumento']=='Factura')? "Factura modificada correctamente":'Tiquete modificado correctamente';
						$GuarMod='Modifico';
  
				  }
				}
				else
				{
					//error al modificar fact borrar Los detalles ingresados	
					
					foreach ($IDDetalles as $IDDet) {
    					
    					$sql="delete from detallefactura where FK_FK_Usuario=$FK_Usuario AND FK_Factura and=$IDFactura AND IDDetalle=$IDDet;";

	            		$Conexion->query($sql);
					}
					
					$Respuesta=($_POST['TipoDocumento']=='Factura')? "Error al modificar la factura":"Error al modificar el tiquete";
					$GuarMod='Error';
					
				}
		}
		else //total anterior y actual son iguales
		{
			//error al modificar fact mostrar error(no se ha gardado el detalle)
			$Respuesta=($_POST['TipoDocumento']=='Factura')? "Error al modificar la factura":"Error al modificar el tiquete";				$GuarMod='Error';
		}
	}
	
		    $users_arr[] = array( 

	                         /*Pasar al vector la Respuesta*/
	                         "Respuesta"=>$Respuesta,
	                         "GuarMod"=>$GuarMod,
	                     );

	    // encoding array to json format
	    echo json_encode($users_arr);
	    exit;
	
}

function ConsultarFactura()
{
    $IDFactura=$_POST['IDFactura'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    $TipoDocAGenerar=(!empty($_POST['TipoDocAGenerar']))?$_POST['TipoDocAGenerar']:"";

    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

    $sql = "SELECT  F.IDFactura,
                    F.FK_Usuario,
                    F.NoFactura,

                    /*Datos del CLiente*/

                    F.FK_Cliente,
                    F.NombreCliente,

                    C.Telefono,
                    C.Email1,
                    C.Direccion,
                    C.Zona,

                    DATE_FORMAT(F.Fecha,'%d-%m-%Y  %h:%i:%s') as Fecha,
                    F.Plazo,
                    F.MedioPago,
                    F.CondicionVenta,
                    F.Status,
                    F.Clave,
                    F.NoOrden,
                    F.TipoMoneda,
                    F.TipoCambio,
                    F.TipoDocumento,
                    F.MontoGravado,
                    F.MontoExento,
                    F.Descuento,
                    F.Impuesto,
                    F.OtroImpuesto,
                    F.ServicioGravado,
                    F.ServicioExento,
                    F.SubtotalFactura,
                    F.TotalFactura,
                    F.Saldo,
                    F.Terminal,
                    F.Sucursal
                    FROM factura F INNER JOIN cliente C ON F.FK_Cliente=C.Cedula WHERE F.IDFactura=$IDFactura AND F.FK_Cliente='$CedulaCliente' AND F.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDFactura=$row["IDFactura"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=ObtenerNombreTipoDocumento($row["TipoDocumento"]);
      
      $NoFactura=($row["NoFactura"]=="") ? ObtenerConsecutivo($TipoDocumento) : $row["NoFactura"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
      $NoTipoDocAGenerar=(!empty($TipoDocAGenerar))?ObtenerConsecutivo($TipoDocumento):"";

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];

      $Telefono=$row["Telefono"];
      $Email1=$row["Email1"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];

      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $Plazo=$row["Plazo"];
      $MedioPago=$row["MedioPago"];
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row['Clave'];
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=$row["TipoMoneda"];
      $TipoCambio=$row["TipoCambio"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoF=$row["MontoGravado"];
      $MontoExentoF=$row["MontoExento"];
      $DescuentoF=$row["Descuento"];
      $ImpuestoF=$row["Impuesto"];
      $OtroImpuestoF=$row["OtroImpuesto"];
      $ServicioGravadoF=$row["ServicioGravado"];
      $ServicioExentoF=$row["ServicioExento"];
      $SubtotalFacturaF=$row["SubtotalFactura"];
      $TotalFacturaF=$row["TotalFactura"];
      $SaldoF=$row["Saldo"];

      /*Poner detalle de Factura*/  

      /*Hay que hacer los calculos de montos por fila con los datos del detalle*/  

      $sql="SELECT 

            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            TotalNeto,
            NoLinea,
            CantidadDevuelta,
            FK_Producto

            FROM detallefactura WHERE FK_Factura=$IDFactura AND FK_FK_Usuario=$FK_Usuario ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
	  
	  $DetalleFactura = array();
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $IDDetalle= $ri['IDDetalle'];
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $PrecioVentaSinIV= $ri['PrecioVenta'];
	         $UnidadMedida= $ri['Medida'];
	         $ImpuestoVenta= $ri['Impuesto'];
	         $Descuento= $ri['Descuento'];
	         $PrecioCosto= $ri['PrecioCosto'];
	         $Bonificacion= $ri['Bonificacion'];
	         $TotalNeto= $ri['TotalNeto'];
	         $NoLinea= $ri['NoLinea'];
			 $CantidadDevuelta=$ri['CantidadDevuelta'];


	         /*Variables para calculos*/
	          $MontoExento=0;
	          $MontoGravado=0; 
	          $MontoSExento=0;
	          $MontoSGravado=0;
	          $MontoIV=0;
	          $MontoOtroI=0;
	          $MontoDescuento=0;
	          $SubtotalL=0;
	          $TotalL=0;


	          if($ImpuestoVenta==0) /*Sino Tiene Impuesto*/
	          {
	            $MontoExento=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	            $MontoGravado=0; 
	        
	          }
	          else /*Si Tiene Impuesto*/ 
	          {
	            $MontoExento=0;
	            $MontoGravado=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	          }
	          
	          if($UnidadMedida=='sp' AND $ImpuestoVenta==0)
	          {
	            $MontoSExento=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2); /*Precio sin impuesto x Cantidad*/
	            $MontoSGravado=0;
	          }
	          else if($UnidadMedida=='sp' AND $ImpuestoVenta!=0)
	        {
	          $MontoSExento=0;/*Precio con impuesto x Cantidad*/
	            $MontoSGravado=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);
	        }
	        else
	        {
	          $MontoSExento=0;
	          $MontoSGravado=0;
	        }     

	          $MontoIV=bcdiv(((($PrecioVentaSinIV*$Cantidad)-(($PrecioVentaSinIV*$Cantidad)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
	          $MontoOtroI=0;/*se va a cambiar mas adelante*/
	          $MontoDescuento=bcdiv((($PrecioVentaSinIV*$Cantidad)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


	          $SubtotalL=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);/*Precio sin impuesto x Cantidad*/ 
	          $TotalL=bcdiv(($SubtotalL-$MontoDescuento+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/



	         $DetalleFactura[] = array(
	                                 "IDDetalle" => $IDDetalle,
	                                 "IDProducto" => $IDProducto, 
	                                 "NombreProducto" => $NombreProducto,
	                                 "PrecioVSinIV" => number_format($PrecioVentaSinIV,2), 
	                                 "Impuesto" =>$ImpuestoVenta, 
	                                 "Descuento" =>$Descuento, 
	                                 "Cantidad"=>$Cantidad,
	                                 "UM"=>$UnidadMedida,
	                                 "PrecioCosto"=>$PrecioCosto,
	                                 "Bonificacion"=>$Bonificacion,
	                                 
	                                 "MontoE"=>$MontoExento,
	                                 "MontoG"=>$MontoGravado,
	                                 "MontoSE"=>$MontoSExento,
	                                 "MontoSG"=>$MontoSGravado,
	                                 "MontoImpV"=>$MontoIV,
	                                 "MontoOImp"=>$MontoOtroI,
	                                 "MontoDesc"=>$MontoDescuento,
	                                 "SubtL"=>number_format($SubtotalL,2),
	                                 "TotL"=>number_format($TotalL,2),
	                                 "NoLinea"=>$NoLinea,
	                                 "CantidadDevuelta"=>$CantidadDevuelta,
	                                );
	  }


    }

    $users_arr[] = array( 
                            "IDFactura"=>$IDFactura,
                            "FK_Usuario"=>$FK_Usuario,
                            "NoFactura"=>$NoFactura,
                            "FK_Cliente"=>$FK_Cliente,
                            "NombreCliente"=>$NombreCliente,
                            "Telefono"=>$Telefono,
                            "Email1"=>$Email1,
                            "Direccion"=>$Direccion,
                            "Zona"=>$Zona,
                            "Fecha"=>$Fecha,
                            "Plazo"=>$Plazo,
                            "MedioPago"=>$MedioPago,
                            "CondicionVenta"=>$CondicionVenta,
                            "Clave"=>$Clave,
                            "Status"=>$Status,
                            "NoOrden"=>$NoOrden,
                            "TipoMoneda"=>$TipoMoneda,
                            "TipoCambio"=>$TipoCambio,
                            "TipoDocumento"=>$TipoDocumento,
                            "NoTipoDocAGenerar"=>$NoTipoDocAGenerar,

                            /*Campos de Calculo de factura*/
                            "MontoGravado"=>number_format($MontoGravadoF,2),
                            "MontoExento"=>number_format($MontoExentoF,2),
                            "Descuento"=>number_format($DescuentoF,2),
                            "Impuesto"=>number_format($ImpuestoF,2),
                            "OtroImpuesto"=>number_format($OtroImpuestoF,2),
                            "ServicioGravado"=>number_format($ServicioGravadoF,2),
                            "ServicioExento"=>number_format($ServicioExentoF,2),
                            "SubtotalFactura"=>number_format($SubtotalFacturaF,2),
                            "TotalFactura"=>number_format($TotalFacturaF,2),
                            /*******************************/
                            "Saldo"=>$SaldoF,
                            "Terminal"=>$Terminal,
                            "Sucursal"=>$Sucursal,
                            
                            /*DetalleFactura*/
                            "DetalleFactura"=>$DetalleFactura,
                       );

      // encoding array to json format
      echo json_encode($users_arr);
      exit;

}

function BorrarFactura($IDDocumento)
{
	$FK_Usuario=$_SESSION['IDUsuario'];
	
	$Respuesta;
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 
	
	 $TotalFilas=0;
	 $TotalModificaciones=0;
	 
	 //trer el detalle de la factura antes de borrar y sumar cantidad del detalle al inventario por cada fila del detalle
	
		$sql="SELECT 
		
            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            TotalNeto,
            NoLinea,
            FK_Producto

            FROM detallefactura WHERE FK_Factura=$IDDocumento AND FK_FK_Usuario=$FK_Usuario ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $TotalFilas++;
      	
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $Bonificacion= $ri['Bonificacion'];
	         $Medida= $ri['Medida'];
	         
	         if($Medida!='sp')
	         {
			 	$sql="UPDATE producto 
	    set SaldoAnterior=SaldoActual, SaldoActual=SaldoActual+($Cantidad+$Bonificacion) where FK_Usuario=$FK_Usuario and IDProducto='$IDProducto';";
	         
		         if($Conexion->query($sql))
				 {
					$TotalModificaciones++;	
				 }	
			 }
			 else
			 {
			 	$TotalModificaciones++;	
			 }
	  }
	  
	  if($TotalFilas==$TotalModificaciones)
	  {
	  		$sql="delete from detallefactura where FK_FK_Usuario=$FK_Usuario AND FK_Factura=$IDDocumento";

			if($Conexion->query($sql))
			{
				  	$sql="delete from factura where FK_Usuario=$FK_Usuario AND IDFactura=$IDDocumento";
				
							if($Conexion->query($sql))
							{								
								$Respuesta="Factura borrada correctamente";
							}
							else
							{
								$Respuesta="Error al borrar la factura";
							}
				
			}	
	  }
	  else
	  {
	  		$Respuesta="Error al borrar la factura";	
	  }
	
	  $users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
}

function ConsultarEstadoAntesDeEnvio($IDapi,$NoClaveLarga)
{
//Enviar a hacienda	    

$JSONConsulta='{
			  "data": {
			    "nombre_usuario": "'.$IDapi.'",
			    "clavelarga": "'.$NoClaveLarga.'"
			  }
			}';
			            
//API URL Envio
$urlEnvio = "http://35.170.39.96/api/server/consultar";

//create a new cURL resource
$ch = curl_init($urlEnvio);

//poner JSON en curl
curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONConsulta);

//set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

//return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute the POST request
$result = curl_exec($ch);

$Respuesta= $result;

//close cURL resource
curl_close($ch);	
                
//guardar el estado de la respuesta

$ResultadoEstado = '';

if( strpos( $Respuesta,'aceptado' ) !== false) 
{
$ResultadoEstado='aceptado';
}
else if( strpos( $Respuesta,'rechazado' ) !== false) 
{
$ResultadoEstado='rechazado';
}
else if( strpos( $Respuesta,'procesando' ) !== false) 
{
$ResultadoEstado='procesando';
}
else if( strpos( $Respuesta,'El documento ha sido enviado a Hacienda pero Hacienda no ha dado respuesta' ) !== false) 
{
$ResultadoEstado='sin respuesta';
}
else
{
	$ResultadoEstado='no enviado';
}

return $ResultadoEstado; 

}

function ConsultarEstado($IDDoc,$FK_Cliente,$FK_Usuario)
{
	$IDapi=ObtenerID_API($FK_Usuario);
	$NoClaveLarga=ObtenerClaveLarga($IDDoc,$FK_Usuario,$FK_Cliente);
	
	//Enviar a hacienda	    
	
	$JSONConsulta='{
				  "data": {
				    "nombre_usuario": "'.$IDapi.'",
				    "clavelarga": "'.$NoClaveLarga.'"
				  }
				}';
				            
	//API URL Envio
	$urlEnvio = "http://35.170.39.96/api/server/consultar";

	//create a new cURL resource
	$ch = curl_init($urlEnvio);
	
	//poner JSON en curl
	curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONConsulta);

	//set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

	//return response instead of outputting
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	//execute the POST request
	$result = curl_exec($ch);

	$Respuesta= $result;

	//close cURL resource
	curl_close($ch);	
	                
	//guardar el estado de la respuesta
		
		$ResultadoEstado= '';
				
				if( strpos( $Respuesta,'aceptado' ) !== false) 
			    {
			    	$ResultadoEstado='aceptado';
				}
				else if( strpos( $Respuesta,'rechazado' ) !== false) 
			    {
			    	$ResultadoEstado='rechazado';
				}
				else if( strpos( $Respuesta,'procesando' ) !== false) 
			    {
			    	$ResultadoEstado='procesando';
				}
				else if( strpos( $Respuesta,'El documento ha sido enviado a Hacienda pero Hacienda no ha dado respuesta' ) !== false) 
			    {
			    	$ResultadoEstado='sin respuesta';
				}
				else
				{
					$ResultadoEstado='no enviado';
				}

				/*
				$IniMsj = '"message":{"0":"';
				$FinalMsj = '"},"clave":';
				$ResultadoMsj = ObtenerEstatusYMensajeHacienda($Respuesta,$IniMsj,$FinalMsj);
		*/
		
	$sql="UPDATE factura SET Status='$ResultadoEstado', MensajeHacienda='' WHERE IDFactura=$IDDoc and FK_Usuario=$FK_Usuario;";

	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  if($Conexion->query($sql) === TRUE) 
  { 
      $Exito='';
  }
   
  	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
  
}

function EnviarAHacienda()
{
	$IDDocumento=$_POST['IDDocumento'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    $Respuesta='';
    
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

    $sql = "SELECT  F.IDFactura,
        F.FK_Usuario,
        F.NoFactura,
        
        /*Datos del Usuario*/
        U.ID_API,
        U.TipoCedula,
		LPAD(CAST(U.Cedula AS CHAR(12)), 12, '0') AS CedulaUsuario,

        /*Datos del CLiente*/

        F.FK_Cliente,
        F.NombreCliente,

        C.Telefono,
        C.Email1,
        C.Email2,
        C.Direccion,
        C.Zona,

        DATE_FORMAT(F.Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha,
        DATE_FORMAT(F.Fecha,'%d%m%y') AS FechaPClave,
        DATE_FORMAT(F.Fecha,'%Y-%m-%d  %h:%i:%s') as FechaPCliente,
        F.Plazo,
        F.MedioPago,
        F.CondicionVenta,
        F.Status,
        F.Clave,
        F.NoOrden,
        F.TipoMoneda,
        F.TipoCambio,
        F.TipoDocumento,
        F.MontoGravado,
        F.MontoExento,
        F.Descuento,
        F.Impuesto,
        F.OtroImpuesto,
        F.ServicioGravado,
        F.ServicioExento,
        F.SubtotalFactura,
        F.TotalFactura,
        F.Saldo,
        F.Terminal,
        F.Sucursal
        FROM factura F 
		  INNER JOIN usuario U ON F.FK_Usuario=U.IDUsuario
		  INNER JOIN cliente C ON F.FK_Cliente=C.Cedula 
		  WHERE F.IDFactura=$IDDocumento AND F.FK_Cliente='$CedulaCliente' AND F.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDFactura=$row["IDFactura"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=$row["TipoDocumento"];
      $NombreTipoDoc=ObtenerNombreTipoDocumento($TipoDocumento);
      
      $NoFactura=($row["NoFactura"]=="") ? ObtenerConsecutivo($NombreTipoDoc) : $row["NoFactura"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
	  $ID_API=$row["ID_API"];
	  $TipoCedula=ObternerTipoIdentificacionNumerica($row['TipoCedula']);
	  $CedulaUsuario=$row["CedulaUsuario"];

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];

      $Telefono=$row["Telefono"];
      $Email1=$row["Email1"];
      $Email2=$row["Email2"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      
      //Partir la Zona
      $Provincia=substr($Zona,0,1);
      $Canton=substr($Zona,1,2);
      $Distrito=substr($Zona,3,4);

      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $FechaPClave=$row["FechaPClave"];
      $FechaPCliente=$row["FechaPCliente"];
      $Plazo=$row["Plazo"];
      $MedioPago= MedioParaJSON($row["MedioPago"]);
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row['Clave'];
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=ObtenerCodigoMoneda($row["TipoMoneda"]);
      $TipoCambio=$row["TipoCambio"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoF=$row["MontoGravado"];
      $MontoExentoF=$row["MontoExento"];
      $DescuentoF=$row["Descuento"];
      $ImpuestoF=$row["Impuesto"];
      $OtroImpuestoF=$row["OtroImpuesto"];
      $ServicioGravadoF=$row["ServicioGravado"];
      $ServicioExentoF=$row["ServicioExento"];
      $SubtotalFacturaF=$row["SubtotalFactura"];
      $TotalFacturaF=$row["TotalFactura"];
      $SaldoF=$row["Saldo"];
      
      /*Variables que hay que guardar*/
      $Consecutivo=($row["NoFactura"]=="") ? $Terminal.$Sucursal.$TipoDocumento.$NoFactura : $row["NoFactura"];
      $ClaveLarga=($Clave=='')?"506".$FechaPClave.$CedulaUsuario.$Consecutivo."1".'00000000':$Clave;
      
      $DetFact=DetalleFacturaParaJSON($IDFactura,$FK_Usuario);
      
      $Resultado = ConsultarEstadoAntesDeEnvio($ID_API,$ClaveLarga);
      
      if($Resultado=='aceptado' || $Resultado=='rechazado' || $Resultado=='procesando')
      {
      		$Respuesta='El documento ya fue enviado anteriormente y se encuentra '.$Resultado;
	  		
			$sql="UPDATE factura SET Status='$ResultadoEstado', MensajeHacienda='$ResultadoMsj' WHERE IDFactura=$IDFactura and FK_Usuario=$FK_Usuario;";

			if($Conexion->query($sql) === TRUE) 
			{ 
			  $Exito='';
			}
			 
	  }
	  else
	  {
	  	  if($NombreTipoDoc=='Factura')
	      {
		  		$JSONEnvio=
	      			'{
					  "data": {
					    "tipo": "1",
					    "nombre_usuario": "'.$ID_API.'",
					    "situacion": "normal",
					    "consecutivo": "'.$Consecutivo.'",
					    "clavelarga": "'.$ClaveLarga.'",
					    "sucursal": "'.$Sucursal.'",
					    "fechafactura": "'.$Fecha.'",
					    "terminal": "'.$Terminal.'",
					    "otro_telefono": "",
					    "receptor": {
					      "tipoIdentificacion": "'.$TipoCedula.'",
					      "numeroIdentificacion": "'.$FK_Cliente.'",
					      "nombre": "'.$NombreCliente.'",
					      "provincia": "'.$Provincia.'",
					      "canton": "'.$Canton.'",
					      "distrito": "'.$Distrito.'",
					      "otrasSenas": "'.$Direccion.'",
					      "codigoPais": "506",
					      "telefono": "'.$Telefono.'",
					      "website": "",
					      "correo": "'.$Email1.'",
					      "correo_gastos": "'.$Email2.'"
					    },
					    "condicionVenta": "'.$CondicionVenta.'",
					    "plazoCredito": "'.$Plazo.'",
					    "numFecha": "",
					    "numReferencia": "'.$IDFactura.'",
					    "codigoVendedor": "00",
					    "medioPago": [
					        '.$MedioPago.'
					    ],
					    "detalles": [
					      '.$DetFact.'
						],
					    "codigoMoneda": "'.$TipoMoneda.'",
					    "tipoCambio": "'.$TipoCambio.'",
					    "totalServGravados": "'.$ServicioGravadoF.'",
					    "totalServExentos": "'.$ServicioExentoF.'",
					    "totalMercanciasGravados": "'.$MontoGravadoF.'",
					    "totalMercanciasExentos": "'.$MontoExentoF.'",
					    "totalGravados": "'.($ServicioGravadoF+$MontoGravadoF).'",
					    "totalExentos": "'.($ServicioExentoF+$MontoExentoF).'",
					    "totalVentas": "'.(($ServicioGravadoF+$MontoGravadoF)+($ServicioExentoF+$MontoExentoF)).'",
					    "totalDescuentos": "'.$DescuentoF.'",
					    "totalVentasNetas": "'.$SubtotalFacturaF.'",
					    "totalImpuestos": "'.($ImpuestoF+$OtroImpuestoF).'",
					    "totalComprobantes": "'.$TotalFacturaF.'",
					    "referencia": "",
					    "otros": {
					      "otroTexto": ""
					    }
					  }
					}';
		  }
		  else if($NombreTipoDoc=='Tiquete')
		  {
		  		$JSONEnvio=
	      			'{
					  "data": {
					    "tipo": "4",
					    "nombre_usuario": "'.$ID_API.'",
					    "situacion": "normal",
					    "consecutivo": "'.$Consecutivo.'",
					    "clavelarga": "'.$ClaveLarga.'",
					    "sucursal": "'.$Sucursal.'",
					    "fechafactura": "'.$Fecha.'",
					    "terminal": "'.$Terminal.'",
					    "otro_telefono": "",
					    "condicionVenta": "'.$CondicionVenta.'",
					    "plazoCredito": "'.$Plazo.'",
					    "numFecha": "",
					    "numReferencia": "'.$IDFactura.'",
					    "codigoVendedor": "00",
					    "medioPago": [
					        '.$MedioPago.'
					    ],
					    "detalles": [
					      '.$DetFact.'
						],
					    "codigoMoneda": "'.$TipoMoneda.'",
					    "tipoCambio": "'.$TipoCambio.'",
					    "totalServGravados": "'.$ServicioGravadoF.'",
					    "totalServExentos": "'.$ServicioExentoF.'",
					    "totalMercanciasGravados": "'.$MontoGravadoF.'",
					    "totalMercanciasExentos": "'.$MontoExentoF.'",
					    "totalGravados": "'.($ServicioGravadoF+$MontoGravadoF).'",
					    "totalExentos": "'.($ServicioExentoF+$MontoExentoF).'",
					    "totalVentas": "'.(($ServicioGravadoF+$MontoGravadoF)+($ServicioExentoF+$MontoExentoF)).'",
					    "totalDescuentos": "'.$DescuentoF.'",
					    "totalVentasNetas": "'.$SubtotalFacturaF.'",
					    "totalImpuestos": "'.($ImpuestoF+$OtroImpuestoF).'",
					    "totalComprobantes": "'.$TotalFacturaF.'",
					    "referencia": "",
					    "otros": {
					      "otroTexto": ""
					    }
					  }
					}';
		  }
				
				//actualizar clave y consecutivo de la factura
				
				  $sql="UPDATE factura SET NoFactura='$Consecutivo', Clave='$ClaveLarga' WHERE IDFactura=$IDFactura and FK_Usuario=$FK_Usuario;";

		          if($Conexion->query($sql) === TRUE) 
		          { 
		          	 //Actualizar consecutivo del documento sumar 1	solo si es una factura nueva
		          	 if($Clave=='' || $row["NoFactura"]=="")
		          	 {
					 	$sql="UPDATE consecutivo SET $NombreTipoDoc=$NombreTipoDoc+1 WHERE FK_Usuario=$FK_Usuario;";
			
						if($Conexion->query($sql))
						{								
							$Exito="";
						}
					 }
		          	 
		  	      }
				
				//Enviar a hacienda	    
				            
				//API URL Envio
				$urlEnvio = 'http://35.170.39.96/api/server/enviar';

				//create a new cURL resource
				$ch = curl_init($urlEnvio);
				
				//poner JSON en curl
				curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

				//set the content type to application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

				//return response instead of outputting
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				//execute the POST request
				$result = curl_exec($ch);

				$Respuesta= $result;

				//close cURL resource
				curl_close($ch);	
				                
				//guardar el estado de la respuesta
				
				 $ResultadoEstado= '';
				
				if( strpos( $Respuesta,'aceptado' ) !== false) //error con aceptado por que esta dos veces "status":"aceptado"
			    {
			    	$ResultadoEstado='aceptado';
				}
				else if( strpos( $Respuesta,'rechazado' ) !== false) 
			    {
			    	$ResultadoEstado='rechazado';
				}
				else if( strpos( $Respuesta,'procesando' ) !== false) 
			    {
			    	$ResultadoEstado='procesando';
				}
				else if( strpos( $Respuesta,'El documento ha sido enviado a Hacienda pero Hacienda no ha dado respuesta' ) !== false) 
			    {
			    	$ResultadoEstado='sin respuesta';
				}
				else
				{
					$ResultadoEstado='no enviado';
				}
				/*
				$IniMsj = '"message":{"0":"';
				$FinalMsj = '"},"clave":';
				$ResultadoMsj = ObtenerEstatusYMensajeHacienda($Respuesta,$IniMsj,$FinalMsj);
		*/
				$sql="UPDATE factura SET Status='$ResultadoEstado', MensajeHacienda='' WHERE IDFactura=$IDFactura and FK_Usuario=$FK_Usuario;";

			  if($Conexion->query($sql) === TRUE) 
			  { 
			      $Exito='';
			  }
		
				if($NombreTipoDoc=='Factura' && ($Clave=='' || $row["NoFactura"]==""))
				{
					  $sql="UPDATE cliente SET FechaUltimaFacturacion='$FechaPCliente', TotalVentas=TotalVentas+$TotalFacturaF WHERE Cedula='$FK_Cliente' and FK_Usuario=$FK_Usuario;";

					  if($Conexion->query($sql) === TRUE) 
					  { 

					      $Exito='';
					  }	
				}	
		  }		    
	}
	
	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
    
}

function ReenviarEmail()
{
	$IDDocumento=$_POST['IDDocumento'];
    $CedulaCliente=$_POST['CedulaCliente'];
    $FK_Usuario=$_SESSION['IDUsuario'];
    
    $Respuesta='';
    
    $Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

    $sql = "SELECT  F.IDFactura,
        F.FK_Usuario,
        F.NoFactura,
        
        /*Datos del Usuario*/
        U.ID_API,
        U.TipoCedula,
		LPAD(CAST(U.Cedula AS CHAR(12)), 12, '0') AS CedulaUsuario,

        /*Datos del CLiente*/

        F.FK_Cliente,
        F.NombreCliente,

        C.Telefono,
        C.Email1,
        C.Email2,
        C.Direccion,
        C.Zona,

        DATE_FORMAT(F.Fecha,'%d-%m-%YT%H:%i:%s%-06:00') AS Fecha,
        DATE_FORMAT(F.Fecha,'%d%m%y') AS FechaPClave,
        DATE_FORMAT(F.Fecha,'%Y-%m-%d  %h:%i:%s') as FechaPCliente,
        F.Plazo,
        F.MedioPago,
        F.CondicionVenta,
        F.Status,
        F.Clave,
        F.NoOrden,
        F.TipoMoneda,
        F.TipoCambio,
        F.TipoDocumento,
        F.MontoGravado,
        F.MontoExento,
        F.Descuento,
        F.Impuesto,
        F.OtroImpuesto,
        F.ServicioGravado,
        F.ServicioExento,
        F.SubtotalFactura,
        F.TotalFactura,
        F.Saldo,
        F.Terminal,
        F.Sucursal
        FROM factura F 
		  INNER JOIN usuario U ON F.FK_Usuario=U.IDUsuario
		  INNER JOIN cliente C ON F.FK_Cliente=C.Cedula 
		  WHERE F.IDFactura=$IDDocumento AND F.FK_Cliente='$CedulaCliente' AND F.FK_Usuario=$FK_Usuario;";
              
    $result = $Conexion->query($sql);

    if ($result->num_rows > 0) 
    {
      $row = $result->fetch_assoc();

      $IDFactura=$row["IDFactura"];
      $FK_Usuario=$row["FK_Usuario"];
      
      $TipoDocumento=$row["TipoDocumento"];
      $NombreTipoDoc=ObtenerNombreTipoDocumento($TipoDocumento);
      
      $NoFactura=($row["NoFactura"]=="") ? ObtenerConsecutivo($NombreTipoDoc) : $row["NoFactura"];
      /*sino tiene consecutivo ir a consultar por donde va. Si tiene ponerle el que tiene*/
      
	  $ID_API=$row["ID_API"];
	  $TipoCedula=ObternerTipoIdentificacionNumerica($row['TipoCedula']);
	  $CedulaUsuario=$row["CedulaUsuario"];

      /*Datos del CLiente*/

      $FK_Cliente=$row["FK_Cliente"];
      $NombreCliente=$row["NombreCliente"];

      $Telefono=$row["Telefono"];
      $Email1=$row["Email1"];
      $Email2=$row["Email2"];
      $Direccion=$row["Direccion"];
      $Zona=$row["Zona"];
      
      //Partir la Zona
      $Provincia=substr($Zona,0,1);
      $Canton=substr($Zona,1,2);
      $Distrito=substr($Zona,3,4);

      $Fecha=$row["Fecha"];//DateTime::createFromFormat('Y-m-d H:i:s', $row["Fecha"])->format('d-m-Y H:i:s');
      $FechaPClave=$row["FechaPClave"];
      $FechaPCliente=$row["FechaPCliente"];
      $Plazo=$row["Plazo"];
      $MedioPago= MedioParaJSON($row["MedioPago"]);
      $CondicionVenta=$row["CondicionVenta"];
      $Status=$row["Status"];
      $Clave=$row['Clave'];
      $NoOrden=$row["NoOrden"];
      $TipoMoneda=ObtenerCodigoMoneda($row["TipoMoneda"]);
      $TipoCambio=$row["TipoCambio"];
      
      $Terminal=$row["Terminal"];
      $Sucursal=$row["Sucursal"];
      
      $MontoGravadoF=$row["MontoGravado"];
      $MontoExentoF=$row["MontoExento"];
      $DescuentoF=$row["Descuento"];
      $ImpuestoF=$row["Impuesto"];
      $OtroImpuestoF=$row["OtroImpuesto"];
      $ServicioGravadoF=$row["ServicioGravado"];
      $ServicioExentoF=$row["ServicioExento"];
      $SubtotalFacturaF=$row["SubtotalFactura"];
      $TotalFacturaF=$row["TotalFactura"];
      $SaldoF=$row["Saldo"];
      
      /*Variables que hay que guardar*/
      $Consecutivo=($row["NoFactura"]=="") ? $Terminal.$Sucursal.$TipoDocumento.$NoFactura : $row["NoFactura"];
      $ClaveLarga=($Clave=='')?"506".$FechaPClave.$CedulaUsuario.$Consecutivo."1".'00000000':$Clave;
      
      $DetFact=DetalleFacturaParaJSON($IDFactura,$FK_Usuario);
      
      if($Status=='aceptado')
      {
	  	  if($NombreTipoDoc=='Factura')
	      {
		  		$JSONEnvio=
	      			'{
					  "data": {
					    "tipo": "1",
					    "nombre_usuario": "'.$ID_API.'",
					    "situacion": "normal",
					    "consecutivo": "'.$Consecutivo.'",
					    "clavelarga": "'.$ClaveLarga.'",
					    "sucursal": "'.$Sucursal.'",
					    "fechafactura": "'.$Fecha.'",
					    "terminal": "'.$Terminal.'",
					    "otro_telefono": "",
					    "receptor": {
					      "tipoIdentificacion": "'.$TipoCedula.'",
					      "numeroIdentificacion": "'.$FK_Cliente.'",
					      "nombre": "'.$NombreCliente.'",
					      "provincia": "'.$Provincia.'",
					      "canton": "'.$Canton.'",
					      "distrito": "'.$Distrito.'",
					      "otrasSenas": "'.$Direccion.'",
					      "codigoPais": "506",
					      "telefono": "'.$Telefono.'",
					      "website": "",
					      "correo": "'.$Email1.'",
					      "correo_gastos": "'.$Email2.'"
					    },
					    "condicionVenta": "'.$CondicionVenta.'",
					    "plazoCredito": "'.$Plazo.'",
					    "numFecha": "",
					    "numReferencia": "'.$IDFactura.'",
					    "codigoVendedor": "00",
					    "medioPago": [
					        '.$MedioPago.'
					    ],
					    "detalles": [
					      '.$DetFact.'
						],
					    "codigoMoneda": "'.$TipoMoneda.'",
					    "tipoCambio": "'.$TipoCambio.'",
					    "totalServGravados": "'.$ServicioGravadoF.'",
					    "totalServExentos": "'.$ServicioExentoF.'",
					    "totalMercanciasGravados": "'.$MontoGravadoF.'",
					    "totalMercanciasExentos": "'.$MontoExentoF.'",
					    "totalGravados": "'.($ServicioGravadoF+$MontoGravadoF).'",
					    "totalExentos": "'.($ServicioExentoF+$MontoExentoF).'",
					    "totalVentas": "'.(($ServicioGravadoF+$MontoGravadoF)+($ServicioExentoF+$MontoExentoF)).'",
					    "totalDescuentos": "'.$DescuentoF.'",
					    "totalVentasNetas": "'.$SubtotalFacturaF.'",
					    "totalImpuestos": "'.($ImpuestoF+$OtroImpuestoF).'",
					    "totalComprobantes": "'.$TotalFacturaF.'",
					    "referencia": "",
					    "otros": {
					      "otroTexto": ""
					    }
					  }
					}';
					
					//Enviar email    
				            
				//API URL Envio
				$urlEnvio = "http://35.170.39.96/api/server/reenviarfactura";

				//create a new cURL resource
				$ch = curl_init($urlEnvio);
				
				//poner JSON en curl
				curl_setopt($ch, CURLOPT_POSTFIELDS, $JSONEnvio);

				//set the content type to application/json
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json;charset=UTF-8'));

				//return response instead of outputting
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				//execute the POST request
				$result = curl_exec($ch);

				$Respuesta= $result;

				//close cURL resource
				curl_close($ch);
		  }
		  	    
		}
	}
	$users_arr[] = array(
	  	"Respuesta"=>$Respuesta,
	  );
	
	  echo json_encode($users_arr);
      exit;
    
}

function ObternerTipoIdentificacionNumerica($TipoCedula)
{
	$TipoDocumento;
	
	if($TipoCedula=='F')
	{
		$TipoDocumento='01';
	}
	else if($TipoCedula=='J')
	{
		$TipoDocumento='02';
	}
	else if($TipoCedula=='D')
	{
		$TipoDocumento='03';
	}
	else//N
	{
		$TipoDocumento='04';
	}
	
	return $TipoDocumento;
}

function ObtenerCodigoMoneda($TipoMoneda)
{
	$CodMoneda='';
	
	if($TipoMoneda=='C')
	{
		$CodMoneda='CRC';
	}
	else if($TipoMoneda=='D')
	{
		$CodMoneda='USD';
	}
	else if($TipoMoneda=='E')
	{
		$CodMoneda='EUR';
	}
	
	return $CodMoneda;
}

function QuitarFormatoNumero($Numero)
{
	//$Numero=number_format((float)$Numero, 2, '.', '');
	
	return str_replace(",", "", $Numero);
}

function MedioParaJSON($MedioPago)
{
	$Medio='';
	
	$MedioPago=str_replace('0','',$MedioPago);
	
	$chars = str_split($MedioPago);
	foreach($chars as $char)
	{
    	if($char=='1')
    	{
			$Medio.='{"codigo": "01"},'.PHP_EOL;
		}
	    else if($char=='2')
    	{
			$Medio.='{"codigo": "02"},'.PHP_EOL;
		}
		else if($char=='3')
    	{
			$Medio.='{"codigo": "03"},'.PHP_EOL;
		}
		else if($char=='4')
    	{
			$Medio.='{"codigo": "04"},'.PHP_EOL;
		}
		else if($char=='5')
    	{
			$Medio.='{"codigo": "05"},'.PHP_EOL;
		} 	
	}
		
	return rtrim($Medio, ',' . PHP_EOL);
}

function DetalleFacturaParaJSON($IDFactura,$FK_Usuario)
{
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 


	$sql="SELECT 

            IDDetalle,
            NombreProducto,
            Cantidad,
            Medida,
            Bonificacion,
            PrecioVenta,
            PrecioCosto,
            Impuesto,
            Descuento,
            TotalNeto,
            NoLinea,
            FK_Producto

            FROM detallefactura WHERE FK_Factura=$IDFactura AND FK_FK_Usuario=$FK_Usuario ORDER BY NoLinea;";

	  $result = $Conexion->query($sql);
	  
	  $DetalleFactura = '';
      
      while($ri =  mysqli_fetch_array($result))
      {
      		 $IDDetalle= $ri['IDDetalle'];
	         $IDProducto= $ri['FK_Producto'];
	         $NombreProducto= $ri['NombreProducto'];
	         $Cantidad= $ri['Cantidad'];
	         $PrecioVentaSinIV= $ri['PrecioVenta'];
	         $UnidadMedida= ConvertirUnidades($ri['Medida']);
	         $ImpuestoVenta= $ri['Impuesto'];
	         $Descuento= $ri['Descuento'];
	         $PrecioCosto= $ri['PrecioCosto'];
	         $Bonificacion= $ri['Bonificacion'];
	         $TotalNeto= $ri['TotalNeto'];
	         $NoLinea= $ri['NoLinea'];


	         /*Variables para calculos*/
	          $MontoExento=0;
	          $MontoGravado=0; 
	          $MontoSExento=0;
	          $MontoSGravado=0;
	          $MontoIV=0;
	          $MontoOtroI=0;
	          $MontoDescuento=0;
	          $SubtotalL=0;
	          $TotalL=0;


	          if($ImpuestoVenta==0) /*Sino Tiene Impuesto*/
	          {
	            $MontoExento=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	            $MontoGravado=0; 
	        
	          }
	          else /*Si Tiene Impuesto*/ 
	          {
	            $MontoExento=0;
	            $MontoGravado=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);  /*Precio sin impuesto x Cantidad sino tiene impuesto*/
	          }
	          
	          if($UnidadMedida=='sp' AND $ImpuestoVenta==0)
	          {
	            $MontoSExento=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2); /*Precio sin impuesto x Cantidad*/
	            $MontoSGravado=0;
	          }
	          else if($UnidadMedida=='sp' AND $ImpuestoVenta!=0)
	        {
	          $MontoSExento=0;/*Precio con impuesto x Cantidad*/
	            $MontoSGravado=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);
	        }
	        else
	        {
	          $MontoSExento=0;
	          $MontoSGravado=0;
	        }     

	          $MontoIV=bcdiv(((($PrecioVentaSinIV*$Cantidad)-(($PrecioVentaSinIV*$Cantidad)*($Descuento/100)))*($ImpuestoVenta/100)),1,2); /*((Precio sin impuesto x Cantidad)*Descuento) x IV*/
	          $MontoOtroI=0;/*se va a cambiar mas adelante*/
	          $MontoDescuento=bcdiv((($PrecioVentaSinIV*$Cantidad)*($Descuento/100)),1,2);/*(Precio sin impuesto x Cantidad)*Descuento*/


	          $SubtotalL=bcdiv(($PrecioVentaSinIV*$Cantidad),1,2);/*Precio sin impuesto x Cantidad*/ 
	          $TotalL=bcdiv(($SubtotalL-$MontoDescuento+$MontoIV),1,2);/*Subtotal-Descuento+Impuesto*/
	          
	          
	          if($ImpuestoVenta=='0.00')
	          {
			 $DetalleFactura.='{
						         "numero": "'.$NoLinea.'",
						         "codigo": [
						           {
						             "descripcion": "'.$IDProducto.'",
						             "tipo": "01"
						           }
						         ],
						         "cantidad": "'.$Cantidad.'",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioVentaSinIV*$Cantidad).'",
						         "montoDescuento": "'.$MontoDescuento.'",
						         "descripcionDescuento": "Descuento",
						         "subtotal": "'.$SubtotalL.'",
						         "totalLinea": "'.$TotalL.'"
						       },';
			  }
			  else 
			  {
			 $DetalleFactura.='{
						         "numero": "'.$NoLinea.'",
						         "codigo": [
						           {
						             "descripcion": "'.$IDProducto.'",
						             "tipo": "01"
						           }
						         ],
						         "cantidad": "'.$Cantidad.'",
						         "unidad": "'.$UnidadMedida.'",
						         "detalle": "'.$NombreProducto.'",
						         "precioUnitario": "'.$PrecioVentaSinIV.'",
						         "montoTotal": "'.($PrecioVentaSinIV*$Cantidad).'",
						         "montoDescuento": "'.$MontoDescuento.'",
						         "descripcionDescuento": "Descuento",
						         "subtotal": "'.$SubtotalL.'",
						         "impuesto": [
							       {
							         "codigo": "01",
							         "tarifa": "'.$ImpuestoVenta.'",
							         "monto": "'.$MontoIV.'"
							       }
							     ],
						         "totalLinea": "'.$TotalL.'"
						       },';
			  }
	}
	
	return rtrim($DetalleFactura,',');
}

function ObtenerID_API($FK_Usuario)
{
	$ID_API='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

	$sql="SELECT ID_API FROM usuario where IDUsuario=$FK_Usuario;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $ID_API=$row["ID_API"];
    }
    
	return $ID_API;			
}


function ObtenerClaveLarga($IDDocumento,$FK_Usuario,$FK_Cliente)
{
	$ClaveLarga='';
	
	$Conexion = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    } 

	$sql="SELECT Clave FROM factura where FK_Usuario=$FK_Usuario and FK_Cliente='$FK_Cliente' and IDFactura=$IDDocumento;";

    $result=$Conexion->query($sql);

    if($result->num_rows > 0)
    {
      $row = $result->fetch_assoc();
      $ClaveLarga=$row["Clave"];
    }
    
	return $ClaveLarga;		
	
		
}

function ConvertirUnidades($UnidadMedida)
{
	$UM='';

	switch ($UnidadMedida) 
	{
	    case "Unidad":
	        $UM='Unid';
	        break;
	    case "Kg":
	        $UM='kg';
	        break;
	    case "oz":
	        $UM='Oz';
	        break;
	    case "l":
	        $UM='L';
	        break;
	    case "gal":
	        $UM='Gal';
	        break;
	    case "m":
	        $UM='m';
	        break;
	    case "min":
	        $UM='min';
	        break;
	    case "h":
	        $UM='h';
	        break;
	    case "d":
	        $UM='d';
	        break;
	    case "ml":
	        $UM='mL';
	        break;
	    case "g":
	        $UM='g';
	        break;
	    case "t":
	        $UM='t';
	        break;
	    case "sp":
	        $UM='Sp';
	        break;
	    default:
	        $UM='';
	}
	
	return $UM;	
}

function FormatoTipoCedulaNumerico($TipoCeduLetra)
{
	$TipoCedulaNum='';
	
	if($TipoCeduLetra=='F')
	{
		$TipoCedulaNum='01';
	}
	else if($TipoCeduLetra=='J')
	{
		$TipoCedulaNum='02';
	}
	else if($TipoCeduLetra=='D')
	{
		$TipoCedulaNum='03';
	}
	else if($TipoCedula=='E')
	{
		$TipoDocumento='05';
	}
	else if($TipoCeduLetra=='N')
	{
		$TipoCedulaNum='04';
	}
	
	return $TipoCedulaNum;
}

?>